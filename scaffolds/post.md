---
title: {{ title }}
date: {{ date }}
tags:
---

![](AlbumCover.jpg)

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

{% iframe 'https://open.spotify.com/embed/album/67GJo4ajqUWHsuY0f4aQz2?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B00IFMV80G/?id=9dofG9nUp1&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/gateway-to-dignity/820819483' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/26417118' '100%' '96' %}





