---
title: Jah Wobble - Snake Charmer
date: 2024-07-26 21:22:55
tags:
- jah wobble
- holger czukay
- the edge
- public image ltd
- can
- u2
- progressive rock
- avant-garde rock
- 1980s rock
---

![](SnakeCharmer.jpg)

Jah Wobble was a very busy man in the early 80s. He left his short-lived position as the bass player in Public Image Ltd. in late 1980 (he only appeared on one record: 1979's *Metal Box*).

One of his final performances with the group was on the television show *The Old Grey Whistle Test* where they played "Poptones" and "Careening":

<iframe width="560" height="315" src="https://www.youtube.com/embed/aDKDc5zq53k?si=Hi8uWw1nCXRn-Aat" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Immediately after leaving PiL Jah Wobble began forming new bands and collaborating with a wide variety of musicians.

The band he formed was called Invaders of the Heart that took the *Metal Box* template and took it in jazzier and more avant-garde directions. Here they are performing in 1983:

<iframe width="560" height="315" src="https://www.youtube.com/embed/DheYkVISW7U?si=MgvtbdB7g6BH2fnp" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

He also started working with ex-members of the German band Can: Holger Czukay and Jaki Liebezeit. They first appeared together on Holger Czukay's 1981 solo record *On the Way to the Peak of Normal* on the song "Hiss 'n' Listen":

<iframe width="560" height="315" src="https://www.youtube.com/embed/5lV8ejWpyzU?si=pC_zdR2FINOWImOy" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

This trio turned into a more long term working unit. They recorded a full album together called *Full Circle* in 1982 and made a music video for the song "How Much Are They?" which is pretty representative of the whole album:

<iframe width="560" height="315" src="https://www.youtube.com/embed/sFBHnjPQ8W0?si=8jhyZ-npWyThRCHQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

For the next album, they took the concept even further. Somehow, they recruited U2 guitarist The Edge to collaborate on the next album, *Snake Charmer*, which is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

Details are sparse as to how The Edge got involved in the recording sessions. It seems like he would have been smack dab in the middle of the massive U2 tour for *War* when the recordings were made! Perhaps there was a few off days on the tour that coincided with the session that the other players were already a part of? 

Nevertheless, I'm glad the opportunity arose because *Snake Charmer* is probably some of the wildest stuff The Edge has ever been a part of. 

"Hold On To Your Dreams" is my favorite song on the album. It's sort of a mutant disco / prog rock / avant-jazz hybrid that really sounds like nothing else The Edge has been involved in, but you can still hear his distinct style of guitar playing throughout. Plus, Arthur Russell wrote the lyrics to the song. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/XmpFRV0uxs8?si=96poQOvXT_JS2lTK" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

If you like this, you'll definitely dig the rest of the record since it follows a similar template. It's definitely an obscure, outré record (even though it was released on Island Records!!) but it still boggles my mind that this doesn't have wider exposure or acclaim nowadays. A real hidden gem!

 You can [listen to Jah Wobble - *Snake Charmer* on your streaming platform of choice](https://album.link/s/27GyGfz5YScW6y5QeSRvlB) (including Spotify, Amazon Music, Apple Music, Tidal, YouTube Music and more!).
