---
title: Thomas Dolby - The Flat Earth
author: Phil Plencner
date: 2020-04-20 15:57:16
tags:
- thomas dolby
- matthew seligman
- 80s rock
- dance rock
---
One of the perks / things I miss about working for Johns Hopkins was the ability to interact with Thomas Dolby. The man behind the novelty hit "She Blinded Me With Science" had a very interesting post-hit career....balancing a unique blend of music and technology. 

He eventually ended up as a professor at Johns Hopkins and spearheading their new Digital Media school. Even though I wasn't a student in any of his classes, I still dropped by his office hours and if he wasn't busy he would love to talk about his experiences and where he sees digital music going in the future. Fascinating person.

Anyways, in the early years of his band his bass player was Matthew Seligman...who later became a pretty world renown session musician in his own right. He passed away over the weekend due to COVID-19, which is what brings me back to today's pick. The excellent 2nd album by Thomas Dolby *"The Flat Earth"* where Matthrew is featured predominately thoughout.

<iframe src="https://open.spotify.com/embed/album/0H5Yo2A9J1cWggemke95Kp" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

I'll also highlight one of my favorite Thomas Dolby songs that was originally on an EP that contained "She Blinded Me With Science" called "One Of Our Submarines". It eventually got tacked onto re-issues of *"Golden Age Of Wireless"* once SBMWS became a huge hit.

<iframe src="https://open.spotify.com/embed/track/1yM90Jy5CQWJFJcRYcuhaT" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
