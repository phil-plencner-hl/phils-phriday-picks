---
title: Van Halen David Lee Roth Power Mix
author: Phil Plencner
date: 2020-10-06 14:19:05
tags:
- van halen
- eddie van halen
- 80s rock
- david lee roth
---
This is an old David Lee Roth-focused playlist I made a long time ago, but Eddie is *ALL OVER* the Van Halen tracks here, so its worth a **(LOUD)** listen. 

One of the first stadium rock concerts I ever saw was Van Halen *OU812* tour. 

Gah.

<iframe src="https://open.spotify.com/embed/playlist/0RoIM49XaQcFBSb4XSFnsH" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>