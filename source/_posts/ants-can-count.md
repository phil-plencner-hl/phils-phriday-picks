---
title: Bruce Fowler - Ants Can Count
date: 2022-07-29 14:16:47
tags:
- bruce fowler
- fowler brothers
- frank zappa
- captain beefheart
- ed mann
- jazz fusion
---

![](AntsCanCount.jpg)

Trombonist Bruce Fowler is primarily known for his work with Frank Zappa. He first started playing with Zappa in late 1972 as part of his Wazoo band. Although he didn't appear on the studio album *The Grand Wazoo* he was part of the touring band. He stuck with Zappa pretty much all the way until his last rock tour in 1988:

- He was part of the "Petite Wazoo" band that appeared on *Apostrophe* and *Overnite Sensation*
- He played with the "Roxy band" of 1974/1975
- He played on the Zappa / Beefheart "Bongo Fury" tour of 1975
- He was included as part of the epic "Lather" recording sessions of the late 1970s
- He played with Zappa's stunning '88 band who could seemingly play everything

Allow me to highlight just a couple pieces of live footage to illustrate how amazing his trombone playing is with Zappa.

Here he is playing T'Mershi Duween at the Roxy in 1974:

<iframe width="560" height="315" src="https://www.youtube.com/embed/LPfzh_CpIa8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Not awesome enough? How about him playing and doing weird dances with Zappa and Jean-Luc Ponty also from the early 70s:

<iframe width="560" height="315" src="https://www.youtube.com/embed/-ENMPlT5CA8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Finally, here he is as part of the horn line of the 1988 Zappa band playing The Black Page, which truly must be seen / heard to be believed:

<iframe width="560" height="315" src="https://www.youtube.com/embed/_ciYP_LCPYw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

In the late 70s, he left Zappa to be part of Captain Beefheart's Magic Band and played on a couple of his latter-day albums (including one of my personal favorites *Doc At The Radar Station*).

After that stint, he did session / soundtrack work along with occasionally releasing albums as part of a band that included his brothers Tom Fowler (bass) and Walt Fowler (Trumpet) at different times called Air Pocket or The Fowler Brothers.

After the '88 Zappa band imploded, he released a solo album called *Ants Can Count*, which is today's pick! I was lucky enough to find it in a cutout / bargain bin in the mid 90s.

The band he assembled for *Ants Can Count* include some Zappa alumni (Chester Thompson on drums, Ed Mann on percussion) along with his brothers. It also includes this [rather cool line drawing of Bruce in the booklet by Captain Beefheart](https://i.discogs.com/atEU_TMUzBQCMw2PJbLkIQsJMnKV5g3kGjQ_a02y4E0/rs:fit/g:sm/q:90/h:573/w:600/czM6Ly9kaXNjb2dz/LWRhdGFiYXNlLWlt/YWdlcy9SLTgyMjA2/MDktMTQ1NzM4Njk3/NS01ODMwLmpwZWc.jpeg)!

The music itself is much more subdued than his fare with Zappa / Beefheart, but still uniformly excellent. There is a definite modern-classical influence mixed with jazz throughout the whole album.

Along with the pieces for a full band there are also other interesting situations. "Ode to Stravinsky and the American Indians" features Bruce Fowler overdubbing himself many times to make it sound like a trombone ensemble. There are a pair of duets with french horn player Suzette Moriarty and flute with his brother Steve. Additionally, he includes a nice trombone solo called "One Man One Bone".

Overall, a well-rounded album which makes me wish he would have released more solo albums through the years. 

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/album/18sn4TbVFjvgNYA2sge0Vl?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

<iframe id='AmazonMusicEmbedB000QZSV64' src='https://music.amazon.com/embed/B000QZSV64/?id=7ZUjBRtvvd&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *; clipboard-write" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/ants-can-count/279763206"></iframe>

<iframe src="https://embed.tidal.com/albums/34324832" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>