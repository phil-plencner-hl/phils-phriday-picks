---
title: The Ventures - The Ventures In Space
date: 2022-01-28 15:56:36
tags:
- the ventures
- surf rock
- 60s rock
---

Don Wilson, a founding member of the instrumental surf rock band The Ventures, passed away this week. He was 88 years old.

As an aside, I was fortunate enough to [meet Don Wilson in 2003 backstage at the Ryman Auditorium](https://www.instagram.com/p/CZDUTfvsZal/?utm_source=ig_web_copy_link). He was a super-nice guy and his performance that night was amazing. 

The Ventures most famous song was probably "Walk, Don't Run". They originally recorded it in the late 50s, but during the height of the surf-rock boom they re-recorded it in 1964. This is the version that became an international hit.

By "international" I mean they were HUGE in places like Japan.

They are primarily known for playing Fender Guitars and amps. However, soon after "Walk, Don't Run ('64)" they signed a deal with Mosrite Guitars who made futuristic looking guitars for them. They ended up using these for the album *The Ventures in Space*, which is today's pick!

Mostly ditching the surf sound that made them famous, *...in Space* finds them playing an early version of space rock with sound effects and wild production effects. Sort of like a long-lost soundtrack to a 1960s Star Trek episode! 

They also recruited the awesome steel guitar player Red Rhodes to play on the album as well, which adds an even more space-age feel to the proceedings.

Definitely an anomoly in The Ventures catalog, but one of my favorites. Soon afterwards, they switched back to their traditional Fender Guitars and sound. 

<iframe src="https://open.spotify.com/embed/album/3IxRddBoeU6KNMut00rrOZ?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>




