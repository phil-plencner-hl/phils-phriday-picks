---
title: Molchat Doma - Etazhi
author: Phil Plencner
date: 2020-08-20 14:52:28
tags:
- molchat doma
- etazhi
- belarus
- post-rock
- industrial rock
- 80s music
- dance music
---

Today's pick is a cool industrial / postrock band from Belarus called Molchat Doma! 

It totally sounds like something out of late-80s WaxTrax! Records with a mostly monotone vocalist singing in Russian on top of it. Totally awesome stuff!!!

<iframe src="https://open.spotify.com/embed/album/1vPytXXwF0VXnof89Z91Pm" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>