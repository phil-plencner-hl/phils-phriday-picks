---
title: Melvins - Trilogy
date: 2021-09-17 09:08:55
tags:
- melvins
- mike patton
- 90s rock
- alternative rock
- heavy metal
- grunge
---

The Melvins were part of the Seattle grunge scene in the 1990s. However, they never received the popular success much of their brethren did. 

In fact, Melvins drummer Dale Crover was an early member of Nirvana (appearing on some circulated demo recordings). 

Their music is a very heavy, hard rock...more often than not with very sludgy and slow tempos. However, they also liked to defy expectations with strange lyrics, bizarre mood shifts and generally just being oddballs.

During the height of the Seattle Grunge explosion the even signed with major label Atlantic. They released a few excellent albums during their tenure there, but definitely didn't achieve large mainstream success. 

Part of the reason why was, they put out really weird albums! The overtly experimental leanings of their 1996 album *Stag* was the final straw for Atlantic Records, who dropped them soon after. 

Around this same time, guitarist Buzz Osbourne had befriended Faith No More singer Mike Patton. They soon started a new experimental group called Fantomas along with Mr. Bungle bassist Trevor Dunn and Slayer drummer Dave Lombardo. They were even weirder than any of their members other bands, so finding a label to put out their releases proved to be a challenge.

To solve that issue, Mike Patton started his own record label Ipecac Recordings. The first release was the self-titled Fantomas record, but The Melvins were the next band they signed. They were allowed to release whatever they wanted without any oversight.

The Melvins took advantage of this newfound freedom and put out three albums in the span of a year, allegedly all recorded at the same time. Those albums were *The Maggot*, *The Bootlicker* and *The Crybaby*. After the fact they became known as "The Trilogy". 

All three of these albums are this week's pick!

*The Maggot* is probably the most "traditional" Melvins album of the bunch. Slow, heavy, down-tuned riffs with nonsensical lyrics. Oddly, the songs were broken down into separate tracks, seemingly in random spots. So you could queue up the middle of a song for some reason? It also included an wild cover of the Fleetwood Mac (The Peter Green-fronted-era) tune "The Green Manalishi (With the Two-Pronged Crown)".

The second album in the series, *The Bootlicker*, by contrast was much more mellow. It even features a bunch of almost ambient stretches of acoustic music. However, it still contains its share of sludge and heavy funk, so it isn't took far out of the box.

Finally, the trilogy closed with *The Crybaby*. This is easily the wildest one of the bunch. It opens with a straight cover of Nirvana's "Smells Like Teen Spirit" with Leif Garrett on vocals and only gets more bizarre from there. 

In fact, every song features guest musicians or vocalists. Most strange are covers of two country songs ("Ramblin' Man" and "Okie from Muskogee") with Hank Williams III and Helmet's Henry Bogdan.

Other guests include David Low (Jesus Lizard), J.G. Thirlwell (Foetus), Eric Sanko (Skeleton Key) and Kevin Sharp (Brutal Truth). Stylistically it really runs the gamut from metal to funk to jazz to industrial. At over an hour, it's a lot to take in.

In fact, the entire trilogy is a lot to take in, but its very much worth deep-diving into the whole thing to get the full experience of what the Melvins released in a very short period of time during a particularly creative and prolific period of their musical career. 

<iframe src="https://open.spotify.com/embed/album/4nFHlaE9xD4FSWhs3LKJHQ" width="100%" height="380" frameBorder="0" allowtransparency="true" allow="encrypted-media"></iframe>

<iframe src="https://open.spotify.com/embed/album/6mSDFCtcNKXB2bgjspwJUr" width="100%" height="380" frameBorder="0" allowtransparency="true" allow="encrypted-media"></iframe>

<iframe src="https://open.spotify.com/embed/album/1dyrmriLRtAbwI1AkrLmlZ" width="100%" height="380" frameBorder="0" allowtransparency="true" allow="encrypted-media"></iframe>

