---
title: Karl Sanders - Saurian Meditation
date: 2022-07-22 11:41:37
tags:
- karl sanders
- nile
- death metal
- egyptian music
- grindcore
---

![](SaurianMeditation.jpg)

Nile is a completely fascinating death metal band. Founded and masterminded by guitarist Karl Sanders, they play an extremely fast, extremely technical brand of death metal. Aside from their astounding musical ability, what sets them apart is their reliance on Egyptian / Middle Eastern history in the lyrics and musical themes.

Karl Sanders is not a neophyte on these historical topics. He devours books and other documents related to Egyptology. Here is [a fascinating interview with him from 2010 about the subject](https://thequietus.com/articles/03522-karl-sanders-of-ithyphallic-death-metallers-nile-on-ancient-egypt).

To my ears, the first three Nile albums (*Amongst the Catacombs of Nephren-Ka*, *Black Seeds of Vengeance* and *In Their Darkened Shrines*) are untouchable death metal classics. I think at this point they had already perfected the mixture of metal and Middle Eastern music. 

Check out this absolutely brutal music video of "Execration Text" from *In Their Darkened Shrines*:

<iframe width="560" height="315" src="https://www.youtube.com/embed/44jRUTKDLYI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Nile's albums are also peppered with interludes that are more subdued and include more traditional Middle Eastern music and instruments. "The Nameless City of the Accursed" from *Black Seeds of Vengeance* is a fine example:

<iframe width="560" height="315" src="https://www.youtube.com/embed/cbR7LE83eYs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The visual elements of their albums was also focused on Egyptian themes. For their album *Ithyphallic*, they released [a limited-edition version of the CD encased in a pyramid](https://www.discogs.com/release/2957576-Nile-Ithyphallic)! Bonkers stuff.

Their album's liner notes also went into massive detail explaining the song lyrics and putting them into historical context.

In 2004, Karl Sanders started working on a solo album focused completely on that style of music.  Sanders explained that "he got sick of hearing big loud death metal everyday after touring", and started writing quieter music to relax, and recorded them.

The result was *Saurian Meditation* which is today's pick!

Karl Sanders played most of the instruments himself (including baglama saz, acoustic guitar, electric guitar, ebow, guitar synthesizer, keyboards and bass guitar). However, original Nile drummer Pete Hammoura also plays percussion throughout.

Much of it is acoustic and oddly hypnotic and psychedelic. A very enjoyable album that is almost like a movie soundtrack in scope and mood.

Of course, the liner notes also include [massive historical notes on all the songs](https://i.discogs.com/6kk2g3Vo2dgHEDn_avLRs8NSI0TsB-J3AH7Qk1rWjoU/rs:fit/g:sm/q:90/h:450/w:600/czM6Ly9kaXNjb2dz/LWRhdGFiYXNlLWlt/YWdlcy9SLTQzODcz/Mi0xNjI1NDQ2ODg0/LTY2MjQuanBlZw.jpeg), which really helps the listener immerse themselves in the album.

Nile continues to record new albums and tours, but ever since drummer George Kollias joined in 2005, they really haven't been the same for me. I don't think it's George's fault, as I absolutely love his drumming:

<iframe width="560" height="315" src="https://www.youtube.com/embed/RqzZmNqdWck" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Karl Sanders also occasionally puts out solo records similar to *Saurian Meditation* (in fact his first one in 13 years, *Saurian Apocalypse*, came out today!) but I think *Saurian Meditation* is still the best one.

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/album/7ASP2xPA3fyfWK937dBTiN?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

<iframe id='AmazonMusicEmbedB08KFLBHK8' src='https://music.amazon.com/embed/B08KFLBHK8/?id=G0oIDCoLV5&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *; clipboard-write" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/saurian-meditation/1533512913"></iframe>

<iframe src="https://embed.tidal.com/albums/156703772" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>