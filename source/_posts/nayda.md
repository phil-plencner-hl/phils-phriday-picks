---
title: Bab L' Bluz - Nayda!
author: Phil Plencner
date: 2020-08-06 15:12:01
tags:
- bab l' bluz
- nayda
- real world records
- peter gabriel
- moroccan music
---

Today's pick is the the latest album released by Peter Gabriel's "Real World" label: *"Nayda!"* by Moroccan rock band Bab L' Bluz. 

The label's press release describes it better than I could... "...singing words of freedom in the Moroccan-Arabic dialect of darija. Ancient and current, funky and rhythmic, buoyed by Arabic lyrics, soaring vocals and bass-heavy grooves, *Nayda!* seems to pulse from the heart of the Maghreb." 

Definitely an inspiring album with great music.

<iframe src="https://open.spotify.com/embed/album/2Ctf8UeMAI9Iy2ODkUQyxv" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>