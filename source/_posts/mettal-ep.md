---
title: Rodrigo y Gabriela - Mettal EP
author: Phil Plencner
date: 2020-02-28 16:24:25
tags:
- rodrigo y gabriela
- thrash metal
- mexican music
- jazz
---
Mexican acoustic guitar duo Rodrigo y Gabriela put out new music for the first time in several years today. 

If you want to hear acoustic instrumental versions of 3 thrash metal classics, look no further!!!

<iframe src="https://open.spotify.com/embed/album/5vcvEvxMGJmBTnzWVRztyH" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>