---
title: Dark Angel - Time Does Not Heal
date: 2021-03-19 07:47:09
tags:
- Dark Angel
- Gene Hoglan
- Heavy Metal
- Thrash Metal
- Progressive Rock
---

Dark Angel was part of the original 80s thrash metal scene in California (although they were from L.A. and not where the most active scene was up in San Francisco).  They never reached the popularity of the "Big Four" thrash bands of the era (Metallica, Megadeth, Slayer, Anthrax). However, they were definitely in the next tier (along with groups such as Exodus and Overkill).

They were formed by one of my favorite metal drummers of all time: Gene "The Atomic Clock" Hogan. As his nickname implies, his drumming is very precise. Not only that it is also very complex and fast.

The first few Dark Angel albums flew under my radar when they were first released, even though I was well into the "Big Four" bands at that point. This all changed with their final studio album *Time Does Not Heal* which came out 30 years ago in February 1991. It Still sounds amazing in 2021.

I remember buying this at Camelot Music in Lakehurst Mall (Waukegan, Illinois!) solely because of the hype sticker: "Nine songs, 67 minutes, 246 riffs!" I took it home, and it blew my 16-year-old brain straight out of my skull. Gene Hoglan’s acrobatic, speedy drumming alone was worth the price of admission. 

This was definitely the most complex thrash metal album released at the time (far exceeding Metallica's *...And Justice For All*). There was a lot of progressive rock influences involved in the song compositions.

Along with the technical music, the lyrics also incorporate very serious topics (also written by Gene Hoglan!). Check out the highbrow lyrics to songs like *Trauma and Catharsis* for example. 

This album eventually broke up the band. Gene Hoglan went on to be an integral part of other bands and projects (Death's *Individual Thought Patterns*, Strapping Young Lad's *Alien*, and the Dethklok project just to name a few) and he insisted for a long while that Dark Angel would never reunite. 

Eventually though, they did! In 2014 I saw a reformed Dark Angel perform at Maryland Deathfest! While the set mostly consisted of songs from the earlier album *Darkness Descends*, they did play the title song from *Time Does Not Heal*, which fulfilled my teenage dream of finally seeing a song from the album performed live.

<iframe src="https://open.spotify.com/embed/album/1Wq9Ikuh6PpXgrUxEpPDHb" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>




