---
title: Kid Creole And The Coconuts - Wise Guy
author: Phil Plencner
date: 2019-09-06 14:19:30
tags:
- kid creole and the coconuts
- 80s rock
- no wave
- disco
- jazz funk
---

It's Friday! 

I refuse to believe summer is over, so let's jam to this classic Kid Creole and the Coconuts album. 

Originally called *"Tropical Gangsters"* this is probably my favorite album in their (mostly great) catalog. 

"Hot cha cha cha!"

<iframe src="https://open.spotify.com/embed/album/5qDmlru1XX2WxAmgxmvE48" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>