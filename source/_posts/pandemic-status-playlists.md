---
title: Pandemic Status Playlists
date: 2021-12-16 20:34:50
tags:
- pandemic status
- covid 19
- custom playlists
- progressive rock
- 90s rock
- 70s rock
- jazz fusion
---

In April 2021, we were about a year into the COVID-19 pandemic that changed the world. I spent a majority of that time in my house, only occasionally venturing outside for food and supplies. I work from home, which made this possible.

That period of extreme isolation definitely started affecting my mood. As a form of self-care I decided to make a large playlist of tunes both topical and personal favorites. This ended up being my *Pandemic Status: Isolated and Weird* playlist. 

<iframe src="https://open.spotify.com/embed/playlist/1emt8fk1PHIOVawxkutB6D?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

It runs the gamut from progressive rock to jazz fusion to 80s pop music to hip-hop to death metal to country. A little bit of everything I love with the intent to cheer me up.

It turns out it worked. I listened to this playlist dozens of times. Initially this playlist stopped at Frank Zappa's "Watermelon In Easter Hay". However, about a month later I fleshed it out with another 27 songs. I listened to it even more. I felt like I hit the sweet spot of size and scope.

In June, I was inspired by a trip to visit family. Vaccines were available and the weather was nice. I thought the world was turning a corner. So, I made *Pandemic Status 2: Are We Having Phun Yet?* This one still covered a (perhaps too wide) range of music, but had a lot more positive songs and pop music on it. While a little shorter at 8 hours, I felt like it was the perfect scope for this particular effort.

<iframe src="https://open.spotify.com/embed/playlist/1jnxS297X4LtRZLJheHxwn?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

I started spreading these playlists to select friends and music fanatics and received some positive feedback. This inspired me to make Pandemic Status an ongoing series!

August brought the rise in the Delta Variant of COVID-19 which inspired the darker *Pandemic Status 3: Delta Disaster* playlist.

<iframe src="https://open.spotify.com/embed/playlist/1r0fOPCa4nqnqTVbUyCTts?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

By the end of August my mood was really getting sullen so started extending *Delta Disaster* but ended up having so much material that it warranted a brand-new playlist called *Pandemic Status 4: No Hope In Sight*.

<iframe src="https://open.spotify.com/embed/playlist/6fnbe8conMh66PxAuwTsDf?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

As fall set in during September, another playlist emerged: *Pandemic Status 5: Feel Like Shit...Deja Vu*, which was titled in an obvious nod to the fantastic album by Suicidal Tendencies.

<iframe src="https://open.spotify.com/embed/playlist/6qW11Kgm9VVCAyjhx49zH8?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

With the chill of November setting in, I made the final installment of 2021 called *Pandemic Status 6: Omicron Winter Hibernation* which I feel might be the best one yet.

<iframe src="https://open.spotify.com/embed/playlist/6PL6EiAJD82ONBKV1aFtoi?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

I still listen to all the playlists with great frequency. I find them inspiring while writing code or even while working around the house. 

Will there be a Pandemic Status 7? Only time will tell. In the meantime, I hope you find the time to listen to the entire collection and find something inspiring within.

Additionally, this will be the last Phil's Phriday Pick of the year. I will be taking a break over the holidays and will return in January 2022. 

Happy New Year!
