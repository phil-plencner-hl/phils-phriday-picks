---
title: Pet Shop Boys - Hotspot
author: Phil Plencner
date: 2020-01-24 12:07:47
tags:
- pet shop boys
- 80s music
- dance music
- pop music
---

Happy Friday! 

There is a new Pet Shop Boys album, which is always a cause for celebration!

<iframe src="https://open.spotify.com/embed/album/3Ap37NIDZhcgORhznOgSmG" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>