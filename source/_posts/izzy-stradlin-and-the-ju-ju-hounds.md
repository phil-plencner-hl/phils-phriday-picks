---
title: Izzy Stradlin And The Ju Ju Hounds
date: 2021-07-01 10:04:57
tags:
- izzy stradlin
- guns n roses
- rolling stones
- georgia satellites
- social distortion
- hard rock
- 90s rock
- classic rock
---

Izzy Stradlin was one of the founding members of Guns N' Roses and was a huge part of the songwriting and overall sound on *Appetite of Destruction*, but was mostly playing 2nd fiddle to Slash and Axl Rose who were more of the face of the group.

By the time the *Use Your Illusion* albums came out in 1991, he was adding more songs and even singing lead on some songs ("Dust N Bones", "You Ain't The First" etc). At this point he started butting heads with Axl. It also didn't help that he got sober when the rest of the band did not. 

He quit the band at the height of their success...and assembled an oddly named band called the Ju Ju Hounds.

The Ju Ju Hounds consisted of guitarist Rick Richards (The Georgia Satellites), bassist Jimmy Ashhurst (later of Buckcherry), and Charlie Quintana (Social Distortion). 

Their sole self-titled album also included contributions from Ronnie Wood (The Rolling Stones), Ian McLagen (The Faces), and Nicky Hopkins (The Kinks).

Obviously, from that cast of characters you can suspect that they were a very blues rock focused ensemble.

It definitely sounds like an lost Ronnie Wood or Keith Richards solo album. This is a good thing.

Along with some great original material ("Somebody Knockin" and "Shuffle It All" were the singles and were written by Stradlin), it also includes some awesome covers. There's the hard-rocking version of The Maytals reggae classic "Pressure Drop" and, appropriately enough, Ronnie Wood's "Take a Look At The Guy" which was originally released on *I've Got My Own Album To Do*.

The Ju Ju Hounds didn't last after touring behind this album. Nevertheless, Izzy Stradlin continues to regularly release solo albums. Rick Richards is the only member of the Ju Ju Hounds who continues to record with Izzy. 

Most of his solo albums are excellent. If you're into the sound on the Ju Ju Hounds album, they follow a similar blueprint and are worth seeking out.

Izzy also occasionally appears with Guns N' Roses but is not a current member.

<iframe src="https://open.spotify.com/embed/album/0C5dXQc43DCHdGqTU857Wt" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>


