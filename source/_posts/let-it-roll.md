---
title: Midland - Let It Roll
author: Phil Plencner
date: 2019-08-23 15:32:14
tags:
- midland
- country music
- texas music
---

Happy Friday! Most people associate my musical tastes with the outer fringes of rock and jazz, but the fact of the matter is I was raised in a house that played almost exclusively country music. 

So I have an affinity for the more traditionalist 70s/80s country sound. The Texas band Midland, who put out their new album today, is a modern band that is cut from the George Strait / Dwight Yoakam cloth...so it is right up my alley! 

Maybe yours as well?

Bonus: their excellent cover of the Jerry Reed tune [East Bound And Down](https://open.spotify.com/album/1ACfrm46Pjku8SZ1F1JVMe?si=RMU_Dg6USYK5yqNzPnPCag) that they released last year.

<iframe src="https://open.spotify.com/embed/album/558uSttH0s3zk6tRa0T7tF" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>