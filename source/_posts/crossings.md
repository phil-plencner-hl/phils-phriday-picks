---
title: Herbie Hancock - Crossings
author: Phil Plencner
date: 2020-05-19 14:50:24
tags:
- herbie hancock
- miles davis
- jazz fusion
- funk rock
- jazz funk
- mwandishi
- packtrick gleeson
---
fter Herbie Hancock left Miles Davis' Quartet and before he formed the Headhunters, he had a relatively obscure band called Mwandishi. 

Their 2nd album "Crossings" was a way ahead-of-its-time fusion of electronic and acoustic instruments. Even though they had cutting edge synthesizers involved, and Hancock duets with Patrick Gleeson on Moog(!!!) it retains a pretty organic sound overall. 

Of course, he later went all-in on the funk and completely digitized his sound...and this likely was the launching pad of all of that.

<iframe src="https://open.spotify.com/embed/album/1uPpf0LfR227I6vruxYkxZ" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>