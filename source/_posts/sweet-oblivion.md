---
title: Screaming Trees - Sweet Oblivion
author: Phil Plencner
date: 2020-05-14 14:58:35
tags:
- screaming trees
- 90s rock
- grunge
- mark lanegan
- barrett martin
- tuatara
- mad season
---

The new Mark Lanegan autobiography (*"Sing Backwards and Weep"*) flopped through my letterbox yesterday, accompanied by his new studio album. 

While I haven't fully absorbed those yet, it did get me to revisit Screaming Trees *"Sweet Oblivion"* for the first time in a while...and it really holds up! 

It is their first album with drummer Barrett Martin, and I think that's when they really started to gell as a band. FYI Barrett when on to start his own bands (including the excellent Tuatara and the semi-popular Mad Season).

<iframe src="https://open.spotify.com/embed/album/3SOcm48I0DOX1KEHF2IEVY" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>