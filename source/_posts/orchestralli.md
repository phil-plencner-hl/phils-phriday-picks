---
title: Stewart Copeland - Orchestralli
date: 2021-11-05 08:03:20
tags:
- stewart copeland
- andy summers
- sting
- the police
- 80s rock
- orchestral music
---

Fun fact: I have seen all 3 members of The Police in concert....but not actually together as The Police!

The first was Andy Summers. He recently released his solo album *Earth & Sky* in 2002 and played a solo acoustic show at The Old Town School of Folk Music in Chicago. Small venue, intimate show. Good stuff.

I saw Sting the next year, when he played a huge free concert in Grant Park. 

I was very excited to see this, because Vinnie Colaiuta was playing drums with him then and the dude is a beast.

Sting played the (solo and The Police) hits. It was pretty good, but polar opposite of the Andy Summers show. You can actually hear a [recording of the show that someone taped off the radio](https://www.youtube.com/watch?v=FibOR4wjdKE) (remember doing that?) on YouTube.

The only member left was Steward Copeland. It didn't seem likely I would catch one of his rare solo shows.

Later in 2009, I was on a trip to Rome, Italy visiting family. While there I saw an advertisement on a billboard about Stewart Copeland playing a solo show at a festival that same week! I couldn't believe my luck!

I went to the show, and he was playing original songs with a percussion-heavy group. It was unlike anything I had ever heard. An absolutely stunning performance. Easily better than the Sting and Andy Summers shows.

After the set ended, I went to the merchandise table hoping to find a similar recording to what I just witnessed. Unfortunately, it seemed like this group was a one-off occasion. The helpful person selling merch recommended an Italy-only album Stewart Copeland released called *Orchestralli*. I bought it. 

It was a live recording in Italy of Stewart Copeland with a different group back in 2004. As the name implied, it includes a small chamber orchestra and a percussion ensemble called Ensemble Bash along with Copeland on drums.

It is a very crisp live recording of a pretty amazing group playing very cool, almost jazz fusion based music. There are elements of some of his solo / soundtrack works peppered throughout (longtime fans might recognize some parts from *Rumble Fish* for example.)

Additionally, it came with a [documentary DVD of the project](https://www.youtube.com/watch?v=3utrfgrU8sk), which is also excellent. It doesn't have any of the full performances from the show, but there is a lot of interesting interviews and bits of rehearsal footage.

I will always associate this album with the show I caught in Rome, even though it is completely different. Even without that personal memory, I hope you enjoy *Orchestralli* as this week's pick.

<iframe src="https://open.spotify.com/embed/album/3EonFVs3uiwUMM302ztrRe" width="100%" height="380" frameBorder="0" allowtransparency="true" allow="encrypted-media"></iframe>



