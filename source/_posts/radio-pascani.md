---
title: Fanfare Ciocarlia - Radio Pascani
author: Phil Plencner
date: 2020-04-22 15:52:39
tags:
- fanfare ciocarlia
- romanian music
- trey spruance
- secret chiefs 3
---
Who here is into hyper-speed Romanian Brass bands? :raised_hand:Today's pick is *Radio Pascani* by Fanfare Ciocarlia! 

Fanfare Ciocarlia was a local dance band from a remote town in Romainia, who was discovered by a documentary film maker. They eventually toured Europe and the United States putting out albums along the way. I first heard about them around 1998-1999 when *Radio Pascani* came out because Trey Spruance (of Faith No More / Mr. Bungle / Secret Chiefs 3 fame) wrote a review about the album saying they were hyper-complex music and "faster than Napalm Death". Obviously, I was very intrigued! (And Trey was very much correct).  

I eventually had the pleasure of seeing them perform at a [free festival at Grant Park in Chicago in 2003](https://www.chicagoreader.com/chicago/fanfare-ciocarlia/Content?oid=912336) and it was awesome! (Especially watching people attempt to dance to their more zippy numbers). 

Their more recent albums are still good, but they've kind of smoothed out the rough-edges and toned down their sound over time. I'll stick to this one

<iframe src="https://open.spotify.com/embed/album/0x3aFSS6rffdgqNFT6w3Ip" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>