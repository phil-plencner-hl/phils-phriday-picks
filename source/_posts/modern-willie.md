---
title: Willie Nelson - Modern Willie
author: Phil Plencner
date: 2019-09-27 14:15:45
tags:
- willie nelson
- country music
- country rock
- classic country
---
Did you know that Willie Nelson put out an a new album (*"Ride Me Back Home"*) in June? Did you also know that it's awesome? 

Did you also know that he's been quietly releasing amazing albums like this consistently for the last decade (16 albums since 2009!) as well as throughout his whole career (RMBH is is 69th overall album!!!).  

If you answered "No", I'm here to catch you up with my "Modern Willie" playlist. 2 hours of my favorite Willie Nelson songs from 2009-Present.

<iframe src="https://open.spotify.com/embed/playlist/1L23ryhHic4wDL1iZ06jW2" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>