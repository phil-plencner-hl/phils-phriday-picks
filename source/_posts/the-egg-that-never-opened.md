---
title: High Castle Teleorkestra - The Egg That Never Opened
date: 2022-06-24 11:24:01
tags:
- estradasphere
- mr bungle
- secret chiefs 3
- high castle teleorkestra
- avant-garde rock
- death metal
- progressive rock
- jazz fusion
- jazz rock
---

![](TheEggThatNeverOpened.jpg)

Last week the avant-garde rock super-group High Castle Teleorkestra released their debut album *The Egg That Never Opened*. I have spent the time between then and now immersing myself in this unbelievable magnum opus and still unraveling all the greatness contained within.

For the uninitiated, High Castle Teleorkestra consists of members of bands that were previous Phil's Phriday Picks (Estradasphere, Deserts of Traun) and similar groups (Mr. Bungle, Secret Chiefs 3, Farmers Market). If you have any familiarity with those ensembles you have a general idea about what this sounds like, but even with those expectations set you will likely be blown away by this.

It basically takes the template of Estradasphere's excellent last album, *Palace Of Mirrors*, and pushes every element to the absolute extreme.

High Castle Orchestra takes surf rock, film soundtracks, jazz fusion, 20th century classical, death metal, prog rock, and European folk music and throws it all into a blender at high speed. 

What you suspect might come out as a bunch of silly noise, half-baked ideas or jarring juxtapositions actually comes out as a stunningly complex, yet catchy, highly enjoyable listening experience.

A fine example is a song they released with a music video before the album came out: "Mutual Hazard". A song that combines Balkan folk with death metal and surf rock played in 7/8 with a tempo pushing 300 BPM. Believe me, you are not prepared:

<iframe width="560" height="315" src="https://www.youtube.com/embed/F__WZePyk5A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

As if the music itself wasn't enough, the whole album is inspired by the book *Radio Free Albemuth* by Philip K. Dick.

To learn more about the project overall, I recommend listening to this radio interview by bass player Tim Smolens:

<iframe width="560" height="315" src="https://www.youtube.com/embed/-lzPIk9V_Gc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

You could also just dive in to *The Egg That Never Opened* and the experience will be just the same: a jaw-dropping listening session. 

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/album/3BEEKfVCfGncTwDgsuG0jy?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

<iframe id='AmazonMusicEmbedB09ZHBGLTP' src='https://music.amazon.com/embed/B09ZHBGLTP/?id=ELYR6pmyJN&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *; clipboard-write" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/the-egg-that-never-opened/1622329847"></iframe>

<iframe src="https://embed.tidal.com/albums/227434551" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>

