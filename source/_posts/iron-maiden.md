---
title: Iron Maiden - s/t
date: 2020-04-15 14:47:48
tags:
- iron maiden
- 80s rock
- heavy metal
- thrash metal
---
If there is one band I have probably listened to and enjoyed the longest in my life, it has to be Iron Maiden. Ever since a classmate smuggled me a cassette of Powerslave back in '85 I've been hooked! 

One of the great things about Iron Maiden is the level of quality in everything they do, from their songs and albums to their elaborate concerts. Even their latest studio album *Book of Souls* was amazingly good (It was also a triple album, over 90 minutes long!) 

Observant team members might also remember me sneaking away from the group during our team building outing last summer to play an Iron Maiden pinball machine that happened to be at the barcade.....boy, I can talk about Iron Maiden all day! 

Phil! What is the meaning of all this?! The point is that the very first Iron Maiden studio album came out 40 years ago yesterday! Wow! While the first album didn't have the "classic" lineup in place yet (Bruce Dickenson and Nicko McBrain were still a few years away from joining) it still has the Steve Harris / Dave Murray combo which is one of the best guitar / bass duos in all of rock. 

It also contains songs that are still concert staples today ("Running Free", "Phantom Of The Opera", "Sanctuary", "Iron Maiden"). 

Enough, Phil! Stop talking about Iron Maiden! Just give us the pick!

(UP THE IRONS!!!!!)

<iframe src="https://open.spotify.com/embed/album/5nyyw7ThJdClJ0jPisOta3" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>