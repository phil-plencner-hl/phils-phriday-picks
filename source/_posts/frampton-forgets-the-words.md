---
title: Peter Frampton - Frampton Forgets The Words
date: 2021-05-21 09:06:16
tags:
- peter frampton
- glenn worff
- mark knopfler
- dire straits
- 70s rock
- disco
- radiohead
- instrumental music
---

Peter Frampton has come a long way since his days in the band Humble Pie. Obviously, his largest success was *Frampton Comes Alive* and the associated hits ("Baby I Love Your Way", "I'm In You", "Do You Feel Like We Do" etc). However, he has been quietly doing his thing pretty successfully in the decades since.

In 2019 Frampton announced that he would no longer tour because he suffers from a rare inflammatory muscle disease called  inclusion body myositis. 

This did not stop him from recording an instrumental album of songs originally by musicians that inspire him. The album was released a couple weeks ago with the witty title *Frampton Forgets The Words*. It's very similar to his other recent instrumental album (2006's *Fingerprints*) but I think it is a much stronger work. 

Even though he isn't collaborating with heavyweights like members of Pearl Jam and The Rolling Stones like he did on *Fingerprints*, the band backing him up on *Frampton Forgets The Words* is great. In fact, Glenn Worf is the bassist who is best known as a long-time member of Dire Straits mastermind Mark Knopfler's solo band.  

The selection of songs is really wide-ranging: George Harrison's "Isn't It A Pity", Roxy Music's "Avalon", Stevie Wonder's "I Don't Know Why", Lenny Kravitz's "Are You Gonna Go My Way", and even Radiohead's "Reckoner" (Which has an [amazing video](https://www.youtube.com/watch?v=RHdj_-x_51c)). Talk about running the gamut!

Hopefully Peter Frampton will continue to be healthy enough to release a few more recordings like this in the future.

<iframe src="https://open.spotify.com/embed/album/4p3rxhM3TqcRa05LBkKBBF" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>


