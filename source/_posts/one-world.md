---
title: John Martyn - One World
date: 2024-04-26 21:41:49
tags:
- john martyn
- lee scratch perry
- steve winwood
- chris blackwell
- robert palmer
- folk music
- jazz folk
- reggae
- dub 
- trip hop
---

![](OneWorld.jpg)

John Martyn was a singer and musician who was way ahead of his time. He started out playing mostly folk rock (with a heavy jazz influence) in the late 60s and early 70s. What made him stand apart was his use of guitar effects (especially a tape delay effect called an Echoplex) plus drum machines and loops. 

His earlier albums such as *Solid Air* and *Inside Out* were good, but didn't sell very well. Similar to his friend Nick Drake he was critically acclaimed but wasn't a commercial success. In fact, John Martyn wanted to put out a live record but his label, Island Records, refused because they didn't think it would be successful. John Martyn ended up releasing the album, *Live At Leeds*, himself via mail order.

He had a heavy touring schedule, including playing a bunch of shows in the United States opening for Traffic. This didn't lead to greater success. After the tour a guitarist in his backing band, Paul Kossoff (originally from the band Free), passed away from pulmonary embolism. Around the same time his buddy Nick Drake also passed away. 

All of this tragedy mixed with heavy drug use lead to John Martyn getting disillusioned with the music industry. He decided to take a sabbatical to Jamaica to clear his head and regroup. This turned out to be a very good decision.

While in Jamaica, Island Records founder Chris Blackwell introduced John to Lee "Scratch" Perry and he immersed himself in reggae culture. Similarly singer Robert Palmer (later of the band The Power Station and known for big hits like "Addicted To Love" and "Simply Irresistible") also moved to Jamaica and dove into reggae music. Robert Palmer recorded a single produced by Lee "Scratch" Perry under the pseudonym Bree Daniel called "Oh Me Oh My". John Martyn played guitar on the record:

<iframe width="560" height="315" src="https://www.youtube.com/embed/W5Si2c4C9uA?si=RCuiPJ8GB0nxD4Z2" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Inspired by this session, John Martyn started writing his own music heavily influenced by dub music. He recorded demos in Jamaica including "Blackman On Your Shoulder":

<iframe width="560" height="315" src="https://www.youtube.com/embed/P3Vh0b81Muk?si=hWT322jpbyXFSzDC" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

He eventually flew home to England and recorded his new music outdoors on Chris Blackwell's farm.  To avoid too much background noise while recording it was mostly performed of it in the middle of the night. This gave the songs a very spacy and nocturnal mood. Chris Blackwell also brought in a bunch of amazing musicians to perform the songs with Martyn including Steve Winwood, bassist Danny Thompson and drummer John Stevens.

These recordings became the album *One World*, which is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

One of the highlights of the record include the wild "Dealer" which includes tons of Echoplex mixed with Steve Winwood's bonkers Moog soloing. 

Another great tune is "Big Muff" which was co-written with Lee "Scratch" Perry. This is basically a jazz-dub tune: a very unique combination! There's not much out there quite like it. 

Finally the last song "Small Hours" is a relaxed epic with more Echoplex, pattering percussion and a very boozy, drugged, late night feel to it. You can tell this song was definitely recorded outside because you can hear geese honking in the background and the rustle of a passing locomotive. 

To promote the record, John Martyn assembled an incredible band that included Steve Winwood and drummer Pierre Moerlen (from the band Gong)! They played at the Rainbow Theater in London. Luckily it was recorded. Here is "Dealer" from that concert:

<iframe width="560" height="315" src="https://www.youtube.com/embed/y2tPYgK2Fzg?si=gwbgyeYdgzOGVVeJ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

After that John Martyn performed solo on the TV show *Old Grey Whistle Test*. One of the highlights from that concert was "Big Muff" that showcases all the great Echoplex and other effects he employed on the record:

<iframe width="560" height="315" src="https://www.youtube.com/embed/Y5_-uN1uMBY?si=xl9il29yzSguJ5rY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

*One World* was a big success. It eventually lead to future collaborations in the 1980s with Eric Clapton and Phil Collins on albums like *Glorious Fool* and *No Little Boy* but none of them matched the experimental and groundbreaking songs that encompassed *One World*. In fact, members of Portishead have cited it as an influence in their music and the trip-hop genre in general. 

John Martyn passed away in 2009 from acute respiratory distress syndrome, likely caused by his drug and alcohol habits that he was never able to break free from. His legacy is assured with the masterpiece *One World*.

You can [listen to John Martyn - *One World* on your streaming platform of choice](https://songwhip.com/john-martyn/oneworld) (including Spotify, Amazon Music, Apple Music, Tidal, YouTube Music and more!).
