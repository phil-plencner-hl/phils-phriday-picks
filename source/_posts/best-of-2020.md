---
title: Best Of 2020
date: 2020-12-15 11:06:03
tags:
- best of list
---

2020 has been a pretty wild year. Since I've been in my home most of the year (working and otherwise) this has given me plenty of opportunities to listen to brand-new music. I listened to a TON of it this year. According to my internal counting, I listened to almost 500 new albums that came out this year. Yowza!

Distilling that down to my absolute favorites has been challenging. I ended up creating a few ways to present this wealth of information to you.

The first is the TL;DR; version of the list. These are 25 songs from the albums I enjoyed the most this year. If you want to hear the-best-of-the-best then this is the place for you:
<iframe src="https://open.spotify.com/embed/playlist/2NKCPwzmB9gCtudPbkmOMU" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

If you want to dive into the inner workings of my brain...here are the top 200 songs(!!!) from new albums released this year. It runs the gamut of all genres....death metal to country to jazz. 15.5 hours...this is no holds barred, but still all-killer, no-filler in my opinion. For those who want to do the deep, schizophrenic dive, look no further:

<iframe src="https://open.spotify.com/embed/playlist/5z3M1xfLqDnJPru9tw1lF0" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

If you don't want the whiplash of hearing Testament flow into Thundercat, but still want the deep dive experience, I've also broken up these 200 songs into genre-specific playlists. So if you want to hear 46 Jazz songs or 70 rock songs, your experience is somewhere over here:

<iframe src="https://open.spotify.com/embed/playlist/5hE76yBD8q4T4zNH3HrtdN" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

<iframe src="https://open.spotify.com/embed/playlist/6aW2uIhR4U8UVQXrIgO0Wx" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

<iframe src="https://open.spotify.com/embed/playlist/4aMZhEwCsUAiv6ou5K7pZU" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

<iframe src="https://open.spotify.com/embed/playlist/03dalyR73bxw2gXg3lg0OJ" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

<iframe src="https://open.spotify.com/embed/playlist/6Egh8mNcSqZve8VP5sR35A" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

I hope you find something to enjoy, or new treasures to uncover in these playlists. 

Here's to a better, safer 2021!

