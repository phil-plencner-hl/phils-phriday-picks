---
title: Daryl Hall - Sacred Songs
date: 2023-05-12 20:25:50
tags:
- daryl hall
- hall and oates
- robert fripp
- tony levin
- king crimson
- progressive rock
- 70s rock
- 80s rock
- peter gabriel
---

![](SacredSongs.jpg)

Did you know that Daryl Hall (of Hall & Oates fame) and Robert Fripp (of King Crimson fame) worked together on a couple of albums in the late 70s and almost formed a band together? It sounds crazy, but it is absolutely true!

In 1974, Robert Fripp broke up King Crimson. He completely left the the music industry for a while immersing himself in new age philosophies. He even joined philosopher John G. Bennett's International Academy for Continuous Education for a couple years. 

Eventually he was coaxed out of his retirement by Brian Eno, who convinced him to do session work with David Bowie (That's his guitar you hear on "Heroes"). Fripp consequently moved to New York City and became involved in the city's punk and new wave scenes.

He devised a concept he called "The Drive to 1981". This was a loose idea he had about being active in a few smaller groups instead of reforming King Crimson and each group would have a unique style and put out their own album. He also incorporated his concept of "Frippertronics" into his music heavily around this time. "Frippertronics" involved playing solo guitar along with tape loops of his playing in a mostly ambient style. This concept was first released on the *(No Pussyfooting)* album he made with Brian Eno in 1973. 

At the same time Hall and Oates had recently released *Bigger Than Both of Us* which spawned their first #1 hit "Rich Girl". But Daryl Hall was feeling constrained by group's formula and wanted to try something new. 

He ended up teaming up with Robert Fripp while he was working on his solo album *Exposure*. Daryl Hall ended up co-writing a lot of the music on the album and originally sang on all the songs. However, Hall's record company got wind of this and freaked out. They didn't want him appearing on what they deemed an uncommercial progressive rock album at the height of new wave and punk. Ultimately the record company and Robert Fripp reached some sort of agreement that hall's vocals would only appear on two songs and the rest would be re-recorded by Peter Hammill, who was the vocalist of Van Der Graaf Generator.

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

Fripp was also producing and playing on Peter Gabriel's 2nd album simultaneously. As if that weren't enough, Fripp continued to work with Daryl Hall on his own solo album called *Sacred Songs*. These three albums became part of an informal trilogy that Fripp claimed the point "was to investigate the 'pop song' as a means of expression". *Sacred Songs* is today's pick!

*Sacred Songs* is really interesting because it is essentially what King Crimson would sound like if they tried to be a pop band. The songs mostly remain in a reasonable 3-4 minute time frame ("Babs And Babs" is a notable exception that pushes close to 8 minutes). In fact some of the lyrics even poke fun at the pop music format (such as "Something In 4/4 Time"). 

Fripp's influence does loom large though. "Urban Landscape" is entirely a solo Frippertronics song and "NYCYC" is the same music as "NY3" from *Exposure* but with different lyrics. 

After *Sacred Songs* was completed it was still not satisfactory to the record label. They refused to release it in the 70s and shelved it. The label begrudgingly finally let it come out after Hall & Oates had a huge string of bonkers hits on their 1980 album *Voices* ("Kiss On My List", "You Make My Dreams" etc.).

Because of all these complications, Daryl Hall never properly toured behind *Sacred Songs* which is a shame. Fripp and Hall did try to put together a live band to take on the road (including bassist Tony Levin and drummer Jerry Marotta) but it never fully got off the ground. Eventually Bill Bruford and Adrian Belew joined and they became the new version of King Crimson while Daryl Hall re-focused on Hall & Oates.

Over the years, *Sacred Songs* has become more well-known and revered. Daryl Hall even played some of the songs from *Sacred Songs* on his hit television show *Live From Daryl's House*!

Here he is with Guster playing "Babs And Babs":

<iframe width="560" height="315" src="https://www.youtube.com/embed/uGC12rmS30g" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

And here he is with Butch Walker playing "Why Was It So Easy":

<iframe width="560" height="315" src="https://www.youtube.com/embed/0578oOh8NHg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

*Sacred Songs* is probably my favorite record that Daryl Hall ever played and sang on. Listen for yourself and see if you have the same assessment.

{% iframe 'https://open.spotify.com/embed/album/3U3ZUZfY7AtFTbUGLMPLZz?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B004FYI9MS/?id=2tIjFsP6Bp&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/sacred-songs/408513360' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/5140809' '100%' '96' %}
