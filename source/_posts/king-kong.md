---
title: Jean-Luc Ponty - King Kong
date: 2022-02-25 14:41:43
tags:
- jean-luc ponty
- frank zappa
- george duke
- ian underwood
- art tripp iii
- progressive rock
- jazz rock
- jazz fusion
---

Back in 1967, Jean-Luc Ponty was a classical violinist who began exploring jazz. He was inspired by Miles Davis, especially the contemporary at the time recordings of his Second Great Quintet (which included Wayne Shorter, Herbie Hancock, Ron Carter and Tony Williams) which lead to the invention of jazz rock fusion.

Jean-Luc was one of the first besides Miles' group to record in this style. He played at the Monterey Jazz Festival, which widened his exposure. This lead to him forming a group in the United States with keyboardist George Duke. They played in Hollywood and the concert was released as the live album *The Jean-Luc Ponty Experience with the George Duke Trio*. 

Frank Zappa heard the concert and convinced both Ponty and Duke to join him in the studio to play some of his compositions. These sessions produced the excellent album *King Kong: Jean-Luc Ponty Plays the Music of Frank Zappa*, which is today's pick!

The album mostly included songs that were in the Mothers of Invention repertoire at that time ("King Kong", "Idiot Bastard Son", "America Drinks and Goes Home"), but there was also material that was brand new. 

These include an early version of "Twenty Small Cigars" which was soon after released on Zappa's album *Chunga's Revenge* with his first post-Mother's rock band. 

The centerpiece of the album is "Music for Electric Violin and Low-Budget Orchestra". A long form piece taking up most of the 2nd side of the original record. A stunning composition with excellent playing from the entire ensemble (including Ian Underwood and Art Tripp III who were members of the Mother's). It includes themes from earlier Zappa compositions, but really stands alone as an excellent song on its own. 

The song was later re-recorded in the mid-70s by another Zappa ensemble and released on *Lather* / *Studio Tan* as "Revised Music for Guitar and Low-Budget Orchestra". 

Also included is a song Ponty wrote called "How Would You Like to Have a Head Like That" that is inspired by Zappa's compositional approach. It also includes a killer guitar solo by Zappa himself.

Soon after the album's release George Duke joined Zappa's band...and Jean-Luc Ponty did as well a few years after that appearing on a couple of his more popular albums: *Over-Nite Sensation* and *Apostrophe(')*.

Afterwards, Ponty went on to continued success both with his solo records and as members of Chick Corea's band and The Mahavishnu Orchestra.

<iframe src="https://open.spotify.com/embed/album/35RtSV5vwTe2ndQ26sWo8z?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

<iframe id='AmazonMusicEmbedB000UL9550' src='https://music.amazon.com/embed/B000UL9550/?id=w7KqmTDWm9&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/king-kong-jean-luc-ponty-plays-the-music-of-frank-zappa/724662852"></iframe>
