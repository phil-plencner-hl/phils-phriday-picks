---
title: The Roots - Do You Want More?!!!??!
author: Phil Plencner
date: 2020-07-30 15:38:19
tags:
- the roots
- do you want more
- malik b
- philadelphia
- hip-hop
- rap
- dusty groove records
---

Today's pick is in tribute to Malik B. from The Roots. 

I first heard The Roots by the suggestion of the owner of Dusty Groove in Chicago in 1996 (back when it was just a mail order business with occasional "store hours' in a loft).  "Do You Want More?!!!?!" was recommended to me and I was intrigued so I bought it. What an amazing album!! 

At the time hip hop and jazz hybrids was a newly emerging and trendy thing...but this was at a whole other level. Malik B. was a big part of that. Overall it's amazing to see The Roots trajectory to becoming the House Band for The Tonight Show. They deserve all the praise and accolades. 

Respect.

<iframe src="https://open.spotify.com/embed/album/3N0wHnD5Rd8jnTUvNqOXGz" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>