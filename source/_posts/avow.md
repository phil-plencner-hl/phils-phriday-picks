---
title: Portal - Avow
date: 2021-05-28 08:18:53
tags:
- death metal
- heavy metal
- australia
- avant-garde
---

Every time a new album by Australian crazies Portal comes out, it should be a day of celebration. Today is one of those days, as they just released their new album *Avow*.

I've been a huge fan of Portal since they put out the excellent *Swarth* in 2009. A completely claustrophobic slab of heavy metal. It's an odd mix of death and black metal with crazed repeating riffs, blast beats and howling vocals all buried under a production of haze and grime. As you might expect, it is spectacular. 

I saw Portal play a show at Maryland Death Fest when they toured behind *Swarth* in 2010. This was back when their singer, "The Curator" had a giant grandfather clock for a head. I am not making this up.

Check out this amazing footage from the show. Bonus points if you can find me in the swarming masses (You won't).

<iframe width="560" height="315" src="https://www.youtube.com/embed/e7CSk4lWeDw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

By the time Portal put out their next album *Vexovoid* in 2013 the overall sound was still bonkers and suffocating, but the production was a little cleaner. They gained more notoriety...they were even covered by [NPR](https://www.npr.org/sections/allsongs/2013/01/14/169355456/song-premiere-portal-the-back-wards) and [The New York Times](https://www.nytimes.com/2013/02/19/arts/music/albums-by-portal-and-iceage.html)! 

I saw them again at Maryland Death Fest in 2015 with a much larger crowd due to all the acclaim...here is more killer footage.

<iframe width="560" height="315" src="https://www.youtube.com/embed/PfaLQzTZj-Q" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

2018's *Ion* removed more of the dank and the murk from their approach...in fact some of it even resembled more straightahead death metal bands.

This brings us to today's release of *Avow*. I've only given it one listen so far, but I can safely report that are back to full on dementia. The wild thing about the album is...not only do you think they are insane for playing and recording the music, you also feel like you are losing your mind by listening to it. Not an easy feat.

Portal also put out an accompanying album called *Hagbulbia* today. It is a completely ambient and mellow album. It appears to be almost the same length. Perhaps you are supposed to play them both simultaneously like The Flaming Lips' *Zareeka* or something? 

I will likely be spending a good chunk of the long holiday weekend immersing myself in this madness.

<iframe src="https://open.spotify.com/embed/album/1V7usKn0ebZ9D4XtHw1JFy" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

<iframe src="https://open.spotify.com/embed/album/3DqyIQB2O0kBYyav8Q5Sz8" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>



