---
title: Jazz Sabbath - s/t
author: Phil Plencner
date: 2020-05-06 15:14:03
tags:
- jazz sabbath
- black sabbath
- adam wakeman
- yes
- ozzy osbourne
- 70s rock
- jazz
- jazz fusion
---
Here's a fun new album. You probably didn't think you needed to hear a jazz band performing Black Sabbath covers...but you actually do need to hear a jazz band performing Black Sabbath covers. 

Jazz Sabbath is the brainchild of keyboardist Adam Wakeman, who is actually the son of famous Yes keyboardist Rick Wakeman. 

Adam is currently in Ozzy's live band and has played live with recent Black Sabbath reunions as well, so this is about as legit a tribute as you can get. 

They even made a funny "documentary" trying to make it sound like Jazz Sabbath came first and Black Sabbath stole their ideas!

<iframe src="https://open.spotify.com/embed/album/19i0r1gPdmPUuUwsPfQ8bM" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>