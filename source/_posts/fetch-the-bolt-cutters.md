---
title: Fiona Apple - Fetch The Bolt Cutters
author: Phil Plencner
date: 2020-04-17 16:00:36
tags:
- fiona apple
- 90s rock
- alternative rock
---

The new Fiona Apple album dropped today, so I know what I'm listening to. 

Happy Friday!

[Further reading](https://www.newyorker.com/magazine/2020/03/23/fiona-apples-art-of-radical-sensitivity)

<iframe src="https://open.spotify.com/embed/album/0fO1KemWL2uCCQmM22iKlj" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>