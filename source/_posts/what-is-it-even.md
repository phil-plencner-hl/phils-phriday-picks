---
title: Joshua Ray Walker - What is it Even?
date: 2024-06-14 21:15:44
tags:
- joshua ray walker
- lizzo
- the cranberries
- country
- country rock
---

![](WhatIsItEven.jpg)

Joshua Ray Walker is an incredible singer and songwriter. While he doesn't look like a typical country musician, his talent excels past much more popular artists.

He has been singing since he was kid, but started getting bigger breaks about five years ago after putting out his first solo record *Wish You Were Here*. The album included ten original songs of old-school country music featuring his powerful voice and a tight backing band.

One of the singles was "Canyon" which aptly illustrates what the whole record sounds like:

<iframe width="560" height="315" src="https://www.youtube.com/embed/AO7urf16Mvw?si=ujmlr2fKyxKkPK_w" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

He released two more albums in quick succession. *Glad You Made It* in 2020 and *See You Next Time* in 2021. After his third record he hit the big time when he was invited to perform on The Grand Ol' Opry. Here he is recalling that experience:

<iframe width="560" height="315" src="https://www.youtube.com/embed/hh33l8HTank?si=Nv2zodi9y4NDwPUf" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Along with the original songs, he also performed cover songs. He released a cover of Lionel Ritchie's "Hello" soon after his Opry debut including reenacting the original music video:

<iframe width="560" height="315" src="https://www.youtube.com/embed/bgwdd8wnDvw?si=etqdeibZPxKoHQ6t" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

This lead him to record an entire album of cover songs, but with a twist: Every song was originally performed by a female singer! This record is called *What Is It Even?* and is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

The album starts with an awesome cover of Lizzo's "Cuz I Love You". Here is the music video for that:

<iframe width="560" height="315" src="https://www.youtube.com/embed/oAa8rDwFE5I?si=aapUIvfz49rY1yFb" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Here he is performing "Cuz I Love You"  on *Jimmy Kimmel Live*:

<iframe width="560" height="315" src="https://www.youtube.com/embed/6EHQzayBqwo?si=rvfT___G95bK50X9" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Another highlight is a cover of "Linger" originally by The Cranberries. It even includes a cameo of Kyle from Tenacious D on recorder!

<iframe width="560" height="315" src="https://www.youtube.com/embed/H5VFaiQOU3w?si=52yJdProT_GidLIQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

The rest of the songs are equally as incredible and runs the gamut: "I Want To Dance With Somebody" (Whitney Houston), "Believe" (Cher), "Blue" (Leann Rimes), "Nothing Compares 2 U" (Sinead O'Connor) and "Halo" (Beyonce) are also included. They are all well performed with sincerity and are not done in a cheesy or kitschy way.

This lead to a high profile tour opening up for The Killers. Here they are performing together near the end of the tour:

<iframe width="560" height="315" src="https://www.youtube.com/embed/hZLAPqTCp30?si=Nb0FD3SiVpw9zgcc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

At the end of the tour he ended up getting appendicitis and was hospitalized. He released a live record poking fun at the experience called *I Opened For The Killers And All I Got Was Appendicitis*.

Unfortunately, it wasn't funny. While at the hospital it was revealed that he actually has colon cancer! All his career momentum has slowed down as he is being treated. Here is an interview where he talks about the current situation:

<iframe width="560" height="315" src="https://www.youtube.com/embed/WHGDF9GZoQw?si=95xdwm3KUK7XbZBo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Despite his diagnosis and treatment he is still making music. He re-recorded a bunch of his original songs solo and released an album called *Thank You For Listening* including the brand new title track, which is absolutely beautiful:

<iframe width="560" height="315" src="https://www.youtube.com/embed/YotR4BJf82k?si=pa1LQx8gdUmjjbNM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Hopefully he will fully recover soon. In the meantime, *What Is It Even?* remains in very heavy rotation on my stereo. It stands alone as a great record that is infinitely re-playable.

You can [listen to Joshua Ray Walker - *What Is It Even?* on your streaming platform of choice](https://songwhip.com/joshua-ray-walker/what-is-it-even) (including Spotify, Amazon Music, Apple Music, Tidal, YouTube Music and more!).

