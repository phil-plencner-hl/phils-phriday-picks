---
title: Thundercat - It Is What It Is
author: Phil Plencner
date: 2020-04-07 15:03:34
tags:
- thundercat
- funk rock
- jazz
- jazz fusion
- kendrick lamar
- flying lotus
---
You might know of bass virtuoso Thundercat without realizing it. 

He has played and been a major contributor to other artists outside of his solo work that have reached large acclaim (Kendrick Lamar, Flying Lotus, Kamasai Washington, Suicidal Tendencies, Mac Miller etcetcetc). 

His new album is pretty great...not as wide reaching as his previous album, Drunk, but it nevertheless is still an excellent example of modern day soulful jazz funk fusion.

<iframe src="https://open.spotify.com/embed/album/59GRmAvlGs7KjLizFnV7Y9" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>