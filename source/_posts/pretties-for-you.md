---
title: Alice Cooper - Pretties For You
author: Phil Plencner
date: 2020-06-18 22:11:27
tags:
- alice cooper
- frank zappa
- straight records
- 70s rock
- progressive rock
---

I'm continuing my ongoing series of "debut albums that sound nothing like what they eventually became popular for"  with Alice Cooper's *"Pretties For You"*.

This is a psychedelic rock classic that shows none of the hard rock Alice Cooper eventually became famous for (even though the band is exactly the same as classics such as "Killer" and "Schools Out" etc.) 

It was also produced by Frank Zappa and released on his Straight Records imprint. Lots of weird time signatures, sound effects and a huge musical range. 

I wonder what would have happened if they continued on this direction?

<iframe src="https://open.spotify.com/embed/album/2WcMeeNO4eMBYKrPQMJO80" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>