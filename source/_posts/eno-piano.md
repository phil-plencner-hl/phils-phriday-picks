---
title: Bruce Brubaker - Eno Piano
date: 2024-02-23 21:23:32
tags:
- bruce brubaker
- brian eno
- harold budd
- cluster
- jon hopkins
- leo abrahams
- ambient music
- 70s music
- classical music
---

![](EnoPiano.jpg)

I have long been fascinated with Brian Eno's ambient music concepts. More than merely background music this style is, in the famous words of Eno himself "as ignorable as it is interesting".

One of Eno's earliest experiments in this style was the collaboration with King Crimson's Robert Fripp called *(No Pussyfooting)*. It was the first ambient music I knowingly heard and I came across it because of the association with Fripp. I was pretty shocked and surprised with how it sounded because it sounds nothing like the bombastic progressive rock of King Crimson. It consists of what became known as "Frippertronics" consisting of the guitar played with tape delay and loops in a very slow and calculated way.

Here is Robert Fripp describing Frippertronics in 1973:

<iframe width="560" height="315" src="https://www.youtube.com/embed/kJ0_IlD7c14?si=gtr8EmTN7wubKfu3" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Brian Eno's most famous ambient work is *Music For Airports* which he composed mostly for piano and synthesizer. It also incorporates tape loops and often has multiple piano parts playing simultaneously but in very calming sustained notes.

Few have tried to replicate *Music For Airports* live, but one notable group that did was Bang On A Can. They performed the music as an ensemble of strings, guitar, synthesizer and percussion. Here they are performing "1/1" from *Music For Airports* at the San Diego Airport in 2015:

<iframe width="560" height="315" src="https://www.youtube.com/embed/6T92kvk4nXs?si=NQhsZ1CeVcst6KqK" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

More recently, pianist Bruce Brubaker has performed and recorded three of the four songs from *Music For Airports* for solo piano. This was released as part of the album *Eno Piano* which is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

Bruce Brubaker is a Julliard trained musician and composer who has focused a large part of his career around performing ambient music from composers like John Cage and Philip Glass. The [biography on his website](https://brucebrubaker.com/biography/) has a pretty detailed rundown on his illustrious career.

Additionally there is a [great interview with Bruce about his time at Julliard](https://www.pbs.org/wnet/americanmasters/archive/interview/bruce-brubaker/) that was recorded for PBS' *American Masters* series which is worth checking out.

For *Eno Piano* he worked with French sound designer Florent Colautti to rig up a piano with an electromagnetic device to allow the notes of the piano to resonate for long periods of time without decay. The New England Conservatory (where Bruce Brubaker is currently a faculty member) [released an excellent piece describing the process in great detail](https://necmusic.edu/news/piano-department-co-chair-bruce-brubaker-reimagines-brian-eno-november-10-album-release).

Additionally there is a video clip of the piano with the electromagnetic gizmo that helps visualize the concept:

<iframe width="560" height="315" src="https://www.youtube.com/embed/T-TgaXQMDVg?si=FzD6WCosMWjOx6JS" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

In addition to the *Music For Airports* pieces, *Eno Piano* also includes other Brian Eno ambient songs that were originally conceived for piano. These include "By This River" (from *Before And After Science*), "The Chill Air" (from the Harold Budd collaboration *Ambient 2: The Plateaux of Mirror*) and "Emerald And Stone" (from *Small Craft on a Milk Sea*).

All of the selections make for a great overall album. The performance of each piece is amazing and unique, yet flows together as a unified album. 

*Eno Piano* is an excellent reimaging of Brian Eno's ambient works that I enjoy immensely both as background music or more active listening.

{% iframe 'https://open.spotify.com/embed/album/3eoVjC9sGK7XJ3Qbiyio5K?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B0CH111W7B/?id=2C11TRNea7&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/eno-piano/1705197321' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/317525864' '100%' '96' %}
