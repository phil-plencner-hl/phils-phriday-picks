---
title: Franklin County Trucking Company - The Further Adventures Of
author: Phil Plencner
date: 2019-11-08 14:03:21
tags:
- franklin county trucking company
- the legendary shack shakers
- the supersuckers
- country music
- country rock
- punk rock
---

Today's pick is a fun country-rock supergroup; Franklin County Trucking Company. 

Includes members of the Legendary Shack Shakers and the Supersuckers! As the group name implies, the album consists of trucking songs. 

Let's drive off into the 3 day weekend!

<iframe src="https://open.spotify.com/embed/album/3AihnotYHTg0HtLcoZpqzu" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>