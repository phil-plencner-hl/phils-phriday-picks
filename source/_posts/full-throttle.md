---
title: Blue Meanies - Full Throttle
author: Phil Plencner
date: 2020-05-27 14:35:35
tags:
- blue meanies
- thick records
- 90s rock
- punk rock
- ska
---

Today's pick is the absolutely wild and hyper Blue Meanies. 

Chicago's strangest punk/ska band, they blew the roof off of venues there and across the country with their manic style. 

If you've never heard them before, I don't think you're ready for the sonic onslaught after just reading the words "punk / ska". 

My favorite Blue Meanies album is Full Throttle, but their whole discography is pretty solid.

<iframe src="https://open.spotify.com/embed/album/5AhuxwiRy1Sq0FVBfpbjyQ" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>