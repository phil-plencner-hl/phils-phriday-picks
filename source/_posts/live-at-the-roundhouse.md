---
title: Nick Mason's Saucerful of Secrets - Live at The Roundhouse
date: 2022-09-02 14:07:34
tags:
- nick mason
- pink floyd
- lee harris
- guy pratt
- gary kemp
- dom beken
- orb
- spandau ballet
- transit kings
- 60s rock
- psychedelic rock
---

![](LiveAtTheRoundhouse.jpg)

Nick Mason has the unique distinction of being the only person who has appeared on every Pink Floyd album. From 1967's *The Piper At The Gates Of Dawn* through this year's single "Hey, Hey Rise Up!" he has been the drummer of Pink Floyd. 55 years is a long time to be doing anything. It is especially a long time to be doing something well.

If you really want to be technical, he was also part of Pink Floyd when they were called The Tea Set in 1965. No matter how you slice it, he has been the most constant member of the psychedelic rock band.

Pink Floyd's most popular material starts with the 1973 album *The Dark Side of The Moon*. Since that time the band rarely, if ever, played the songs on the earlier albums anymore.

In 2018 Nick Mason sought to change that. He started a new band called Nick Mason's Saucerful of Secrets. They exclusively play Pink Floyd songs that were recorded prior to 1973, including material written by Syd Barret that hasn't been performed since he left the group in late 1968!

Along with Nick Mason, the group is comprised of Lee Harris on guitar, Guy Pratt on bass, Gary Kemp (of Spandau Ballet fame!) on guitar and vocals, and Dom Beken (who played with The Orb side-project The Transit Kings) on keyboards.

Alert Pink Floyd fans might recognize Guy Pratt as Pink Floyd's live bassist from 1987's *Momentary Lapse of Reason* tour and beyond.

Here is a video from that tour, where you can see Nick Mason and Guy Pratt playing together on "One Slip":

<iframe width="560" height="315" src="https://www.youtube.com/embed/HrHWI1AIjf4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

In 2018 and 2019 they extensively toured Europe and the United States. As part of that tour, they recorded *Live at The Roundhouse*, which is today's pick!

It is an excellent recording of a band that sounds incredible. Everyone is locked in and plays the songs with the right amount of precision yet keep their crazed psychedelic character intact. 

The setlist runs the gamut from recognizable fan favorites like "Interstellar Overdrive" (from *A Piper At The Gates Of Dawn*), "Set The Controls For The Heart Of The Sun" (from *A Saucerful of Secrets*), "One Of These Days" (from *Meddle*) to more obscure selections like "Vegetable Man" (intended for release on *A Saucerful Of Secrets* but not officially released until 2016!), "If" (from *Atom Heart Mother*), "Obscured By Clouds" (from the soundtrack of the movie *La Vallée*), and "The Nile Song" (from the soundtrack to the movie *More*). Something for everybody!

As part of the album package, there was also a DVD/Blu-Ray of the concert. It is beautifully shot. Here is a clip of them playing "Astronomy Domine":

<iframe width="560" height="315" src="https://www.youtube.com/embed/Wdn1OzCK5Yw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Fun fact about Nick Mason: Aside from his drumming, he also collects and races classic cars! Here is an incredible video of him and A/C singer Brian Johnson zipping around in his Ferrari 250 GTO:

<iframe width="560" height="315" src="https://www.youtube.com/embed/B-5FpSvyvE4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Nick Mason's Saucerful of Secrets is touring the United States again starting later this month. The tour has been postponed since 2020, so it is highly anticipated. I'm looking forward to it!

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/album/7ActmIF0QsJwdcZlN2rUKf?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

<iframe id='AmazonMusicEmbedB083ZMMP3M' src='https://music.amazon.com/embed/B083ZMMP3M/?id=OOXN1GlJ7X&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *; clipboard-write" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/live-at-the-roundhouse/1495412702"></iframe>

<iframe src="https://embed.tidal.com/albums/128536282" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>