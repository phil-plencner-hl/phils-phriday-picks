---
title: Goodbye
author: Phil Plencner
date: 2019-08-09 15:36:46
tags:
- 90s rock
- 70s rock
- jazz
- hip-hop
- progressive rock
---
Today is the last day working with the UM interns, so I feel it would be apt to share my "Goodbye" playlist. 

I made this for my team when I left Johns Hopkins Medicine to work for Harvard Library last summer.  

The director of the Marketing and Communications team there noted that I included "Dirty Work" by Steely Dan on the playlist, but I wouldn't read too much into that. 

Anyways, farewell to the interns!

<iframe src="https://open.spotify.com/embed/playlist/7nDryt4j4GajTa58SZhqhI" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>