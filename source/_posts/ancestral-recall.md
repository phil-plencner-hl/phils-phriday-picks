---
title: Christian Scott - Ancestral Recall
author: Phil Plencner
date: 2019-11-22 13:58:33
tags:
- christian scott
- jazz
- jazz fusion
---

The Grammy's listed their 2020 nominees earlier this week, and one of the more exciting nominees for me was the Best Contemporary Instrumental Album nod for trumpet player Christian Scott's amazingly awesome album *Ancestral Recall*. 

If you haven't had the pleasure of immersing yourself with this album, I suggest you do. The album was self described as "a map to decolonize sound; to challenge previously held misconceptions about some cultures of music; to codify a new folkloric tradition and begin the work of creating a national set of rhythms". 

YES.

<iframe src="https://open.spotify.com/embed/album/2XKpZN3mHtwpnwLfDLT18C" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>