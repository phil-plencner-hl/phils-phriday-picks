---
title: Robert Wyatt - Rock Bottom
author: Phil Plencner
date: 2020-07-29 15:41:03
tags:
- robert wyatt
- rock bottom
- soft machine
- matching mole
- nick mason
- pink floyd
- cheer-accident
- progressive rock
- 70s rock
---

Over the weekend I re-obsessed over a long time favorite of mine: *"Rock Bottom"* by Robert Wyatt. 

Robert Wyatt was originally the drummer for English progressive rock bands Soft Machine and Matching Mole (where he also sang). 

After Matching Mole broke up he was attending a birthday party and accidentally fell out of a 4th floor window. He was consequently paralyzed from the waist down, effectively ending his drumming career. *"Rock Bottom"* was the music that came directly after this experience. 

A very sorrowful album full of long expressionist jazzy compositions. The album was produced by Pink Floyd drummer Nick Mason, so it has a lot of their earlier spacey ambiance throughout. His singing style went on to influence previous PPP Cheer-Accident. 

Overall, a pretty great album to listen to in our continued quarantine.

<iframe src="https://open.spotify.com/embed/album/6CGNTxZBa20mHcsAIqQtit" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>