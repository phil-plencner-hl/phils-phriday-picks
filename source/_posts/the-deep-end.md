---
title: Gov't Mule - The Deep End Vol. 1 and Vol. 2
date: 2021-04-16 07:43:58
tags:
- allman brothers band
- gov't mule
- warren haynes
- matt abts
- allen woody
- 90s rock
- 2000s rock
- power trio
- blues rock
---

Guitarist Warren Haynes and bassist Allen Woody first came into prominence by being recruited to take part in the late 80s reunion of The Allman Brothers Band. This is where they met and formed a musical bond. 

Eventually The Allman Brothers band imploded again due to differences in direction with founding member Dickey Betts. 

Gov't Mule was formed out of the ashes of this with Haynes and Woody joined by drummer Matt Abts, who also recently left Dickey Betts' side-project solo band (Mr. Betts must be a real nice guy!)

Gov't Mule is a power trio in every sense of the term...with an emphasis on the word *power*. They also expanded the usual template of what a power trio plays, including a lot of hard funk, progressive rock and (free) jazz influences. 

They put out a few great early albums with this lineup. I was lucky enough to see them perform on tours for the 2nd album (*Dose*) and their 3rd album (*Life Before Insanity*) and they were incredible experiences. 

Here's some amazing footage from 1999 performing the instrumental *Thorozine Shuffle*:

<iframe width="560" height="315" src="https://www.youtube.com/embed/W-NP-LdLu3g" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Unfortunately, just as their popularity was starting to reach a peak, tragedy struck. Allen Woody was found dead in his hotel room as a result of a drug overdose.

Most bands would probably pack up shop after such a thing occurred (especially a power trio). However, Gov't Mule decided to turn this tragedy into a celebration of Allen Woody and bass players in general. They came up with the concept of *The Deep End*. This would be an album of new and cover songs that would feature a different bass player on every selection. 

They ended up recruiting so many bass players and had so much great material that they turned it into two separate albums (*The Deep End Vol. 1* and *The Deep End Vol. 2*). Both are excellent, so they both are today's pick!

The list of bass players is incredible. A small sampling includes Jack Bruce (Cream), John Entwistle (The Who), Mike Watt (The Minutemen), Bootsy Collins (P-Funk), Flea (Red Hot Chili Peppers), Les Claypool (Primus), Phil Lesh (The Grateful Dead), Tony Levin (King Crimson), Meshell Ndegeocello, Chris Squire (Yes) and many, many more. Some players fit more seamlessly into Gov't Mule's tradional sound and others (especially Les Claypool and Bootsy Collins) more obviously influence the material they play on.

The album showcases a large cross section of Gov't Mule's sound and influences. Everything from classic rock to funk to country to progressive rock to jazz fusion is on display. It's an amazing collection of tunes.

They took the "Deep End" concept a step further by performing a one-day concert in New Orleans showcasing 13 different bass players during a nearly 3 hour set. It was released as a stunning live DVD called *The Deepest End*, which is worth seeking out for those who want to hear the climax of this amazing concept.

Afterwards, Gov't Mule settled on a full-time bass player (Andy Hess) and continue to release new albums and tour, but I don't believe they have ever reached the heights of the era with Allen Woody or the "Deep End" concept that came immediately after.

<iframe src="https://open.spotify.com/embed/album/378K25f02JVVcrMbeL6Pp1" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

<iframe src="https://open.spotify.com/embed/album/20RP2mZPU9ezDgjFo5lg3F" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>




