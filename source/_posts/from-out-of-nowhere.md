---
title: Jeff Lynne's ELO - From Out Of Nowhere
author: Phil Plencner
date: 2019-11-01 14:05:37
tags:
- jeff lynne
- elo
- classic rock
- 70s rock
---

Literally out of nowhere this morning, Jeff Lynne has resurrected the ELO name and sound with a new album. 

I don't even have to listen to the whole thing to know this is epic and it is this week's official Phriday Pick!!

<iframe src="https://open.spotify.com/embed/album/4JjLldxv4cJVhZpkyJIq4C" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>