---
title: Tomahawk - Anonymous
date: 2021-11-24 10:12:41
tags:
- tomahawk
- duane denison
- mike -patton
- kevin rutmanis
- john stanier
- faith no more
- melvins
- jesus lizard
- hank iii
- helmet
- alternative rock
- hard rock
---

Phil's Phriday Picks is early this week, due to the upcoming Thanksgiving Holiday.

Tomahawk is a band that was formed when vocalist Mike Patton (Faith No More / Mr. Bungle / Fantomas) met guitarist Duane Denison (Jesus Lizard / Hank Williams III) in 2001. They recruited bassist Kevin Rutmanis (Melvins) and drummer John Stanier (Helmet / Battles). 

As you could surmise from the list of other bands the members played in, it was a supergroup of sorts that played mathy hard rock with an overall dark vibe.

They put out a couple albums in 2001 and 2003 that were pretty solid, then went on an extended hiatus.

During that time off, Duane Denison toured Native American reservations with Hank Williams III and became interested in the culture's traditional music. He researched the music and found transcriptions of the compositions. 

He decided to take those original tunes and expand them into a heavy rock trio format. His interpretations of the songs became the next Tomahawk record in 2007 called *Anonymous* (because the transcriptions he found did not credit the original songwriters).

The album is definitely a left-turn for the band. It sounds nothing like their others. In fact, it's pretty unique for rock bands in general to tackle material such as this.

As part of my Thanksgiving celebrations every year since it's release I have always played this album. This is why it is this week's pick!

Happy Thanksgiving.

<iframe src="https://open.spotify.com/embed/album/5LUV8QPz1zK9mJuB72ezKU?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>
