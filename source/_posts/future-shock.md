---
title: Herbie Hancock - Future Shock
date: 2021-04-09 08:18:51
tags:
- herbie hancock
- bill laswell
- grand mixer dxt
- pete cosey
- miles davis
- material
- jazz rock
- hip-hop
---

Today's pick is one of the first albums my parents bought me that was not a Disney record or something like that: Herbie Hancock's *Future Shock*.

Obviously, *Future Shock* was built around the hit song "Rockit". However, this was not originally a Herbie Hancock song!

"Rockit" was written and performed by Bill Laswell's avant-funk band Material. In fact, you can see footage of the band Material playing "Rockit" while on tour in 1983, before Herbie Hancock's involvement. [This Daily Motion post captures the moment, starting at about the 21:30 mark](https://www.dailymotion.com/video/x5syaev). 

Material at this time comprised free-jazz guitarist Sonny Sharrock, but by the time the studio recordings were made, he was replaced by the equally awesome Pete Cosey, who played with Miles Davis' crazed funk bands of the early 1970s. Fair enough.

The band also included turntablist Grandmixer DXT. This song was probably one of the earliest exposures of mainstream culture to this art form and was a huge part of "Rockit"'s success and novelty at the time.

Bill Laswell showed off an early recording of Rockit to Herbie Hancock, and he was excited and wanted to get involved. Eventually he added tons of synthesizers (including the iconic melodic line) to the song. Near the end of the recordings they incorporated an Africa Bambaataa sample (from "Planet Rock") which brought the song its hook and inspired its final title.

The song was promoted by a cutting-edge music video, produced and directed by Godley & Creme (who used to be the masterminds of the band 10cc!). It blew up on MTV and won a bunch of awards.

<iframe width="560" height="315" src="https://www.youtube.com/embed/GHhD4PD75zY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The pinnacle of "Rockit" mania was probably the 1984 Grammy awards. The song was performed by Herbie Hanock (on a keytar!) with a bunch of dancing mannequins! This is where I first saw and heard music like this and couldn't believe that music was made this way. I bugged my parents until they bought me the record.

<iframe width="560" height="315" src="https://www.youtube.com/embed/jWeBJsg6FHA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

If you really want to go down a 80s synthesizer internet rabbit hole today, I also suggest this completely ridiculous footage of Howard Jones, Stevie Wonder, Thomas Dolby and Herbie Hancock playing a melody of hits, including "Rockit".

<iframe width="560" height="315" src="https://www.youtube.com/embed/E-NgnympxAE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The rest of *Future Shock* is worth listening to beyond "Rockit". The title track is a wild cover of a Curtis Mayfield song. Some other songs have a more interesting mix of traditional jazz piano with the digital band. Overall, a pretty interesting time capsule worth revisiting.

Herbie Hancock also continued in this musical direction on a couple more albums (*Sound-System* and *Perfect Machine*) and while those are also great, *Future Shock* is probably the best of the lot.

<iframe src="https://open.spotify.com/embed/album/108uNBYGawRo3aQiaA7lQY" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
