---
title: The Moog Cookbook - Ye Olde Space Band
author: Phil Plencner
date: 2020-04-30 15:33:51
tags:
- moog cookbook
- jellyfish
- 90s rock
- 70s rock
- classic rock
---
The Moog Cookbook is a fun "band" that was formed by the Keyboardist of power pop band Jellyfish after their breakup. 

He teamed up with another keyboard playing buddy (who later ended up playing live in a reformed version of The Who, but I digress!). 

The concept was to play cover songs of popular hits entirely on Moog synthesizers. 

The first album is my favorite, as they cover alternative rock hits from the 90s (sadly, not on Spotify!) but the follow up in which they perform Classic Rock gems is still pretty great.

<iframe src="https://open.spotify.com/embed/album/6W2UlntAS3IpIZmDH0qFWk" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>