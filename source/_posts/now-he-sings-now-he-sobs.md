---
title: Chick Corea - Now He Sings Now He Sobs
date: 2024-03-15 21:00:11
tags:
- chick corea
- roy haynes
- miroslav vitous
- jazz
- hard bop
- avant jazz
- 1960s music
---

![](NowHeSings.jpg)

Drummer Roy Haynes [celebrated his 99th birthday this week](https://www.dailykos.com/stories/2024/3/13/2229257/-Roy-Haynes-99-years-old)! It's hard to believe that Haynes is still alive and still reportedly playing. His career began in the 1940s and has continued for 80 years. He has played with seemingly everyone famous in jazz music:  Lester Young, Stan Getz, Charlie Parker, Miles Davis, Sarah Vaughn, Thelonious Monk, Eric Dolphy, Andrew Hill, Chick Corea and many more. An incredible achievement. 

Some of the earliest footage of him I could find was from 1966, playing with Stan Getz. Gary Burton and Steve Swallow were also in the group (Roy Haynes played extensively with both of them throughout the years afterwards).  Here is "Scrapple From The Apple":

<iframe width="560" height="315" src="https://www.youtube.com/embed/wlSg2f58w_I?si=-vjdFQz1zqzeiyjv" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

They are really cookin' there! Even better is this footage of the same band playing "Jive Hoot" complete with a monstrous drum solo that sounds like something a heavy metal drummer would play if it was 20 years later:

<iframe width="560" height="315" src="https://www.youtube.com/embed/kBeBnnqpOcs?si=r0VEUspt8nRZALUj" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Speaking of Roy Haynes drum solos, here is a one from 1973 that is astonishing:

<iframe width="560" height="315" src="https://www.youtube.com/embed/jvDkZ-paj5o?si=zlcJ-xzc3FeOjraO" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

With such a lengthy career, it was hard for me to pick one album that showcases his playing more than any other. I considered Andrew Hill's *Black Fire*, Jackie McClean's *Destination...Out!* and Gary Burton's *Duster* but ultimately decided on *Now He Sings, Now He Sobs* the Chick Corea record from 1968.

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

*Now He Sings, Now He Sobs* is quite possibly the best jazz trio record ever made (Miroslav Vitous is the bass player). It is a universally acclaimed album that showcases the virtuosity of each musician. It is mostly improvised, but does include some melodic ideas that Chick Corea wrote that became standards such as "Steps" and "Windows". 

The original record only had 5 songs, but in 1988 it was re-released with 7 songs from the same recording sessions (including the aforementioned "Windows" along with the Thelonious Monk tune "Pannonica"). 

The 1988 version is what I first heard back in high school. I was previously familiar with Chick Corea from his albums that were electric jazz fusion (especially his bands Return to Forever and the Chick Corea Elektric band) so the acoustic, free playing of *Now He Sings, Now He Sobs* was pretty shocking to me, but very alluring. It has to be one of the jazz albums I listened to the most since then. It's mixture of hard bop and avant / free jazz was highly influential to my own playing. 

Chick Corea and Roy Haynes continued collaborating through the decades. This particular trio recorded a couple more albums together: *Trio Music* in 1982 and *Trio Music Live In Europe* in 1986 (which actually is live recordings from 1984). All three records are essential listening, but they never reached the awesome heights of *Now He Sings, Now He Sobs*. 

This is not to say that the music they made was not great. Need further proof? They were invited to The White House to perform in 1982. Luckily, the internet has you covered if you're interested in seeing and hearing this unique performance:

<iframe width="560" height="315" src="https://www.youtube.com/embed/8OJyVXXWwDc?si=KcsXQlERqIQiBMRK" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Later on, Roy Haynes was part of Chick Corea's Freedom Band in 2010. At 85 years old, Haynes certainly had not lost his fastball. Check out this drum solo from that tour:

<iframe width="560" height="315" src="https://www.youtube.com/embed/EEb-wA2hgB4?si=nrcKHmUYTs7tyKN9" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Even more recently, Jon Batiste interviewed Roy Haynes on *The Late Show With Stephen Colbert* in 2017:

<iframe width="560" height="315" src="https://www.youtube.com/embed/yS-mLA_zvKM?si=TKBwa2L6hkq2eWa8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Roy Haynes career is so full of highlights and so lengthy that I have barely scratched the surface of it all. Nevertheless, *Now He Sings, Now He Sobs* remains one of my all-time favorites. Every time I listen I am inspired. There is usually something new I hear each time. 

Don't take it only from me. *Now He Sings, Now He Sobs* was inducted into the Grammy Hall of Fame in 1999. 

Bonus: If you want to hear even more highlights from Roy Haynes illustrious career, I have created a [playlist with 20 of my favorite songs featuring him](https://open.spotify.com/playlist/0pxJesbKoQ6vM6tBSvpWL4?si=aa2977af0c59460e) (including "Steps - What Was" from *Now He Sings, Now He Sobs*). Dig in!

{% iframe 'https://open.spotify.com/embed/album/7wKVcBB5SgqVX3Cj3LPllE?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B000SZBJIO/?id=RSK8XzFq8o&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/now-he-sings-now-he-sobs/724790507' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/153028' '100%' '96' %}
