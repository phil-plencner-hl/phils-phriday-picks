---
title: Isaiah Collier and The Chosen Few - The World is on Fire
date: 2024-11-22 20:07:53
tags:
- isaiah collier
- john coltrane
- free jazz
- spiritual jazz
- avant-jazz
---

![](WorldIsOnFire.jpg)

Isaiah Collier is a bandleader, composer and saxophone player who has been regularly putting out awesome records and performing exciting shows, both solo and with his band The Chosen Few for close to a decade. He's taken inspiration from the classic spiritual jazz music of John Coltrane, Pharoah Sanders and Don Cherry and modernized it in new and exciting ways.

Here is some early footage of the group performing John Coltrane's "Impressions" in 2019 at a Chicago street festival:

<iframe width="560" height="315" src="https://www.youtube.com/embed/1kZiN9NLVvk?si=gThJBaVkj5z_y5Hf" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Each of Isaiah's records are theme based. For example, *Cosmic Transitions* from 2021 was inspired by the planet Mercury, especially when it is in retrograde. Interestingly, it was recorded on John Coltrane's birthday in 2020 at the famous Van Gelder Studio (where Coltrane recorded many of his classic albums including *Africa / Brass*, *Ascension* and *A Love Supreme*).

*Cosmic Transitions* is a 5 part suite, with each song flowing into the next. The third part of the suite is called "Understanding". Here is the band playing it like their lives depended on it in 2022:

<iframe width="560" height="315" src="https://www.youtube.com/embed/YCNfmkp5D7M?si=HGxskKDWnMGbuJpC" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

This year, Isaiah Collier and The Chosen Few have put out two records. The first, released back in the spring, is *The Almighty*. It was inspired by John Coltrane's *Interstellar Space* (dig the sleigh bells at the beginning of *The Almighty* that is an obvious nod to that record, for example). 

Here is an incredible performance of Isaiah Collier and The Chosen Few playing "Love" from *The Almighty*. Total fire!

<iframe width="560" height="315" src="https://www.youtube.com/embed/EFtgfAVmYRQ?si=lAjaqChWjkv2q4Fh" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

I didn't think Isaiah Collier would be able to top *The Almighty*, but I am very wrong. Last month he released *The World is on Fire*. It takes the template of *The Almighty* and cranks up the intensity to the breaking point. It will likely be one of my favorite records of the year when it is all said and done. *The World is on Fire* is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

I'm not the only one who thinks this will probably be one of the best albums of 2024. [Hank Shteamer at *The New York Times* also agrees](https://www.nytimes.com/2024/09/08/arts/music/isaiah-collier-the-world-is-on-fire.html?unlocked_article_code=1.cE4.5W-S.0IMl_cvixWW0&smid=url-share).

Each song on the record is inspired by current events.  Many of them even include snippets of news footage and other quotes to drive home the message he is hoping to convey.

The title track is inspired by the recent California Wildfires along with other effects of climate change. "Amerikkka the Ugly" is in reference to the January 6, 2021 insurrection. "Ahmaud Arbery" is obviously about the hate-crime murder of Arbery in Georgia in 2020 and so on. 

The whole record is not all doom and gloom. The final song on the record "We Don't Even Know Where We Are Headed" is actually filled with optimism and hope. A fitting way to end this album. In fact, it is a fitting way to end the group. Isaiah Collier has [claimed this will be his last album with The Chosen Few](https://www.wbez.org/music/2024/10/23/isaiah-collier-music-jazz-new-album-the-world-is-on-fire).

In what might be one of their final performances, here is The Chosen Few performing "The Hate You Give is the Love You Lose" and "Ahmaud Arbery" from *The World is on Fire*:

<iframe width="560" height="315" src="https://www.youtube.com/embed/uQBIrhORpLc?si=mh3ECDcTYeVDyMxO" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

The end of the year is quickly approaching, so I'm starting to compile a list of my favorite albums of 2024. *The World is on Fire* will clearly be near the top of my list.  Stay tuned for the rest.

{% iframe 'https://open.spotify.com/embed/album/3dEmEoutUFwiwc427SFkMK?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B0DGQ7ZG3X/?id=JrEWUuR2Mb&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/the-world-is-on-fire-feat-isaiah-collier/1767773289' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/386652646' '100%' '96' %}
