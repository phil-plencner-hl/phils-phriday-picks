---
title: Dark - Tamna Voda
date: 2024-08-23 16:12:01
tags:
- jazz fusion
- indian music
- progressive rock
- frank zappa
- mark nauseef
- leonice shinneman
- mark london sims
- miroslav tadic
- l shankar
- david torn
---

![](TamnaVoda.jpg)

Drummer Mark Nauseef is one of those players you've likely heard play, but you didn't know it. He played with some famous people but generally stayed out of the limelight.

In fact, his first "big break" was playing for a latter-day version of The Velvet Underground! This was after all the original lineup (including Lou Reed) had already departed. It was the Doug Yule version of the group. Mark Nauseef played live with them as part of a European tour in 1972 and the group was done for good soon after. 

Afterwards he joined Ronnie James Dio in his pre-Rainbow group called Elf. He played on the final Elf record *Trying to Burn the Sun* and toured with them in 1975. Here they are performing around that time. The footage is raw, but I think it is the only known video of this group in action (in a unique double-drummer lineup!):

<iframe width="560" height="315" src="https://www.youtube.com/embed/jec4nGsL2LA?si=NvcTpwxmSfzXAY68" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Everyone else in Elf besides Mark Nauseef joined up with guitarist Ritchie Blackmore to form Rainbow. They all went on to larger success and Nauseef stayed in the shadows.

Ritchie Blackmore was previously in Deep Purple with singer Ian Gillian. When that version of Deep Purple disbanded Ian Gillian started his own group (called The Ian Gillan Band, naturally!) and Mark Nauseef joined him. Confused yet?

The Ian Gillian Band released three records, including *Clear Air Turbulence* in 1977. Here is The Ian Gillian Band performing "Money Lender" from *Clear Air Turbulence*:

<iframe width="560" height="315" src="https://www.youtube.com/embed/GQTCbO0ZpJU?si=dBOcaiWeyIRANn-A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

After that group disbanded, Mark Nauseef became tired of playing in rock bands. He decided to immerse himself in much different styles like gamelan, Indian and Ghanaian music. 

By the late 1980s he joined forces with percussionist Leonice Shinneman and formed a group named Dark. Leonice Shinneman is primarily known for playing tabla. Here is a fine recent example showing his style and virtuosity:

<iframe width="560" height="315" src="https://www.youtube.com/embed/sVYBOZoYSE8?si=o0cGlOiCQFgY0RmI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Dark expanded their membership to include bassist Mark London Sims (who played in early versions of Nels Cline's trios appearing on the album *Silencer*) and Bosnian guitarist Miroslav Tadic. This group forms the basis for the record *Tamna Voda* (meaning "Dark Water" in Croatian) which is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

In addition to the core lineup *Tamna Voda* also includes guests David Torn (on guitar and loops fresh from recording *Secret of the Beehive* with David Sylvian) and L. Shankar (on double-violin...who played with Frank Zappa for a while in the late 70s). They don't play on every song on the album, nor do they ever appear on a song together thus they are essentially guests on the record and not full-time members of Dark.

Double-violin? Yes, double-violin. This was a custom instrument that L. Shankar plays that is basically a double-necked version of the instrument. Here is some killer footage of L. Shankar playing this unique instrument:

<iframe width="560" height="315" src="https://www.youtube.com/embed/CL_PeLSi2Ts?si=b_aqU0Yn5a77JqGM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

So, what does *Tamna Voda* sound like? It's an amazing mix of progressive rock, jazz fusion and Indian music. 

The album opens with "Trilok" which showcases the heavy drums and percussion that are featured throughout the record and includes L. Shankar going nuts during his solo:

<iframe width="560" height="315" src="https://www.youtube.com/embed/XHdhBpL9vSM?si=2yeicORCaNmxJUZN" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Another highlight is their cover of Jimi Hendrix's "Drifting" (originally on *The Cry of Love*) also featuring L. Shankar absolutely ripping it up:

<iframe width="560" height="315" src="https://www.youtube.com/embed/XcbFgz5IYwk?si=-4rHvXOwtQgbbQo1" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Since I don't want to leave out David Torn's contributions I'll also highlight "Smoke at Will" where you can definitely hear Torn's unique style of playing layered throughout:

<iframe width="560" height="315" src="https://www.youtube.com/embed/d_qOsX14L5s?si=LzCxo661jBRYqmcp" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

If you like these three songs, chances are you will enjoy the entire record. It has been a favorite of mine since a friend insisted I buy the CD in the early 2000s. 

It is another example of an obscurity that should have reached more ears at the time. Luckily, its availability on streaming services can help solve that problem!

{% iframe 'https://open.spotify.com/embed/album/6wP4vUgmWnxh6Ya1T0W1aT?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B0BNC6R3NY/?id=sZbX54J0CX&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/gb/album/tamna-voda/325268385' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/262718989' '100%' '96' %}