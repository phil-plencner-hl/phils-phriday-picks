---
title: Deep Purple - Fireball
author: Phil Plencner
date: 2020-02-21 11:56:45
tags:
- 70s rock
- classic rock
- deep purple
---
Over the past 3 day weekend I was succumbed to a classic rock station and heard an old standby Deep Purple song. This lead me to re-examine their early catalog. 

Deep Purple's most popular albums are defined by the "hits".....*Deep Purple In Rock* ("Speed King", "Child in Time") and of course the gargantuan *Machine Head* ("Highway Star", "Maybe I'm a Leo", "Space Truckin", "Smoke On The Water"). 

However, sandwiched in between those albums is the seemingly forgotten album *Fireball*. The title track alone is worth the price of admission with its crazed organ solo and hard driving drumming....most of the rest of the album follows suit, bringing THE ROCK & ROLL! 

Crank it up.

<iframe src="https://open.spotify.com/embed/album/3oitf8NRK5tSDhCRVcOewu" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>