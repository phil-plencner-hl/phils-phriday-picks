---
title: The Contortions - Buy
date: 2024-06-21 21:35:37
tags:
- james chance
- james white
- the contortions
- lydia lunch
- no wave
- brian eno
---

![](Buy.jpg)

Innovative no wave musician James Chance passed away this week at the age of 71. There are [tons of retrospectives and tributes online](https://www.nytimes.com/2024/06/20/arts/music/james-chance-dead.html), so I won't dive too much into it here.

I first became obsessed with James Chance back in 2003, when a [comprehensive box set called *Irresistible Impulse*](https://www.discogs.com/release/546687-James-Chance-Irresistible-Impulse) was released. I snapped it up when I saw it at Reckless Records and basically spent months pouring over all the music. It contains tons of great music, but with an approximate 4 hour run time, it’s a lot to take in (It's also woefully out of print). Instead I'll highlight the first record by James Chance's band The Contortions, *Buy*, which was included as part of the box set and is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

The Contortions were part of the original no wave scene in New York City in the late 70s. The scene was first documented by Brian Eno on the compilation called *No New York* (which includes The Contortions along with Teenage Jesus and the Jerks, Mars and DNA). It accurately showcases all four of those bands and is a great overall introduction to the genre and music scene.

*Buy*, which was released soon after *No New York*, was a tour-de-force for James Chance and the band. It is a relentless blur of funk rhythms, jagged guitar, stabbing organs and wailing free jazz inspired saxophone. 

Their live shows at the time were even more unhinged. Check out the original lineup of the band (the lineups fluctuated wildly in The Contortions short lifespan) playing at Max's Kansas City in 1979:

<iframe width="560" height="315" src="https://www.youtube.com/embed/ipeCg1DVEbw?si=OvZYUz-RKq8ozncG" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

If you like this, you'll love *Buy*. It was a hugely influential record for me showcasing that you didn't need to play like a virtuoso to play music...you just needed to bring a lot of passion, power and a unique frame of mind. Its still very inspirational for me even today. 

Rest In Peace James Chance!

You can [listen to The Contortions - *Buy* on your streaming platform of choice](https://songwhip.com/the-contortions/buy) (including Spotify, Amazon Music, Apple Music, Tidal, YouTube Music and more!).