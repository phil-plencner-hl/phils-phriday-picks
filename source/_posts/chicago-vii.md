---
title: Chicago - VII
author: Phil Plencner
date: 2020-06-03 22:38:06
tags:
- Chicago
- Chicago Transit Authority
- Terry Kath
- 70s rock
---

Today's pick is the seventh album by the rock group Chicago. 

At this point in Chicago's career they were wildly successful (they already had huge hits like "25 or 6 to 4", "Make Me Smile", "Saturday In The Park" etc) , but had not crossed over into the strictly syrupy ballads that became their bread n' butter in the 80s. 

They recently bought a secluded ranch out in Colorado and did most of their recording there. They decided to write a ton of instrumental jazz fusion songs, with the intent to have that be the whole album! This was a pretty crazy move for a popular band on a major label in 1974. Some band members argued against it, and they eventually relented by making it a double-album, with more pop-oriented material sprinkled in. 

They stuck the bulk of the awesome instrumental fusion at the beginning of the album (in fact there are no vocals until halfway through the 2nd side of the first record, and there are 2 drum solos!!). 

The pop material was also great, especially since it included the huge hit "Wishing You Were Here" that included background vocals by 3 of The Beach Boys (which lead to the amazing "Beachago" tour in 1975). 

It also included their Pointer Sisters collaboration "Skinny Boy" (which was also the name of keyboardist Robert Lamm's solo album that soon followed after this album as well). This is an amazing time capsule of a time when basically anything goes for the band Chicago, and is one of my favorite albums in their catalog.

<iframe src="https://open.spotify.com/embed/album/4O8dmYS0wfxvY56LJdp280" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>