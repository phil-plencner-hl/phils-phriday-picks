---
title: The Minutemen - Double Nickels On The Dime
date: 2021-04-02 08:27:25
tags:
- the minutemen
- d boon
- mike watt
- george hurley
- punk rock
- 80s rock
- alternative rock
---

The Minutemen's guitarist / vocalist D. Boon would have been 63 yesterday if he wasn't killed in a tragic car accident in 1985.

My favorite album by The Minutemen (actually, one of my favorite albums in any genre...by any artist) is Double Nickels On The Dime. A rare double-album without a single bad song. Even the cover songs are awesome! It is pretty much the crowning achievement of The Minutemen, SST Records, california punk rock, american music etc!

Words don't do this album justice or explain the impact it has had in my life. So I'll just let it speak for itself.

<iframe src="https://open.spotify.com/embed/album/5viZ5HyYtV0wafK7DoXmgF" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

PS - For some reason, when it was re-released on CD 3 of the songs were removed (maybe due to CD length constraints?). Spotify didn't add them back in, so I'm going to include them separately below, so you have the full experience!

<iframe src="https://open.spotify.com/embed/track/2w5EbceQX8XL9AZyM995tP" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

<iframe src="https://open.spotify.com/embed/track/1tah3FDBn6IPJYb5ktvYQL" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

<iframe src="https://open.spotify.com/embed/track/3wlOO8HSHBbM27UZ1SWJD1" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>



