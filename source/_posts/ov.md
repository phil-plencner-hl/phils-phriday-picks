---
title: Orthrelm - Ov
date: 2021-10-01 08:27:09
tags:
- orthrelm
- mick barr
- josh blair
- brutal prog
- progressive rock
- minimalism
- krallice
- flying luttenbachers
---

Mick Barr is one of my favorite guitar players. No doubt about it.

I can throw a dart at his discography (and it is a lengthy discography) and pick an incredible piece of music every time.

He is probably most renown at this point for playing guitar in the modern American black metal band Krallice. Krallice is great, but I tend to gravitate towards Mick's earlier music in contexts including Crom-Tech, Octis, Ocrilim, and especially Orthrelm.

A couple of the aforementioned names are solo projects. Either Mick Barr playing solo guitar pieces (Ocrilim) or playing with a drum machine (Octis). 

Orthrelm was a duo of Mick Barr and drummer Josh Blair. For most of their short existence they played hyperspeed songs that consisted of short thorough-composed non-repeating parts played in unison. 

Mathy, progressive, and completely over-the-top. To add to the mystery and complexity, all their album titles and songs consisted of almost unpronounceable words  and phrases. The album covers were super-detailed line drawings that were also hand-drawn by Barr.

Octis is basically Orthrelm with a drum machine instead of the live drummer. It's pretty great, but It's hard to beat two humans pushing themselves to limit playing the songs.

 Orthrelm reached their peak of this concept with their 2002 album *Asristr Vieldroix*. It was 99 songs of 5 to 10 seconds each. The whole thing clocks in at about 10 minutes. The first time I heard this album, my mind was basically turned into dust. 
 
*Asristr Vieldroix* is not available on Spotify, but you can [buy it directly from the band at Bandcamp](https://orthrelm.bandcamp.com/album/asristirveildrioxe).

At this time, they were a formidable live act that had to be seen and heard to be believed. I was lucky enough to catch them a few times during this period and probably still have not recovered.

Here is an example of what their live set sounded like in 2002. Amazing.

<iframe width="560" height="315" src="https://www.youtube.com/embed/1FPvNfPBAlM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Since they took this concept as far as it could go by this point, they decided to flip the script completely with 2005's *Ov*.

For *Ov* they went in the completely opposite direction. Instead of short bursts of songs, it consisted of one long 40 minute piece. There are still likely dozens of parts in the composition, but each phrase is repeated a ton of times putting the listener in an almost trance-like state of being. 

It's basically heavy metal minimalism. Like Steve Reich decided to compose for guitar and drums instead of an orchestra.

Even though they constructed and recorded *Ov* over a period of many sessions, they still performed the composition live on tour. Which is also pretty incredible to witness.

Unfortunately, Orthrelm disbanded soon after the *Ov* tour. Thankfully, we still have this incredible recorded document of their astounding achievement.

<iframe src="https://open.spotify.com/embed/album/7kF5sVU0fcRTDS2mYnXOSv" width="100%" height="380" frameBorder="0" allowtransparency="true" allow="encrypted-media"></iframe>