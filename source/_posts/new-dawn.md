---
title: Marshall Allen - New Dawn
date: 2025-02-21 21:39:30
tags:
- marshall allen
- sun ra
- sun ra arkestra
- knoel scott 
- shabaka hutchings
- space jazz
- free jazz
- avant garde 
---

![](NewDawn.jpg)

There are many times that a Phil's Phriday Pick revolves around the tragic death of a musician. This week I'm flipping the script on that trend. 

Today, I'm going to celebrate the life of Marshall Allen, who turned 100 years old last year! Marshall Allen is best known as being a (very) longtime member of Sun Ra's Arkestra. 

Regular Phil's Phriday Pick readers have come to realize that I am a gigantic Sun Ra fanatic. This is illustrated by [my large (yet very incomplete) collection of Sun Ra vinyl records](https://www.instagram.com/reel/C5ZQml3rwkj/?utm_source=ig_web_copy_link). You could say, I dive pretty deep into the Sun Ra catalog. 

Marshall Allen first joined Sun Ra's band back in the late 1950s. Along with fellow saxophonist John Gilmore he was practically there from the beginning. One of his earliest recordings with Sun Ra was on *Jazz In Silhouette*, which came out in 1959. The record was more straighahead jazz than the wild avant-garde excursions that lay ahead, but it still contained some pretty outsider music. A good example is "Ancient Aiethopia":

<iframe width="560" height="315" src="https://www.youtube.com/embed/jy59x48KeI4?si=jzbZvd-_E8qaCaUb" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

It's easy to fall down a giant rabbit hole of classic Sun Ra live footage featuring Marshall Allen on the internet. I'll save you the trouble for now by just highlighting this incredible performance recorded for French TV in 1969:

<iframe width="560" height="315" src="https://www.youtube.com/embed/HKmzFVCgzm0?si=3U6ca5ZU3V7PKZ2x" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Fast-forward to the 1990s. When Sun Ra passed away in 1993, the band, billed as the Sun Ra Arkestra, continued under the direction of John Gilmore with Marshall Allen in tow. John Gilmore passed away a couple years later in 1995 and Marshall Allen took over as the leader. He has been leading the band for the next 30 years and counting.

You might think that Marshall Allen would start slowing down once he reached his 90s, but you would be very mistaken. 

Here he is performing "Space Is the Place" in 2018 when he was "only" 93 years old:

<iframe width="560" height="315" src="https://www.youtube.com/embed/RZL8B7JJY3I?si=Ch0Gae7FoQ9lWBCD" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

The next year he was still bringing the fire:

<iframe width="560" height="315" src="https://www.youtube.com/embed/DrhvkmZxdQM?si=lzaJO_7BZm6uwVX3" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

He certainly hasn't lost his fastball!

Marshall Allen has been living in the same house in Philadelphia that Sun Ra and the band lived and rehearsed in since 1968! Here is an interview and tour of the house Marshall Allen participated in when he was 99 years old in 2023:

<iframe width="560" height="315" src="https://www.youtube.com/embed/mw-zE7zGEkM?si=xgGOdoKt5xqFlg47" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

In May of 2024 Marshall Allen turned 100 years old. He has been around for a century but has no signs of slowing down. In fact, two days after he reached that milestone he recorded a new studio album. Oddly enough it is the first record credited to him solo.  The album is called *New Dawn* and is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

*New Dawn* continues the traditions of Sun Ra's music, even though Marshall Allen composed all the material. It is equal parts crazed and beautiful. The record was produced by Knoel Scott and also features him playing saxophone and drums. Knoel Scott is also a long-time member of Sun Ra's bands.

The album features a large string section as well. Seven violin players, two viola players and a cello round out the regular jazz ensemble giving it a full, lush sound.

The title track of the record features Nenah Cherry on vocals. Here is the official video of the song:

<iframe width="560" height="315" src="https://www.youtube.com/embed/W_-XTiIyXQc?si=n4LHYm6skP0aA3Tz" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Another highlight of *New Dawn* is the swinging jump blues of "Are You Ready". Marshall Allen can still groove and swing!

<iframe width="560" height="315" src="https://www.youtube.com/embed/KMCpwgr-xfY?si=PDIT8rIl-8GpOdEg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

One of Marshall Allen's most famous compositions is "Angels and Demons at Play". It was part of Sun Ra's repertoire for most of the Arkestra's existence. It has been recorded again for *New Dawn* and still sounds great. Compare an early version of it recorded in 1960 with the version recorded 65 years later on *New Dawn*:

<iframe width="560" height="315" src="https://www.youtube.com/embed/9DY7ohSkMvA?si=lkH5muCPf1B-veSx" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/ppdr5klIOaM?si=mCbpFw58DSzRiwCM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

It's also still a live staple for the Arkestra. Here's a pretty incredible version of it, which also includes guest musician Shabaka Hutchings (of The Comet Is Coming fame):

<iframe width="560" height="315" src="https://www.youtube.com/embed/_LjlFeO27xg?si=xrox2GjL3fJssmPW" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

*New Dawn* is an incredible document of a 100 year old musician that sounds like he could live another 100 years. While that might be an exaggeration, I hope he still has many years of living filled with music ahead of him. He's still performing and still isn't slowing down. Want proof? Here's footage of his 100th birthday celebration show here in Baltimore. I don't think anything else needs to be said. Long live Marshall Allen!

<iframe width="560" height="315" src="https://www.youtube.com/embed/ccVSSwLrKDY?si=wmYwenIUGUFZaPwK" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

{% iframe 'https://open.spotify.com/embed/album/0BOZmnGO69A24yXIJ6lOpd?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B0DPXTM52X/?id=SY98kt6CLb&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/ng/album/new-dawn/1784995969' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/404647948' '100%' '96' %}
