---
title: Modern Christmas 2019
author: Phil Plencner
date: 2019-12-06 13:49:14
tags:
- christmas music
---
It's December! 

With everyone's Holiday schedules starting to ramp up, I thought it would be a great time to drop my Christmas Playlist for 2019 here. 

Every year since 2008 (except for 2009 when I didn't think it was going to be an annual thing and only a one-off) I have made holiday mix CDs /  playlists for friends and family and the tradition continues this year in it's 11th edition! 

I hope you enjoy this year's focus on modern holiday hits. I will also include in a thread every previous year's playlist along with a massive 13 hour megamix of all the playlists combined! 

Put that one on shuffle and Christmas will be here before you know it. 

Happy Holiday's everyone!

- [13 HOUR MEGA MIX!](https://open.spotify.com/playlist/5NqMi2wSRDRXP5L4G5McRW?si=ZwmbuH3iTkyzRb3IihO9UA)
- [It's Christmas Again? 2018](https://open.spotify.com/playlist/0KVsnggEkc7MftYJIRmmZl?si=cIhjjkEySeKKOC4g5AyCQA)
- [New Christmas Classics 2017](https://open.spotify.com/playlist/73rcF9IZMKENn9xoZ6aLE2?si=esHyVl4jTJOvE8950mxAcw)
- [Merry Christmas 2016](https://open.spotify.com/playlist/15Q28CMAlNDhnxvpdzPril?si=q7XgWr3hRRiyDtsDDxBD_Q)
- [Festive 2015](https://open.spotify.com/playlist/32yvyJJiGGcxAJVwf4VyOZ?si=HqEeppCZSvmFLVtK-fIB7w)
- [80s Christmas 2014](https://open.spotify.com/playlist/4YEulevL9dKg0uXmXgUfML?si=KojXWrunRk69fNoxHSNf_g)
- [Jazzy Christmas 2013](https://open.spotify.com/playlist/1WQE7qzJugPui8TsK4EngF?si=a_PCH1vJTBqLMSh9a6ik2A)
- [Sounds of Christmas 2012](https://open.spotify.com/playlist/13KoC1HiL5NOM153QjrJk8?si=2gKKSXoJQ0OrSx21LiIBLg)
- [cRaZy ChRiStMaS 2011](https://open.spotify.com/playlist/5t2qUwrQHDiMpakUa2j7KM?si=a_zUuWcOSAmUWcrZZKxeTQ)
- [Wacky Christmas 2010](https://open.spotify.com/playlist/3KPGMZ8X8J1MDZa4NM6XIE?si=pUxfVmnSQGKZuNlDsUSXjA)
- [Ultimate Christmas 2008](https://open.spotify.com/playlist/3AylbZDuVKYQH7g2ik5ciw?si=jtjSA8thTLyrbjf1AAkGYQ)

<iframe src="https://open.spotify.com/embed/playlist/1NiCJLrp4eX6cGvSwYBd7I" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

