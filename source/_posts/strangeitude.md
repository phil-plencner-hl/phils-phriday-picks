---
title: Ozric Tentacles - Strangeitude
date: 2021-02-26 10:28:15
tags:
- ozric tentacles
- space rock
- 90s rock
- progressive rock
---

Ozric Tentacles are a pretty crazy band. They are essentially a modern-day space rock band...but unlike many of their peers their jams are usually highly melodic and not very meandering. The musicians themselves (especially mastermind Ed Wynne) are highly accomplished and technical players. 

Their first few albums were cassette only, and distributed at makeshift parties / festivals in the U.K. that they would hold in the wilderness. Eventually the Ozric Tentacles garnered further fame, and their albums reached a wider audience. 

They first grabbed my attention with the release of *Stangeitude* in 1991. Their albums were distributed to the United States and were much easier to obtain by this point. To me, this is their peak period (also included in this period is *Arboresence* in 1993 and *Jurassic Shift* in 1994). 

Part of what makes this era their peak in my opinion was the personnel. Drummer Mervin Pepler had a really forceful attack, yet a smooth grooviness to his playing. Joie Hinton was otherworldly with his approach to playing the synthesizers. By 1995, they had left the band to form the dance / techno group Eat Static and while Ozric Tentacles remained good (in fact I saw them perform a couple of times in the early 2000s, and they were positively mind-expanding experiences) they were never really the same since then.

White Rhino Tea comes out of the gate swinging and sets the mood of the album. They follow it up with the inventive Sploosh! the album continues to grow and build to the spacy climax of Weirditude. 

Expand your horizons and take a visit to the Bizarre Bazaar with Ozric Tentacles' *Strangitude*!

<iframe src="https://open.spotify.com/embed/album/1O8LiHN6IizBkjNv1kqnKg" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>