---
title: Alice Cooper - Zipper Catches Skin
date: 2022-03-25 13:46:38
tags:
- alice cooper
- steve vai
- yngwie malmsteen
- 70s rock
- 80s rock
- new wave
- punk rock
- classic rock
---

The common consensus among classic rock fans is that Alice Cooper had a lapse of time in the early 1980s where he didn't do anything of note and it is considered that 1986's Kip Winger-featured *Constrictor* is a comeback album. I dispute this notion. 

In that time frame I believe he put out some of his more unique and challenging albums. Much of them inspired by new wave and punk rock...basically abandoning his classic hard rock sound. Particularly the trifecta of albums Alice Cooper refers to as his "blackout" albums: *Special Forces*, *Zipper Catches Skin* and *DaDa*. They are called the "blackout" albums because Alice Cooper has no recollection of writing or recording them because of his cocaine abuse during that time!

Of these three albums, I believe the middle album, *Zipper Catches Skin* is the strongest and is today's pick!

Like I previously alluded to, the album is basically a combination of classic heavy metal with new wave. Sort of if Grand Funk Railroad and The Cars joined forces to make music. It is simultaneously amazing and ridiculous. The Steve Miller Band was doing something similar around this time with *Italian X Rays*, but I think Alice Cooper did the style better than his contemporaries.

The album starts off with a song written from the perspective of Zorro (complete with castanets!) and just gets more insane from there.

For example, there's the Steven Spielberg / E.T. tribute song "No Baloney Homosapiens" which really lives up to the bizarre title and theme.  Additionally a kind of new wave power ballad "Adaptable" is about as close as Alice ever got to a love song. "Remarkably Insincere" is the polar opposite: a song that explains how he lies to his significant other with lyrics that border on Weird Al Yankovic levels of ridiculousness. 

The only thing close to a "hit" on the album was "I Am The Future" which was included in the pre-Family-Ties Michael J. Fox movie *Class of 1984*. It probably is one of the more straightforward songs on the album.

Finally, the album closes with "I'm Alive (That Was the Day My Dead Pet Returned to Save My Life)" in which the title kind of speaks for itself.

Alice Cooper never toured behind the album and none of the songs have ever been performed live, so this album truly lives on as some sort of awesomely obscure time capsule in his discography.

One other fun bit of trivia about this album is that the drummer is Jan Uvena. He only appeared on this one Alice Cooper album, and joined the shred-metal band Alcatrazz (who's membership included Yngwie Malmsteen and Steve Vai) soon afterwards.

Jan Uvena eventually left the music industry and now [manages a U.S. Cellular store in Walpole, MA](https://www.sentinelsource.com/news/local/keene-musicians-golden-past-of-touring-with-alice-cooper-alcatrazz/article_9893efad-e72c-557f-ba3d-b9f5dda82a99.html). Perhaps you have bought a phone from him.

<iframe src="https://open.spotify.com/embed/album/5H8v7Fh0ePzOBsscqLZqmB?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

<iframe id='AmazonMusicEmbedB00122PK9Q' src='https://music.amazon.com/embed/B00122PK9Q/?id=a4BojQtwta&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/zipper-catches-skin/215637781"></iframe>

<div class="tidal-embed" style="position:relative;padding-bottom:100%;height:0;overflow:hidden;max-width:100%"><iframe src="https://embed.tidal.com/albums/702046?layout=gridify" allowfullscreen="allowfullscreen" frameborder="0" style="position:absolute;top:0;left:0;width:100%;height:1px;min-height:100%;margin:0 auto"></iframe></div>
