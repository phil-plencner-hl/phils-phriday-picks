---
title: Camel - A Live Record
date: 2024-09-13 20:28:10
tags:
- camel
- caravan
- king crimson
- progressive rock
- 70s rock
- british rock
---

![](ALiveRecord.jpg)

The band Camel was never one of the bigger names in England, but they are highly influential. They originated what came to be known as "Neo-Prog". Bands that came after them such as Marillion, Beardfish and Porcupine Tree owe a lot to Camel for laying the foundation.

If you asked someone to list British progressive rock bands from the 1970s, groups like Yes, Gentle Giant, Genesis or King Crimson would likely be chosen...but in my opinion Camel should be right up there with those bands.

One of their biggest masterpieces (both in sales and length / scope) was *The Snow Goose* from 1975. That record is a nearly 45 minute instrumental suite that was inspired by Paul Gallico's book of the same name. In fact, Gallico sued the band and forced them to make the actual title of the record *Music Inspired by The Snow Goose* but people pretty much just call it by the original simpler name anyways.

*The Snow Goose* consists of mostly composed music with very little improvisation so that it can properly follow the tale of the original book. The songs are catchy and melodic with just enough complexity to make it interesting. The entire song suite flows well and you can truly relate it to the story told by Gallico. 

Soon after the release of the album, Camel played a huge show at The Royal Albert Hall in London performing the complete suite accompanied by the London Symphony Orchestra. Luckily the entire performance was recorded and was later released in 1978 as part of a live album they aptly called *A Live Record*, which is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

To give you a sense of what the music is like in a live setting, here is Camel playing "The Snow Goose / Friendship / Rhayader Goes to Town" from *The Snow Goose* on BBC TV in 1975:

<iframe width="560" height="315" src="https://www.youtube.com/embed/JJiEOZ5vujM?si=pre5zkGl_0iDqPTE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

After *The Snow Goose* Camel released another concept album called *Moonmadness* in 1976. Four of the songs were based on the personality of each band member! Pretty wild. 

Sadly, the original lineup didn't last. Bass player Doug Ferguson left after *Moonmadness* because he thought they were getting too "jazzy". He was replaced by Richard Sinclair who was previously a member of bands associated with the Canterbury progressive rock scene (Caravan, Hatfield and the North). Additionally, Mel Collins (of King Crimson fame) joined on saxophone. Adding these people certainly didn't dispel Ferguson's notion that they were getting more jazzy.

The next studio album was *Rain Dances* in 1977. After the tour for *Rain Dances* is when they released *A Live Record*. 

What makes *A Live Record* interesting and unique is that not only does it include the 1975 performance of *The Snow Goose* it also features music from the *Moonmadness* and *Rain Dances* tours. So, it gives the listener a wide-ranging, comprehensive overview of the band's live concerts. This is great for both neophytes and obsessive fanatics alike.

*A Live Record* opens with "Never Let Go" from their self-titled debut album, but played by the new lineup. Here is another version from it from the same era:

<iframe width="560" height="315" src="https://www.youtube.com/embed/2WAt4EEZO-g?si=k40ziNSyJ0D0snqv" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Another highlight is "Lunar Sea" originally from *Moonmadness*. Here is a live version that is similar:

<iframe width="560" height="315" src="https://www.youtube.com/embed/_Gu7mJvhjBA?si=WYSEsCVWUbLAtQhx" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

*The Snow Goose* suite was not the only lengthy composition featured on *A Live Record*. There is a nearly 15 minute rendition of "Lady Fantasy" (originally on *Mirage*) that sounds incredible as well. While this is not the same recording, it still showcases how great the song is and how impeccably the performed it:

<iframe width="560" height="315" src="https://www.youtube.com/embed/yrt50p29mAY?si=GrRoa6Q0-XksEXuO" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

In 2002 *A Live Record* was re-releaed with a bunch of songs not on the original album. Luckily this is the version that is available on streaming services so we can enjoy the deluxe version of the record. "Metrognome" and "Chord Change" are especially welcome additions to the original album. 

Camel plodded along in the 1980s but they never reached the height of their early records. If you enjoy *A Live Record*, I encourage you to work your way through all of those early albums. 

{% iframe 'https://open.spotify.com/embed/album/4NLWM4hSjXnB1oA6HkIGjc?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B01K91FUIG/?id=svWiBkc2Mq&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/a-live-record/1452873022' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/576920' '100%' '96' %}