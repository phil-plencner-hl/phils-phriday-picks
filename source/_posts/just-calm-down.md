---
title: Panic Problem - Just Calm Down
author: Phil Plencner
date: 2020-03-19 16:13:52
tags:
- panic problem
- punk rock
- pop punk
- alternative rock
---
My friend Neil’s pop punk band recently put out a new album aptly called “just calm down”. Worth a spin or two...

<iframe src="https://open.spotify.com/embed/album/1022rQ1FJSDrDyUvwiuOEc" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>