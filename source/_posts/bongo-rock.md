---
title: The Incredible Bongo Band - Bongo Rock
date: 2021-01-14 10:08:54
tags:
- incredible bongo band
- 70s rock
- jazz
- funk
- michael viner
---

I recently watched an incredible documentary on Netflix called ["Sample This"](https://www.netflix.com/title/70290909). It recounts the convoluted tale of a studio group called The Incredible Bongo Band. They are best known for their cover of a song by surf rock band The Shadows called "Apache". The movie itself kind of spins it into the more modern day sampling of "Apache" by early hip-hop pioneers such as Kool-Herc and Grandmaster Flash...but it really is an all-encompassing history of the band itself. If you are unfamiliar with the story of The Incredible Bongo Band it is well worth watching.

To quickly recap: The Incredible Bongo Band was a studio-only band idea by producer Michael Viner. He basically wanted to bring the most incredible studio musicians together from L.A. to make instrumental hit records. It was never intended to be a real band. Photographs of the group during that time were basically people who looked good and not people who actually played on the recordings. When the band rarely toured, they were also not the people who recorded the songs but hired guns for the tour. 

The movie even includes the side story about prolific drummer Jim Gordon (who played with Beach Boys, Derek and the Dominos, Frank Zappa, Joe Cocker etc etc etc) who allegedly was part of the "Apache" recording sessions who went on to notoriety for going into a schizophrenic rage and killing his parents. 

Watch the documentary if you haven't already!

This brings us to today's pick. The original studio album by The Incredible Bongo Band called "Bongo Rock". It includes "Apache" as well as minor hits like "Let There Be Drums" and their cover of Iron Butterfly's "In A Gadda Da Vida". Its still a totally over-the-top, fun, catchy record even almost 50 years after it was originally released.

<iframe src="https://open.spotify.com/embed/album/3xXsJL1bNBWU1jbQDxLJdf" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>