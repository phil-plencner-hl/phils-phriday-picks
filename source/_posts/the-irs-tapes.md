---
title: Willie Nelson - The IRS Tapes -  Who'll Buy My Memories?
date: 2023-04-28 13:33:50
tags:
- willie nelson
- country music
- johnny paycheck
- paul simon
- outlaw country
---

![](TheIRSTapes.jpg)

Willie Nelson will be [celebrating his 90th birthday this weekend](https://blackbirdpresents.com/concert/willie-nelson-90/)! It is truly amazing that not only has Willie lived this long, but he continues to record and perform at pretty much the same pace as he has most of his life. Along with the star-studded 90th birthday concert, he is also going on tour (playing over 30 shows headlining the "Outlaw Music Festival" with a rotating set of support acts). Last month, he also put out his 73rd album *I Don't Know A Thing About Love: The Songs Of Harlan Howard*! 

Things weren't always easy for Willie Nelson. In November of 1990, the I.R.S. raided his Texas ranch and seized his assets, claiming he owed over $16 million in taxes.

How did this happen? Apparently, Price Waterhouse, who was managing his money neglected to pay his taxes over a period of many years. They also made many bad investments on his behalf. Willie Nelson later sued them. [Texas Monthly wrote a pretty detailed article about the situation in May 1991](https://www.texasmonthly.com/arts-entertainment/poor-willie/).

The I.R.S. attempted to auctioned off Willie's Austin ranch, but after several attempts it [only sold for the minimum bid of $203,840](https://www.nytimes.com/1991/01/30/us/irs-sells-singer-s-ranch.html)...a far cry from the millions of dollars owed and well below market value for the sprawling 44-acre ranch. It was bought by a huge Willie Nelson fan who later sold it back to Willie, so the joke was on the I.R.S. in the end.

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

What did Willie Nelson do to get out of this situation? Well, at the time he had his own theater in Branson, Missouri (similar to what [many other country stars had at the time](https://www.washingtonpost.com/archive/lifestyle/style/1992/12/20/the-land-that-nashville-forgot/5aeed3d5-8477-460f-8236-5db3f3a10087/)) and he played a bunch of shows there. Check out this T-Shirt from the theater:

![](OzarkTheater.jpg)

Farm Aid was also a popular concert, but that didn't help him because the money raised there was already earmarked to save farms from forclosure. Nevertheless, he still performed. Here he is playing "Always On My Mind" at Farm Aid in 1992:

<iframe width="560" height="315" src="https://www.youtube.com/embed/ih2Syc2W35w" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Ultimately, Willie Nelson recorded a stripped-down album of songs he wrote (it only features himself and his trusty guitar Trigger) called *The I.R.S. Tapes: Who'll Buy My Memories?* specifically to raise funds to pay off the tax debt. *The I.R.S. Tapes* is today's pick!

Originally it wasn't even sold in stores. It was advertised on television, and you had to call a phone number to order it. Here are a couple of those commercials, including one with this buddy (and fellow Highwayman) Kris Kristofferson going to bat for him:

<iframe width="560" height="315" src="https://www.youtube.com/embed/v64D0EGlVzU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/0H1Np6CWCfA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

They also sold t-shirts, which has the album cover on it, but in an uncensored state where you can clearly read the message on Willie's shirt in the photo:

![](IRSShirt.jpg)

The album sold for $19.95 and the money broke down like this: $9.95 went to the telemarketing company promoting the album, $1.60 went to album expenses, $2.49 went to his record label Sony.  The remaining $6 went to Willie: $3 to pay down the tax debt, $1 to pay for his lawsuit against Price Waterhouse, and $2 in sales tax.

The New York Times [wrote an extensive article](https://www.nytimes.com/1991/09/02/business/willie-nelson-hopes-for-a-hit-so-does-the-irs.html) about it at the time, which has all the gory details.

After a year or so, the album got a wider release in stores. All told, they ended up raising $6 million specifically for the I.R.S. from the album alone. Willie Nelson raised more money from playing shows and eventually [settled in early 1993](https://apnews.com/article/e2f8bee7826ca9607de3d0fde8a920c8).

So how is the album itself? It is uniformly excellent. Willie's singing and playing are in fine form, and the intimate nature of the recording helps to make it one of his essential albums from this era. He didn't even record any of his biggest hits for the record, choosing lesser-known songs which really speaks to how much care Willie put into what was basically a cash-in record. He could have just played "Crazy", "Always On My Mind" and "On The Road Again" but really made a more hardcore-fan-focused record. We're better off with that decision today.

Willie Nelson, of course, bounced back from his tax debacle. In fact, throughout the 2000s I believe he put out some of his strongest albums. As a bonus I'll share my *Modern Willie* playlist I made a few years ago that focus on highlights from 2009's *Lost Highway* through 2019's *Ride My Way Back Home*.

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/playlist/1L23ryhHic4wDL1iZ06jW2?utm_source=generator" width="100%" height="352" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

Happy Birthday, Willie! Here's to many more years of music (and no more years of tax trouble)!

{% iframe 'https://open.spotify.com/embed/album/4rtPO0LoyHpEgi9bkf7ms4?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B01BXF5ZGO/?id=b2bnZlxqgG&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/the-irs-tapes-wholl-buy-my-memories/1085316945' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/57425177' '100%' '96' %}
