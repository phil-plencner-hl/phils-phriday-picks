---
title: Cynic - Focus
author: Phil Plencner
date: 2020-01-27 12:05:33
tags:
- cynic
- death metal
- jazz fusion
- heavy metal
- sean reinert
---
Another special Monday morning edition of Phil's Phriday Picks....because another huge drumming influence of mine passed away over the weekend. 

Sean Reinert, best known for 2 albums that changed the game for heavy metal drumming. Death's pioneering *"Human"* album is probably the one that gets the most attention, but Cynic's *"Focus"* is the complete game-changer. 

27 years after its release, there is still nothing that sounds quite like it. A perfect mix of death metal and jazz fusion....including a mixture of vocal styles (vocoder!) The drumming still boggles the mind today. 

For those that don't know, now you know.

Also Hank Shteamer explains why its important better than me [over at Rolling Stone](https://www.rollingstone.com/music/music-features/sean-reinert-drummer-tribute-cynic-death-943148/), much like he did for Neil Peart.

<iframe src="https://open.spotify.com/embed/album/549e0F39N284sxDE7sqGi3" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>