---
title: christmas-2023
date: 2023-12-08 14:04:44
tags:
---

My Christmas mix tradition has reached a milestone: 15 years! This year's mix is on the jazzy side. A healthy selection of instrumental classics mixed with my usual brand of comedy and weirdness plus some highlights from this year's new pop Christmas albums. It runs the gamut.

Previous mixes are also listed below...including the nearly 19 hour, 335 song megamix of every previous year! Put that thing on random play and your holiday party guests will not believe you're playing banger after banger!

The schedule for Phil's Phriday Picks the next few weeks might be spotty. I'll be undergoing a medicial procedure Monday and it's unclear when I'll be able to blog in the near future. I hope to present the latest (and likely final) Pandemic Status playlist as well as my list of favorite albums of 2023. 

Also note that because TinyLetter is shutting down, I am [migrating the email newsletter to Substack](https://philsphridaypicks.substack.com/). The archives there are kind of wonky right now, but will hopefully be cleaned up soon. I might also start featuring some special content for just paid subscribers...stay tuned! Who knows what 2024 will bring?

In the meantime, Merry Christmas!

{% iframe 'https://open.spotify.com/embed/playlist/5bkhmh2jk3g5ian28iqm2K?utm_source=generator' '100%' '352' %}

- [19  HOUR MEGA MIX!](https://open.spotify.com/playlist/5NqMi2wSRDRXP5L4G5McRW?si=ZwmbuH3iTkyzRb3IihO9UA)
- [Christmas 2022](https://open.spotify.com/playlist/0xcrzSFIKysMMFi6ogGxTG?si=87f9ac0ccf42403a)
- [Christmas 2021](https://open.spotify.com/playlist/6ZRGinlvxFJYSqqLR60mxj?si=d273d0565b274585)
- [COVID Christmas 2020](https://open.spotify.com/playlist/6ZEzVZLOLb7jEWAWwFMOeR?si=45ae978fa6f549c4)
- [Modern Christmas 2019](https://open.spotify.com/playlist/1NiCJLrp4eX6cGvSwYBd7I?si=EWPGwVPVR9qe4WWVUyUQaw)
- [It's Christmas Again? 2018](https://open.spotify.com/playlist/0KVsnggEkc7MftYJIRmmZl?si=cIhjjkEySeKKOC4g5AyCQA)
- [New Christmas Classics 2017](https://open.spotify.com/playlist/73rcF9IZMKENn9xoZ6aLE2?si=esHyVl4jTJOvE8950mxAcw)
- [Merry Christmas 2016](https://open.spotify.com/playlist/15Q28CMAlNDhnxvpdzPril?si=q7XgWr3hRRiyDtsDDxBD_Q)
- [Festive 2015](https://open.spotify.com/playlist/32yvyJJiGGcxAJVwf4VyOZ?si=HqEeppCZSvmFLVtK-fIB7w)
- [80s Christmas 2014](https://open.spotify.com/playlist/4YEulevL9dKg0uXmXgUfML?si=KojXWrunRk69fNoxHSNf_g)
- [Jazzy Christmas 2013](https://open.spotify.com/playlist/1WQE7qzJugPui8TsK4EngF?si=a_PCH1vJTBqLMSh9a6ik2A)
- [Sounds of Christmas 2012](https://open.spotify.com/playlist/13KoC1HiL5NOM153QjrJk8?si=2gKKSXoJQ0OrSx21LiIBLg)
- [cRaZy ChRiStMaS 2011](https://open.spotify.com/playlist/5t2qUwrQHDiMpakUa2j7KM?si=a_zUuWcOSAmUWcrZZKxeTQ)
- [Wacky Christmas 2010](https://open.spotify.com/playlist/3KPGMZ8X8J1MDZa4NM6XIE?si=pUxfVmnSQGKZuNlDsUSXjA)
- [Ultimate Christmas 2008](https://open.spotify.com/playlist/3AylbZDuVKYQH7g2ik5ciw?si=jtjSA8thTLyrbjf1AAkGYQ)
