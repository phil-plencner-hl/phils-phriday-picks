---
title: McDonald And Giles - McDonald And Giles
date: 2022-02-11 08:19:59
tags:
- ian mcdonald
- michael giles
- king crimson
- progressive rock
- 70s rock
- steve winwood
---

Today it was announced that Ian McDonald, a founding member of King Crimson and Foreigner, [passed away from cancer at the age of 75](https://www.nme.com/news/music/ian-mcdonald-king-crimson-and-foreigner-co-founder-dead-at-75-3159387).

He only appeared on one King Crimson album. Their debut: *In The Court Of The Crimson King*. He appeared on the Foreigner trifecta of *Foreigner*, *Double Vision* and *Head Games* but was fired from the group before the massive success of *4*.

His performances on all those recordings will and should be praised (because appearing on even one of those albums would make you beyond reproach).

Here is a tantalizing, yet extremely short, clip of King Crimson with Ian McDonald performing live in 1969 when they opened for the Rolling Stones...complete with footage of confused hippies.

<iframe width="560" height="315" src="https://www.youtube.com/embed/MM_G0IRLEx4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

In between these two groups (actually, right after he left King Crimson) he joined Michael Giles,the drummer on *In The Court Of The Crimson King* who also recently quit the band, for a new project. They named themselves McDonald and Giles and released only one self-titled album in 1970. This is today's pick.

They are joined on the album by Michael's brother, Peter, who also had ties to King Crimson/Robert Fripp as a member of pre-KC group Giles, Giles & Fripp. As an aside, if you haven't heard [*The Cheerful Insanity of Giles, Giles & Fripp*](https://www.instagram.com/p/CDM51KtJ-jW/?utm_source=ig_web_copy_link) you're really missing out!

Steve Winwood also contributes piano and organ to the album. Traffic was recording *John Barleycorn Must Die* in the same studio at the time.

Many of the songs were written with the intent of being included on the next King Crimson album, so there are definitely shades of *In The Court Of The Crimson King* and *In The Wake Of Poseidon* peppered throughout (in fact "Flight of Ibis" contains many similarities to the King Crimson song "Cadence and Cascade"). If you enjoy early King Crimson then this album will certainly be up your alley.

The centerpiece of the album is the epic, 21-minute "Birdman", broken up into 6 movements,  which takes up the entire second side of the record. It is a astonishing piece of music that escalates into a grand conclusion. An underrated and unsung composition that truly deserves more popularity and recognition. 

<iframe src="https://open.spotify.com/embed/album/1uVDOdHQm8nGI74eo4w2KU?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

<iframe id='AmazonMusicEmbedB09JXY7RSD' src='https://music.amazon.com/embed/B09JXY7RSD/?id=vra8z2mFRA&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/mcdonald-and-giles/1591315770"></iframe>


