---
title: Carl Stalling - The Carl Stalling Project
date: 2022-10-07 14:32:20
tags:
- carl stalling
- warner brothers cartoons
- roadrunner and coyote
- disney
- film soundtrack
---

![](CarlStallingProject.jpg)

When I was a little kid, I was obsessed with Roadrunner and Wile E. Coyote cartoons. Not just of the cartoons themselves (which I found hilarious) but also the crazed music.

What was this off-the-wall symphonic music? Who comes up with this and who performs it? I loved it so much, when classic Warner Brothers Cartoons played on TV (Especially the Roadrunner and Wile E. Coyote ones) I would have a little tape recorder handy to make my own tapes of just the soundtrack to play back whenever I wanted (My life as an adolescent music pirate)!

Here is the first Roadrunner cartoon, "Fast and Furry-ous" from 1949:

<iframe width="560" height="315" src="https://www.youtube.com/embed/0rniHAgGd58" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

As it turns out, it was composed and conducted by Carl Stalling. He worked as the musical director of Warner Brothers Cartoons from 1936 until he retired in 1958. In that time, he wrote and conducted scores for over 600 cartoons!

Before he worked for Warner Brothers, he was an organist at a movie theater who was discovered by Walt Disney. Disney hired him first to spearhead the Silly Symphonies series. Here is the first one he made, "The Skeleton Dance", from 1929:

<iframe width="560" height="315" src="https://www.youtube.com/embed/vOGhAV-84iI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

At Warner Brothers perhaps his most famous work was "Putty Tat Trouble", which was a Tweety and Sylvester short:

<iframe width="560" height="315" src="https://www.youtube.com/embed/_F8XF_KB_Es" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

In the early 1990s, Hal Willner compiled and produced two compilation albums featuring the "best of" Carl Stalling's Warner Brothers work. The first volume of *The Carl Stalling Project* is today's pick!

It is a fascinating glimpse. Along with full scores, there are also rehearsal recordings which give the listener a behind-the-scenes peek at how the symphony worked through the more difficult material. I was hoping at the time that there would be many more volumes of the series (after all, there are over 600 cartoons!) but alas only 2 volumes were ever released. Sadly, no compilation of Roadrunner scores were officially released (although I did acquire a bootleg CD of "The Complete Roadrunner Scores" from a perhaps nefarious source somewhere along the way).

After Carl Stalling retired in 1958, Milt Franklyn took over the musical director's chair and wrote in a similar style until 1962.

I hope you enjoy and feel nostalgia from these innovative recordings of classic cartoons.

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/album/1dtdWVUHSEOYR6TMEPAjzd?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

<iframe id='AmazonMusicEmbedB003WNZBLU' src='https://music.amazon.com/embed/B003WNZBLU/?id=6DwnlLcT2H&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *; clipboard-write" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/the-carl-stalling-project-music-from-warner-bros/272506574"></iframe>

<iframe src="https://embed.tidal.com/albums/10920746" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>