---
title: The Residents - Duck Stab / Buster & Glen
date: 2021-11-19 10:02:31
tags:
- the residents
- snakefinger
- 70s rock
- avant-garde
- progressive rock
- primus
- 90s rock
---

Back in 1990, I was a big fan of the band Primus. The had just put out *Frizzle Fry* and were starting to gain notoriety in alternative rock circles. 

They were also fans of The Residents.

I discovered this when I grabbed a free promotional CD from a local Tower Records around that time called [*On The Ninties Tip*](https://www.discogs.com/release/2916673-Various-On-The-Nineties-Tip). It was a bunch of alternative rock bands doing cover songs. Examples included Bad Brains covering The Beatles and The Rolling Stones, White Zombie covering Kiss and Mind Over Four covering Queen. All pretty recognizable covers. The exception was Primus covering "Hello Skinny" and "Constantinople" by a band I never heard of called The Residents. You can hear their take on it over on YouTube:

<iframe width="560" height="315" src="https://www.youtube.com/embed/padeNqsWVJw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

I really loved the track, but in the early 90s it was very hard to find Residents albums. They were mostly out-of-print and very obscure at the time. 

Primus teased their love of The Residents even more a couple of years later in 1992 when they released *Miscellaneous Debris*, which contained another song by The Residents called "Sinister Exaggerator". Now, I really needed to hear this band, but their records were still pretty much impossible to find.

Thankfully in 1997 a Rykodisc subsidiary called East Side Digital did a huge reissue campaign of all the major albums by The Residents, including *Duck Stab / Buster & Glen* which is where all 3 of the songs Primus covered were originally released. I was finally able to snag a copy and hear the originals!

What I was amazed to find was how much Les Claypool was influenced by their music and overall aesthetic. Everything from the vocals to the dark carnival vibe were all pretty much cribbed from this album.

So who were these guys? Well, their identities have always been shrouded in mystery. They always wore costumes and masks in public and never revealed who they were. In more recent days, it's pretty well known who they are...but for the purposes of this post I'm going to keep that all a secret and an exercise for the reader who wants to dive further. 

Nowadays I'm a much bigger fan of The Residents than of Primus. They were pretty incredible and ahead of their time in many ways. *Duck Stab / Buster & Glen* was a great starting point for me, because it contains probably their most accessible material (*The Commercial Album* which is a collection of 40 1-minute "top ten hits" is probably a close second). It's still very weird, but uses regular song structures and keeps songs at a reasonable length.

Diving back from their into their catalog, I found that they explored longer, more complex songs (*Fingerprince*), snubbed their nose at pop culture (*Third Reich and Roll*) and even complex concept albums (The entire *Mole Trilogy*). Through it all *Duck Stab / Buster & Glen* probably remains my favorite, but there's so many amazing highlights throughout their entire catalog.

Eventually I was able to see The Residents live in 2001 in Chicago as part of their *Icky Flix* tour. They played a bunch of favorites including stuff off of *Duck Stab / Buster & Glen* which was spectacular to finally witness first-hand.

In the past couple years, The Residents have been re-releasing all their albums again, this time in lavish deluxe editions with all kinds of live tracks, demos and unreleased material as part of their pREServed series. This is the one that I'm picking for today! 

<iframe src="https://open.spotify.com/embed/album/2FVDah80hvq5otDys3DM4R?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>


