---
title: Peter Gabriel - Melt
author: Phil Plencner
date: 2020-06-10 22:24:47
tags:
- peter gabriel
- genesis
- king crimson
- 70s rock
- progressive rock
---

Last night I was revisiting the 3rd self-titled Peter Gabriel solo album (later referred to as *Peter Gabriel 3* or *"Melt"*...Peter Gabriel ended up naming his first 4 solo albums *"Peter Gabriel"* and they were unofficially subtitled later to avoid the purposeful confusion). 

This album rules for a number of reasons, but part of it for me is the dark nature of the lyrics...Intruder and No Self Control especially so. 

This album's biggest hit was "Games Without Frontiers" with Kate Bush on background vocals, which would be enough to cement it in rock lore. However, it also marks the first instance of Phil Collin's 80's "Gated Drum" sound, as heard on Intruder. It was an accidental discovery while in the studio and came to define drum production on Phil's first solo album Face Value as well as other popular albums for most of the 80s. 

Another overall drum-centric thing to note about this album is the complete lack of any cymbals on the entire album, at insistence of Peter Gabriel. This makes the overall sound really unique and brooding. 

I Don't Remember is also excellent as he came to refine this sound on stuff like Sledgehammer later in his career. (It helps that Tony Levin and Robert Fripp appear there as well...King Crimson alert!) Also: wow this album turned 40 years old last month!

<iframe src="https://open.spotify.com/embed/album/0LF0vWmmKRVPXoikpNkO5W" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>