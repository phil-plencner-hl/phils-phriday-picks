---
title: Axiom Funk - Funkcronomicon
date: 2022-10-28 11:40:02
tags:
- axiom funk
- p-funk
- parliament funkadelic
- george clinton
- bill laswell
- buckethead
- praxis
- 90s rock
- funk
- 70s rock
- avant-garde
---

![](Funkcronomicon.jpg)

Bill Laswell is a bass player that had a very wide reach. He was part of the band Material and other related projects in the 1970s Downtown New York City avant-garde rock scene. 

He was also a prolific producer and had his hands in successful albums by Sly and Robbie, Mick Jagger, PiL, Motorhead, Ramones, Iggy Pop, and Yoko Ono. 

Because of his massive success as a producer, he was able to start a lot of vanity projects and his own record labels. The Axiom label (which he started in 1990 with Island Record's Chris Blackwell) is one of them.

This label put out records by guitarist Sonny Sharrock, sax player Henry Threadgill along with his own bands like Tabla Beat Science and Praxis.

Praxis played a wild mix of funk, metal, hip-hop and jazz. Their membership included guitarist Buckethead (who literally wears a KFC bucket on his head and at one point was a member of Guns N' Roses), drummer Brain (who was briefly the drummer for Primus), along with Parliament-Funkadelic members Bernie Worrell and Bootsy Collins. 

Here they are playing a show typical of their sound in 1996:

<iframe width="560" height="315" src="https://www.youtube.com/embed/VF9UMona74w" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

George Clinton and Parliament-Funkadelic were in a bit of a resurgence in the early to mid 90s. The main group was the P-Funk All-Stars and they were playing large festivals such as Lollapalooza and released *Dope Dogs* in 1995. 

Here they are playing a festival in Georgia in 1993:

<iframe width="560" height="315" src="https://www.youtube.com/embed/HYZIQbQeZkE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Bill Laswell decided to combine P-Funk All-Stars and Praxis, along with many other guests into a gigantic supergroup called Axiom Funk and released their sole album (a massive 2-CD set) called *Funkcronomicon* in 1995 on the Axiom label. *Funkcronomicon* is today's pick!

This is an underrated gem in the massive P-Funk catalog. It boasts the distinction of being perhaps the final recording of guitarist Eddie Hazel (he of *Maggot Brain* fame). His playing on "Orbitron Attack", "Pray My Soul", and "Sacred To The Pain" (playing with Umar Bin Hassan from The Last Poets) is nothing short of amazing.

Buckethead is also featured predominantly throughout the album. Most notably on the Jimi Hendrix cover "If 6 Was 9" along with Bootsy Collins.

Other guests include Sly and Robbie (on the great cover of Funkadelic's "Cosmic Slop"), Herbie Hancock, George Clinton and "Zigaboo" Modeliste (the drummer for The Meters).

It also features the last cover artwork from Pedro Bell, who designed all the classic Parliament-Funkadelic albums from the 1970s.

To say, this album brings *THE FUNK* is a massive understatement.

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/album/7aoK7Ur4zqvoanHVQKcXxf?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

<iframe id='AmazonMusicEmbedB00GKCEDT6' src='https://music.amazon.com/embed/B00GKCEDT6/?id=aLrVdyaPd9&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *; clipboard-write" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/funkcronomicon/1444142726"></iframe>

<iframe src="https://embed.tidal.com/albums/35313613" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>