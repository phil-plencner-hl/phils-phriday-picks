---
title: Frank Zappa - Zappa Original Motion Picture Soundtrack
date: 2020-12-04 10:54:56
tags:
- frank zappa
- jazz rock
- jazz fusion
- 70s rock
- 60s rock
- progressive rock
---

Today marks 27 years since the death of Frank Zappa (December 4, 1993). Frank Zappa is obviously one of my favorite musicians and composers of all time, so this is always a day I try to spend time listening to his music.

Luckily, there is new stuff to explore! There is a brand-new documentary film called Zappa that explores his life. It was created by Alex Winter and is excellent and highly recommended! It is streaming online on a [bunch of platforms](https://www.thezappamovie.com/watch-at-home/). 

A soundtrack was also released with the film and its pretty comprehensive. It spans Zappa's entire career from the pre-Mothers of Invention material up until the final orchestral performances he conducted before he passed away. It also includes music from some of his influences (Stravinsky, Varese) and bands that were on his label (The GTOs and Alice Cooper). There is also an original score from the movie included by John Frizzell. More information about what is on the soundtrack and what records the songs originally appeared on (along with opportunities to purchase CD and Vinyl editions) can be found on the [official Zappa website](https://www.zappa.com/news/zappa-original-motion-picture-soundtrack-available-today-zappa-recordsume).

The soundtrack is excellent listening for both new and old fans alike (as is the film). I encourage you to immerse yourself in Zappa's music today, especially the new film soundtrack.

<iframe src="https://open.spotify.com/embed/album/391z2l9eAfCttmdvE4O5LP" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>