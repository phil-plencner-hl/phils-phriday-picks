---
title: Heldon - Stand By
author: Phil Plencner
date: 2020-04-01 15:13:22
tags:
- heldon
- space rock
- 70s rock
- progressive rock
---
Today's pick is an awesome space-rock epic by French obscurity Heldon. *"Stand By"* is part of their latter triology of albums, that opens with the Epic 20+ minute "Bolero". 

If you like this, I'd also recommend *"Interface"* from the same era.

<iframe src="https://open.spotify.com/embed/album/48BhHUvOOPu8SXrmPHLDLO" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
