---
title: Not Our First Goat Rodeo
author: Amy Deschenes
date: 2020-07-08 16:20:48
tags:
- not our first goat rodeo
- stuart duncan
- bluegrass
---

One of my favorite albums from the 2010's was 2011's The Goat Rodeo Sessions. Well, they got the band back together and put out a second album! Great work background music if you’re into bluegrass or classical. 

<iframe src="https://open.spotify.com/embed/album/4NDXeXypRq8YdFCKqg4sAn" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>