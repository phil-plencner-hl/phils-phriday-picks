---
title: Alina Bzhezhinska - Reflections
date: 2023-03-31 15:16:24
tags:
- alina bzhezhinska
- shabaka hutchings
- harp
- the comet is coming
- avant jazz
- jazz fusion
- pharoah sanders
- alice coltrane
- john coltrane
- joe henderson
- hip-hop
---

![](Reflections.jpg)

The harp is not something you hear about so much outside of a classical orchestra setting. There are a few notable exceptions such as them sometimes appearing on a Beatles song ("She's Leaving Home") or the playing of Zeena Parkins (her excellent playing is all over Bjork's *Vespertine*). Ukrainian harpist Alina Bzhezhinska is looking to change that.

Alina Bzhezhinska came across my radar very recently. I recently read cover-to-cover [the winter 2022 issue of Finnish magazine *We Jazz*](https://wejazzrecords.bandcamp.com/merch/we-jazz-magazine-winter-2022-revelation) and one of the many highlights was an interview with Alina.

What initially caught my interest was her association with the new wave of British jazz stars like Shabaka Hutchings (of The Comet Is Coming fame). Here they are playing together along with keyboardist Kamaal Williams from 2021:

<iframe width="560" height="315" src="https://www.youtube.com/embed/J0R94Q0Smbw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

 However, the more I dove into her catalog the more impressed I became with her music. Especially with how she is trying to combine 60s/70s spiritual jazz with a more modern flair (such as trip-hop and acid jazz influences). She pays homage to the past while looking ahead to the future in very imaginative ways.

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

 One of those ways is through an organization she founded called the [HipHarp Collective](https://www.hipharpcollective.com/).  According to their bio they are "made up of contemporary artists and collaborators, professional music makers, electronic music producers and harp lovers, who support everything innovative, new unusual and out of the box". Sign me up for that!

One of the many things that came out of the work of HipHarp Collective is Alina Bzhezhinska's most recent album, *Reflections*, which is today's pick! 

The band on the album includes Tony Kofi (Saxophones), Jay Phelps (Trumpet), Julie Walkington (Double Bass) and vocalist Vimala Rowe, strongly supported by international talents Mikele Montolli (Electric Bass), Joel Prime(Percussion), Adam Teixeira (Drums) and Ying Xue (Violin & Viola). A top-notch set of musicians to help realize her creative vision. 

Here are a couple of those musicians performing live with Alina as an acoustic jazz trio that is breathtaking:

<iframe width="560" height="315" src="https://www.youtube.com/embed/YEj7bGUnjoE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

The album looks to "pay homage to some of jazz, funk and hip-hop’s greatest innovators" and I think it hits the bullseye on that target. Along with the great original songs there are also fascinating covers of Joe Henderson ("Fire"), John Coltrane ("Alabama") and Duke Ellington ("African Flower"). 

If this album is any indication of the future, I look forward to what Alina Bzhezhinska and the rest of the HipHarp Collection do next! 

{% iframe 'https://open.spotify.com/embed/album/7vm57UmS0tpc8zaAV0rSUR?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B0B25WW99M/?id=adyYPaYodq&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/ca/album/reflections/1625924788' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/230706983' '100%' '96' %}