---
title: Chicago - Live At 55
date: 2024-11-27 11:27:42
tags:
- chicago
- steve vai
- robert randolph
- chris daughtry
- robin thicke
- kingfish
- jazz rock
- jazz funk
- 70s rock
---

![](LiveAt55.jpg)

Due to the Thanksgiving holiday, I am sending out this week's pick early! I know Phil's Wednesday Picks doesn't have the same ring to it, but hopefully everyone is fine with an early missive this week.

I've been a fan of the band Chicago since at least high school (although likely earlier since it was seemingly impossible to avoid their music on the radio back in those days). I was in the band that performed at basketball games, playing favorites like "25 or 6 to 4", "Make Me Smile" and "Does Anyone Really Know What Time It Is". Those songs are all classics. 

I am one of those fans that generally subscribe to the rule that the best Chicago records were the ones that included guitarist Terry Kath (who passed away from an accident involving a firearm in 1978). Every album from that era (through *Chicago XI*) are all front-to-back bangers. Their live records from the era also illustrate how they were one of the best rock bands on the planet during that time.

I'll showcase a couple deep-cuts that they played live here. We all know the classic hits, so no need to rehash it here. First up is "Dialogue Pts 1 & 2" from *Chicago V*, performed at the Arie Crown Theater in Chicago, November 1972:

<iframe width="560" height="315" src="https://www.youtube.com/embed/gje_0OMj4h4?si=sBrTEuutIcrOoT1k" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

It's great hearing Robert Lamm and Peter Cetera singing their hearts out on that one! The rest of the band is in the zone.

Another one worth watching is this incredible medley of "(I've Been) Searchin' So Long / Mongonucleosis" from 1977:

<iframe width="560" height="315" src="https://www.youtube.com/embed/nxGOeebsgHM?si=ngQnLfIhpvxUX_2c" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

It’s hard to top that performance. Terry Kath's solo alone is worth the price of admission here.

Ok, I think that's enough evidence for justification for my Chicago fandom.

Even after the classic era, I'd still occasionally check in on what the band was up to, although much less so once Peter Cetera left the group in 1985. By that point that were basically a soft-rock, ballad heavy group with David Foster producing and co-writing songs with the group and generally pulling the strings.  

Through all of the dreck, every once in a while, a gem would sneak out. A good example was their "big band" record in 1995 called *Night and Day*. They took old big band jazz songs and rearranged them into the jazz-rock style of Chicago. Worth checking out. A good example of what this was like is this performance of "Moonlight Serenade":

<iframe width="560" height="315" src="https://www.youtube.com/embed/fN2un3lnqw0?si=yJQnMUq2CqsCD6ae" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

In the summer of 2001, I went on a week-long road trip throughout the midwest and south. At the beginning of that trip I went to the Illinois State Fair and Chicago happened to be performing that night. I checked it out and it was surprisingly good, so I kept them on my radar. Unfortunately, not much was happening for them album-wise during that time besides rehashing of the greatest hits and a bunch of Christmas albums (Somehow Chicago has released 4 Christmas records between 1998 and 2019! Why??).

Nevertheless, another pretty cool latter-day Chicago record was *Stone of Sisyphus*. It was also recorded in the mid 90s, but the record company hated it (because it went back to their early jazz-rock sound with some progressive rock twists and not the soft rock balladry). It became a mythical "lost album" for a long time but was eventually released in 2008. Chicago toured behind the record in 2008 and here is the title track:

<iframe width="560" height="315" src="https://www.youtube.com/embed/N9Nr6_jWcqk?si=AW0FwjU3WrBrFbpR" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Aside from that there has been not much to report about Chicago besides a constant shuffling of drummers, bass players and guitarists (The core horn section and Robert Lamm have been mostly still in the fold)....until recently!

That's because late last year Chicago performed a special show in Atlantic City to celebrate the 55th anniversary of their debut album *Chicago Transit Authority*. It included special guests such as Steve Vai on guitar, Robert Randolph on pedal steel, and vocalists Chris Daughtery, Robin Thicke and Kingfish. The kicker was they claimed they were going to play the entire *Chicago Transit Authority* front-to-back! Luckily the entire performance was recorded and released as *Live At 55* which is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

The entire show was broadcast on PBS earlier this year ([you can still stream it online](https://www.pbs.org/show/chicago-friends-live-at-55/)) but the accompanying record came out just last week.

I was initially skeptical, but I'm telling you....the band sounds re-invigorated! The songs sound awesome and the performances are very exciting. They certainly aren't going through the motions. [PBS has a "sneak peek" preview video](https://www.pbs.org/video/sneak-peek-chicago-friends-live-at-55-bydbxg/) that gives you the gist of what they sound like. Tight!

While they didn't play the ENTIRE first record (obviously no "Free Form Guitar" and sadly no "Liberation") they still play songs that the band hasn't performed in 40-50 years! 

For example, here is "Listen" (with Robert Randolph) that they haven't played since 1973!

<iframe width="560" height="315" src="https://www.youtube.com/embed/P2lwgGQGXAM?si=Fkh1Nb4e3U17n-Ra" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Here is "Southern California Purples" and "Poem 58" with Steve Vai. they haven't played "Southern California Purples" since 1981 and I'm not sure they ever played "Poem 58" in concert. If they have, it was an exceedingly rare occurrence:

<iframe width="560" height="315" src="https://www.youtube.com/embed/dcXFvJ6oHmg?si=6SZvCLbgAGoZtMEB" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

There are tons of other highlights throughout the entire two-and-a-half-hour performance including "Wishing You Were Here", "Saturday In The Park", "Free" (another rarely played classic from *Chicago III*), "Mongonulceosis" and "Old Days" all sound killer. They even revive the disco-inspired "Street Player" from *Chicago 13* and it doesn't sound half bad.

Sure, they play a couple David Foster-era duds ("Hard to Say I'm Sorry", "You're the Inspiration") and the sappy Cetera tune "If You Leave Me Now", but those are easy to skip over. 

One more major highlight is their performance of the Spencer Davis Group song "I'm A Man" (which Chicago also originally recorded on the debut record). It includes an incredible drum solo duet between Walfredo Reyes Jr. and Ramon Yslas. This isn't the exact performance from *Live In 55* but it is from an earlier show. The drum duet is similar:

<iframe width="560" height="315" src="https://www.youtube.com/embed/-gJ95BvpcKA?si=4ZaeLUDy0PHRSy1c" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

If you are a fan of old school Chicago, don't sleep on *Live at 55*! It is a surprisingly great live recording of a band that is still going strong over a half-century later. I'm seriously considering getting tickets to a concert if they come my way soon to catch up on what I've been missing.

{% iframe 'https://open.spotify.com/embed/album/51HetOJ0XC1F2snGFWoDKc?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B0DJC8WM5R/?id=x7wtqtbKsS&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/live-at-55/1771687108' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/399668536' '100%' '96' %}
