---
title: Jan Akkerman - Tabernakel
date: 2022-03-11 12:18:05
tags:
- jan akkerman
- focus
- 70s rock
- progressive rock
- jazz fusion
---

Jan Akkerman is a virtuoso guitar player from the Netherlands. He originally came to world renown as a member of the progressive rock band Focus. 

Focus had an unlikely top-10 hit in 1973 with the song "Hocus Pocus" from their second album *Moving Waves*. It is a absolutely monstrous progressive rock tune with crazed yodeling and flute throughout. How this became an international sensation is mind boggling, but very cool.

If you haven't heard "Hocus Pocus" before, you're in for a treat. Here is a completely killer version of it on when they performed it on NBC(!!!) at the height of its popularity:

<iframe width="560" height="315" src="https://www.youtube.com/embed/e8sEo53HYtE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Later that same year, Jan Akkerman put out a solo record called *Tabernakel*. To say it was different than what Focus was doing at the time is an understatement.

On *Tabernakel* he plays mostly acoustic guitar and lute! Turns out he is a pretty phenomenal lute player. While he did include a reworking of "House Of The King" from Focus' first album, overall I'm sure this confused the core Focus fan base.

Most of the first side of the album consists of pieces from the Renaissance and Baroque age. His playing is equal parts beautiful and mind blowing in it's effortless complexity.

The second side consists of "Lammy", a 14-minute progressive rock epic. Here he is joined by bassist Tim Bogert and drummer Carmine Appice who were originally in the excellent boogie rock band Cactus. Bogert later rose to fame playing with Jeff Beck. Appice became a member of The Vanilla Fudge and backed Rod Stewart. So you can get a sense of the rock power that they lend to this song. As if that weren't enough, "Lammy" also includes a choir and orchestra. 

Before you dive in to *Tabernakel* I also urge you to partake in this excellent footage of Jan Akkerman in 1975 showing off his formidable lute skills playing the John Dowland composition "Fantasia": 

<iframe width="560" height="315" src="https://www.youtube.com/embed/N-mQVjikNkE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

With that appetizer out of the way, your expectations are set to properly enjoy *Tabernakel* in all its glory.

<iframe src="https://open.spotify.com/embed/album/3FWm5uU219iOXEGnc5ZXKq?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

<iframe id='AmazonMusicEmbedB00123MBG0' src='https://music.amazon.com/embed/B00123MBG0/?id=oSUzGbmabR&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/tabernakel/50268795"></iframe>