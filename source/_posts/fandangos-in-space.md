---
title: Carmen - Fandangos In Space
author: Phil Plencner
date: 2020-04-10 14:55:31
tags:
- carmen
- 70s rock
- progressive rock
- weird rock
- flamenco
---
 I suspect what you didn't think was missing in your life (but most definitely is missing in your life!) is a 1970s progressive rock band with flamenco influences!  
 
 Carmen is the answer to your prayers! The beyond bonkers album "Fandangos in Space" is today's pick!  
 
 Check out the carefully recorded dancing on several of the songs as well!
 
 <iframe src="https://open.spotify.com/embed/album/7r0yYd0tsnU9dfplpefYeK" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>