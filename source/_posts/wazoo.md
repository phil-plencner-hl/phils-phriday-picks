---
title: Frank Zappa - Wazoo
author: Phil Plencner
date: 2020-05-26 14:37:25
tags:
- frank zappa
- progressive rock
- 70s rock
- wazoo
---
Today's pick is the *"Wazoo"* live album by Frank Zappa. 

Recorded in 1972 and the Music Hall in Boston (now known as the Wang Theatre) it showcases the large-scale, mostly instrumental band Zappa assembled in the early 70s after the breakup of The Mothers (Smoke on the Water etc....look it up). 

Eventually Zappa determined it wasn't economically feasible to continue to tours such a large band (20 members) and scaled it down to the 10-piece "Petite Wazoo" band. 

This recording is the full Wazoo band in fine form, playing probably some of Zappa's most complex and sophisticated jazz-rock music he composed.

<iframe src="https://open.spotify.com/embed/album/6JmbKM2yCii8Xmw4KQ7QR9" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>