---
title: Stephen Stills - Thoroughfare Gap
date: 2023-05-19 10:37:36
tags:
- stephen stills
- bee gees
- andy gibb
- classic rock
- disco
- 70s rock
---

![](ThoroughfareGap.jpg)

A quick missive for the Phil's Phriday Picks today as I am in transit. This pick coming to you live from Boston's Logan Airport!

This weekend is the Preakness Stakes in Baltimore (which is part of the triple crown). I don't really follow much horse racing, but when I think of it, I always think of the album cover of Stephen Still's fifth solo album *Thoroughfare Gap* so it is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

Stephen Stills had recently recorded with Abba in 1976 (playing percussion on "You Should Be Dancing") so he had disco fever. He decided to make an album that had one foot in his typical classic rock sound and the other foot in disco. It was not very well received at the time but has grown to be a cult favorite by some (including me).

Andy Gibb and the Bee Gee's producer Mike Lewis appear along with drummer Joe Vitale so the disco quotient is pretty high, but really enjoyable. There are also covers of The Allman Brothers "Midnight Rider" and Buddy Holly's "Not Fade Away" which gives the whole album a well-rounded, though slightly schizophrenic overall feel.

It's almost time to catch my flight, so enjoy *Thoroughfare Gap*.

{% iframe 'https://open.spotify.com/embed/album/5liXgioyiptRTTu9H2Nu6m?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B001URTA80/?id=OYOJTR7tZR&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/thoroughfare-gap/306564734' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/3381706' '100%' '96' %}
