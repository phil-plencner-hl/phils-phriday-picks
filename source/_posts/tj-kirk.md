---
title: T.J. Kirk - T.J. Kirk
date: 2025-02-07 20:57:06
tags:
- tj kirk
- charlie hunter
- john schott
- will bernard
- scott amendola
- thelonious monk
- james brown
- rahsaan roland kirk
- jazz
- funk jazz
- jazz fusion
---

![](TJKirk.jpg)

Charlie Hunter is one of my all-time favorite guitar players. He doesn't play a regular guitar like other mere mortals. He plays an 8 string guitar, with the lower strings acting as a bass. He can play bass and guitar parts simultaneously, which is pretty mind boggling.

In the early 90s his band, The Charlie Hunter Trio, consisted of drummer Scott Amendola and saxophonist Dave Ellis. Here is a great live performance of them playing their original song "Funky Niblets" off of their self-titled debut record which was released on Les Claypool's (the bassist from Primus) Prawn Song label:

<iframe width="560" height="315" src="https://www.youtube.com/embed/lOycpHrQcDs?si=6inac44jCn0gnl_a" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Incredible stuff! They are locked into a pretty ferocious groove, anchored by Scott Amendola's great drum breaks. He later went on to be part of the Nels Cline Singers, but he has been a steady foil for Charlie Hunter in a wide variety of contexts and groups over the decades. 

One of the groups that Charlie Hunter and Scott Amendola played together in was called T.J. Kirk (not to be confused with the modern day weirdo podcaster). The band was named T.J. Kirk because of their repertoire: They play covers of Thelonious Monk, James Brown and Rahsaan Roland Kirk...sometimes mixing the artists together into wild medleys! In fact, the band was originally called James T. Kirk, but they were threatened a lawsuit from Paramount (who owns the rights to Star Trek) and ended up changing their name.

The band was a quartet.  Hunter and Amendola were joined by two other guitarists: John Schott and Will Bernard. The three guitars allowed them to play some pretty intricate grooves, even while one of them is soloing. 

The band was short lived, only playing together from 1995-1997. They put out two studio records (in 1995 and 1996) and a live album. Their self-titled debut record is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

As I mentioned, they exclusively play songs by Thelonious Monk, James Brown and Rahsaan Roland Kirk. One of my favorite songs on the album is a medley of Thelonious Monk's "Shuffle Boil" and James Brown's "You Can Have Watergate, Just Gimme Some Bucks and I'll Be Straight":

<iframe width="560" height="315" src="https://www.youtube.com/embed/BhzoqLlCBTk?si=ljCEc4H2xl5C-Zph" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

They don't always play the covers in a straightforward manner. One example of their out-of-the-box thinking is their rendition of Rashaan Roland Kirk's "Volunteered Slavery" which they turned into a heavy reggae number:

<iframe width="560" height="315" src="https://www.youtube.com/embed/hm3nlOaBz7w?si=mWi-B3PZe0cifl7r" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

They also turn Thelonious Monk's "Epistrophy" into a crushing heavy metal tune:

<iframe width="560" height="315" src="https://www.youtube.com/embed/2AKmVZOxk18?si=6DD6DSNx-MZYO_0i" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Another song I like a lot is their cover of Monk's "Teo" which has a lot of weird, choppy stops and starts that keeps the listener off balance:

<iframe width="560" height="315" src="https://www.youtube.com/embed/x1m5Ws-JyiU?si=9jQba8yosk7Dp_3n" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

While the group seems like a novelty act, all the musicians actually took the source material very seriously. Here is a promo video the record label Warner Brothers put out to promote the first record, which features interviews of all the band members along with some live snippets and other cool footage:

<iframe width="560" height="315" src="https://www.youtube.com/embed/v6Q0l9Rf13U?si=zr6d9ymQOVsp3TNs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

T.J. Kirk wasn't just a studio band. They were also a formidable live act that was able to pull off their wild song arrangements while making it look effortless, yet fun. Speaking of fun, they frequently each wore a fez during their shows. Here is an great example of them playing "Cold Sweat" with "Rip, Rig and Panic":

<iframe width="560" height="315" src="https://www.youtube.com/embed/FXjPSuppYck?si=vB0_yy66bFlfu9Wv" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Finally, here is some more excellent footage of them playing Monk's "In Walked Bud" and Brown's "I Got To Move" which even includes an awesome drum solo:

<iframe width="560" height="315" src="https://www.youtube.com/embed/c1fE31I372g?si=wogL8_RSXovilv5t" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

T.J. Kirk's second record *If Four Was One* was nominated for a Grammy in 1996 but didn't win. It is also well worth hearing (they even expand their scope a bit playing Prince's "Rockhard in a Funky Place" on the album), but I like the debut better. It sounds a little more raw, fresh and exciting. 

{% iframe 'https://open.spotify.com/embed/album/1WvcJO2adKLvJX3xsSEcR2?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B09NQQZHM9/?id=SuYbasPxXL&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/t-j-kirk/1600782669' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/209544343' '100%' '96' %}
