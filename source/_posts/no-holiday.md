---
title: The Muffs - No Holiday
author: Phil Plencner
date: 2019-10-18 14:09:49
tags:
- the muffs
- 90s rock
- alternative rock
---
Before Kim Shattuck succumbed to ALS, she worked hard with the rest of the Muffs to record one more album. 

It's out today, and on first listen its another inspiring slab of rock that will surely get a lot of repeat listens at Casa De Plencner.

There is also a recent great [LA Times article](https://www.latimes.com/entertainment-arts/music/story/2019-10-09/kim-shattuck-muffs-roy-mcdonald-no-holiday) that goes into the nitty gritty.

<iframe src="https://open.spotify.com/embed/album/2NoiNeDsjcXD1AYzwoIghX" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>