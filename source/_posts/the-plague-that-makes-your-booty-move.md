---
title: Infectious Grooves - The Plague That Makes Your Booty Move...It's The Infectious Grooves
date: 2022-05-13 12:28:09
tags:
- suicidal tendencies
- infectious grooves
- metallica
- janes addiction
- fishbone
- mike muir
- rocky george
- robert trujillo
- 90s rock
- heavy metal
- funk
- alternative rock
---

![](InfectiousGrooves.jpg)

Suicidal Tendencies has always been a punk / metal band that was a little bit weird and goofy, despite their name.

The vocalist, Mike Muir, has always been the driving force behind the band. They first came across my radar via the video for "Possessed To Skate" from *Join The Army* in 1987. You can definitely see the humor in this video!

<iframe width="560" height="315" src="https://www.youtube.com/embed/xW3taePImYg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Their next album *How Will I Laugh Tomorrow, When I Can't Even Smile Today* was probably their first major breakthrough...when they started becoming much more of a metal band than a punk band. Soon after that album bassist Robert Trujillo joined the band and they put out probably their best and most successful album *Lights...Camera...Revolution*. 

The interesting thing about that album was how Trujillo's bass brought a new sense of funkiness to the proceedings. Rocky George's guitar with his bass was a pretty amazing combination. Here was their big hit from that album "You Can't Bring Me Down" which is a pretty good example of the style:

<iframe width="560" height="315" src="https://www.youtube.com/embed/nxcJW6bs5os" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

At that point, Mike Muir wanted to explore funk and funk-metal more, so he decided to start a side project called Infectious Grooves. It was influenced by early Red Hot Chili Peppers and the lyrics and overall aesthetic was much more lighthearted and fun than Suicidial Tendencies.

Infectious Grooves consisted of Muir and Trujillo along with guitarist Dan Pleasants (who ended up eventually replacing Rocky George in Suicidal Tendencies when he left to play with Fishbone), and Stephen Perkins (of Jane's Addiction fame) on drums.

Their first album *The Plague That Makes Your Booty Move...It's The Infectious Grooves* is today's pick!

There's definitely a ton of fun rock on the album, mostly written by Muir and Trujillo: "You Lie...And Your Breath Stank", "Stop Funk'n With My Head" (where its debatable if they actually always say "funk" in the chorus), and "Punk It Up" are all highlights. 

They even convinced Ozzy Osbourne to lend a hand on vocals for the first single released from the album called "Therapy":

<iframe width="560" height="315" src="https://www.youtube.com/embed/h4eqC3DX_oE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

This collaboration actually lead to Robert Trujillo leaving Suicidal Tendencies to join Ozzy's band...which eventually lead him to being the bassist of Metallica in the early 2000s. So Infectious Grooves is actually an interesting catalyst in the history of major rock bands, even though it seems like it is just a footnote.

The album also contains some fun skits with a singing reptile called Aladdin Sarsippius Sulemenagic Jackson III. 

*The Plague That Makes Your Booty Move...It's The Infectious Grooves* is a fun party record that I come back to often when I need a pick me up.

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/album/5jhtqs995J5dSPcT22luDi?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

<iframe id='AmazonMusicEmbedB09HLBVZTY' src='https://music.amazon.com/embed/B09HLBVZTY/?id=hz9jfjeISs&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *; clipboard-write" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/the-plague-that-makes-your-booty-move-its-the/1588313280"></iframe>

<iframe src="https://embed.tidal.com/albums/199546569" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>
