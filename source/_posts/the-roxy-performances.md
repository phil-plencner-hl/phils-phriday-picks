---
title: Frank Zappa - The Roxy Performances
author: Phil Plencner
date: 2020-03-18 16:16:23
tags:
- frank zappa
- jazz rock
- jazz fusion
- 70s rock
- progressive rock
---

Popping in to remind everyone that Frank Zappa's 1973 touring band was awesome, and their performance at The Roxy is a creative peak of all of rock n' roll.
<iframe src="https://open.spotify.com/embed/album/55is2UQHc7nDRDevCuBZbU" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

...and if you REALLY want to go all in, here is the complete run of shows there, totaling 8 hours for your self-isolating needs.

<iframe src="https://open.spotify.com/embed/album/51cjNmDteqloO1E3O0wrfd" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
