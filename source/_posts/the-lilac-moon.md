---
title: Traun - The Lilac Moon
author: Phil Plencner
date: 2020-01-10 12:17:58
tags:
- traun
- the deserts of traun
- estradasphere
- jazz fusion
- progressive rock
---

Welcome to the 1st episode of Phil's Phriday Picks of 2020! 

Several months ago, I believe one of my Picks was an album from the excellent band Estradasphere, which unfortunately no longer exists. 

Back in their early-2000's prime their drummer (Dave Murray - not  to be confused with Iron Maiden's guitar player) put out a side-project called the Deserts of Traun. It supposedly told a story within the music, but being an instrumental album it was not readily apparent. 

Over the Winter Recess I was reorganizing some of the music collection at Casa De Plencner and came across the old Deserts of Traun CD. I went to Spotify to see if it existed there (it does), but I also discovered he has continued the project (shortening the name to just Traun) and put out 3(!!!!) albums continuing the storyline in 2017! 

This was amazing news to me, so I'm going to share the first of the new albums here as well as the amazing [accompanying website](http://traunmusic.com/) that fully documents the history of the project.

<iframe src="https://open.spotify.com/embed/album/2VdIoT6fOOCg4oKfJx2r2h" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>