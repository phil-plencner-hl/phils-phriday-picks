---
title: Post Now Round One
author: Phil Plencner
date: 2019-03-29 16:09:40
tags:
- skin graft records
- flying luttenbachers
- cheer-accident
- 90s rock
- progressive rock
- alternative rock
- noise rock
---

Today's selection is an awesome new rock compilation from the always reliable Skin Graft Records label, based in Chicago. 

Some of my favorite bands are featured, and its a fun way to roll into the weekend!

<iframe src="https://open.spotify.com/embed/album/17u994y5n3DkU802MPvyKM" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>