---
title: Stan Getz - Captain Marvel
author: Phil Plencner
date: 2019-04-05 16:07:22
tags:
- stan getz
- chick corea
- jazz
- jazz fusion
---

Today we're setting the Wayback Machine for 1972...for tenor saxophonist Stan Getz's excellent album *Captain Marvel*. 

On this particular session he went outside of his traditional cool jazz sound for full on jazz fusion. This is mostly because the backing band was Chick Corea, Stanley Clark, Airto Moreira (who all formed fusion band Return to Forever around the same time) and drummer extraordinaire Tony Williams. 

Return to Forever's debut album also contains the song La Fiesta, but this I think is the better take. 

Five Hundred Miles High is another Chick Corea composition on this album that went on to be one of his standards he continually revisited throughout his career. 

Enjoy!

<iframe src="https://open.spotify.com/embed/album/5PQGbH3xpNg8H4cYIKMQRi" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>