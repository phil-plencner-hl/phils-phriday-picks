---
title: Jeff Beck - Jeff Beck's Guitar Shop
date: 2023-01-20 11:57:54
tags:
- jeff beck
- tony hymas
- terry bozzio
- frank zappa
- hard rock
- jazz fusion
---

![](GuitarShop.jpg)

With the [recent passing of Jeff Beck on January 11, 2022](https://www.washingtonpost.com/obituaries/2023/01/11/guitarist-jeff-beck-dead/) I have been revisiting the guitarist's diverse catalog. 

I have been debating what album to choose as this week's pick and ultimately decided on *Jeff Beck's Guitar Shop*. This was the first Jeff Beck solo album I ever heard and is still one of my favorites.

I discovered it through an [interview with drummer Terry Bozzio in the June 1990 issue of Modern Drummer magazine](https://www.moderndrummer.com/wp-content/uploads/2017/06/md127cs.pdf). I tracked down the *Jeff Beck's Guitar Shop* CD soon after. The article (along with the rest of that issue) is well worth re-exploring almost 33 years later.

In 1990 I was already a huge Terry Bozzio fan due to his playing with Frank Zappa, Missing Persons and the prog band U.K. Plus he was pretty active in the drum clinic circuit at that time and was always in musician / drumming magazines around then. So, I was very intrigued to hear about this new album / band revolving around guitarist Jeff Beck and keyboard player Tony Hymas (there was no bass player in this trio. The "bass" parts were part of what Tony played on keyboard). 

The album is an awesome slice of late 80s / early 90s hard rock played by virtuoso musicians. It is mostly instrumental, though Terry Bozzio does do some humorous speaking on a couple of the songs.

"Sling Shot" was the song they heavily promoted from the album at the time, and it is a good representation of the whole record. Here they are playing it on The Arsenio Hall Show in December 1989:

<iframe width="560" height="315" src="https://www.youtube.com/embed/uEsxMbMzO4A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Jeff Beck's guitar playing is off-the-charts on this record as always. Aside from the more straightforward hard rock, there is also "Behind the Veil" which is kind of a heavy-reggae and the nice ballad "Two Rivers". 

Best played loud.

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/album/1JJtA0AmTd6N72qzbPOAjA?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

<iframe id='AmazonMusicEmbedB00136RVAM' src='https://music.amazon.com/embed/B00136RVAM/?id=7jE4Nn5W44&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *; clipboard-write" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/jeff-becks-guitar-shop-with-terry-bozzio-tony-hymas/157277697"></iframe>

<iframe src="https://embed.tidal.com/albums/33986466" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>

