---
title: Opeth - Morningrise
author: Phil Plencner
date: 2020-06-24 22:05:06
tags:
- opeth
- morningrise
- black metal
- death metal
---
Finally: I read online today that Opeth's 2nd album *"Morningrise"* was released 26 years ago today. Wow! 

I remember special-ordering this album at Chicago shop ["Metal Haven"](https://www.chicagoreader.com/chicago/metal-haven-closing-mark-weglarz-ravenswood-music-stores/Content?oid=1418106) back when I first heard about it. At that time it was an import and took a long time to arrive. Well worth the wait...

It still might be my favorite Opeth album, especially because of the opening song "Advent". 

A few years later, while working for Guitar.com I caught their first-ever North American show, at [Milwaukee Metalfest 2000](https://www.youtube.com/watch?v=ifivL0ZWgAU) (Yes, they played Advent!) We ended up interviewing Mikael after the gig. My co-worker John (who called it "the greatest show ever on American soil") asked the questions and I manned the camera. 

Here's my [press-pass from the day](https://www.instagram.com/p/BfM5PWVnivY/) 

ANYWAYS I DIGRESS, let's listen to Morningrise!

<iframe src="https://open.spotify.com/embed/album/7mov9rVPoOWLi2mksFx5ex" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>