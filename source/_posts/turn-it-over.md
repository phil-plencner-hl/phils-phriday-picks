---
title: Tony Williams Lifetime - Turn It Over
date: 2023-07-07 12:36:32
tags:
- tony williams
- john mclaughlin
- jack bruce
- larry young
- cream
- miles davis
- jazz fusion
- jazz rock
---

![](TurnItOver.jpg)

Tony Williams was already a legendary drummer by the time the late 60s rolled around. He was part of Miles Davis' "Second Great Quartet" from 1964-1968 (along with Wayne Shorter, Herbie Hancock and Ron Carter). Albums such as *E.S.P*., *Miles Smiles*, *Sorcerer*, *Nefertiti*, *Miles in the Sky*, and *Filles de Kilimanjaro* are all classics. He also put out solo albums for Blue Note and played with other jazz heavyweights at the time including Eric Dolphy and Andrew Hill. 

As if all this wasn't enough, Tony Williams became more interested in heavy, loud rock music. So, in 1969 he formed a new band called Tony Williams Lifetime. It was originally a trio, with guitarist John McLaughlin (pre-Mahavishnu Orchestra) and organist Larry Young (who was at the time working with Miles Davis on *Bitches Brew* and recently released his watershed solo album *Unity*). Their first album was a sprawling 2-record set called *Emergency!*. It featured a loud, heavy brand of jazz fusion with a heavy influence from progressive rock and free jazz. This was a POWER trio in every sense of the term. Most songs are over 8 minutes long, and some pushed 15 minutes. A massively epic recording.

For the follow-up album, the recruited bassist Jack Bruce. He, of course, was part of another heavy power trio: Cream. They had recently broken up and was looking to do something new.

The resulting record was *Turn It Over*, released in 1970. It is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

For this record, they tighten up the song lengths. Only one song (A wild cover of Antonio Carlos Jobim's "Once I Loved") exceeds the five-minute mark. However, this didn't stop them from increasing the intensity. Every song is like a runaway freight train. All the players are at the top of their game. This is especially so with John McLaughlin and Larry Young who rip out some incredible solos during the opening two-part song "To Whom It May Concern". 

Other highlights include the crazed cover of John McLaughlin's "Big Nick", the Tony Williams-penned "Vuelta Abajo" and the Larry Young feature "Allah Be Praised".

Unfortunately, this lineup only lasted for one album. John McLaughlin formed Mahavishnu Orchesta and Jack Bruce formed his own power trio with Corky Laing and Leslie West.

Tony Williams continued using the Lifetime name, expanding the lineup to include multiple percussionists, Ron Carter on bass and Ted Dunbar on guitar. Unfortunately, there's not too much early live video footage of Lifetime out there. But here is a killer example of the larger band (with Larry Young still involved) in 1971:

<iframe width="560" height="315" src="https://www.youtube.com/embed/frwrlk-J9p8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

*Turn It Over* remains kind of a hidden gem in the jazz fusion genre. I'm not sure why it hasn't gotten wider praise and recognition because it is one of my favorites. In the late 90s producer Bill Laswell re-mixed and "re-imagined" it with the intent to release it as *Turn It Over (Redux)* but unfortunately it was never officially put out. You can find it if you search hard enough on the internet though...

Nevertheless, the original record is an incredible album. In the liner notes, Tony Williams repeatedly instructs the listener to play the album at a high volume...and it’s hard to argue with that! TURN IT UP!

{% iframe 'https://open.spotify.com/embed/album/4F87oejn638g4Os0rZzZct?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B000VZXAKG/?id=Rqef3O6oYH&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/turn-it-over/1444126594' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/36436734' '100%' '96' %}
