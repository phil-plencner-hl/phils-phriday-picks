---
title: The Rising and Are You Passionate
author: Phil Plencner
date: 2020-09-11 14:35:53
tags:
- bruce springsteen
- neil young
- 9/11
- classic rock
- 80s rock
---
Today is 9/11. 

It's usually too overwhelming me to turn on the TV much on this day and watch the tributes and remembrances roll in. 

Instead, I use this as a day to annually revisit Bruce Springsteen's *"The Rising"* and Neil Young's *"Are You Passionate?"* both of which were influenced by the events of this day in 2001.

<iframe src="https://open.spotify.com/embed/album/23vzCh5cDn0LzdGmGWrT1d" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

<iframe src="https://open.spotify.com/embed/album/7ELKbABaYNrfoaJNulSTLf" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
