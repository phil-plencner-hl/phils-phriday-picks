---
title: Prince - Live In Minneapolis 1985
author: Phil Plencner
date: 2020-05-15 14:55:48
tags:
- prince
- minneapolis
- funk rock
- 80s rock
---
Did anyone else catch the Prince & The Revolution concert stream last night?! Live in Minneapolis 1985. It'll be up on the Prince YouTube channel through the weekend, and then will be gone....so don't sleep on this! 

There is also a soundtrack on Spotify as well. Let's Go Crazy!

<iframe src="https://open.spotify.com/embed/album/2wLAcpM7FxdoC1dOdXfSNp" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>