---
title: Hal Willner - Weird Nightmare
date: 2021-03-22 07:59:09
tags:
- hal willner
- charles mingus
- hsrry partch
- 60s jazz
- avant-garde jazz
- 90s rock
- vernon reid
- elvis costello
- bill frisell
---

The 90s were a very weird time for music, especially major record labels desire to put out strange compilation albums. This was an opportunity for producer Hal Willner to go all-out and really shine.

Hal Willner was already known for putting together wild concept albums (His *Stay Awake* album interpreting classic Disney songs already brought him notoriety). However, his 1992 Charles Mingus tribute *Weird Nightmare* was probably his craziest and wildest. It is probably also my favorite of his productions.

Paying tribute to the legendary Charles Mingus is already a tall order (either his bass playing or music composition would be challenging on its own). So Hal really took it to an extreme with *Weird Nightmare*. The fact that this was released on major label Columbia Records still blows my mind.

He enlisted a huge array of musicians from a wide spectrum of music from rock to jazz (Bill Frisell, Vernon Reid, Henry Rollins, Keith Richards, Charlie Watts, Don Byron, Henry Threadgill, Gary Lucas, Bobby Previte, Robert Quine, Leonard Cohen, Diamanda Galás, Chuck D, Francis Thumm, and Elvis Costello all appear). 

He also received permission from the estate of Henry Partch to use some of his custom-made instruments tuned to unique scales to make the record sound even more "out" than it already is. 

Not only that, but Ray Davies from the band The Kinks also filmed all the recording sessions and made a completely bonkers movie about it. [You can watch the movie on YouTube](https://www.youtube.com/watch?v=r8b6KdnnM8Y).

For an extremely in-depth look at *Weird Nightmare* I recommend you read [this detailed piece about it that appeared in the Washington Post when the album was originally released](https://www.washingtonpost.com/archive/lifestyle/style/1992/09/27/how-a-dream-became-his-nightmare/7353117d-5333-44e8-97ab-b05ff64d7d85/). The fact this strange album got this much press is also pretty astonishing.

Of course, what I'm really recommending is the bonkers album itself. It's hard for me to describe what it sounds like, so your best bet is to just dive in.

Hal Willner passed away last year due to complications from COVID-19.

<iframe src="https://open.spotify.com/embed/album/7s3aBheDTuad5yzqLvslGh" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

