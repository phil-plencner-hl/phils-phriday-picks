---
title: Johnny Cash - American IV The Man Comes Around
date: 2022-10-21 08:55:35
tags:
- johnny cash
- tom petty
- u2
- soundgarden
- rick ruben
- bob dylan
- country
- classic country
- alternative rock
- 90s rock
---

![](AmericanIV.jpg)

In the early 90s, Johnny Cash's career was in a decline. His last huge success was probably with the country supergroup The Highwaymen (Kris Kristofferson, Johnny Cash, Waylon Jennings, and Willie Nelson) in 1985. Their self-titled album was a huge smash, selling over one million copies. I had a good fortune of seeing them perform in 1990 and it is one of my all-time favorite shows. 

Here is Johnny Cash performing "The Highwayman" in 1986 (without the rest of the Highwaymen) in a gripping performance:

<iframe width="560" height="315" src="https://www.youtube.com/embed/5xfnFsrYezU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

His solo albums were not having the same success (but not without the lack of trying). In 1998 he put out *Water from the Wells of Home* which included a star-studded roster (Paul McCartney, Waylon Jennings, Hank Williams Jr., Glen Campbell, Emmylou Harris Rosanne Cash) and containing old classics like "Ballad of a Teenage Queen". It didn't really sell, and the Mercury label dropped him.

His fortunes picked up in 1992, when he performed on the Bob Dylan 30th Anniversary Concert Celebration at Madison Square Garden that was released on home video:

<iframe width="560" height="315" src="https://www.youtube.com/embed/qRjlVRVWS5Y" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Producer Rick Rubin, known for making hit records around that time with LL Cool J, The Red Hot Chili Peppers, Danzig and Slayer saw the performance and approached Johnny Cash about signing him to his new American Recordings record label and giving Johnny complete artistic control. 

Around the same time, Bono wrote a song specifically with Johnny Cash in mind called "The Wanderer". Johnny was persuaded to record it with U2 and it was released on *Zooropa* in 1993. U2 still uses the recording as intermission music on their tours. Here is an example from their 2016 tour:

<iframe width="560" height="315" src="https://www.youtube.com/embed/szBYY2-7lpM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Johnny Cash's first record with Rick Rubin was also called *American Recordings* and was a very stripped-down record. Mostly just Johnny Cash performing acoustic guitar and singing. The single "Delia's Gone" is a good example of the haunting nature of the record. It was critically lauded but was not a smashing sales success.

For the follow up record, *Unchained*, Rick Rubin had the brilliant idea of using Tom Petty and the Heartbreakers as the backing band. Along with that core group, other guests were brought in such as Marty Stuart, Flea, Lindsey Buckingham and Mick Fleetwood. There was also a focus on performing covers of alternative rock songs such as Beck's "Rowboat" and Soundgarden's "Rusty Cage". The record again had critics raving and sold better but was still not a world beater.

<iframe width="560" height="315" src="https://www.youtube.com/embed/9CrBqRb3VGM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

A 3rd album in a similar format *Solitary Man* met similar success. They finally blew the doors wide open with the 4th and final album in the series *American IV: The Man Comes Around* which is today's pick!

This one was a complete smash, selling well over 2 million copies. The guests included Fiona Apple (singing a duet on Simon & Garfunkel's "Bridge Over Troubled Water"), Don Henley (singing and playing drums on "Desperado") and The Red Hot Chili Pepper's John Frusciante performing with Cash on Depeche Mode's "Personal Jesus". However, the song that *really* put the album over-the-top was the Nine Inch Nails cover "Hurt". 

The video for "Hurt" is incredible. Featuring haunting footage of Johnny Cash and June Carter Cash and showing the run-down state of the House Of Cash museum in northern Tennessee (which I had the pleasure of visiting in the year 2000 on an epic music-centric road trip I took. It was in a slightly better state then). The video won a ton of awards and is attributed to the album's overall smashing success. 

Fun fact: It was [filmed exactly twenty years ago this month](https://www.savingcountrymusic.com/20-years-ago-today-johnny-cash-films-the-video-for-hurt/).

<iframe width="560" height="315" src="https://www.youtube.com/embed/8AHCfZTRGiI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The Cash family was not able to bask in the success of *American IV: The Man Comes Around*. The album was released on November 5, 2002. The "Hurt" video released in February 2003. June Carter Cash passed away May 15th, 2003, and Johnny Cash passed away September 12, 2003.

U2 only performed "The Wanderer" one time in a live setting. It was for a Johnny Cash tribute concert in 2005:

<iframe width="560" height="315" src="https://www.youtube.com/embed/bW6hnrZQamQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*American IV: The Man Comes Around* still sounds fantastic twenty years after its release. After immersing yourself in it, I also recommend circling back to the other three American Recordings records.

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/album/2BlL4Gv2DLPu8p58Wcmlm9?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

<iframe id='AmazonMusicEmbedB00AYBU7TE' src='https://music.amazon.com/embed/B00AYBU7TE/?id=E1f35ztHpL&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *; clipboard-write" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/american-iv-the-man-comes-around/1440811207"></iframe>

<iframe src="https://embed.tidal.com/albums/35467963" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>