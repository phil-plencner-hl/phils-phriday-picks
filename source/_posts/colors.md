---
title: Between The Buried And Me - Colors
date: 2021-08-20 08:10:29
tags:
- between the buried and me
- dream theater
- metalcore
- progressive rock
- death metal
---

Since Between The Buried And Me released their highly anticipated new album *Colors II* today, I decided to choose the original *Colors* for today's Pick.

Between The Buried And Me started out as a pretty typical metalcore band of the early 2000s putting out a couple albums that didn't think too far outside the genre's box. 

That changed once they shuffled the lineup and included drummer Blake Richardson, guitarist Dustie Waring and Dan Briggs on bass in 2004-2005.

The band went in a much more progressive rock direction going forward, starting with the release of *Alaska* in late 2005. It was still very much a metalcore album, but included more technical instrumental sections and longer songs.

They completely turned into a progressive rock band with *Colors* in 2007 (albeit one who was still very influenced by metalcore and death metal). 

*Colors* is an incredible concept album that runs over an hour, but only has eight songs (a couple of the songs come close to the 15-minute mark!). Every song flows directly into each other, making it seem like the entire album is one giant 65 minute composition.

The amount of stylistic ground they cover is mind-boggling. Elements of death metal, jazz fusion, space rock...and even a bit of country are all part of the complex puzzle.

My favorite song that encapsulates the whole album, is the final track "White Walls". 

Shortly after they released the album, Between The Buried and Me toured as part of Dream Theater's "Progressive Nation" show in 2008 along with Opeth. I was lucky enough to catch a show on this tour in Cleveland and Between The Buried And Me blew the other bands away that day (and I'm a big fan of the other bands too!). I think they only played 2-3 songs (all from *Colors* including "White Walls") but it was still a 45 minute set. Completely amazing.

After that tour they played the entire *Colors* album for a few additional shows. They recorded one of the shows and released it as the excellent *Colors Live* DVD soon after. If you want to see them work their magic, [the entire performance is on Youtube.](https://www.youtube.com/watch?v=mu2Lx1wr4rM&list=PLEZafYMWbXig-CRPWD08PJyVNoS3N2XJh)

Between The Buried And Me continue to regularly put out albums that push the envelope of prog rock and I pretty much recommend them all. However, *Colors* is still excellent and is when they really started to take flight. 

I cannot wait to dig into the just-released *Colors II*, but for now I will recommend revisiting the original.

<iframe src="https://open.spotify.com/embed/album/56mXsvBsKgRCXgmtzOAC22" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

