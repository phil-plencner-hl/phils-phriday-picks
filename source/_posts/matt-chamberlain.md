---
title: Matt Chamberlain - s/t
date: 2021-07-16 13:48:35
tags:
- matt chamberlain
- edie brickell
- pearl jam
- tori amos
- 90s rock
- alternative rock
---

Matt Chamberlain is a fine example of an amazing drummer you have probably heard, but didn't realize it.

His first taste of mainstream success was his work as a member of Edie Brickell And The New Bohemians (post *Shooting Rubberbands at the Stars*)

He briefly was in Pearl Jam (toured with them in '91 but did not appear on a record) and toured with Soundgarden when Matt Cameron was not available.

He was also the Saturday Night Live house-band drummer in the early 90s when Adam Sandler, Chris Farley, David Spade etc were on the cast.

Additionally, he was the drummer on every Tori Amos album from 1998's *From The Choirgirl Hotel* through 2009's *Midwinter Graces*. 

Needless to say, he got around.

Eventually he involved himself in the Web of Mimicry scene with Trey Spruance (Faith No More / Secret Chiefs 3) and put out a solo album on Spruance's label in 2005. This is today's pick.

He self-described it as as "an imaginary soundtrack to an Asian-Western-Sci-Fi-Horror Movie".  Sounds right to me.

Some heavy hitter's appear with him on the album, including violinist Eyvind Kang, Warr Guitar player Trey Gunn (King Crimson) and Jon Brion (Aimee Man) just to name a few.

<iframe src="https://open.spotify.com/embed/album/5Hlstj1ol9pOxH1du2OhDW" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
