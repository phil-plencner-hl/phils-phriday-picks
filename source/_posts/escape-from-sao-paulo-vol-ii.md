---
title: Escape From Sao Paulo Vol. II
author: Phil Plencner
date: 2020-09-18 17:04:55
tags: 
- brazil
- sao paulo
- dance music
---
Last week I stumbled across this amazing compilation of undeground dance music from Sao Paulo, Brazil! 

Some pretty crazy / trippy stuff on here and it has been in heavy rotation here at PPP headquarters this week! 

Dig in!

<iframe src="https://open.spotify.com/embed/album/1M9Rmdw8lo44pSQpqSZu8J" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>