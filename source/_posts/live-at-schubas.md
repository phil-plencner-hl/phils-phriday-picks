---
title: Verbow - Live At Schubas
date: 2022-01-21 10:13:55
tags:
- verbow
- jason and alison
- bob mould
- superchunk
- brad wood
- alternative rock
---

Jason Narducy and Alison Chesley started out playing together as an acoustic duo under the name "Jason & Alison". Jason played guitar and Alison played cello. They wrote cool alternative rock songs with a lot of muscle, considering the instrumentation. I saw them a couple times playing coffee shops in Chicago and bought their *Woodshed* CD. Early fan.

Eventually they became an electric quartet, adding a bass player and drummer. They called themselves Verbow. Verbow caught the eye of Bob Mould, who ended up producing their excellent first album *Chronicles* in 1997. 

Here they are during that time, performing the album's single "Holiday" at Park West:

<iframe width="560" height="315" src="https://www.youtube.com/embed/SqSoHY-Bres" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Sadly, Verbow only released one other album (The Brad Wood produced *White Out* in 2000) before breaking up.

In the meantime, they played a lot of shows, especially in Chicago at Schubas Tavern. I attended a couple of those concerts, and the shows were fun and high-energy.

Thankfully, they released highlights from their shows on an album aptly called *Live At Schubas*, which is today's pick. While the studio albums are great, I think they were an even better live act and this record showcases why.

After Verbow, Alison formed a new band called Helen Money. Jason formed a band called Rockets Over Sweden, but had greater success playing bass with Bob Mould (appearing on his last few studio albums), Liz Phair and Superchunk.

<iframe src="https://open.spotify.com/embed/album/6ZoMzTFNgLqrRFGxhgaevb?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>