---
title: Zoogz Rift - Idiots On The Miniature Golf Course
date: 2021-12-03 12:52:37
tags:
- zoogz rift
- sst records
- frank zappa
- captain beefheart
- progressive rock
- 70s rock
- jazz rock
---

Zoogz Rift was a very fascinating and strange individual.

I first heard of him back in the mid-90s. He would cross-post advertisements for handmade compilation cassettes of his best songs on Usenet forums like alt.fan.frank-zappa and alt.fan.captain-beefheart that I would frequently read in my college dorm. 

There is still an archive of Usenet forum posts online, so [you can still see excellent examples of the types of things I would see](https://www.usenetarchives.com/view.php?id=alt.fan.zoogz-rift&mid=PDRrMTc3ZCQ2akBubnJwMS5uZXdzLnByaW1lbmV0LmNvbT4).

He was constantly posting about how his music was inspired by Captain Beefheart and Frank Zappa. Eventually I gave in and ordered the tapes. 

I was blown away by what I heard! It really was incredible stuff. Taking the weirdest and wildest elements of Zappa and putting them into a blender at high speed.

He was signed to SST records during the 80s, but by the mid-90s was very obscure and pretty much forgotten about.  I started collecting those records such as *Water II: At Safe Distance* and the compilation album *Looser Than Clams*. They were good but most of the weirder material wasn't on them.

In the early 2000s, [Zoogz started promoting a complete collection of material on Usenet](https://www.usenetarchives.com/view.php?id=alt.fan.zoogz-rift&mid=PDU2OTJhOTdjLjAyMDgyNjAwNDkuMTI0NGU3MzFAcG9zdGluZy5nb29nbGUuY29tPg). Since it was hard to find his albums, I jumped at that chance and ordered the mp3s as well. In this case, he was pretty ahead of his time. Selling mp3s on CD-Rs in 2002!

Through that I was able to discover that my favorite material was on his earlier, self-released albums such as *Idiots On The Miniature Golf Course*, which is today's pick!

There is lots of odd-time-signature rock and roll on here with absurd lyrics. There are also elaborate marimba and horn arrangements. Plus his guitar playing is raw and cool.

Zoogz also semi-famously was a promoter / announcer for Universal Wrestling Federation. Pretty wild stuff.

Unfortunately, he passed away in obscurity in 2011 from complications with diabetes.

Luckily, the bulk of his material is available on Spotify today, including *Idiots On The Miniature Golf Course*.

<iframe src="https://open.spotify.com/embed/album/164q0VCAI4wJvEYkrNDorh?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>


