---
title: Behold The Arctopus - Horrorscension
date: 2020-11-23 13:06:06
tags:
- Behold The Arctopus
- Colin Marston
- Weasel Walter
- Death Metal
- Progressive Rock
---

Today's pick is the absolutely bonkers 2nd album by the instrumental technical death metal band Behold The Arctopus called *Horrorscension*.

Behold The Arctopus play instrumental music inspired by both heavy metal and 20th century classical music. This technical and progressive style is executed on a regular electric guitar and drum kit but involves a 12-string Warr guitar, an instrument that covers the range of a bass and guitar and is generally played by tapping with either one or two hands.

BTA's original drummer, Charlie Zeleny, recently left the group to be a full-time session musician, so PPP favorite Weasel Walter (of the Flying Luttenbachers fame) became their new drummer. This album focused on a very hyper-focused, extremely complex, convoluted style of death metal that I believe is a benchmark of all future metal albums.

Blinding speed with thorough-composed song structures that take many listens for your brain to wrap around and comprehend. This is not easy listening by any stretch of the imagination, but is well worth the time you put into it.

<iframe src="https://open.spotify.com/embed/album/6wg22SVyKBCsjoCdj7uDC7" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe> 