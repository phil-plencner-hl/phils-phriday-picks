---
title: Bedsore - Dreaming the Strife for Love
date: 2025-01-24 22:03:26
tags:
- bedsore
- 20 buck spin
- tomb mold
- immortal bird
- death metal
- avant-garde metal
- progressive rock
- progressive metal
- 70s rock
---

![](DreamingTheStrifeForLove.jpg)

As many of you know, I am somebody who dives pretty deep into the world of underground death metal. It's not for everyone, but it is definitely for me! When you immerse yourself into the genre it is pretty obvious that while there is a lot of great metal from the United States, there is also a ton that originates from elsewhere in the world.  Much of that is even more extreme and forward thinking than what is happening stateside!

There are a few record labels that I keep tabs on their releases, because they have the pulse on what is new and unique in extreme metal around the globe: Nuclear War Now! Productions, Comatose Records, I Voidhanger Records and especially 20 Buck Spin. 

20 Buck Spin is a Pittsburgh based label. They don't seem to put out as many records as the other labels I mentioned but I think that's because they subscribe to the philosophy of quality over quantity. Some bands worth checking out on their label include Tomb Mold and Immortal Bird. However, one band I particularly am fond of is an Italian group called Bedsore.

They started out as a progressive death metal band not too far off from what groups like Death were doing on classic records like *Human* and *Individual Thought Patterns*. Longer songs with a ton of shifting parts and highly technical playing coupled with guttural, screaming vocals. 

Bedsore's first record on 20 Buck Spin, *Hypnagogic Hallucinations*, is a brutal slab of progressive death metal. The song that closes the record, "Brains on the Tarmac", is one of my favorites...they really stretch out here:

<iframe width="560" height="315" src="https://www.youtube.com/embed/44ifc2IgHto?si=Q6WqTmb7hgTCf01s" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

They eventually started pushing the envelope more. Taking cues from bands like Opeth they included more synthesizers and started writing epics that are not far off in scope from what bands like Yes were doing with *Close To The Edge*. I'm not kidding. A fine example is their side of a split release they did with the band Mortal Incarnation. It's a 15+ minute prog suite called "Shapes from Beyond the Veil of Stars and Space". It starts out with very overture like 70s progressive rock inspired synthesizer groove that gradually builds to a full-on metal explosion. There is a ton of shapeshifting melodies and fluctuating dynamics that keep the song exciting and interesting throughout the entire lengthy runtime:

<iframe width="560" height="315" src="https://www.youtube.com/embed/GjBT_B2JuHY?si=COqOkJnwOCe6Imr-" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Bedsore is not just a studio concern. They are also apparently a tight live act. I don't believe they have brought their shows to the United States yet, but judging from footage like this I am eagerly awaiting that possibility!

<iframe width="560" height="315" src="https://www.youtube.com/embed/PcKizHi_8rY?si=D-5E7NaTiuk9_nh1" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

That incredible show includes selections from *Hypnagogic Hallucinations* and even a full performance of "Shapes from Beyond the Veil of Stars and Space"! I was definitely floored. 

Even with these progressive rock inspirations and ambitions I was not ready for what came next. Late last year, they released a new full length record called *Dreaming the Strife for Love* which is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

For their new record, Bedsore fully embrace their 70s progressive rock tendencies. The album is chocked full of synthesizers, Mellotron, organ, and 12 string guitar to go along with their more traditional death metal instrumentation. It even includes guest musicians on saxophone, flute, trumpet and trombone. Full orchestration! It's almost like if 1974-era Emerson, Lake and Palmer played death metal and the Italian band Goblin sits in with them. Completely crazed and amazing!

A fine example of their recent sound is "Scars of Light". They released a mind-bending animated music video for it:

<iframe width="560" height="315" src="https://www.youtube.com/embed/yJ2ng_vLb9Q?si=4XfRw1Oh7J8hL0eI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Bedsore's progressive rock ambitions don't stop with the music alone. The entire album is apparently inspired by a book written in the 1400s called *Hypnerotomachia Poliphili*, which was written by a Venetian Catholic Priest named Francesco Colonna. I haven't read the book, but [this summary of it makes it sound like a bonkers, psychedelic journey of love and lust](https://echoesofegypt.peabody.yale.edu/hieroglyphs/hypnerotomachia-poliphili). Sounds perfectly inscrutable for a progressive rock concept record!

The story on the album is so convoluted that Bedsore felt the need to turn it into their own book! Not only does it apparently include detailed explanations of all the themes and lyrics throughout it also includes sheet music and illustrations! Wow! The book is currently only available at their live shows (please come to the U.S. Bedsore, I am begging you!) but they are teasing that it'll one day be available on their Bandcamp page as well. Here are the details straight from their Facebook page:

<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fbedsoredeath%2Fposts%2Fpfbid02Yi3x7ZZNcmYdbDpx1jCjsSzXU7hWbyLkRLAETDMumRC99xwYufkEpFE3zG3ndFD4l&show_text=true&width=500" width="500" height="718" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>

Speaking of their Facebook page, they also made separate posts with brief explanations of all six songs on *Dreaming the Strife for Love* as well: 
- [Minerva's Obelisque](https://www.facebook.com/share/p/1DUL2kHuWD/) 
- [Scars of Light](https://www.facebook.com/share/p/19kTTfkQZR/)
- [A Colossus, an Elephant, a Winged Horse the Dragon Rendezvous](https://www.facebook.com/share/p/18ZwUnnjid/)
- [Realm of Eleuterillide](https://www.facebook.com/share/p/188zGTi1bR/)
- [Fanfare for a Heartfelt Love](https://www.facebook.com/share/p/1KuXf8WL1Q/)
- [Fountain of Venus](https://www.facebook.com/share/p/18WXyMt24K/)

Obviously, this is a lot to take in. This is part of the appeal of the album for me. There is so much to chew on, it is endlessly replayable to me as I try to unpack all the music as well as the story (this is especially challenging for me since the lyrics are all in Italian).

While most of the songs on the album are lengthy (average length is 6-8 minutes each) the centerpiece of the album is the 15 minute "A Colossus, an Elephant, a Winged Horse the Dragon Rendezvous". This is a highlight that includes everything but the kitchen sink. Musical ideas pile on top of each other and completely overwhelms your senses. If there's only one song you hear from *Dreaming the Strife for Love*, let it be this one:

<iframe width="560" height="315" src="https://www.youtube.com/embed/clRu5tKeI6k?si=2poD3zPPYMSYvZ-Q" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

While this style of intense, extreme music is admittedly for a niche audience I still think it deserves wider acclaim. It is a record I've played quite a bit and does not get old. It'll probably stay in heavy rotation for me for many months to come.

{% iframe 'https://open.spotify.com/embed/album/3KFaCfyta9b7t08QAXuI5P?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B0DJH8VZFK/?id=2vUL4Zv43a&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/dreaming-the-strife-for-love/1772303975' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/391378689' '100%' '96' %}