---
title: Juliana Hatfield Sings ELO
date: 2023-11-17 13:34:11
tags:
- juliana hatfield
- electric light orchestra
- 90s rock
- 70s rock
- alternative rock
- punk rock
---

![](JulianaHatfieldSingsELO.jpg)

Musician Juliana Hatfield (of Blake Babies, Lemonheads and The Juliana Hatfield Three fame) has been recording a series of cover albums over the past several years. In 2018 there was *Juliana Hatfield Sings Olivia Newton John*. Then in 2019 came *Juliana Hatfield Sings The Police*. Today, the 3rd installment in the series has been released: *Juliana Hatfield Sings ELO*.

I have been awaiting this release for a long time. She released the excellent first single from the album, "Don't Let Me Down" back in May. Half a year later, we finally get the entire record (although two further singles "Can't Get It Out Of My Head" and "Telephone Line" were released in between).

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

Like the previous covers albums, Juliana Hatfield plays all the instruments (except the drums which were played by Chris Anzalone). Unlike the original Electric Light Orchestra tunes, there are no large string sections involved. She instead transposed the orchestration down to keyboard or guitar...and even occasionally singing them such as on "Showdown". 

She also doesn't stick to just the ELO classics, choosing to play "Ordinary Dream" off of *Zoom*, which was originally released in 2001. 

The record is fun and exciting, yet intimate. She really hits the sweet spot between the overblown Jeff Lynne orchestrations and a garage rock band. 

I really liked *Juliana Hatfield Sings The Police* when it first came out and didn't think she could top it. After listening to *Juliana Hatfield Sings ELO* a few times already I believe I have been proven wrong. This is probably the best record in her covers project yet! I cannot wait to hear what she has in store for us next.

{% iframe 'https://open.spotify.com/embed/album/6mMU8OKg3obE2D8xZzdQVE?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B07VHBTJ8D/?id=eyv3pY9ewk&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/juliana-hatfield-sings-elo/1685555953' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/293728069' '100%' '96' %}

