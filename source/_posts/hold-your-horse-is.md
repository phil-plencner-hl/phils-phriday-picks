---
title: Hella - Hold Your Horse Is
date: 2021-01-22 09:05:25
tags:
- hella
- death grips
- zach hill  
- brutal prog
- prog rock
- 00s rock
- avant rock
---

Today's pick is the debut album by avant-rock duo Hella called "Hold Your Horse Is". At the beginning Hella comprised of guitar and drums, but they made a large enough racket to sound like a march larger group (they later expanded to a 5-piece band for "There's No 666 In Outer Space"...but I digress).

"Hold Your Horse Is" is a very mathy, yet melodic and jazzy instrumental masterpiece. For me, drummer Zach Hill is obviously the focus, and he is ferocious here. He plays otherworldly beats with finesse, precision and power and adds to the melodic nature of the songs.  

Subsequent albums got much noisier and obtuse and are not for the faint of heart. "Hold Your Horse Is" is a good start before you dive deeper into their challenging catalog.

I caught Hella several times during their duo era and the shows were mind-bogglingly good. They eventually put out a DVD as one half of the "[Concentration Face](https://www.youtube.com/watch?v=lYvw5w1Qs2E) / Homeboy" release. It is well worth watching if you want to see them actually pull this stuff off in a live setting.  

Zach Hill later went on to greater success / notoriety as part of the experimental rap group Death Grips. While they have their moments, I don't think they ever reached the heights of Hella, especially this album.

<iframe src="https://open.spotify.com/embed/album/2RHAyIlKGMvJ8cHlK4zpXw" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
