---
title: Return To Forever - Romantic Warrior
date: 2021-02-19 16:52:16
tags:
- return to forever
- chick corea
- al di meola
- lenny white
- stanley clarke
- jazz fusion
- 70s rock
- miles davis
---

Last week the jazz world lost a giant in Chick Corea. His career ran the gamut from electric jazz fusion, to funk to jazz standards. Starting out his professional career with Miles Davis certainly was a nice springboard, and he did some pioneering things on albums such as *In A Silent Way*, *Bitches Brew* and *On The Corner*. He also is renowned for his duet work with Gary Burton. However, this post is going to focus on his amazing fusion band Return to Forever and *Romantic Warrior* (which in my opinion is their commercial and artistic peak).

Return to Forever started out as a much more subdued jazz rock band, but after a few albums and lineup changes they turned into a highly technical, jazz fusion group with progressive rock tendencies. Obviously this is right up my alley.

The "classic" lineup that recorded *Romantic Warrior* was Chick Corea (on mountains of keyboards and pianos), Stanley Clarke (on bass), Al Di Meola (on guitar) and Lenny White (on drums).

This is jazz that rocks hard, with synthesizers and distorted guitar with pummeling drumming. While there are some amazing solos throughout the songs from all the members, the melodic unison sections of the songs are tightly played and are amazing.

The centerpiece of the album is the 12 minute "Duel of the Jester and the Tyrant (Parts I and II)" which has to be heard to believe. In fact, I first bought this album from a college radio station record sale (for just a few dollars). I had heard of Chick Corea, but not this band...but what really sold me was a college DJs single word review of this song ("UNREAL!") marked in pen and then circled several times. All these years later, I still completely agree with this assessment. This is [documented on my Instagram profile](https://www.instagram.com/p/Be9Wt9dnsBm/?utm_source=ig_web_copy_link) (which you should follow!)

<iframe src="https://open.spotify.com/embed/album/2mLtPMLV5nWE0rzjVvcEmt" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>