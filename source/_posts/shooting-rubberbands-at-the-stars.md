---
title: Edie Brickell and New Bohemians - Shooting Rubberbands At The Stars
date: 2023-07-28 12:54:41
tags:
- edie brickell
- brad houser 
- new bohemians
- critters buggin
- 80s rock
- alternative rock
---

![](ShootingRubberbandsAtTheStars.jpg)

There were a bunch of notable musicians who passed away in the last week: Tony Bennett, Sinead O’Connor, and Randy Meisner (founding bass player of The Eagles). 

One musician that also passed away this week who hasn't been given the same attention is Brad Houser, [who passed away from a stroke on July 24th](https://dallas.culturemap.com/news/entertainment/brad-houser-new-bohemians-death/). 

He is perhaps best known for being the bass player (and founding member) of New Bohemians. That group became very famous once singer Edie Brickell joined the group and released *Shooting Rubberbands At The Stars* in 1988. This is today's pick!

"What I Am" was the monster hit on this album, and rightfully so. Here is the music video for the song:

<iframe width="560" height="315" src="https://www.youtube.com/embed/tDl3bdE3YQA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Here are Edie Brickell and New Bohemians playing the same song on *Late Night with David Letterman* in 1988:

<iframe width="560" height="315" src="https://www.youtube.com/embed/AR_NtkIhA0g?start=110" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

In both videos, the band is highlighted. In fact, there is lots of great footage of Brad Houser playing in those clips (you can especially see him getting WAY into the song on Letterman's show). 

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

I believe that a huge part of what made *Shooting Rubberbands At The Stars* sound so amazing is Brad Houser's excellent fretless bass playing throughout. The album is mixed well, and you can hear his tasteful licks in every song. 

The 2nd single from the album was "Circle" (which I think is an even better song that "What I Am"). It came with another video that highlighted the band:

<iframe width="560" height="315" src="https://www.youtube.com/embed/q_GkjymuQ9U" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Aside from the singles, the rest of the album is equally as strong. "Air of December", "The Wheel" and "Keep Coming Back" (which is comparatively fast and rockin' from the rest of the record) are all well-crafted, catchy pop songs. 

Edie Brickell and New Bohemians put out a follow up album *Ghost of A Dog* in 1990 that wasn't nearly as strong. They broke up for a while and eventually reformed in 2006 and 2018...never again reaching their superstar status.

I'd also like to point out that while not performing in New Bohemians, Brad Houser was also a member of the incredible instrumental group Critters Buggin' (with drummer Matt Chamberlain, saxophonist Skerik and percussionist Mike Dillon). That is also a fun rabbit hole to dive down. Check out this crazy live footage of them from 1999:

<iframe width="560" height="315" src="https://www.youtube.com/embed/lNL-dU_q-i0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Anyways, *Shooting Rubberbands At The Stars* is an album I come back to regularly and this week is a great time to revisit it if you haven't heard it for a while. I think you would be pleasantly surprised how well it holds up after 35 years.

{% iframe 'https://open.spotify.com/embed/album/2oZzKVi2mqkQnZOad6DXMw?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B000V63CWQ/?id=OSgIzC5x6E&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/shooting-rubberbands-at-the-stars/1440916509' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/2527321' '100%' '96' %}