---
title: Glenn Kotche - Mobile
date: 2023-03-24 22:36:47
tags:
- glenn kotche
- wilco
- jeff tweedy
- grand ulena
- darin gray
- alternative rock
- drums
- jazz
---

![](Mobile.jpg)

When you think of the band Wilco, you don't typically think of them as a drum-focused group. This is unfortunate, because their drummer, Glenn Kotche, is a very unique and interesting percussionist.

He joined Wilco in 2002 and the first album he played on was *Yankee Hotel Foxtrot*. This came about because Wilco front man Jeff Tweedy played a show with experimental guitarist Jim O'Rourke in 2000. Jim knew Glenn Kotche previously and introduced him to Jeff. They ended up forming an experimental trio called Loose Fur. This lead to all of them working together on *Yankee Hotel Foxtrot*. Glenn joined Wilco as a full-time member, but Jim essentially was a session musician on the album.

"Jesus Etc." is one of the highlights from that album. Here is Wilco, with Glenn Kotche playing the song on The Late Show with David Letterman:

<iframe width="560" height="315" src="https://www.youtube.com/embed/v4_O4Sj-XTs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

While that song is a pretty straightforward rock song, much of *Yankee Hotel Foxtrot* had a more experimental vibe with less traditional drums and percussion. An example of that is "I Am Trying To Break Your Heart". Here is Glenn Kotche playing the elaborate percussion parts in a solo format:

<iframe width="560" height="315" src="https://www.youtube.com/embed/y3paspn2oYw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

You can see and hear the elaborate drum set that Glenn typically plays there, including live with Wilco...and it is pretty awesome!

At the same time Glenn was working on solo material. When I say "solo" I mean exactly that. He ended up writing 7 compositions for vibraphone, kalimba, mbira, and drum kit and recorded them all (along with a cover of Steve Reich's "Clapping Music") for an album called *Mobile*, which is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

One of the highlights is the 11+ minute "Monkey Chant" which makes it feel like you're stuck in the jungle, surrounded by local wildlife with a drum ensemble performing nearby. Way cool!

In 2005 and 2006, during a break from touring with Wilco, Glenn Kotche went on a solo tour playing songs from *Mobile* along with other original material. I was lucky enough to catch him in Evanston, Illinois on that tour and it was incredible to witness.

Here he is performing "Monkey Chant" in 2006 to give you an idea of what those shows were like:

<iframe width="560" height="315" src="https://www.youtube.com/embed/oJXMxkLEO2M" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Glenn Kotche continues to play with Wilco in present day and also occasionally releases solo and percussion-based albums as well. *Mobile* remains my favorite album of his and is continually inspiring.

{% iframe 'https://open.spotify.com/embed/album/3xG6EfATGqx3naZ8wYVQNz?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B0014JBL22/?id=FNVLdylcPj&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/mobile/127969528' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/2996698' '100%' '96' %}
