---
title: Frank Zappa Friday
author: Phil Plencner
date: 2019-04-26 16:04:10
tags:
- frank zappa
- jazz rock
- jazz
- 60s rock
- 70s rock
- progressive rock
---

I declare today *Frank Zappa Friday*. Presenting a trifecta of personally compiled Zappa playlists!

First up is a re-creation of an all-vinyl DJ set I performed last winter. 30 minutes, all killer, no filler:

<iframe src="https://open.spotify.com/embed/playlist/4D474nTn4cpti9Mz7iX57B" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

The 2nd playlist is a re-creation of a compilation CD I used to burn and distribute to friends who were curious about what all the fuss about Zappa was about in the late 90s/early 2000s!  A good mix of his instrumental excursions along with his (perhaps politically incorrect) vocal selections:

<iframe src="https://open.spotify.com/embed/playlist/16bNh0tTpMaISk4DbtEKLn" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

And finally, a completely bonkers, massive playlist thats over 6 hours of THE HARD STUFF. Crazy prog-rock and jazzy instrumentals, long guitar solos, 20th century classical homages! Not for the faint of heart and for true fans only:

<iframe src="https://open.spotify.com/embed/playlist/1EWx6ggW6e6t3hSbI9XGk0" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>