---
title: Billy Cobham - Shabazz
date: 2022-07-01 09:11:19
tags:
- billy cobham
- john abercrombie
- brecker brothers
- mahavishnu orchestra
- jazz fusion
- jazz rock
- 70s rock
---

![](shabazz.jpg)

I've waxed poetic before about Billy Cobham here (*Inner Conflicts* was a previous pick back in 2020). His studio albums in the 70s are beyond reproach (especially the timeless *Spectrum* which first blew my mind as a freshman in high school). However, his live concerts in this era were completely ridiculous.

Of course, I cannot talk about live Billy Cobham without at least mentioning his time in the pioneering jazz rock outfit The Mahavishnu Orchestra. Here is a full set from 1972, with Billy Cobham practically stealing the spotlight from guitarist John McLaughlin throughout.

<iframe width="560" height="315" src="https://www.youtube.com/embed/B8X6kt5y2Lg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Soon after this he released *Spectrum* with Mahavishnu keyboardist Jan Hammer and guitarist Tommy Bolin (who later had a short stint with Deep Purple before sadly passing away).

Cobham picked up the pieces from the short-lived *Spectrum* band by joining forces with the Brecker Brothers and guitarist John Abercrombie. They released a excellent studio album *Crosswinds* and hit the road.

This band was absolutely on fire!! Taking songs from *Spectrum* along with new material and pushing the tempos and volume up to bonkers levels was the name of the game. 

Luckily much of this was recorded. Here is the band playing "The Pleasant Pheasant" and "Red Baron":

<iframe width="560" height="315" src="https://www.youtube.com/embed/-TaaCkjoITQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Here they are absolutely blazing through a new tune called "Tenth Pinn", bookended by jaw-dropping Cobham drum solos:

<iframe width="560" height="315" src="https://www.youtube.com/embed/YNyYzfkn4yc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

 This material has been in heavy rotation for me the past week. Yesterday, I went out to the local record store to find the live album documenting this band and tour called *Shabazz*. It was kismet and I bought it without thinking twice.

 Coincidentally (and hilariously!) my good friend Patrick in Chicago also bought *Shabazz* yesterday! If that isn't fate (and motivation for choosing it as today's pick) I don't know what is.

 A short, yet powerful recording! Along with "Tenth Pinn" and the title track it also features two *Spectrum* tunes ("Red Baron" and "Taurian Matador"). The band is in top form absolutely blazing through the songs. 

 Essential listening for Billy Cobham fans or jazz fusion enthusiasts in general.

 <iframe src="https://open.spotify.com/embed/album/1O66eTfMf0CBfL9gSoo7Ow?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

 <iframe id='AmazonMusicEmbedB00122LYCI' src='https://music.amazon.com/embed/B00122LYCI/?id=Fod7CdfNln&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

 <iframe allow="autoplay *; encrypted-media *; fullscreen *; clipboard-write" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/shabazz-recorded-live-in-europe/256716345"></iframe>

 <iframe src="https://embed.tidal.com/albums/10922595" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>