---
title: Bruce Ditmas - Yellow Dust
date: 2023-03-03 22:27:06
tags:
- bruce ditmas
- jaco pastorious
- gil evans
- thad jones
- jazz
- big band jazz
- avant garde jazz
- drums
- moog
---

![](YellowDust.jpg)

Bruce Ditmas is a drummer who straddles two different worlds. He first came into success by playing in Judy Garland's big band and backing Barbra Streisand in the 1960s.

Eventually he started playing in more interesting groups. In the 1970s he was playing with trumpeter / composer Thad Jones big band. Here he is in 1974 playing a show in Norway. His powerful playing is really driving this large ensemble:

<iframe width="560" height="315" src="https://www.youtube.com/embed/5JyuHGUdtro" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

He also hooked up with pianist / arranger Gil Evans (who was fresh off his success working with Miles Davis) who also started his own big band but with more of a electric / jazz fusion focus. Also in that group was guitarist John Abercrombie. Here they are in playing in France, also in 1974:

<iframe width="560" height="315" src="https://www.youtube.com/embed/CCPCc_xZnbw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

This group was essential for Bruce Ditmas for many reasons. Abercrombie's presence led him to working with a ton of artists associated with the ECM record label over the decades. He also was able to meet Jaco Pastorius and was part of his big bands before he joined Weather Report.

Along with all that, Gil Evans also loaned him a bunch of high tech gear (drum machines, Moog synthesizers) to experiment with. 

Those wild experiments lead to Bruce Ditmas' two solo albums that he released in 1977: *Aerey Dust* and *Yellow*. They are both extremely rare and hard to find. Nevertheless, those that were able to come across them were confronted with really crazed avant-garde funk and jazz performed completely by Bruce Ditmas' on a Mini-Moog, an Arp 2600 and tons of electronically treated percussion.

Luckily, U.K.-based archival label Finders Keepers collected the best music from those two albums and released them as *Yellow Dust* for a wider audience. 

This is an essential collection that really still sounds out-of-this-world even today...which is why it is today's pick! A free jazz freakout with Moog / Arp synths and drums exploding all over the place that is an overwhelming tour-de-force which is oddly accessible even while being completely bonkers. I recommend you play it at maximum volume and wake up the neighbors! 

<iframe src="https://open.spotify.com/embed/album/0eSbT6OjOqv91Wrg0MSlD0?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

<iframe id='AmazonMusicEmbedB00UCQNEDW' src='https://music.amazon.com/embed/B00UCQNEDW/?id=vWrgnsL04J&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *; clipboard-write" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/yellow-dust/974602406"></iframe>

<iframe src="https://embed.tidal.com/albums/43365748" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>
