---
title: COVID Christmas 2020
date: 2020-12-15 10:53:20
tags:
- christmas music
---

Christmastime is here again! However, things are a little different this year with the raging pandemic outside.

This doesn't stop my yearly tradition of making a new Christmas Music mix, as I have since I was burning CD-Rs back in 2008. 

This year's mix is mostly songs from albums released in 2020 with a few modern classics sprinkled in for good measure.

Previous mixes are also listed below...including the now bursting-at-the-seams 14.5 hour megamix!

Happy Holidays! Wear a mask!

Happy Holiday's everyone!

<iframe src="https://open.spotify.com/embed/playlist/6ZEzVZLOLb7jEWAWwFMOeR" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

- [14.5 HOUR MEGA MIX!](https://open.spotify.com/playlist/5NqMi2wSRDRXP5L4G5McRW?si=ZwmbuH3iTkyzRb3IihO9UA)
  [Modern Christmas 2019](https://open.spotify.com/playlist/1NiCJLrp4eX6cGvSwYBd7I?si=EWPGwVPVR9qe4WWVUyUQaw)
- [It's Christmas Again? 2018](https://open.spotify.com/playlist/0KVsnggEkc7MftYJIRmmZl?si=cIhjjkEySeKKOC4g5AyCQA)
- [New Christmas Classics 2017](https://open.spotify.com/playlist/73rcF9IZMKENn9xoZ6aLE2?si=esHyVl4jTJOvE8950mxAcw)
- [Merry Christmas 2016](https://open.spotify.com/playlist/15Q28CMAlNDhnxvpdzPril?si=q7XgWr3hRRiyDtsDDxBD_Q)
- [Festive 2015](https://open.spotify.com/playlist/32yvyJJiGGcxAJVwf4VyOZ?si=HqEeppCZSvmFLVtK-fIB7w)
- [80s Christmas 2014](https://open.spotify.com/playlist/4YEulevL9dKg0uXmXgUfML?si=KojXWrunRk69fNoxHSNf_g)
- [Jazzy Christmas 2013](https://open.spotify.com/playlist/1WQE7qzJugPui8TsK4EngF?si=a_PCH1vJTBqLMSh9a6ik2A)
- [Sounds of Christmas 2012](https://open.spotify.com/playlist/13KoC1HiL5NOM153QjrJk8?si=2gKKSXoJQ0OrSx21LiIBLg)
- [cRaZy ChRiStMaS 2011](https://open.spotify.com/playlist/5t2qUwrQHDiMpakUa2j7KM?si=a_zUuWcOSAmUWcrZZKxeTQ)
- [Wacky Christmas 2010](https://open.spotify.com/playlist/3KPGMZ8X8J1MDZa4NM6XIE?si=pUxfVmnSQGKZuNlDsUSXjA)
- [Ultimate Christmas 2008](https://open.spotify.com/playlist/3AylbZDuVKYQH7g2ik5ciw?si=jtjSA8thTLyrbjf1AAkGYQ)