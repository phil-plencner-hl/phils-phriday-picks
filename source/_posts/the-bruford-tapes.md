---
title: Bill Bruford - The Bruford Tapes
author: Phil Plencner
date: 2020-03-23 16:09:47
tags:
- bill bruford
- king crimson
- yes
- genesis
- 70s rock
- progressive rock
- jazz fusion
- jazz rock
---
Do you want to hear a jazz-rock band playing with the energy and intensity of a punk band? Of course you do! 

Bill Bruford's band in the late 70s (after he quit UK and before he re-joined King Crimson) fits that description. Especially so on their live album *"The Bruford Tapes"* which was originally a radio broadcast of a New York show in 1979.

<iframe src="https://open.spotify.com/embed/album/2UZQwYg1JLs9TZviXubsOW" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
