---
title: Jazz For Our Times
author: Phil Plencner
date: 2020-06-12 22:20:01
tags:
- free jazz
- avant jazz
- 70s rock
- jazz
---

I've been spending a few weeks sketching and crafting today's Pick and I feel like it's finally ready to start sharing with humanity...a massive, epic, inspiring playlist called Jazz For Our Times. 

80 songs. Over 14 hours. These recordings showcase Black jazz artists focusing on the era of the late 60s and early 70s. Many of the tunes are focused on topics like Civil Rights, Black Power, Black Heritage and other relevant subjects. 

No "standards" or smooth-jazz here! This music is as challenging as the subject matter: Avant-Garde, Free Jazz, Afrobeat, World Fusion and even a touch of Hard Funk. 

Evan at 14 hours, it really still only scratches at the surface of this truly American art form. I've been fanatically collecting the albums featured here for close to 30 years, ever since I first stumbled upon *'The Major Works Of John Coltrane'* [back in 1992](https://www.discogs.com/John-Coltrane-The-Major-Works-Of-John-Coltrane/master/243485) ....and its amazing to me how it is all easily available on Spotify now as opposed to digging through bargain bins, record fairs, garage sales, dusty basements, tape trading networks etc to find many of these previously hard to find albums. 

I hope you are able to enjoy the entire collection....and I hope it inspires you to delve deeper into the artists, recordings and topics contained within.

<iframe src="https://open.spotify.com/embed/playlist/1KFECEi4TcN03Vkfq12QO4" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>