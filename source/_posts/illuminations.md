---
title: Carlos Santana & Alice Coltrane - Illuminations
author: Phil Plencner
date: 2020-04-21 15:55:15
tags:
- carlos santana
- alice coltrane
- santana
- jazz rock
- jazz fusion
- free jazz
- jack dejohnette
- dave holland
---
Did you know that Carlos Santana put out a free jazz record with Alice Coltrane, Dave Holland and Jack Dejohnette? In the mid-70s Santana broke up his band and went into wild excursions like this. 

There are other albums from this era that are well worth checking out...but this one is awesome, and it's right here.

<iframe src="https://open.spotify.com/embed/album/2uikR6eZ1HzoGH4Ki6eneU" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>