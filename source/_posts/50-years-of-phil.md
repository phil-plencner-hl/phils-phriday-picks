---
title: 50 Years of Phil
date: 2024-05-08 13:08:05
tags:
- happy birthday
---

![](HelloFifty.jpg)

I'm sending out an early special pick this week. This Friday I will be celebrating my 50th birthday and will be otherwise busy and not picking an album. Wow, I've been on this planet for a half century.

Since I've been picking albums for so long including all genres and decades I wondered if I had selected an album from every year since 1974, the year I was born. Sadly, this is not the case but I did come very close to realizing this goal without even trying.

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

Since 1974 I have only missed picking albums released on these individual 8 years: 1986, 1987, 2009, 2013, 2015, 2016, 2017 and 2018. Not too shabby.

My picks have also been pretty evenly represented by decades. Since 1974 there have been 31 1970s picks, 26 1980s picks, 55 1990s picks, 33 2000s picks, 19 2010s picks, and 19 2020s picks.

Now that I've given you all this useless data, I'll turn it into something enjoyable. I created a (perhaps misnamed) playlist called *50 Years of Phil* that includes one song from one pick from each year. Enjoy this nearly 5 hour 42 song chronological deep dive. I'll be back next week with a regular pick!

{% iframe 'https://open.spotify.com/embed/playlist/7aU0MgwtsghE89WzVprsM3?utm_source=generator' '100%' '352' %}

Here are the picks I highlighted in the playlist, including the year:

- Yes - *Relayer* (1974)
- Hatfield & The North - *The Rotters Club* (1975)
- Return to Forever - *Romantic Warrior* (1976)
- Iggy Pop - *The Idiot* (1977)
- The Residents - *Duck Stab* (1978)
- Zoogz Rift - *Idiots on the Miniature Golf Course* (1979)
- Steely Dan - *Gaucho* (1980)
- Dun - *Eros* (1981)
- Ornette Coleman - *Of Human Feelings* (1982)
- Herbie Hancock - *Future Shock* (1983)
- Minutemen - *Double Nickels on the Dime* (1984)
- Black Flag - *The Process of Weeding Out* (1985)
- Living Colour - *Vivid* (1988)
- Jeff Beck - *Guitar Shop* (1989)
- Megadeth - *Rust in Peace* (1990)
- Ozric Tentacles - *Strangeitude* (1991)
- Izzy Stradlin & The Ju Ju Hounds - *s/t* (1992)
- Cynic - *Focus* (1993)
- Vinnie Colaiuta - *s/t* (1994)
- Mike Watt - *Ball Hog or Tugboat?* (1995)
- Opeth - *Morningrise* (1996)
- Blue Meanies - *Full Throttle* (1997)
- Local H - *Pack Up the Cats* (1998)
- Mr. Bungle - *California* (1999)
- Mike Keneally - *Dancing* (2000)
- Estradasphere - *Buck Fever* (2001)
- Flying Luttenbachers - *Infection and Decline* (2002)
- King Crimson - *The Power to Believe* (2003)
- Mike Keneally - *Piano Reductions Volume 1* (2004)
- Orthrelm - *Ov* (2005)
- Glenn Kotche - *Mobile* (2006)
- Porcupine Tree - *Fear of a Blank Planet* (2007)
- Zach Hill - *Astrological Straits* (2008)
- Mike Patton - *Mondo Cane* (2010)
- Patrick Stump - *Soul Punk* (2011)
- Behold the Arctopus - *Horrorscension* (2012)
- Willie Nelson - *Band of Brothers* (2014)
- J. Robbins - *Un-Becoming* (2019)
- Fiona Apple - *Fetch the Bolt Cutters* (2020)
- Portal - *AVOW* (2021)
- Cheer-Accident - *Here Comes the Sunset* (2022)
- Lydia Loveless - *Nothing's Gonna Stand in My Way Again* (2023)