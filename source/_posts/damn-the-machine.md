---
title: Damn The Machine - Damn The Machine
date: 2022-04-08 11:59:36
tags:
- megadeth
- dave mustaine
- chris poland
- thrash metal
- heavy metal
- hard rock
- 90s rock
---

Dave Mustaine has always been the face of the thrash band Megadeth. He is the front-man / songwriter and the only person to appear in all lineups of the band since their formation in 1983. However, he always surrounded himself with extremely proficient players which helped in their overall sound and success. This is especially true where a fair share of the fancy guitar work and soloing was done by the guitarist who is not Mustaine in the group at the time.

In the early days of Megadeth ('84-'87) this slot was filled by Chris Poland. He appeared on *Killing Is My Business...And Business Is Good!* and *Peace Sells...But Who's Buying?*. Poland's fancy playing is all over these thrash metal classics and really helped shape their sound. 

Here he is playing with the band in 1987 in once of his last appearances in the group keeping pace with Mustaine:

<iframe width="560" height="315" src="https://www.youtube.com/embed/wb2TYQomMmc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Unfortunately he was fired soon after because of his alleged drug abuse. This was overall great for Megadeth because this lead to their most successful / classic Mustaine / Ellefson / Friedman / Menza lineup that created *Rust In Peace*, *Countdown To Extinction* and *Youthanasia* which contained the band's biggest hits.

This didn't mean that Chris Poland went away. He recorded a instrumental album called *Return To Metalopolis* in 1990. He plays all the instruments except for the drums which were performed by his brother Mark Poland. While this is a thrash metal classic, it didn't reach any kind of mainstream acceptance. 

So Chris Poland decided to make it into an actual band that would try to appeal to a larger hard rock audience. Bassist Dave Randi and guitarist / vocalist David Judson Clemmons joined the Poland brothers as the group Damn The Machine.

They only released one self-titled album in 1993, which is today's pick.

The album really should have been bigger than it was. It is not unlike Megadeth's *Countdown To Extinction* in that it takes thrash metal and makes it radio friendly and less frantic. Listening back to it in 2022 it still sounds like really fresh music to me, and the guitar playing (especially the solos) are pretty jazzy and amazing.

They released one single from the album, "The Mission". The video is pretty wild to watch now as it certainly seems more relatable in the wake of the COVID pandemic. The internet proves to be extra awesome in this regard because this clip also includes tantalizing footage of the hosts of MTV's Headbangers Ball in all their early-90's glory:

<iframe width="560" height="315" src="https://www.youtube.com/embed/qN0kgclk4ik" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Damn The Machine toured with world to support the album, opening for Dream Theater but due to the lack of success broke up soon after.

Since that time Chris Poland went on to form the jazz fusion group OHM. He also briefly rejoined Megadeth in 2004 to record *The System Has Failed* but was again fired due to disagreements with Mustaine before they were even able to tour behind the album. Opportunity missed.

He is still a pretty amazing guitar player though. Here is some killer footage of him soloing at the NAMM convention in 2020:

<iframe width="560" height="315" src="https://www.youtube.com/embed/uWy9DAYnbXM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Damn The Machine remains a cool, obscure hard rock gem that deserves wider recognition. 

<iframe src="https://open.spotify.com/embed/album/5a81k6i1zQNyGVeUw4MsgX?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

<iframe id='AmazonMusicEmbedB00BTQQU94' src='https://music.amazon.com/embed/B00BTQQU94/?id=SanFDeEiLS&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/damn-the-machine/1444196157"></iframe>

<iframe src="https://embed.tidal.com/albums/35319682" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>




