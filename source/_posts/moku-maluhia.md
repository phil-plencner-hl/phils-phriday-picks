---
title: Jim "Kimo" West - Moku Maluhia
date: 2022-08-05 13:03:13
tags:
- jim kimo west
- weird al yankovic
- 80s music
- hawaiian music
- slack key guitar
---

![](MokuMaluhia.jpg)

Did you know that the guitar player from Weird Al Yankovic’s band is also famous for his slack key guitar playing?

I'm talking about this guy, Jim West:

<iframe width="560" height="315" src="https://www.youtube.com/embed/yR4GkeZATsY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Jim West has been playing with Weird Al since 1983. He played on the album *In 3-D* (which includes the hits "Eat It" and "I Lost on Jeopardy") and has been a part of his band ever since. In fact, everyone in Weird Al's band from 1983 onward has been the same (drummer Jim "Bermuda" Schwartz and bassist Steve Jay).

I always loved the title track and video from 1985's *Dare to Be Stupid* where you can see a lot of Jim West playing stupid guitar in the style of Devo:

<iframe width="560" height="315" src="https://www.youtube.com/embed/SMhwddNQSWQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Anyways, on a break in touring with Weird Al in the late 90s he took a trip to Maui and fell in love with the island. He has been playing and recording traditional and original music on slack key guitar ever since. He has released over a dozen albums in this style for over 20 years!

Here he is playing the Santo and Johnny classic "Sleepwalk":

<iframe width="560" height="315" src="https://www.youtube.com/embed/ERfBENjTsQg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Here is an excellent video (featuring plenty of amazing footage of Maui) of one of his original compositions called "Maui Skies":

<iframe width="560" height="315" src="https://www.youtube.com/embed/wRVJ0LZQs3c" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

In 2018, he won a Grammy in the Best New Age album category for *Moku Maluhia*, which is today's pick!

Phil's Phriday Picks will be on holiday for the next two weeks. I will return on August 26th. Until then, enjoy *Moku Maluhia*!

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/album/2xnjQjDNnojBTyEG6bSedK?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

<iframe id='AmazonMusicEmbedB078VC61WN' src='https://music.amazon.com/embed/B078VC61WN/?id=kI3m7LWDoK&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *; clipboard-write" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/moku-maluhia-peaceful-island/1331833381"></iframe>

<iframe src="https://embed.tidal.com/albums/83113764" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>