---
title: Go - Live From Paris
date: 2022-04-29 10:15:16
tags:
- stomu yamashta
- steve winwood
- michael shrieve
- klaus schulze
- al di meola
- santana
- jazz fusion
- space rock
- 70s rock
---

![](GoLiveFromParis.jpg)

Synthesizer pioneer Klaus Schulze [passed away earlier this week at the age of 74](https://www.rollingstone.com/music/music-news/klaus-schulze-electronic-music-pioneer-dead-obit-1343613/). He was a founding member of Tangerine Dream (though he left before they discovered the sound they were most famous for). 

As a solo musician, he was mostly known for his long-form, ambient, spaced-out compositions (His 1975 epic *Timewind* being one of my favorites from his extremely long discography). He also famously had a fascination with *Dune* and released several recordings inspired by it (including working with Hans Zimmer on last year's *Dune* soundtrack).

One of the more interesting things he was involved in that hasn't got the same amount of hype was a progressive rock / jazz fusion super-group he was a member of in the late 70s called Go.

Go (which translates to "Five" in Japanese) was formed by another synthesizer trendsetter, Stomu Yamashta. 

One of Stomu's claims to fame was that his live album from 1971 *The World Of Stomu Yamash'ta* was the first commercial digital recording ever made! A hard-to-find classic.

Anyways, the members of Go were pretty amazing:
- Stomu Yamashta - percussion and keyboards
- Steve Winwood (Yes, THAT Steve Winwood) - vocals and piano
- Michael Shrieve (Santana's drummer) - drums
- Klaus Schulze - synthesizers
- Al Di Meola (Return To Forever virtuoso) - lead guitar

How can that possibly be bad? The answer, of course, is it isn't bad. It is excellent.

They released two studio albums in their brief existence, but my favorite recording of theirs was the live album from 1976 called *Live From Paris*, which is today's pick.

This is an awesome recording of a sadly forgotten group of star musicians that really needs greater recognition for their performance together.

Basically everything that is great about the individual members is showcased in this recording. Lightning fast guitar runs? Al Di Meola has you covered. Soulful singing and piano playing? Enter Steve Winwood. Powerful, funky drumming? Michael Shrieve to the rescue. Spacey keyboards and textures? Stomu Yamashta and Klaus Schulze fit the bill.

Semi-related: I'm going to close today's pick with footage of Santana's awesome performance of "Soul Sacrifice" from Woodstock, complete with 20 year old Michael Shrieve's ridiculously outstanding drum solo. Because I will never pass up an opportunity to reference it in some way:

<iframe width="560" height="315" src="https://www.youtube.com/embed/xBG6IaSQCpU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

After you soak in that footage, proceed on to enjoy *Live From Paris*!

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/album/4eMLZbhgRyWcdz2SEdD7Bi?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

<iframe id='AmazonMusicEmbedB06W9M5YCT' src='https://music.amazon.com/embed/B06W9M5YCT/?id=DK1ojSAQIU&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/go-live-from-paris/1443435147"></iframe>

<iframe src="https://embed.tidal.com/albums/34970819" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>