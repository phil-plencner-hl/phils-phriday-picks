---
title: Robert Greenidge - A Lovely Cruise
date: 2023-09-08 11:12:16
tags:
- jimmy buffett
- robert greenidge
- michael utley
- club trini
- mac macanally
- kenny chesney
- nadirah shakoor
- arrested development
- country
- country rock
- classic rock
- 70s rock
---

![](ALovelyCruise.jpg)

Jimmy Buffett passed away over Labor Day Weekend at the age of 76. He suffered from an aggressive form of skin cancer called Merkel cell carcinoma. There have been plenty of [detailed obituaries and historical retrospectives](https://www.washingtonpost.com/obituaries/2023/09/02/jimmy-buffett-margaritaville-singer-dead/) written about Buffett in the past week. However, something I haven't seen too much of a focus on was the commitment and long time membership of many of the musicians in his backing band The Coral Reefers.

Most of The Coral Reefers had been playing with Buffet for decades, and had success and careers outside of the group. Some lesser-known examples include pedal steel player Doyle Grisham who was a member since 1974, Jim and Peter Mayer (bass and guitar) members since 1989, percussionist Ralph McDonald from 1974 until his death in 2011, and harmonica player Greg "Fingers" Taylor from 1975 until 2000. 

More predominantly is guitarist and songwriter Mac MacAnally. He started writing songs for Jimmy Buffett in 1980 ("It's My Job" off of *Coconut Telegraph*) but didn't join the touring band until 1994. Mac MacAnally has put out a ton of excellent solo albums and wrote big hits for other artists. His most well known are probably associated with Kenny Chesney. He wrote "Back Where I Come From" which Chesney turned into a hit in 1996 and "Down The Road" which Chesney also turned into a hit in 2008.

<iframe width="560" height="315" src="https://www.youtube.com/embed/4eVrlfVJThI?si=4nAShukYuxdUHo3F" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/H-HtH6HYlz0?si=t4V2-lk2NYZNw_-w" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Mac MacAnally also has the distinction of being part of Jimmy Buffett's last public performance. Jimmy sat in unannounced on a small Mac MacAnally solo show on July 2, 2023:

<iframe width="560" height="315" src="https://www.youtube.com/embed/lY2dH7d_by4?si=n39Vl5Xmcmchao_P" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Another unique member was singer Nadirah Shakoor who joined The Coral Reefers in 1995. She was previously a member of hip-hop group Arrested Development. You can see her in their video for "People Everyday":

<iframe width="560" height="315" src="https://www.youtube.com/embed/a_4Y7Cei_bw?si=Ktcpg2YuCktWQU5d" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Keyboardist Michael Utley (Coral Reefer since 1975) and steel drum player Robert Greenidge (Coral Reefer since 1983) are also very notable. Outside of their work with The Coral Reefers they started their own group called Club Trini. They played instrumental music as a duo, but over time the membership expanded to include other members of The Coral Reefers such as Jim and Peter Mayer, Ralph McDonald and Nadirah Shakoor.

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/).

Much of their music was released on a series of CDs that Jimmy Buffett put out in the mid-90s showcasing bands that played in his Margaritaville bars and restaurants. They are all sadly out of print (and not available on streaming services)...but all 3 (*Margaritaville Cafe: Late Night Menu*, *Margaritaville Cafe: Late Night Gumbo* and *Margaritaville Cafe: Late Night Live*) are worth seeking out due to Club Trini's presence along with all the other great artists that they showcase (including some songs where Jimmy Buffet also joins in).

Here is "Coco Loco" from *Margaritaville Cafe: Late Night Menu*:

<iframe width="560" height="315" src="https://www.youtube.com/embed/ZIhGkN0qfe4?si=0h6lNx7ZVqRHO8o7" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Here is "Come On In" from *Margaritaville Cafe: Late Night Live*:

<iframe width="560" height="315" src="https://www.youtube.com/embed/upk4ENgDAwY?si=caFHCztuBgU94hEI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Club Trini in their expanded form also performed in the parking lot before Jimmy Buffett concerts for the tailgating parrot heads. I saw them play several times when I went to shows in the mid 90s and they were always excellent. Here is an example of what that was like:

<iframe width="560" height="315" src="https://www.youtube.com/embed/hgvEFtA3oK4?si=tVPL3gF5IzwFsXDp" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

A few years ago, Robert Greenidge put out a solo record (that really is a disguised Club Trini record as it includes most members) called *A Lovely Cruise: The Steel Drum Music of Jimmy Buffett*, which is today's pick!

As the title implies, it focuses solely on music that is associated with Jimmy Buffett. Most of the songs are written by him or members of his band but a couple are songs he regularly covered during concerts: Van Morrison's "Brown Eyed Girl" and Jonathan Baham's "Lovely Cruise".

While all the songs on the album are familiar to most Buffett fans, only a few of the "Big 8" songs are included: "Margaritaville", "Come Monday" and "Volcano". This is actually a good thing as it leaves room for songs from throughout the length of Jimmy Buffet's entire career. "Grapefruit Juicy Fruit" from 1973's *A White Sport Coat and a Pink Crustacean* is the earliest song while "Cultural Infidel" from 1996's *Banana Wind* is the most recent tune. So, it definitely covers a wide range of music.

In the days since Jimmy Buffett passed away, I have found myself playing the Margaritaville Cafe series and *A Lovely Cruise* pretty frequently. I think they are a great way to hear Buffett's influence and musical excellence outside of the more well-worn trail of his hits and notorious *Songs You Know By Heart*. I hope you can dive in and enjoy them as well.

{% iframe 'https://open.spotify.com/embed/album/1vOyyahxswXzEqDErLArD5?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B01M0RZTM1/?id=P5RIcmO5w2&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/a-lovely-cruise-the-steel-drum-music-of-jimmy-buffett/589723983' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/18976072' '100%' '96' %}
