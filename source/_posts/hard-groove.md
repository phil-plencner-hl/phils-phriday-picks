---
title: RH Factor - Hard Groove
date: 2024-09-20 21:50:00
tags:
- roy hargrove
- d'angelo
- branford marsalis
- pino palladino
- questlove
- the roots
- erykah badu
- neo soul
- hip hop
- funk
- funkadelic
---

![](HardGroove.jpg)

One of the best modern-day soul records is D'Angelo's *Voodoo*. This is not up for debate. When it came out in the year 2000 it was a game changer for the genres of soul, funk and hip-hop and what they could be in the new millennium. [Entire books have been written about the creation of this masterpiece](https://www.bloomsbury.com/us/dangelos-voodoo-9781501336508/), mostly focusing on the collaboration between D'Angelo and Questlove, the drummer from The Roots.

While that collaboration was a huge part of what made the record so special, there were other musicians involved in it's creation that I think also made giant contributions to the overall awesomeness of *Voodoo*. The overall musicians collective came to be known as The Soulquarians. Two musicians in particular were trumpeter Roy Hargrove and bassist Pino Palladino.

In fact, D'Angelo wrote most of the bass parts of the record with Palladino in mind. Even though D'Angelo played the bass on the majority of the record, there were a couple songs that Palladino played on ("Playa Playa", "Send It On", "Chicken Grease"). Plus, he was a part of the live band that toured behind *Voodoo*.

Roy Hargrove was originally playing more straight-ahead jazz in the 1990s, but he was tapped by Branford Marsalis to be in Buckshot LeFonque (which was a previous PPP). This was likely how D'Angelo got wind of his playing and ability to perform modern funk and soul.

"Send It On" was one of the singles from *Voodoo* and it showcases both players. Here is the music video for "Send It On" that shows them both in action:

<iframe width="560" height="315" src="https://www.youtube.com/embed/YZ48dMwVqPg?si=XXMnOV87yvj-ZOoE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

The summer after *Voodoo* came out, D'Angelo performed at the Montreux Jazz Festival with both of them in tow. The performance is bonkers. Here they are performing "Brown Sugar" at the fest (for some reason split into two parts...probably because there is too much awesome to be contained in one video):

<iframe width="560" height="315" src="https://www.youtube.com/embed/zdOpYOn2DwQ?si=56XhWLL3LNCcFpSv" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/mCy7yaYRwCs?si=BjXcLVcyl4aJyQJK" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Also part of the promotion of *Voodoo*, D'Angelo performed "Devil's Pie" at the MTV Movie Awards:

<iframe width="560" height="315" src="https://www.youtube.com/embed/95Aes_r98sY?si=F-ZT6iYOqMyjbGQU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Finally, I'll highlight D'Angelo's performance of "Chicken Grease" on The Chris Rock Show (Roy Hargrove is not present, but Pino Palladino is *all over* this thing!):

<iframe width="560" height="315" src="https://www.youtube.com/embed/m4XI6LXCsH8?si=ecX70RvKmTSN-cXo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

After the *Voodoo* tour, Roy Hargrove knew that continuing to collaborate with Pino Palladino would be a good thing. So he formed his own funk band with Palladino called The RH Factor. They put out a record in 2003 called *Hard Groove* (get it?) which is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

*Hard Groove* is definitely cut from the same cloth as *Voodoo* but is a lot more up-tempo with more of a heavy funk (almost P-Funk) feel to the proceedings. In fact one of the highlights is their cover of Funkadelic's "I'll Stay" (originally from 1974's *Standing on the Verge of Getting It On*) with D'Angelo on vocals!

<iframe width="560" height="315" src="https://www.youtube.com/embed/7GOB0McTIHE?si=kfvueF53XdlkOCc0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Other members of the Soulquarians also helped out on the record. "Topic" features vocals from Erykah Badu, Meshell Ndegeocello on bass and a great verse from Q-Tip. An all-star cast!

<iframe width="560" height="315" src="https://www.youtube.com/embed/fAHANrrL5Hs?si=-QKEKceGG1onm88X" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

The RH Factor toured behind *Hard Groove* and the live performances might be even better than the record itself. They played the North Sea Jazz Festival and brought the house down. 

Here is "Pastor T" from that concert:

<iframe width="560" height="315" src="https://www.youtube.com/embed/ntNC_eKwViU?si=7bUEMrzpqYwrHOF-" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Another highlight of that show was "Liquid Streets":

<iframe width="560" height="315" src="https://www.youtube.com/embed/Xce7eOFvWuU?si=txlhMBkMyi3vG_-T" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Both of these songs were originally on *Hard Groove*.

The RH Factor only put out one other record (*Distractions* in 2006) before calling it quits. This is unfortunate, because they were an incredible group. 

Unfortunately, there will be no opportunity for a reunion because Roy Hargrove passed away from kidney disease in 2018. Fortunately, *Hard Groove* is one of those albums with endless replay value. It is a party record that is also cerebral which is a difficult feat to pull off.

Phil's Phriday Picks will be going on an extended hiatus during the month of October as I am going on vacation. It is possible there will be a pick or two over the next month, but it is unlikely. I'll be back with regular weekly updates in November!

{% iframe 'https://open.spotify.com/embed/album/7ocjymC4B0S00K0BZ71M9X?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B000W27B30/?id=zoqt9126c7&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/hard-groove/1443070852' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/3693684' '100%' '96' %}