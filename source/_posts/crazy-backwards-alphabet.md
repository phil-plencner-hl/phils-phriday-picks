---
title: Henry Kaiser - Crazy Backwards Alphabet
date: 2021-10-08 08:21:43
tags:
- henry kaiser
- john french
- andy west
- michael maksymenko
- matt groening
- captain beefheart
- dixie dregs
- 80s rock
- avant-garde
- progressive rock
---

Henry Kaiser is a pretty unique individual. He is primarily known as a very skilled guitarist playing a wide range of styles with dozens of collaborators.

He has recorded hundreds of albums, many under his own name, with musicians as wide-ranging as Fred Frith, Richard Thompson, Nels Cline, John Medeski and Herbie Hancock.

Not only that, but he is also a skilled photographer....and a research diver who has spent months living in Antarctica!

If you want to see amazing footage of Henry Kaiser working in Antarctica (including him playing guitar outside in the arctic snow) I highly recommend Werner Herzog's documentary *Encounters At The End Of The World*

<iframe width="560" height="315" src="https://www.youtube.com/embed/6BB3YRtzRxE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Back to the music, one of my favorite Henry Kaiser recordings is with a band that was called Crazy Backwards Alphabet.

Crazy Backwards Alphabet consisted of Henry Kaiser on guitar, along with John "Drumbo" French on drums (Captain Beefheart & His Magic Band), Andy West on bass (Dixie Dregs) and an additional drummer Michael Maksymenko. It is basically throwing Captain Beefheart, jazz fusion and pop music in a blender and turning it on the highest setting.

Somewhat surprisingly, their self-titled first album came out on SST Records, which was mostly known at this point for their allegiance to punk rock music such as Black Flag, The Minutemen and Husker Du. The album cover was also drawn by Matt Groening right before his success with The Tracey Ullman show and The Simpsons.

The music is completely over-the-top. Lots of wild, crazed high-speed instrumentals. Additionally, there are some unusual cover songs including ZZ Top's "La Grange" and Van Halen's "Bottom's Up"...with the lyrics sung in Russian! There is also a cool interpretation of Albert Ayler's "Ghosts".

For those who want a deep dive, here is some amazing live footage of the band posted by Henry Kaiser himself.

<iframe width="560" height="315" src="https://www.youtube.com/embed/_H2oYL8q968" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The studio album is not that far off from what they performed live...which is nothing short of amazing. Which is why it is today's pick!

<iframe src="https://open.spotify.com/embed/album/1jDgg5AlSyI96UfprXIkVQ" width="100%" height="380" frameBorder="0" allowtransparency="true" allow="encrypted-media"></iframe>



