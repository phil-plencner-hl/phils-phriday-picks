---
title: King Crimson DC 2019
date: 2019-09-13 14:17:38
tags:
- king crimson
- 70s rock
- progressive rock
---

Last night, I saw my all-time favorite band in DC: King Crimson! Here is something close to what they sounded like last night.

<iframe src="https://open.spotify.com/embed/playlist/17BqSYLnfYBZwpnUOJGITi" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>