---
title: Bruce Hornsby - Hot House
date: 2021-10-31 19:50:46
tags:
- bruce hornsby
- grateful dead
- the other ones
- jam bands
- 90s rock
- 70s rock
---

One thing that many people probably don't know about Bruce Hornsby: He was a part-time member of The Grateful Dead. He appeared live with them sporadically during the early 90s, playing piano and accordion.

Here's a brilliant rendition of "Eyes Of The World" from 1991 with Bruce Hornsby in the group:

<iframe width="560" height="315" src="https://www.youtube.com/embed/kuSJ0djewQU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

One thing that many people probably don't know about me: I'm a pretty big Deadhead. Unfortunately, I arrived a little late to that particular party. The only time I ever saw the original Grateful Dead live was the very last show with Jerry Garcia at Soldier Field in Chicago on July 8th, 1995. 

It was a pretty good show. I had terrible seats, but slowly sneaked my way up to the front section by the time they hit "Shakedown Street" in the 2nd set. When they played the final song of the night, "Box Of Rain", I managed to be just a few rows back from the stage. Those were the days.

About a week later, Bruce Hornsby released the album *Hot House*. At this time, he was taking the influence from his time with the Grateful Dead and expanding his musical vision. He broke up his original backing band The Range and assembled a jazzier group that was able to go on some pretty lengthy instrumental excursions. 

The *Hot House* album artwork accurately paints a picture of the music contained within. It shows a cartoon of Bill Monroe jamming with Charlie Parker. 

*Hot House* featured musicians such as guitarist Pat Metheny and banjo player Bela Fleck on several songs. The album also includes what might be Jerry Garcia's last studio recording ("Cruise Control").

Bruce Hornsby has always been a pretty great piano player and songwriter, but I feel like *Hot House* is where he really started to show off his amazing skills. For example, the opening song "Spider Fingers" is pretty ridiculous. Check out this live version of it (also from 1995) to see the virtuoso in action:

<iframe width="560" height="315" src="https://www.youtube.com/embed/_DjoZh6XXP0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Soon after the tour for *Hot House* concluded, Bruce Hornsby joined many of the remaining members of The Grateful Dead in a band called The Other Ones. They played the Further Festival tour in 1997. Also on the bill for Further '97 was another Grateful Dead offshoot band...Bob Weir's Ratdog. Ratdog included drummer John Molo who also appears on *Hot House* and continued to be a longtime member of Hornsby's band. 

Some songs from *Hot House* such as "White Wheeled Limousine" were included in The Other One's sets during this time.

Here is some excellent footage of The Other Ones from 1997 featuring Bruce:

<iframe width="560" height="315" src="https://www.youtube.com/embed/XAzRUDCNTtA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

I attended Further Festival 1997 in East Troy, Wisconsin, and it was incredible.

A few years later, in 2000, I saw Ratdog again in Chicago. John Molo was still playing with them, and they were even better than in 1997. Anyways, I digress.

Bruce Hornsby continues to put out great albums in a similar style as *Hot House* through the present day, but I think *Hot House* is the strongest of them all with not a single bad song. A front-to-back banger. 

"The Changes" contains lyrics that seemingly call out his older fans who wished he would play the old hits like "The Way It Is". 

"Hot House Ball" and "Swing Street" continue the theme with lyrics written from a musician's perspective. The music is lively and played with precision yet is still very fun.

Additionally, Hornsby continues to pay tribute to The Grateful Dead. He occasionally peppers Hunter/Garcia songs in his concerts and even plays whole shows of only Dead tunes.  For example, I saw him perform an [excellent set of only Grateful Dead songs at his curated Funhouse Festival in 2017](https://www.bruuuce.com/2017/05/24/bruce-hornsby-setlists-2017/). It brought back a lot of good memories.

If you are only familiar with Bruce Hornsby's hits from the 80s, I encourage you to check out *Hot House*. If you love it, there are plenty of other albums in his catalog that you could deep dive into after that. 

<iframe src="https://open.spotify.com/embed/album/1k4N04Gzr711zDZRlJmUAp" width="100%" height="380" frameBorder="0" allowtransparency="true" allow="encrypted-media"></iframe>








