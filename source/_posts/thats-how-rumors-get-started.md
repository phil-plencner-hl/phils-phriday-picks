---
title: Margo Price - That's How Rumors Get Started
author: Phil Plencner
date: 2020-07-10 16:18:26
tags:
- margo price
- jack white
- third man records
- country music
---

The new Margo Price album finally dropped today. It's a great mixture of late-70s Fleetwood Mac and Kasey Musgraves. An excellent way to kick off the weekend!

<iframe src="https://open.spotify.com/embed/album/0QFMgwRbj0jhGR8FEAmVdL" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>