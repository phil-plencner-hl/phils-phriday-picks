---
title: Kill Rock Stars
date: 2021-04-30 08:17:51
tags:
- kill rock stars
- olympia washington
- 90s rock
- alternative rock
- grunge
- riot grrrl
---

I was recently reminded that 2021 marks the 30th anniversary of the pivotal Olympia, Washington-based record label Kill Rock Stars.

It is a fiercely independent label, and is mostly known for popularizing the "riot grrrl" movement of the early 90s (a feminist punk rock movement including great bands such as Bratmobile and Bikini Kill). It also released early recordings from bands that went on to greater fame such as Nirvana, The Melvins, Heavens to Betsy (fronted by Corin Tucker who went on to form Sleater-Kinney which also released albums on KRS) and much more.

The Guardian recently ran [a comprehensive article about the label](https://www.theguardian.com/music/2021/apr/23/kill-rock-stars-riot-grrrl-label-30-years) that does a much better job than myself describing the storied history of the label. Its worth checking out.

Today's pick is the first compilation album the label put out showcasing a wide variety of independent bands from the Olympia area in 1991. The compilation was also called *Kill Rock Stars*. It's an awesome snapshot of the scene in that region of the country in 1991...and includes lots of great music from Heavens To Betsy, The Melvins, Nirvana (*Kill Rock Stars* came out in August 1991...one month before *Nevermind* dropped), Courtney Love, Bratmobile, Unwound, Bikini Kill, Nation of Ulysses, Jad Fair and tons more. 

Oddly, the album on Spotify does not include the Nirvana song "Beeswax", but it was later included on the *Incesticide* compilation in 1992, so I'll also include that song in this post separately. 

<iframe src="https://open.spotify.com/embed/track/73heu76HyAQRRN1u3vubq6" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

<iframe src="https://open.spotify.com/embed/album/5TZM8FoHBlbqXBKCllJH8i" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>




