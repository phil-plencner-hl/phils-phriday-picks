---
title: Lydia Loveless - Nothing's Gonna Stand In My Way Again
date: 2023-12-02 11:43:29
tags:
- lydia loveless
- bloodshot records
- country rock
- blues rock
- singer songwriter
---

![](Nothing.jpg)

I was in Cambridge, Massachusetts this week for work. While I was there, I was able to check out a live performance from Lydia Loveless at the famous [Middle East club](https://www.mideastoffers.com/). It was an incredible show.

Lydia Loveless is a singer-songwriter who has been gaining popularity and critical acclaim for over a decade. She is closely associated with the Bloodshot Records label in Chicago, even though she isn't strictly in the country mold that the label is famous for.

She first came on my radar in 2011 when she put out the record *Indestructible Machine*. This is definitely more alt-country than the records she is doing now, but the songwriting was already great.

One highlight of that album was the song "Steve Earle" about a stalker who claimed to be the famous musician but actually wasn't.

<iframe width="560" height="315" src="https://www.youtube.com/embed/tDq1_rnJVE4?si=R2PUEMYG2gWvMeba" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Nowadays her records are much more steeped in blues and rock, but the country influence is still present.

Here is a recent concert that showcases her current style:

<iframe width="560" height="315" src="https://www.youtube.com/embed/io1Bk0_gaXE?si=fLz2I6fnMSZfXLA5" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe>. |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

Her latest album is *Nothing's Gonna Stand In My Way Again* which is today's pick!

The first single off of the record, which is a good example of the sound, is "Toothache":

<iframe width="560" height="315" src="https://www.youtube.com/embed/vtk_yTaanyc?si=6G3drOlLey2ndnaH" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

There is also a recent documentary movie about her called *Who Is Lydia Loveless?* which is worth checking out:

<iframe width="560" height="315" src="https://www.youtube.com/embed/D1aIaHL38A8?si=MKvMKFIKDCeJK1hk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

*Nothing's Gonna Stand In My Way Again* will likely be one of my favorite records of the year (full list of 2023 favorites coming soon!), so consider this week's pick a sneak peek!

{% iframe 'https://open.spotify.com/embed/album/1fgPBwfcOfjnlx5cKUrl9o?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B0C5P7ZDHV/?id=fqT2p6BW2u&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/nothings-gonna-stand-in-my-way-again/1688596915' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/295627511' '100%' '96' %}