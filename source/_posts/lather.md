---
title: Frank Zappa - Lather
date: 2023-08-18 15:20:15
tags:
- frank zappa
- 70s rock
- progressive rock
- jazz rock
- terry bozzio
- george duke
- bruce fowler
---

![](Lather.jpg)

People who know me even a little bit are fully aware that I am a obsessive Frank Zappa fanatic. As someone who has been listening to and collecting Zappa for over 30 years, I pretty much have it all: All 120+ officially released albums, all of the albums in the "Beat the Boots" series, a bunch of live DVDs, a smattering of bootleg records and CDs, dozens of books and magazines...you name it, I probably have it in my archives!

So, of course, an obvious question that comes up regularly is "What is your favorite Zappa album?" or "If I'm new to Zappa, where should I start?". These are good, yet challenging inquiries that do not have an easy answer. A lot of times I would point people towards my massive [Untouchable Zappa](https://open.spotify.com/playlist/1EWx6ggW6e6t3hSbI9XGk0?si=4a16609327b7447d) playlist, but at close to 7 hours and running through all the lineups and eras of Zappa's career it can often be too much for the casual listener.

However, if I were hard-pressed to pick one thing that spans a wide range of his musical compositions and styles, with great playing from world-class musicians my answer would be *Lather* (Which is actually pronounced "Leather"). 

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

One of the main reasons for this is because it truly does showcase a healthy cross-section of what makes Frank Zappa's music so great: wild progressive rock ("Re-Gyption Strut", "RDNZL", "The Ocean Is The Ultimate Solution"), orchestral compositions ("Duke of Orchestral Prunes", "Naval Aviation In Art?", "Revised Music for Guitar and Low Budget Orchestra", "Pedro's Dowry"), awesome guitar solos ("Down In De Dew", "Lather", "Filthy Habits"), crucial live recordings ("The Legend Of The Illinois Enema Bandit", "The Purple Lagoon"), fun straight-ahead rock ("Lemme Take You To The Beach", "Big Leg Emma") and his brand of dirty humor ("Broken Hearts Are For A--holes", "Ti--ies and Beer")...this album really does have it all! 

Some of the music was also originally intended to be part of a sci-fi musical Zappa was writing called *Hutchentoot*! These songs include "Time Is Money", "Flambay" and "Spider of Destiny". At the time, they were instrumentals...but when Zappa re-released *Sleep Dirt* on CD in the 1980s he included the vocals recorded by Thana Harris. The full *Hutchentoot* musical has never been fully released...but that is a story and tangent for another time.

I also think *Lather* is not too daunting, despite its nearly 3-hour runtime. 

*Lather* also has a fascinating back-story, which makes it part of my choice for the newbie (and today's pick!) because it also gives you a little bit of historical context.

Zappa was prolific throughout his entire career, but this was especially so when *Lather* was recorded (between 1972-1976). He had recorded and assembled all this material in hopes of releasing it in 1977 as a 4-record box set (along with a single record of "highlights").

Previously Warner Brothers, Zappa’s record label at the time balked at putting out a double album in 1976. It eventually became the single record *Zoot Allures*. So I'm not sure why Zappa thought releasing way more material all at once would be something they would be jazzed about.... but obviously they were not.

Zappa and Warner Brothers fought about the scope of the material for a long while and never really reached an agreement. Frank delivered *Lather* as conceived and Warner Brothers refused it. Then Zappa separated it into 4 different albums (*Sleep Dirt*, *Orchestral Favorites*, *Studio Tan* and *Zappa In New York*). His record contract was for four more albums so he delivered them to the label all at once! They still didn't want to release them at the time.

This led to Zappa eventually being very vocal about his disdain for Warner Brothers: He hung giant banners at his concerts and wore t-shirts that exclaimed "Warner Brothers Sucks!"

![](WarnerBrothersSucks.jpg)

The dispute reached a boiling point when a frustrated Zappa went to Los Angeles radio station KROQ-FM and played the entire *Lather* album on the air in December 1977. Obviously, many people recorded the music off the radio and it was heavily bootlegged.

Warner Brothers eventually released the music that Zappa delivered as the 4 separate albums. Because no artwork was included by Zappa at the time, Warner Brothers contracted cartoonist Gary Panter to come up with album covers (They are all terrible and Zappa never approved or agreed to them).

Finally in 1996, Rykodisc released *Lather* in its original form (Unfortunately, after Frank Zappa had passed away in 1993).

If you really want to dive in to all the historical details of *Lather* (along with everything else in the Zappa universe) I highly recommend the book [*The Big Note*](https://a.co/d/7Eyd6zm).

In the meantime, I recommend just cranking up *Lather* and soaking it all in. If you're not a Zappa fan now, perhaps by the end of it you will be (or you will at least understand what all the hype is about)!

As a bonus to today's pick, here is a great interview and guitar solo (he plays "Black Napkins") on *The Mike Douglas Show* from this era:

<iframe width="560" height="315" src="https://www.youtube.com/embed/kSPdg4yPwAg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

{% iframe 'https://open.spotify.com/embed/album/3o1ccFPWBiGFbflfAWQ8L5?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B00AO34QXK/?id=EEB5wuCDW1&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/gh/album/l%C3%A4ther/1442239266' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/35345476' '100%' '96' %}
