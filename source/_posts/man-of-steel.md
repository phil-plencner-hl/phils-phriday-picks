---
title: Hans Zimmer - Man of Steel
date: 2023-06-30 17:00:51
tags:
- hans zimmer
- movie soundtrack
- josh freese
- vinnie colaiuta
- drum music
---

![](ManOfSteel.jpg)

Last weekend the TV show *60 Minutes* aired a segment on composer Hans Zimmer. It is worth checking out if you have not seen it:

<iframe src="https://www.cbsnews.com/video/hans-zimmer-60-minutes-interview-60-minutes-video-2023-06-25/" id="cbsNewsVideo" allowfullscreen allow="fullscreen" frameborder="0" width="620" height="349"></iframe>

Hans Zimmer, basically needs no introduction. He is one of the most prolific and celebrated modern day film composers. He started with *Rain Man* in 1988 (after a brief stint in the rock band The Buggles) and has basically been on a tear ever since. With over 150 composing credits to his name, it got me thinking what my Hans Zimmer score is? There are plenty of candidates: *The Lion King*, *The Simpsons Movie*, *Pirates of the Caribbean*, *Dune*, *Inception* and *The Dark Knight* would all make my top-10 list, but if I were to pick one it would be *Man of Steel*...which is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

*Man of Steel* is unique in the Hans Zimmer canon because it puts a heavy focus on drums and percussion. This, of course, is right up my alley.

Since this is a Hans Zimmer soundtrack this is accomplished in the most over-the-top and bombastic way possible. Look at the list of drummers that are part of the percussion ensemble of *Man of Steel*:

- Matt Chamberlain (Pearl Jam, Perfect Circle, David Bowie, Tori Amos)
- Josh Freese (Vandals, Devo, Foo Fighters)
- Danny Carey (Tool)
- Jason Bonham (John Bonham's son)
- Pharrell Williams (The Neptunes)
- Sheila E. (Prince)
- John JR Robinson (Michael Jackson)
- Jim Keltner (John Lennon, Bob Dylan, Traveling Wilburys, Ry Cooder)
- Vinnie Colaiuta (Frank Zappa, Sting, Joni Mitchell)

And that's not even everybody! Go big or go home was obviously Zimmer's motto here!

They filmed the recording sessions for *Man of Steel* and there are some interesting snippets of them available online. Here are a couple that are focused on the drummers:

<iframe width="560" height="315" src="https://www.youtube.com/embed/QTOMIyynBPE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/RSFMh0KKl9c" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

And here is one that focuses on the steel guitar players that are also featured throughout, including Chas Smith, Marty Rifkin, Skip Edwards, Boo Bernstein:

<iframe width="560" height="315" src="https://www.youtube.com/embed/AvQOa8CN-gI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

What better way to celebrate the long Independence Day weekend than listening to this amazing soundtrack to a film starring American icon Superman by a German film composer? Happy 4th of July!

{% iframe 'https://open.spotify.com/embed/album/5WmlAFcF8ERAnUvYrwJt91?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B07P6F6NVT/?id=opXZF1KsSq&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/man-of-steel-original-motion-picture-soundtrack/1454398377' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/104823159' '100%' '96' %}
