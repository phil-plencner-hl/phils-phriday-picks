---
title: Blue Oyster Cult - s/t
author: Phil Plencner
date: 2020-06-16 22:15:54
tags:
- blue oyster cult
- 70s rock
- classic rock
---

Today's pick the the awesome debut of Blue Oyster Cult! A prime slab of classic boogie rock. 

This is well before their fame with "(Don't Fear) The Reaper" and "Godzilla", but there are still hits that they still play in concert, like "Cities On Flame With Rock and Roll" and "Then Came The Last Days Of Mary".

The album is overall a little more psych rock than the hard rock they became famous for, which is a plus. I also highly recommend the other two albums of their "Black & White Era": *"Tyranny And Mutation"* and *"Secret Treaties"* (So named, because the album covers were all in black & white)....but lets focus on their debut. 

Besides where else can you hear a song called "She's as Beautiful as a Foot"?

<iframe src="https://open.spotify.com/embed/album/6xxUsZyckaV0shjOJ0cVpf" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>