---
title: The D.O.C. - No One Can Do It Better
author: Phil Plencner
date: 2020-06-09 22:27:33
tags:
- the d.o.c.
- dr. dre
- snoop dogg
- gangster rap
- hip-hop
- 90s music
- n.w.a.
---

Today's pick is *No One Can Do It Better* by The D.O.C. 

The D.O.C. is the unsung hero of early west-coast hip hop. He was associated with NWA (co-writing some of the biggest songs on Straight Outta Compton and Eazy Duz It). He put out this solo album soon after NWA's success...but his career was stalled by a near-fatal car accident.  

After recovering he still co-wrote huge hits that were included on Dr. Dre's *The Chronic* and Snoop Dogg's *Doggystyle*, so he's doing alright. 

But back to the matter at hand: *No One Can Do It Better* is excellent.  He doesn't talk about typical gangster rap topics...more brainy stuff. The beats are supplied by Dr. Dre (except for a few tracks where MC Ren plays live drums!!) so you know the music is on-point as well. 

A complete classic!! 

As a side-note I regretfully loaned my OG vinyl copy to an ex-coworker about 10 years ago and never saw it again.

<iframe src="https://open.spotify.com/embed/album/3wAMdnbT6F7EM1c4mVe6zD" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>