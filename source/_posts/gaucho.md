---
title: Steely Dan - Gaucho
date: 2020-10-23 08:59:14
tags:
- steely dan
- 70s rock
- yacht rock
---

This week I read an [excellent article](https://www.rollingstone.com/music/music-features/good-steely-dan-takes-twitter-interview-1073754/) about a Twitter account called [Good Steely Dan Takes](https://twitter.com/baddantakes/), which lead to yet another one of my obessive listening jags of the Steely Dan catalog.

This [particular tweet](https://twitter.com/baddantakes/status/1288120384693260295) struck me as really funny, because it basically describes the recording of *Gaucho*, which is today's pick.

*Gaucho* was the last Steely Dan album before they went on an extended hiatus in the 1980s and 1990s. They took their studio perfectionism to the absolute extreme...creating one of the most stunning albums in all of rock. *Aja* usually gets all the kudos (and it is indeed excellent), but I prefer *Gaucho* more. 

To illustrate how insane they went to reach perfection, it is best to mention "Wendel", which is the nickname they gave to the music sampler they constructed specifically for the album to [achieve the perfect drum parts](https://en.wikipedia.org/wiki/Gaucho_(album)#Drum_recording)....even though they used the best studio drummers money could by in the late 70s, they were not satisfied!

For the drum / music nerds out there: Here is a cool site I found with [pictures and original owners manuals for the Wendel machine](http://rogernichols.com/wendel)!

Enjoy the massive excess that is Steely Dan's *Gaucho*!

<iframe src="https://open.spotify.com/embed/album/5fIBtKHWGjbjK9C4i1Z11L" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>  
