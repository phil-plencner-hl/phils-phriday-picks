---
title: Christmas 2022
date: 2022-12-16 10:16:23
tags:
- christmas music
---

Happy Holidays! 14 years of making Christmas music mixes is a long time...it is thankless work but someone has to do it.

You know the drill. Over an hour of holiday-themed songs...some old, some new...all killer, no filler.

Previous mixes are also listed below...including the 17.5 hour, 311 song megamix of every previous year! Put that thing on random play and your holiday party will be lit!

Phil's Phriday Picks will be on holiday until January 6, 2023. That pick will reveal my favorite albums from 2022.

Merry Christmas and Happy New Year! 

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/playlist/0xcrzSFIKysMMFi6ogGxTG?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

- [17.5  HOUR MEGA MIX!](https://open.spotify.com/playlist/5NqMi2wSRDRXP5L4G5McRW?si=ZwmbuH3iTkyzRb3IihO9UA)
- [Christmas 2021](https://open.spotify.com/playlist/6ZRGinlvxFJYSqqLR60mxj?si=d273d0565b274585)
- [COVID Christmas 2020](https://open.spotify.com/playlist/6ZEzVZLOLb7jEWAWwFMOeR?si=45ae978fa6f549c4)
- [Modern Christmas 2019](https://open.spotify.com/playlist/1NiCJLrp4eX6cGvSwYBd7I?si=EWPGwVPVR9qe4WWVUyUQaw)
- [It's Christmas Again? 2018](https://open.spotify.com/playlist/0KVsnggEkc7MftYJIRmmZl?si=cIhjjkEySeKKOC4g5AyCQA)
- [New Christmas Classics 2017](https://open.spotify.com/playlist/73rcF9IZMKENn9xoZ6aLE2?si=esHyVl4jTJOvE8950mxAcw)
- [Merry Christmas 2016](https://open.spotify.com/playlist/15Q28CMAlNDhnxvpdzPril?si=q7XgWr3hRRiyDtsDDxBD_Q)
- [Festive 2015](https://open.spotify.com/playlist/32yvyJJiGGcxAJVwf4VyOZ?si=HqEeppCZSvmFLVtK-fIB7w)
- [80s Christmas 2014](https://open.spotify.com/playlist/4YEulevL9dKg0uXmXgUfML?si=KojXWrunRk69fNoxHSNf_g)
- [Jazzy Christmas 2013](https://open.spotify.com/playlist/1WQE7qzJugPui8TsK4EngF?si=a_PCH1vJTBqLMSh9a6ik2A)
- [Sounds of Christmas 2012](https://open.spotify.com/playlist/13KoC1HiL5NOM153QjrJk8?si=2gKKSXoJQ0OrSx21LiIBLg)
- [cRaZy ChRiStMaS 2011](https://open.spotify.com/playlist/5t2qUwrQHDiMpakUa2j7KM?si=a_zUuWcOSAmUWcrZZKxeTQ)
- [Wacky Christmas 2010](https://open.spotify.com/playlist/3KPGMZ8X8J1MDZa4NM6XIE?si=pUxfVmnSQGKZuNlDsUSXjA)
- [Ultimate Christmas 2008](https://open.spotify.com/playlist/3AylbZDuVKYQH7g2ik5ciw?si=jtjSA8thTLyrbjf1AAkGYQ)
