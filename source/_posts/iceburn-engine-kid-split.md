---
title: Iceburn /Engine Kid - Split EP
date: 2021-03-29 08:06:10
tags:
- iceburn
- engine kid
- revelation records
- gentry densley
- greg anderson
- igor stravinsky
- the rite of spring  
- goatsnake
- southern lord records
---

Iceburn was originally a hardcore punk band that was formed in Utah by guitarist Gentry Densley in the early 90s. 

Their early albums like *Fireon* were pretty straightforward. They eventually started incorporating more progressive rock structures in their music with albums like *Hephaestus* and *Poetry Of Fire*. 

By the time the mid-90s rolled around, Iceburn had expanded their sound and lineup. They included saxophones and additional percussion and started playing very complex progressive punk rock. One of the highlights of this era of Iceburn to me was their untitled split EP they put out with the band Engine Kid on [Revelation Records](https://revhq.com) in 1994.

For Iceburn's half of the EP, they played 2 versions of their interpretation of Russian composer Igor Stravinsky's controversial composition "The Rite of Spring". 

They called their interpretations "Danses 10/93" and "Danses 4/94" presumably to avoid attention from the Stravinsky estate. 

I really love the way they arranged and played the piece for a rock ensemble. You don't necessarily need to be familiar with the original to enjoy this, but it likely helps.

Engine Kid was a band formed by Greg Anderson, who later went on to start the successful [Southern Lord Records label](https://southernlord.com) and played in many other bands such as Goatsnake.

Engine Kid played a unique blend of instrumental doom metal and math rock. Worth checking out, but I don't think they were as interesting as the Iceburn side of the EP.

Eventually Iceburn went full-on into free jazz. They added more woodwind instruments, stopped playing compositions and went in a completely improvised music direction. *Polar Bear Suite* and *Power Of The Lion* are fine examples of this era of the group. 

Those albums are ok, but I much prefer when they were heavily influenced by progressive rock and 20th century classical music like they were on the split EP.

[Buy Iceburn / Engine Kid Split from Amazon](https://amzn.to/2PF0eQM)

<iframe src="https://open.spotify.com/embed/album/5nJ689ibSMTJzsrdjiXT1b" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>




