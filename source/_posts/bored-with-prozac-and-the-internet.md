---
title: TV Mania - Bored With Prozac And The Internet?
date: 2023-11-03 20:33:53
tags:
- tv mania
- warren cuccurullo
- nick rhodes
- frank zappa
- missing persons
- duran duran
- 80s music
- dance music
- terry bozzio
---

![](TVMania.jpg)

Warren Cuccurullo was kind of a rock n' roll enigma. You've probably heard and enjoyed his music, but did not even know it.

His professional career started out as a rhythm guitar player in Frank Zappa's band. He was a crazy fan in the late 70s and went to every Frank Zappa show on the east coast. Eventually becoming friends with Zappa and some of the members of the group at the time (notably drummer Terry Bozzio). In fact, you can see him backstage with Frank Zappa in the *Baby Snakes* movie. This is further documented in the movie's soundtrack, where Zappa is egging him on to sing the song "Baby Snakes":

<iframe width="560" height="315" src="https://www.youtube.com/embed/qqwlNOZk4CI?si=0gw1WHvI2RvAyVTx" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

He eventually joined Zappa's live band and appeared on the classic album *Joe's Garage*. But he didn't stay with Zappa too long.

Terry Bozzio and his wife, Dale Bozzio, were starting a new band and convinced Warren to join the group along with Zappa's bassist Patrick O'Hearn. This group became known as Missing Persons. They had several new wave hits in the early 80s, including "Destination Unknown". Here they are playing it on the Dutch TV show TopPop:

<iframe width="560" height="315" src="https://www.youtube.com/embed/g1pahozFjK0?si=OdUsBUzm7vCV5sLX" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Even bigger fame awaited Warren Cuccurullo as he joined Duran Duran as a session guitarist after Roger Taylor left the group in 1986. After appearing on tours and albums such as *Big Thing* and *Liberty* he was officially made a full-time member of the group for their 1993 self-titled album.

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

Warren Cuccurullo's fingerprints are all over the record. In fact, the biggest hit of the album "Ordinary World" was written by Warren. Here is the video of the vastly popular tune:

<iframe width="560" height="315" src="https://www.youtube.com/embed/FqIACCH20JU?si=Zv9F3b-VHcQgLu3X" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

During this time, Warren Cuccurullo and Nick Rhodes were also working separately from Duran Duran on a side project of weird, avant-garde electronic rock music. They named the group TV Mania. Nothing from the group was released initially.

However, their sound influenced the next proper Duran Duran album *Medazzaland*. Much of the songs on that record were originally TV Mania songs that were reworked, including the early single "Electric Barbarella". Here they are performing it on Leno in 1997 (with plenty of footage of Warren):

<iframe width="560" height="315" src="https://www.youtube.com/embed/bb2Bu-N217g?si=ZybCwlTt47jKsuzo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Eventually, the original lineup of Duran Duran regrouped and Warren Cuccurullo was squeezed out. He is no longer in the band.

Nevertheless, the original TV Mania recordings were finally released in 2013! The resulting album was called *Bored With Prozac And The Internet?* and is today's pick! Here is a [short interview with Nick Rhodes](https://www.idolator.com/7442122/duran-duran-nick-rhodes-tv-mania-bored-with-prozac-and-the-internet) at the time of the album’s release.

So what does this album sound like? It is very weird. Heavily influenced by dance music, with lots of weird samples and synthesizers it is also a concept album! Before the modern internet and social media, Nick Rhodes and Warren Cuccurullo had the idea of a story about a family whose daily lives are broadcast (This is also before the movie *The Truman Show*!). The entire world gets hooked and the family's fashion/phrases/ways are soon influencing social trends. It's almost like they had a crystal ball and were looking at the year 2023. Astounding stuff.

Even though the record was recorded decades ago, it sounds like it could have been recorded recently. It is that fresh and thought-provoking now, especially with the rise of social media influencers recording their every move. If you haven't heard this previously, you will be in for a wild, yet enjoyable, ride.

{% iframe 'https://open.spotify.com/embed/album/6DUJf1aXxzVxMZyoSytchL?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B08Y5K8KHP/?id=HYnssT2LSE&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/bored-with-prozac-and-the-internet/1556883575' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/175972133' '100%' '96' %}
