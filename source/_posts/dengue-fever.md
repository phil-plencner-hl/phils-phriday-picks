---
title: Dengue Fever - s/t
author: Phil Plencner
date: 2020-04-27 15:41:40
tags:
- dengue fever
- 60s rock
- psychedelic rock
- cambodia
---
Today I'm setting my sights on Dengue Fever, an american cover band who mostly plays covers of 60s-era psychedelic rock from Cambodia!! 

They achieved some greater success eventually, but I always preferred their first album, which best encapsulates their modus operandi.

<iframe src="https://open.spotify.com/embed/album/3u2DHmi3LIIvbxZ6ij54Hl" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>