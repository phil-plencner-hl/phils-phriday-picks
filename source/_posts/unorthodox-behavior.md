---
title: Brand X - Unorthodox Behavior
author: Phil Plencner
date: 2020-06-24 22:03:05
tags:
- brand x
- phil collins
- genesis
- progressive rock
- 70s rock
- jazz fusion
---
Next up: Did you know that in the mid-70s Phil Collins (yes, THAT Phil Collins) was in an instrumental jazz-fusion band called Brand X? 

This was back when Genesis was still a progressive rock band (before the big 80s hits). Phil was also involved in other prog rock and avant rock circles around that time (playing on a bunch of Brian Eno and Robert Fripp records for example)....but let's talk Brand X! 

Percy Jones is probably the best bass player you never heard of. Coupled with Phil's phenomenal drumming this was a rhythm section to be reckoned with. 

Their first album *"Unorthodox Behavior"* is their best, but the next few (before Phil departed the group) are also pretty good, but not reaching the heights of UB.

<iframe src="https://open.spotify.com/embed/album/269fg0y6CZLjYoWgTiIad0" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>