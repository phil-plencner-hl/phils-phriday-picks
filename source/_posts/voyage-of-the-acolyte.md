---
title: Steve Hackett - Voyage of the Acolyte
date: 2022-12-02 14:20:09
tags:
- steve hackett
- genesis
- john hackett
- phil collins
- mike rutherford
- progressive rock
- 70s rock
---

![](VoyageOfTheAcolyte.jpg)

Steve Hackett was not a founding member of the band Genesis, but he was close. He joined the band in 1971 (around the same time as Phil Collins) and appeared on *Nursery Cryme*. Not only did he play some great guitar on the album (including on the amazing "The Return of the Giant Hogweed") he also contributed to the writing of a lot of the songs. This is especially so with "For Absent Friends" and "The Fountain of Salmacis".

In short, he was an integral part of Genesis' early success. 

By the time of 1974's *The Lamb Lies Down on Broadway* there were a lot of tensions within the band. Vocalist Peter Gabriel had taken tighter control over the songwriting and overall visual aesthetic. This left Steve Hackett writing a lot of material in hotel rooms on the 1975 tour intended to be used outside of Genesis, which he wasn't sure would survive.

Genesis almost didn't survive. Peter Gabriel left the band. Steve Hackett, along with fellow Genesis members Phil Collins and Mike Rutherford recorded Steve's first solo album *Voyage of the Acolyte* shortly afterwards, which is today's pick! Also notable in the recording sessions was Steve's brother John Hackett who played keyboards and flute.

*Voyage of the Acolyte* is basically a concept album revolving around Tarot cards. The titles and lyrics are all based on various cards in a Tarot deck.

Here is an insightful video of Steve and John Hackett talking about *Voyage of the Acolyte* from a documentary called *The Man, The Music*:

<iframe width="560" height="315" src="https://www.youtube.com/embed/Z-B0j47S_RQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

This album is a very underrated 1970s progressive rock masterpiece. Especially "Shadow of the Hierophant" which is a lengthy epic with a massive amount of power behind it. 

Steve Hackett still performs "Shadow of the Hierophant" and it has lost none of its fury as seen in this footage from 2019:

<iframe width="560" height="315" src="https://www.youtube.com/embed/ZZPleiedgvo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

As I'm sure you're aware, Genesis did end up surviving after all. Steve Hackett remained with them for several more years, including the albums *A Trick of the Tail* and *Wind and Wuthering*. He eventually left the band in 1977. Obviously, Genesis carried on to boffo success after that.

Steve Hackett ended up releasing dozens of solo albums over the decades, but few match the awesomeness of *Voyage of the Acolyte*.

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/album/4twSmtePdvfZhSpI2LV5W2?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

<iframe id='AmazonMusicEmbedB000TERIW0' src='https://music.amazon.com/embed/B000TERIW0/?id=8Wi4ZzDWlW&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *; clipboard-write" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/voyage-of-the-acolyte-bonus-edition/723335885"></iframe>

<iframe src="https://embed.tidal.com/albums/1336885" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>