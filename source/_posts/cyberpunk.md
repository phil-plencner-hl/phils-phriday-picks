---
title: Billy Idol - Cyberpunk
date: 2021-03-05 21:27:05
tags:
- Billy Idol
- 80s rock
- 90s rock
- internet
- new wave
- punk rock
---

Did you know that 80s hard rock icon Billy Idol was an internet pioneer? I promise I am not making this up.

He was initially intrigued by the concepts in the book *Snow Crash* by Neal Stephenson in 1992. Around the same time he started working with Trevor Rabin, the guitar player for Yes in the 1980s. Rabin introduced Idol to using a Macintosh computer to record music at home instead of using an expensive studio. These two things combined proved to a perfect storm.

Soon after he was introduced to Gareth Branwyn who helped Idol set up an account on early internet community The Well. He became very active in that community...posting his own messages, sharing his email address and interacting with fans. Through The Well he became very inspired by the Cyberpunk culture and internet technology and decided to write a bonkers concept album also called *Cyberpunk*. 

He completely abandoned his trademark hard rock sound and wrote and recorded New Wave / Electronic music. He also crafted an elaborate backstory and recorded little skits that tied most of the songs together. The music incorporated a lot of synthesizers, drum machines as well...however future Living Colour bassist Doug Wimbish also took place in the recordings of the album. 

The album even included a wild cover of *Heroin* originally by The Velvet Underground. It was released as the first single and they made no less than 5(!!!) music videos for it.

Mark Frauenfelder, who later went on to start the early blog Boing Boing was hired to create the computerized album artwork using Photoshop. Billy also created a multimedia presentation explaining the album's backstory and early pressings of the album included this on a floppy disk. 

All of this stuff was basically first-of-its-kind stuff for rock music albums, especially for a very high-profile major label artist.

Basically Billy Idol went all-in on these cutting edge technologies and concepts! 

I happen to like the album a lot...but his long-time fans and rock critics were not as kind to it. The album flopped and Billy went back to his trademark sound soon after. *Cyberpunk* remains a lone, strange anomaly in his overall catalog.

While it sounds somewhat dated by today's production standards, I think it is still fascinating and listenable in these modern times. 

<iframe src="https://open.spotify.com/embed/album/1fGWSSKg4KSLkZGU8cZt6i" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
