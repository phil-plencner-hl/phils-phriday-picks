---
title: Makaya McCraven - Universal Beings E And F Sides
author: Phil Plencner
date: 2020-07-31 15:35:52
tags:
- makaya mccraven
- universal beings
- jazz
- drummer
---

Previous PPP regular Makaya McCraven put out a new album today of "outtakes" from his excellent album *"Universal Beings"*. 

It's hard to believe they are leftovers, because the songs are really great. Hitting the sweet spot on this Friday afternoon.

<iframe src="https://open.spotify.com/embed/album/1G4aALenBbJf2Erd02gXe5" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>