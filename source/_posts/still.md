---
title: Peter Sinfield - Still
date: 2024-11-15 16:27:33
tags:
- peter sinfield
- king crimson
- robert fripp
- roxy music
- emerson lake and palmer
- progressive rock
- 70s rock
---

![](Still.jpg)

Today it was announced that musician and lyricist [Peter Sinfield passed away at the age of 80](https://dgmlive.com/news/peter-sinfield-has-died). He is best known for writing most of the lyrics in the early days of King Crimson, notably for their debut album *In the Court of the Crimson King* and the song "21st Century Schizoid Man". He even chose the iconic artwork for the cover of that record! A friend of his created the original painting and he showed it to Robert Fripp, who liked it.

Peter Sinfield was part of Robert Fripp's inner circle before the formation of King Crimson. In fact, "I Talk to the Wind", which one of the tunes on *In the Court of the Crimson King*, was originally performed by the group Giles, Giles and Fripp. The lyrics were written by Peter Sinfield. 

Here is a demo recording of "I Talk to the Wind" by Giles, Giles and Fripp with a spoken introduction by King Crimson manager David Singleton explaining the situation:

<iframe width="560" height="315" src="https://www.youtube.com/embed/vXiWbV0d2w0?si=ljX5rSPGkjfQSuZh" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

One of my favorite King Crimson songs with Peter Sinfield lyrics is "Cadence and Cascade" from their 2nd album *In the Wake of Poseidon*. It's a beautiful tune:

<iframe width="560" height="315" src="https://www.youtube.com/embed/9lp-mwBeQSs?si=egqLw5Sjuj8JUTJ0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

I believe the only King Crimson album where Peter Sinfield can be heard playing an instrument was on their fourth record, *Lizard*. He played the newly released EMS VCS 3 synthesizer on two of the songs. One of them is "Indoor Games". This "Live Recording Sessions" excerpt better highlights his playing than the version included on the album itself:

<iframe width="560" height="315" src="https://www.youtube.com/embed/t5RP-HhhqF8?si=7X9zVc7ADogu0dvq" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Peter Sinfield was very fond of his time in King Crimson, even after he parted ways with them in 1971 (after continually butting heads with Fripp). Here he is talking about his time with King Crimson from the 2008 *Prog Rock Britannia* documentary:

<iframe width="560" height="315" src="https://www.youtube.com/embed/g1SqcWnov8o?si=Q-ArMbfYjooYqZhL" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Additionally, here is a similar interview he gave on Japanese television in 2011:

<iframe width="560" height="315" src="https://www.youtube.com/embed/-JcisCoqUK4?si=mBV2TigBxaIZGdU_" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

When King Crimson released the *Epitaph* live box set in 1997 Peter Sinfield participated in the promotional listening session for it. While you don't see him speaking in this clip, you can see him sitting at the large table signing autographs with other members of the group. Apparently, he and Fripp had mended the relationship by then. The rest of this footage with a small performance is incredible too and worth checking out!

<iframe width="560" height="315" src="https://www.youtube.com/embed/mU45x4kaxaw?si=CIqQIvGbwcJOmzlb" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

After leaving King Crimson he worked with Emerson, Lake and Palmer on their *Brain Salad Surgery* record and produced the first Roxy Music album. Apparently, that wasn't enough to keep him busy because he also worked on his one and only solo record during the same time period. It is called *Still* and is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

Many current and former members of King Crimson at the time helped Peter Sinfield out with the recording, including Greg Lake (guitar and vocals), Boz Burrell (bass), Keith Tippet (keyboards), Mel Collins (woodwinds) and Ian Wallace (drums). Future King Crimson bassist John Wetton also appears, which is cool.

The album opens with the epic "The Song of the Sea Goat", which incorporates parts of Vivaldi's Lute Concerto in D Major. Peter Sinfield performed this on *The Old Grey Whistle Test* around the time of the album's release. You can see John Wetton, Ian Wallace, Mel Collins and others are performing in his band:

<iframe width="560" height="315" src="https://www.youtube.com/embed/dzoxJT74W0w?si=l26wQAeCgdzoNaXh" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

They also performed "House of Hope and Dreams" from *Still* on the same show:

<iframe width="560" height="315" src="https://www.youtube.com/embed/Edw9YpgNrUk?si=7WB9ERL3WpmCCSt_" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

As you can hear, these songs are cut from the same cloth as early King Crimson, minus some of the darker, more menacing themes. I think this suits him well.

Another highlight from *Still* is "Under the Sky" which was written by Ian McDonald who was also an early member of King Crimson. He doesn't perform on the song though:

<iframe width="560" height="315" src="https://www.youtube.com/embed/tW6jjhmdFHQ?si=0C0mo5i-8bQOdDs6" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

The title track of *Still* has a powerful vocal performance by John Wetton. He sounds excellent here and it is a preview of what was to come with him as a member of King Crimson:

<iframe width="560" height="315" src="https://www.youtube.com/embed/4wLkS_TjZeo?si=JvWpQFbJxS-gum1s" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Another song I like a lot is the final track on the album. Another mini-epic called "The Night People":

<iframe width="560" height="315" src="https://www.youtube.com/embed/k4KbgDqRuq4?si=hhuub54P0X7tUNGZ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Peter Sinfield was apparently so fond of the lyrics he wrote for the song that he read them many years later at the Genoa Poetry Festival in 2010. It's interesting to hear the words on their own without the musical context:

<iframe width="560" height="315" src="https://www.youtube.com/embed/JazdmSGvlXs?si=WFjOLJUcQm7O1F5y" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

While the album *Still* is not an undisputed classic like many of the King Crimson records from the era it still is an understated minor gem for progressive rock fanatics like me. Even if you're not a King Crimson aficionado I think there is a lot to like throughout the entire record.

{% iframe 'https://open.spotify.com/embed/album/3w4GmRWjpGCBLbpmUSD7Oq?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B09QH8R4X5/?id=3abq4PTIf9&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/still/1605218065' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/212559722' '100%' '96' %}
