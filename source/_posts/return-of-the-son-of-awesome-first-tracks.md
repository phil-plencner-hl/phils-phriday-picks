---
title: Return Of The Son Of Awesome First Tracks
author: Phil Plencner
date: 2020-03-30 15:57:53
tags:
- first tracks
- progressive rock
- jazz fusion
- alternative rock
---
A friend of mine recently challenged me to create a playlist of 20 songs that only contains awesome songs that appear as the first track on an album. 

This is my answer to this challenge.

<iframe src="https://open.spotify.com/embed/playlist/4fieWKaLpGEaicHQRAgSfG" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>