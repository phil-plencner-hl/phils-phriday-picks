---
title: The Residents - Stars and Hank Forever
date: 2024-05-24 11:13:07
tags:
- the residents
- hank williams
- john philip sousa
- country music
- avant-garde rock
- 80s rock
- marching band
---

![](StarsAndHankForever.jpg)

 Besides their weird and mysterious nature (each member is anonymous, and they always appear in public in some sort of costumes or masks), The Residents have always been a very ambitious group. Throughout their long discography they have released many elaborate concept albums. 
 
 This was especially true in the early 80s when they were working on what became known as the "Mole Trilogy". The Mole Trilogy was a series of records that told the story of a conflict between two rival peoples, the Moles and the Chubs. In true form for The Residents the trilogy was never completed in its original form, but there were five albums that were part of the concept: *Mark of the Mole*, *Tale of Two Cities*, *The Big Bubble* plus the live records *The Mole Show* and *Intermission*. 

To kind of take a break from the Mole concept, The Residents came up with a "simpler" idea  for a set of records called The American Composer Series. This was planned to comprise 10 albums which would pay tribute to pop artists and classical composers from the United States. The first album of the series was *George & James*, featuring the music of George Gershwin and James Brown. 

The second record (and unfortunately final one they recorded after losing interest in the idea) was *Stars and Hank Forever*, which is today's pick!


| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

*Stars and Hank Forever* features music of Hank Williams on one half of the record and compositions by John Philip Sousa on the other half. Quite a weird combination, but I think it works well!

For the Hank Williams side they cover "Hey Good Lookin'", "Six More Miles (to the Graveyard)", "Kaw-Liga", "Ramblin' Man" and "Jambalaya". Obviously, they don't play any of them in a straight country style. Each one is very outside of the confines of what anyone might consider country music, but you can still recognize the original tune. 

The most successful of these was actually "Kaw-Liga" which heavily features the bass part of Michael Jackson's "Billie Jean". Its wild, but it works. They released it as a single, complete with dance remixes and it sold really well in Europe. Here is the single of the dance remixes:

<iframe width="560" height="315" src="https://www.youtube.com/embed/LtZ41xG_ub8?si=SHWZgygVRChG6w47" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

For the John Philip Sousa side, they play of six traditional marches by the composer (including "Stars and Stripes Forever", of course). The interesting thing about this was they made the whole thing sound like it was recorded at a parade. The tunes come in from the distance on the left speaker, gradually grow louder and then disappear off the right speaker. There is also background noise, such as people chatting, breezy wind sounds and other ambient noise you might hear at a parade. It's like you're immersed in a very American experience.

Unfortunately, *Stars and Hank Forever* is very much out of print and not available on many streaming services. The Residents have been teasing an elaborate ["American Composers Series" box set](https://meettheresidents.fandom.com/wiki/The_American_Composers_Box) that would include this record along with *George & James* plus tons of unreleased material from the era including covers of Sun Ra tunes, stuff they recorded for *Pee-Wee's Playhouse* and more. It has yet to materialize, but I am eagerly awaiting its eventual release. 

In the meantime, it seems like *Stars and Hank Forever* is currently only available on Apple Music and YouTube. This will have to do for your patriotic Memorial Day celebrations this year.

* [Listen to *Stars and Hank Forever* on Apple Music](https://music.apple.com/be/album/stars-hank-forever-the-american-composers-series-vol/1564713531)
* [Listen to *Stars and Hank Forever* on YouTube](https://www.youtube.com/playlist?list=PLF9E46A870E546CF5)
