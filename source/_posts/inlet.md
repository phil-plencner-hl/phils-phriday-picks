---
title: Hum - Inlet
author: Phil Plencner
date: 2020-06-24 22:01:21
tags:
- hum
- 90s rock
- alternative rock
---
PPP Triple-Feature today, since I'll be out the rest of the week!

First up...awesome alternative rock band Hum just secret-released their first album in 22 years yesterday!!! 

Their big radio hit was "Stars" from 1995, but their whole catalog is great. The new album does not disappoint!!!

<iframe src="https://open.spotify.com/embed/album/6SfnjkSphtKowg3tt7azDi" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>