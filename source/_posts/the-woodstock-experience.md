---
title: Sly And The Family Stone - The Woodstock Experience
author: Phil Plencner
date: 2020-08-18 14:58:02
tags:
- sly and the family stone
- woodstock festival
- classic rock
- 60s rock
---

On this date 51 years ago, the original Woodstock Festival ended. 

There are always the ol' standbys from that festival that are deemed the "best of the fest"....Hendrix, CSNY, Joe Cocker etc. However, I'd like to turn your attention to the full Sly And The Family Stone set from the fest which is pure fire!

<iframe src="https://open.spotify.com/embed/album/39wxaeY2oFX5TcQcOqLkSV" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
