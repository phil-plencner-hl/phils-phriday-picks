---
title: Huntsmen - Mandala Of Fear
authro: Phil Plencner
date: 2020-05-31 14:19:56
tags:
- heavy metal
- doom metal
- huntsmen
---

Today the mood calls for epic doomy metal, and the new album by The Huntsmen fill this void quite well. I'm going to let this music speak for itself.

<iframe src="https://open.spotify.com/embed/album/6h1mELyn2kn5w2kk3PAIQ8" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>