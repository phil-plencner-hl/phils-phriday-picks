---
title: Jim Hall - Conceirto
author: Phil Plencner
date: 2020-04-28 15:39:00
tags:
- jim hall
- steve gadd
- paul desmond
- chet baker
- ron carter
- jazz
- jazz rock
- jazz fusion
---
Back in the early/mid-90s there was an excellent (yet plainly and aptly named) music store next to my local Best Buy called The Compact Disc Shop. I spent many Saturdays digging through the giant room they had in the back devoted to jazz and blues CDs....especially their ridiculously abundant cut-out bin (does anyone remember cut-out bins?! Oh how I miss cut-out bins!).  

I would consult by trusty All Music Guide (book! Before the website!) during the week and go into there with names of musicians and albums floating in my head that I wanted to check out. 

Jim Hall's *"Concierto"* was one such album I found in the bin, but I hadn't yet read about it (or Jim Hall for that matter).  I was struck by the rest of the lineup which were big-time names in my brain at the time: Steve Gadd, Paul Desmond, Chet Baker, Ron Carter! I snatched it up pretty quickly (I doubt I paid more than $3 for it...thanks again The Compact Disc Shop's cut-out bin!) 

I'm glad I did because I still listen to it pretty regularly.  A mostly great 70s guitar jazz session...with the exception of the beyond amazing 20-minute rendition of Joaquín Rodrigo's "Concierto de Aranjuez". If you haven't heard this one before, you're in for a treat!

<iframe src="https://open.spotify.com/embed/album/7N3ZysMrLSuJAtPPCjCba0" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>