---
title: Mike and the Moonpies - Live From The Devil's Backbone
date: 2023-10-13 14:29:47
tags:
- mike and the moonpies
- country music
- honky-tonk
- outlaw country

---

![](DevilsBackbone.jpg)

Texas has traditionally been a hotbed for country music and that still holds true today. Austin-based Mike and the Moonpies has been quietly releasing amazing records for over a decade, mostly under-the-radar and far away from the bro-country and country pop that has been dominating the public consciousness for a while. Make no mistake: Mike and the Moonpies are as far away from drivel like Jason Aldean, Luke Bryan and Sam Hunt as you can get. They keep the flame of "real" country music alive nowadays. 

They first came to my attention back in 2017, when they released a single with the evocative title "Country Music's Dead":

<iframe width="560" height="315" src="https://www.youtube.com/embed/hLKjVEjLTJI?si=w8Gmi19lKoxIN2g_" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Soon afterwards they put out a full-length album called *Steak Night at the Prarie Rose* which is a front-to-back classic. There is not a bad song on it. A great example is "Road Crew"...they even made a clever video to go along with it:

<iframe width="560" height="315" src="https://www.youtube.com/embed/hZz98uKPAm4?si=jajZMhSRKkaoCTy0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Another one of my favorites is *One to Grow On* from 2021. It is another solid effort from a group that can seem to do no wrong. "Hour on the Hour" was the single and video and released from that record:

<iframe width="560" height="315" src="https://www.youtube.com/embed/mid-aQ78y1s?si=l7P6La9O48S3pvfR" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Their prowess as a live act is really their bread and butter though. They have been constantly touring and because of that they are a well-oiled machine at this point. Here are 5 songs from them earlier this year where you can see them effortlessly blazing through their music with a fun attitude that is infectious:

<iframe width="560" height="315" src="https://www.youtube.com/embed/hPrY4JMmi2M?si=xoDEuIX2BGmxIA5g" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Earlier this month, they released a new live album that captures them perfectly called *Live From the Devil's Backbone* which is today's pick!

[The Devil's Backbone](https://www.devilsbackbonetavern.com/) looks like an interesting club that is located between San Antonio and Austin. It used to be a Sinclair gas station and has a history of ghost sightings....and nowadays is a classic country music bar. This makes it a fitting place for Mike and the Moonpies to record their live set.

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

The only disappointment I have with the record is it does not include their rendition of Fastball's "The Way". I saw them perform it in Baltimore at The 8x10 in February...and luckily the club released a video from that particular show:

<iframe width="560" height="315" src="https://www.youtube.com/embed/lN-C59DK-H4?si=3_JO-7JlFbJIy2KK" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Minor selection quibbles aside, *Live From the Devil's Backbone* is an essential document from the group. With a run time of almost 1.5 hours and 22 songs, it covers all the bases of what makes them a formidable live act apart from "The Way". 

As if the record is not enough, TV show *The Texas Music Scene* announced that they filmed the shows as well and will be releasing it in November. I cannot wait to see this. If old footage of Mike and the Moonpies from *The Texas Music Scene* is reflective of what it will look and sound like it appears we will be in for a treat:

<iframe width="560" height="315" src="https://www.youtube.com/embed/8lahm-9fNhM?si=DOmWTdmF-FIfRk0T" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Until then, I'll just put *Live From the Devil's Backbone* on repeat and keep checking their tour dates to see when they are returning to the area. If they ever perform anywhere close to where you are, I strongly urge you to drop everything and check it out. You won't be disappointed. This recent live record should be all the proof you need.

{% iframe 'https://open.spotify.com/embed/album/0FicBBbiZC8PLex0kyEyQ3?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B0CKHR5B1W/?id=5kDuskFGDl&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/live-from-the-devils-backbone/1710533824' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/320011736' '100%' '96' %}