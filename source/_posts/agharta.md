---
title: Miles Davis - Agharta
author: Phil Plencner
date: 2019-06-07 15:53:15
tags:
- miles davis
- jazz
- jazz rock
- jazz fusion
---

Flashback to the early 90s. A young Phil Plencner is playing in school jazz ensembles and has enjoyed listening to early Miles Davis albums. He owns Kind of Blue and Sketches of Spain and wants more. 

At the mall record store he sees a Miles album he's never heard of before called *"Agharta"*. It has a wild, fantasy / space age looking cover and there are only a 5 songs on it spread out over two discs. Hmm. It's only $3, so he takes the plunge. Soon after he gets home and presses play, he's like what the HELL is this?! 

Crazy free-form funk. Blasting electric guitar. Electric organ as basically a feedback machine. TONS of drums. Occassionally Miles peaks through the din heavily covered in wah wah pedal! This is AWESOME!!!! I want more of this! 

And so starts my obsession with early 70s Miles live records. *Agharta* still remains my favorite, but *Dark Magus* is a close second (mainly because of Dominique Gaumont's absolutely crazed guitar solos)...but *Agharta* has James Mtume beating the snot out of his congas. 

If you wanna continue the deep dive, *"In Concert"* is also great, but *"Pangaea"* is kind of a snooze. Also the studio album with members of these ensembles called *"Get Up With It"* is amazing but less unhinged. 

Anyways, I digress! AGHARTA!!!

<iframe src="https://open.spotify.com/embed/album/7ACMLIVtCkZP7EzxzsZISW" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>