---
title: Algiers - There Is No Year
author: Phil Plencner
date: 2020-08-06 15:15:26
tags:
- 2020 rock
- algiers
- there is no year
- post-punk
---

Today's pick is the latest album from Georgia Post-Punk band Algiers. A funky/soulful/rockin' album that will likely be near the top of my "best of 2020" list.

<iframe src="https://open.spotify.com/embed/album/7eVY0677xRMK8I4syfRA6M" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>