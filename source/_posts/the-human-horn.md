---
title: Shooby Taylor - The Human Horn
author: Phil Plencner
date: 2020-03-17 16:19:01
tags:
- shooby taylor
- the human horn
- weird music
- jazz
- outsider music
---
Warning: Ancient Internet History Rabbit Hole...

Way back in the year 2000 (which seems like a lifetime ago!) I was reading the online message board of NJ Radio Station WFMU, who was known for their vast spectrum of strange music programs. I can't remember completely but they may have also been streaming shows via RealAudio(!!!!) back at that time...and if they were I was listening to those too.

There was a bunch of hype about DJ Irwin Chusid putting out a new book of "outsider" music that I quickly bought called *"Songs In The Key Of Z"*. It also had a companion CD of the same name, which I bought at the same time as the book. This was mainly due to my interest in Captain Beefheart and Wesley Willis who were included in the aforementioned media.

While the whole compilation CD was pretty eye-opening, I was pretty floored by an artist named Shooby "The Human Horn" Taylor! He apparently was a person who went into "pay by the hour" recording booths and did really WACKY scat singing over jazz and easy listening records. Completely crazy. I ended up spending months searching Napster / AudioGalaxy etc for any other random recordings I could find by him. I accumulated a large set of songs, each wilder than the next.

Nowadays, music discovery is completely different....and the complete recorded works of Shooby is on Spotify in a single collection...So you guys have it easy! Dig into this, because I can't even describe how fun and awesome it is.

<iframe src="https://open.spotify.com/embed/album/3UzP0YudkLcEZ3cY2GBACR" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

[Further reading on Shooby "The Human Horn" Taylor](https://en.wikipedia.org/wiki/Shooby_Taylor)

Bonus: "Songs In The Key of Z" is also on Spotify!

<iframe src="https://open.spotify.com/embed/album/7hkWmPdLAFG3WnXJKBKyRL" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>