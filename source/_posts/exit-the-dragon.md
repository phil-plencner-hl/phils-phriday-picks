---
title: Urge Overkill - Exit The Dragon
date: 2023-06-16 10:19:40
tags:
- urge overkill
- nash kato
- blackie onassis
- eddie roeser
- louise post
- steve albini
- 90s rock
- alternative rock
---

![](ExitTheDragon.jpg)

This week [Blackie Onassis (John Rowan) drummer from the peak-era (1991-1995) Urge Overkill passed away](https://www.latimes.com/entertainment-arts/music/story/2023-06-14/blackie-onassis-urge-overkill-john-rowan-drummer-dies?curator=MusicREDEF). 

No cause of death has been publicly announced, but it is well known that Blackie struggled with drug abuse which lead to his estrangement from the other two members of Urge Overkill, Nash Kato and Eddie Roeser. For details into this saga, look no further than a [1996 interview in the Chicago Reader with Blackie](https://chicagoreader.com/music/the-mistake/) (after the band crashed and burned following the tour for *Exit The Dragon*) and a [2004 interview in the Chicago Reader with the rest of Urge Overkill](https://chicagoreader.com/news-politics/the-urge-to-re-emerge/) (when Urge Overkill was reforming sans Blackie). 

I don't want to focus on this drama, I'd rather celebrate how awesome Urge Overkill was in their heyday.

Urge Overkill first came on my radar in 1991. I heard they were the opening act on Nirvana's *Nevermind* tour and their CD *The Supersonic Storybook* was languishing in the used section of one of my local record stores for weeks. I don't know why someone sold it seemingly right away and why nobody bought it for what seemed like weeks (I kept seeing it in the rack every time I dropped in the shop). Eventually I took a chance and bought it, not really knowing what they sounded like.

It was a great little rock record! It had a dry production from Steve Albini and some catchy songs (especially "Vacation in Tokyo", "Emmaline" and "The Kids are Insane"). When I bought the record the store owner also recommended their 7" single "Lineman", which was a cover of Glen Campbell's "Wichita Lineman" so I bought that too. I've always loved that song and it was a cool version of it:

<iframe width="560" height="315" src="https://www.youtube.com/embed/Tt6mK4PwmYg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

None of this really prepared me for what came next, which was 1993's *Saturation*, their major label debut.

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

Everything about Urge Overkill's sound and image was pushed to the limits. A completely over-the-top rock record that sounded HUGE! A lot of this is contributed to Blackie Onassis coming into his own as a drummer and full member of the group plus the pristine production from The Butcher Bros.

*Saturation* completely blew up and they were a household name on alternative rock radio and MTV. It is easy to see why with their ridiculous 70s playboy image and the super-hooky tunes. Two of the biggest singles on the album were "Sister Havana" and "Positive Bleeding" which really showcase their style and shtick:

<iframe width="560" height="315" src="https://www.youtube.com/embed/yzFlPdHt1Gk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/ww-f4glTOdw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

They hit the road to support the record, this time opening for Pearl Jam on their *Vs.* tour. While this exposure was great, it paled in comparison to what happened next: Quentin Tarantino used their cover of Neil Diamond's "Girl, You'll Be A Woman Soon" (from the *Stull* EP) in a pivotal scene from the movie *Pulp Fiction*:

<iframe width="560" height="315" src="https://www.youtube.com/embed/1fr1iyhkyVs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

At this point, expectations couldn't be higher for a follow up record. Instead of making *Saturation II*, they made a much darker (yet still rockin') record called *Exit The Dragon*, which is today's pick!

If *Saturation* was a flashy, glitzy party, then *Exit The Dragon* was the next day's hangover. The songs are haunting and claustrophobic sounding, yet still have the fun kitsch they were known for. 

I think it's a much stronger record than *Saturation* which some deep tunes. They released a couple great singles from the record ("The Break" and "Somebody Else's Body"), but it didn't have the same success as the proceeding album, which is a shame.

<iframe width="560" height="315" src="https://www.youtube.com/embed/RF5i7mjCGxM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/FTrDjbbnVQI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Urge Overkill worked hard to promote the album though. Here is a great segment they did on MTV's *120 Minutes* (note that Blackie Onassis does not participate):

<iframe width="560" height="315" src="https://www.youtube.com/embed/QTo-leNA3zM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

They broke up soon after the *Exit The Dragon* tour with Eddie Roeser essentially quitting the group.

Nash Kato worked on a solo record for years, which eventually came out in 2000 as *Debutante* on Stone Gossard's Loosegroove Records. It didn't make a dent and quickly fell into obscurity. Half of the songs on the record are co-written with Onassis (including the title track which also features Veruca Salt's Louise Post on backing vocals) and makes me wistful for what might have been.

<iframe width="560" height="315" src="https://www.youtube.com/embed/tX-JcxJJvSU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Urge Overkill reformed in 2004 without Onassis but they were never the same. They released two albums so far:  2011's *Rock N' Roll Submarine* and 2022's *Oui*. They are good records, but neither of them reaches the heights of *Exit The Dragon*, which I still believe is their best. It deserves wider recognition when talking about the 90s alternative rock scene in Chicago. Why it's not mentioned in the same breath as Liz Phair's *Exile In Guyville* or Smashing Pumpkin's *Siamese Dream* remains a mystery to me.

{% iframe 'https://open.spotify.com/embed/album/3MPGYZzgZaGfDldrABQaA0?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B004LF1AOK/?id=nzmb9L96di&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/exit-the-dragon/1443665066' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/35978154' '100%' '96' %}
