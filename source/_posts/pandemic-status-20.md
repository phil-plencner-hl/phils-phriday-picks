---
title: Pandemic Status 20
date: 2023-12-15 10:39:24
tags:
- pandemic status
- covid 19
- custom playlists
- progressive rock
- 90s rock
- 70s rock
- jazz fusion
---

![](PandemicStatus20.jpg)

The last time I wrote about [my Pandemic Status series of playlists](https://phil.share.library.harvard.edu/philsphridaypicks/2021/12/16/pandemic-status-playlists/) was almost two years ago to the day: Dec 16, 2021. At that time I had created 6 playlists and was speculating if I would make a 7th. 

The answer to that question is a resounding "Yes"! Since that time I have created an additional 14 playlists. Wow!

To recap: As a form of self-care I decided to make large playlists of tunes both topical and personal favorites. They run the gamut from progressive rock to jazz fusion to 80s pop music to hip-hop to death metal to country. A little bit of everything I love with the intent to cheer me up. The only rule is: No repeats! A song cannot appear on a previous edition of the Pandemic Status series. With each playlist containing approximately 150 songs and spanning 12+ hours, this has become quite the challenge the longer this goes on. According to my calculations, at this point the total song count on all the playlists is 2923 and will take 256 hours 12 minutes (10.5 days) to hear in its entirety! Wow!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

Over the past year, the playlists have become more of a "moment in time" of what I've been listening to. They mix new albums, old favorites that I listen to constantly, stuff that was recommended to me my trusted friends and other sources (blogs etc), songs I heard on SiriusXM and things I Shazamed while I was out. Pandemic Status 20 will likely be the last in this series. I may continue something similar under a rebranded format. 

So to close this chapter of my playlisting, here are all of them in reverse chronological order. Dive in, I'm sure there's things here you will enjoy and likely have never heard before.

Next week's pick will be my favorite albums of 2023! 

- [Pandemic Status 20: Over And Out! (167 songs, 13 hours 53 minutes)](https://open.spotify.com/playlist/00hnWKEidtRbJWMu49YQe0)
- [Pandemic Status 19: Hobbling Into Autumn (161 songs, 15 hours 9 minutes)](https://open.spotify.com/playlist/2PPXeS8Oo609KtyvW8uNFh)   
- [Pandemic Status 18: …And The Living’s Easy (155 songs, 13 hours 18 minutes)](https://open.spotify.com/playlist/5ZBMUYjup89iJx5lwQA8Bp)
- [Pandemic Status 17: Is It Really Over? (161 songs, 13 hours 20 minutes)](https://open.spotify.com/playlist/71FiHrEuVjZbwCZzG7IAPw)
- [Pandemic Status 16: With A Spring In My Step (148 songs, 13 hours 35 minutes)](https://open.spotify.com/playlist/19fOFvbyiMWPEdkXJzR54z)
- [Pandemic Status 15: Winter Of My Discontent (149 songs, 13 hours 21 minutes)](https://open.spotify.com/playlist/0B6b4OTNo6fuZfckWhWEml)
- [Pandemic Status 14: Apocalypse ‘23 (164 songs, 13 hours 13 minutes)](https://open.spotify.com/playlist/51jb3aCkfei9fGYduC2hUc)
- [Pandemic Status 13: Forward Momentum (145 songs, 12 hours 42 minutes)](https://open.spotify.com/playlist/34SQxoQwgXoZds6jWoFaPC)
- [Pandemic Status 12: Bivalent Blues (144 songs, 12 hours 50 minutes)](https://open.spotify.com/playlist/6oEh2Xt3m9q3cadDjeEc22)
- [Pandemic Status 11: Crazy From The Heat (153 songs, 13 hours 34 minutes)](https://open.spotify.com/playlist/1WV3264QeU83PQaoBOqCCl)
- [Pandemic Status 10: Inflation Nation (146 songs, 13 hours 25 minutes)](https://open.spotify.com/playlist/2QrUvThw0gTIQEmM6OtUNL)
- [Pandemic Status 9: Variants R Us (141 songs, 12 hours 20 minutes)](https://open.spotify.com/playlist/2CY0RXrb0HGyoRiBkFWVIw)
- [Pandemic Status 8: Thin Line Between Hope And War (160 songs, 14 hours 7 minutes)](https://open.spotify.com/playlist/6WU1PBslmqamGPaJ1w5uav)
- [Pandemic Status 7: Brand New Year, Same Old Trouble (149 songs, 13 hours 10 minutes)](https://open.spotify.com/playlist/2zCwnRjUodRqyxDAjLAYLr)
- [Pandemic Status 6: Omicron Winter Hibernation (139 songs, 12 hours 2 minutes)](https://open.spotify.com/playlist/6PL6EiAJD82ONBKV1aFtoi)
- [Pandemic Status 5: Feel Like Sh*t…Deja Vu (145 songs, 12 hours 48 minutes)](https://open.spotify.com/playlist/6qW11Kgm9VVCAyjhx49zH8)
- [Pandemic Status 4: No Hope In Sight (125 songs, 11 hours 49 minutes)](https://open.spotify.com/playlist/6fnbe8conMh66PxAuwTsDf)
- [Pandemic Status 3: Delta Disaster (136 songs, 11 hours 37 minutes)](https://open.spotify.com/playlist/1r0fOPCa4nqnqTVbUyCTts)
- [Pandemic Status 2: Are We Having Phun Yet? (106 songs, 8 hours 26 minutes)](https://open.spotify.com/playlist/1jnxS297X4LtRZLJheHxwn)
- [Pandemic Status: Isolated and Weird (129 songs, 11 hours 24 minutes)](https://open.spotify.com/playlist/1emt8fk1PHIOVawxkutB6D)
