---
title: Earth Wind & Fire - The Need of Love
date: 2023-03-10 13:43:54
tags:
- earth wind and fire
- doug carn
- jean carn
- donny hathaway
- funk rock
- funk
- jazz rock
- soul
- free jazz
---

![](TheNeedOfLove.jpg)

Recently, I saw an advertisement for a tour consisting of Lionel Richie and Earth, Wind & Fire. They are presumably playing all of their monster hits because the tagline of the concert is "Sing A Long All Night Long" (clever!).

It made me wonder how many of the core members of Earth, Wind & Fire are still touring these days simply because many of them from the classic mid-70s period have passed away (Maurice White, Andrew Woolfolk, Fred White, Charles Stepney etc) and there is a general trend of bands having one original / core member and touring under the band name with a bunch of younger nobodies. 

I was pleasantly surprised to find that original member Verdine White is still playing the bass and longtime members Philip Bailey (vocals) and Ralph Johnson (percussion) are still holding down the fort. 

It's not like Earth, Wind & Fire had a particularly stable lineup for any length of time anyways. In fact, when the band was originally formed in 1970 with brothers Vernice and Maurice White the band had an entirely different membership than when they sold tens of millions of albums and won a ton of Grammys playing R&B and funk.

Originally, they were more of a soul band with jazz fusion tendencies. Philip Bailey was not yet in the group. Their first big break was performing on the soundtrack to the film *Sweet Sweetback's Baadasssss Song* in early 1971.

Here is some recently unearthed footage of the early band playing in 1971 which is incredible:

<iframe width="560" height="315" src="https://www.youtube.com/embed/Fv5DuEYODks" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

The early Earth, Wind & Fire lineup also included organist Doug Carn along with his wife Jean Carn (although I'm not certain if they ever played live with the group or only recorded with them). During this time in 1971 The Carns were associated with Gene Russell's Black Jazz Records label and putting out classic albums like *Infant Eyes* (to give you a sense of what they sounded like, the album included covers by McCoy Tyner, John Coltrane, Wayne Shorter and Horace Silver).

This era of Earth, Wind & Fire is probably my favorite. Especially because of their 2nd album, *The Need of Love*, which is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

The centerpiece of the album is the 9 1/2-minute opener "Energy". It starts literally with a bang...a Maurice White drum solo which leads into length instrumental sections (including Doug Carn's incredible organ solos) and even flirts with free jazz at times. An awesome mission statement. 

The album also includes one of their early hits with the soulful ballad "I Think About Loving You" and the similar "I Can Feel It in My Bones" paving the way to their future success.

The record closes with the Donny Hathaway cover "Everything Is Everything" which includes another excellent Doug Carn organ solo and gives the original song a run for its money.

After this album, most of the band members left and the "classic" lineup, with the soul-funk sound that made them the most famous, was formed.

*The Need of Love* remains a fascinating, embryonic record of the wildly successful band and makes me wonder what they would have done if the original lineup had stayed together longer.

{% iframe 'https://open.spotify.com/embed/album/3EkU0XjJLHN6OBcxbFxgHB?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B003A97OG6/?id=FjjqiWWG1l&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/the-need-of-love/357907385' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/3462063' '100%' '96' %}
