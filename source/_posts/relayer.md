---
title: Yes - Relayer
date: 2021-10-22 08:28:18
tags:
- yes
- patrick moraz
- rick wakeman
- 70s rock
- progressive rock
- classic rock
- jazz fusion
---

I'm a huge fan of all eras of the band Yes. Everything from the completely overblown progressive rock excess of *Tales From Topographic Oceans* to the 80s pop of *90125*. I love it all.

However, if I were to pick a single album as a favorite I would choose 1974's *Relayer* without hesitation.

The early 70s lineup of Yes is considered by many to be the "classic" lineup (Jon Anderson, Steve Howe, Chris Squire, Rick Wakeman, Bill Bruford). That lineup released *Fragile* and *Close To The Edge* which are both highlights of the lengthy Yes discography.

However, by 1973 the group was splintering. Bruford left to join King Crimson. They hired Alan White (from The Plastic Ono Band) and recorded and toured behind *Tales From Topographic Oceans*. This album is famous for consisting of 4 25-minute long songs. The dictionary definition of prog rock excess!!

Rick Wakeman was extremely dissatisfied with the material. He basically sleepwalked through the 1973 tour and quit after it was over. He went on to record and release *Journey To The Centre Of The Earth* and started a mostly successful solo career. 

So Yes embarked on a worldwide search for a new keyboard player to fill Wakeman's shoes. At one point they even auditioned Vangelis!!

In fact, when Patrick Moraz auditioned, he ended up using Vengelis' keyboard rig because he left it behind in the studio. Moraz was chosen as the new keyboardist because of the fact he could play lots of the complicated material and brought a ton of newfound energy to the group.

Scaling back ever-so-slightly from *Tales From Topographic Oceans*, Yes went back to the format that served them well with *Close To The Edge*. Opening with a side-long epic ("The Gates Of Delirium"), followed by two slightly shorter pieces ("Sound Chaser" and "To Be Over").

"The Gates Of Delirium" is one of my favorite rock songs ever. It really showcases Moraz' almost punk-rock intensity in his playing. Even Steve Howe plays some completely unhinged and deranged solos throughout (especially during the "battle scene" portion of the song). Alan White plays some junkyard percussion as well, which was pretty out-of-the-box for a major progressive rock band in the early '70s.

"Sound Chaser" is also pretty crazy, as far as Yes songs go. There are lots of wild solos, Jon Anderson yodels at one point and its basically a runaway train of awesomeness for its 9-minute duration. 

The album closes with the very subdued "To Be Over" which hearkens back to "And You And I" from *Close To The Edge*.

Yes toured behind *Relayer* in 1975 by playing the entire album front-to-back. If I had a time machine, this would be one of the first things I would dial up. Luckily, YouTube can at least take me there with video footage.

<iframe width="560" height="315" src="https://www.youtube.com/embed/WJRVBQtKltM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Yes scaled things back even more after 1975 with *Going For The One* so if you want to hear Yes at their most exciting, I highly recommend soaking in the glorious magnificence of *Relayer*.

<iframe src="https://open.spotify.com/embed/album/7jDcBAmNwUYT11GLtYSlKt" width="100%" height="380" frameBorder="0" allowtransparency="true" allow="encrypted-media"></iframe>












