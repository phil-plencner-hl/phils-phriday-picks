---
title: Neptunian Maximalism - Eons
author: Phil Plencner
date: 2020-08-13 15:05:14
tags:
- belgian music
- free jazz
- psychedelic rock
- neptunian maximalism
- eons
- i voidhanger records
---

Today's pick is the massive 123 minute epic *"Eons"* by Belgian musician collective Neptunian Maximalism. 

They mix doom metal (Think Sun O)))) with psychedelic rock (think Grateful Dead's spacey jams) and 60's outsider jazz (think Sun Ra or John Coltrane's *Ascension*) into a very intoxicating blend. 

Broken into 3 parts (To The Earth, To The Moon, To The Sun) each section has a discreet mood and direction, but the album works as a whole. An absolutely stunning masterwork. Highest possible recommendation. 

Put out by the always amazing Italian record label I, Voidhanger...I recommend deep diving into their catalog if you're into this and want more StRaNgE stuff (although they typically release stuff with a much more death metal focus)

<iframe src="https://open.spotify.com/embed/album/0P25kH1ABGMaF9PzwZzFyc" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>