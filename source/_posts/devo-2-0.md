---
title: Devo 2.0 - s/t
author: Phil Plencner
date: 2020-05-12 15:06:10
tags:
- devo
- disney
- devo 2.0
- jacqueline emerson
- hunger games
---
Here's a fun obscurity. In 2006, probably due to the success of the KidzBop series of albums Disney decided it would be a good idea to have a Devo cover band consisting of kids! 

They called it Dev 2.0. They released an album and a series of music videos at the same time. For the album, Devo actually re-recorded the songs themselves and the kids sang the lyrics (some of which were changed to be "less offensive"). 

The whole project was pretty short lived, since the album didn't sell very well. 

One of the band members happened to be Jacqueline Emerson who went on to later success as Foxface in Hunger Games! Anyways, the album is actually pretty good!

<iframe src="https://open.spotify.com/embed/album/5yOLkUelCWvz7pNb3Uyvu6" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>