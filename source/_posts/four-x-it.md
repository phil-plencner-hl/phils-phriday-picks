---
title: Nate Wood - Four X.it
author: Phil Plencner
date: 2020-07-16 16:13:54
tags:
- nate wood
- rabbit
- four x.it
- jazz
- jazz rock
---

Today's pick is the album Four X.it by phenomenal drummer Nate Wood. On this album, he actually plays multiple instruments simultaneously (bass, keyboards, drums). Everything is played live with no overdubs. Amazing!

[Visual Example](https://www.youtube.com/watch?v=eQCMbbkEAEI)

<iframe src="https://open.spotify.com/embed/album/5JlgdjaAjdBs6rORY30bIH" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>