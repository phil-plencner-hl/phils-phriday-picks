---
title: Brad Mehldau  - Jacob's Ladder
date: 2022-04-22 10:13:26
tags:
- brad mehldau
- joshua redman
- rush
- yes
- gentle giant
- jazz
- jazz fusion
- jazz rock
- progressive rock
- 70s rock
---

![](jacobsladder.jpg)

Pianist Brad Mehldau is someone who refuses to be pigeonholed. While he is world renown for his jazz trio and his association with saxophonist Joshua Redman his overall work encompasses a much larger scope.

He has been known to dabble with rock formats and compositions over the years. For example, here he his performing Radiohead's "Paranoid Android":

<iframe width="560" height="315" src="https://www.youtube.com/embed/SHxuIKjZEiQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

His latest album, *Jacob's Ladder* is heavily inspired by 70s progressive rock. In fact, the title is in reference to the Rush song that first appeared on *Permanent Waves*. 

The first song on the album "maybe as his skies are wide" is part of the lyrics of Rush's "Tom Sawyer" sang in a chant-like way with Brad's beautiful piano and some skittering drums underneath.

It then segues into "Herr und Knecht" which Mehldau claims is inspired by modern metal / djent bands like Periphery complete with some metal-like screams. Very cool stuff.

Later on the album is a fantastic cover of Gentle Giant's Cogs In Cogs (from *The Power And The Glory*) stretched out into a lengthy 3-part interpretation and extrapolation.

The band does the same with Rush's "Jacob's Ladder" itself later in the album.

Sandwiched between those two is a crazed cover of Rush's Tom Sawyer (with Chris Thiele of Nickel Creek fame!) that has to be heard to be believed!

The final track "Heaven" includes interpretations of Yes songs (including the "Life Seeker" and "Wurm" segments of "Starship Trooper") and some nods to Emerson, Lake and Palmer.

This quite possibly will be one of my favorite albums released all year. It is both paying tribute to the past while being innovative and futuristic at the same time. 

Set *Jacob's Ladder* on repeat and play it loud!

<iframe src="https://open.spotify.com/embed/album/73ZJVQvYg4KYSjQpVL8Wvi?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

<iframe id='AmazonMusicEmbedB09QRMS6ZW' src='https://music.amazon.com/embed/B09QRMS6ZW/?id=GFZ4Y7L8XB&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/ag/album/jacobs-ladder/1606023157"></iframe>

<iframe src="https://embed.tidal.com/albums/216411016" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>

