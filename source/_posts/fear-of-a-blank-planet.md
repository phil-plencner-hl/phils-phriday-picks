---
title: Porcupine Tree - Fear Of A Blank Planet
date: 2021-06-10 21:25:25
tags:
- porcupine tree
- steven wilson
- king crimson
- robert fripp
- progressive rock
- alternative rock
---

Porcupine Tree was a british band formed by a guitarist / songwriter named Steven Wilson. He's perhaps best known nowadays as a top-notch producer, specifically his 5.1 surround sound mixes. These include re-releases of the classics in the Yes catalog, most of the King Crimson studio albums and other progressive rock albums by Jethro Tull and Gentle Giant to name a few.

However, in 2007 he was still primary known for fronting Porcupine Tree...when they released what I believe to be their high-water mark: *Fear Of A Blank Planet*. The title is a clever homage to Public Enemy's *Fear Of A Black Planet*, but the comparisons with the two groups doesn't really extend further than that. 

This is a concept album loosely based on Bret Easton Ellis' novel *Lunar Park*.

The lyrics revolve around a teenager with bipolar disorder and attention deficit disorder. The character also deals with social anxiety and drug addiction because of the disorders. It also ties in criticism of the internet, technology and mass media's ability to induce information overload or boredom. 

Musically, it is a pretty great slab of modern, heavy progressive rock with a hard rock edge. 

Basically, this is right up my alley.

The title track is really catchy and sets the scene for the rest of the album. The centerpiece is obviously the 17+ minute Anesthetize...which even includes a killer guitar solo from Rush's Alex Lifeson. 

Even King Crimson's Robert Fripp gets to lay down some sweet guitar on Way Out Of Here. 

Porcupine Tree had so much good material from the recording sessions that soon after the release of *Fear Of A Blank Planet* they released an additional EP called *Nil Recurring*, which included even more playing from Fripp. 

I saw Porcupine Tree in concert during this era playing alongside Opeth. They played a lot of material from this album, and it was, of course, excellent.

After Porcupine Tree disbanded, drummer Gavin Harrison became an integral part of the reformed King Crimson (which you need to experience to believe!).

Along with the production work I previously mentioned, Steven Wilson has put out a series of solo albums. They've been pretty hit-n-miss. Last year's *The Future Bites* is one of the better ones, but I don't think he's ever reached the heights attained with *Fear Of A Blank Planet*.

<iframe src="https://open.spotify.com/embed/album/59J51uy6r6QcYe7cX0Fzz6" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

