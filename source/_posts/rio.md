---
title: Trevor Rabin - Rio
date: 2023-10-06 21:57:43
tags:
- trevor rabin
- yes
- jon anderson
- rick wakeman
- vinnie colaiuta
- progressive rock
- 80s rock
---

![](Rio.jpg)

I'm keeping today's pick short n' sweet as I have been spending most of this week recovering from COVID-19. This will not completely stop the PPP from moving forward!

Today's pick is the new solo album from former Yes singer / guitar player Trevor Rabin. Trevor, of course, first joined Yes in 1982 and wrote "Owner of a Lonely Heart" and most of the other songs on the album *90125*....but of course with all things Yes it was not that simple. 

Long story short: Originally, he just was working with keyboardist Tony Kaye in a new band they were calling Cinema. Eventually singer Jon Anderson heard the material and joined the group along with drummer Alan White. Record executives intervened and convinced them all to resurrect the band name Yes. This was obviously a very successful and savvy move.

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

Trevor Rabin stayed in Yes until 1994. His last album with the group was the underrated *Talk*. I really liked this record and saw the tour which was really great. One of the highlights from the album and tour was the epic "Endless Dream":

<iframe width="560" height="315" src="https://www.youtube.com/embed/yw5NKaIKt60?si=h3t9b2PqPH-q64LQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Trevor Rabin's post-Yes career has basically been as an in-demand film soundtrack composer. He has contributed to over 50 movie soundtracks and has essentially put rock music in his past...until recently.

In 2011 he put out his first solo album since 1989 called *Jacaranda*. However, it wasn't a full-blown return to his mainstream rock as it was an all-instrumental jazzy affair.

He also joined forces with Jon Anderson and Rick Wakeman in 2016 to tour playing old Yes songs. Unfortunately, a studio album of this group was never made, but they did put out a pretty great live album.

Today, that all changed with the release of his new album *Rio*. It truly is a "solo record" in the strictest sense: He plays most of the instruments himself (although monster drummer Vinnie Colaiuta appears on "Push" among other infrequent guests). He also sings all the vocals, produced the album and created the cover artwork himself.

[Forbes magazine recently wrote an in-depth feature](https://www.forbes.com/sites/davidchiu/2023/10/04/trevor-rabin-on-making-rio-his-first-new-rock-album-with-vocals-in-34-years/?sh=52b4c720639b) about Trevor Rabin and *Rio* that is worth checking out. 

I've only listened to *Rio* once so far, but I'm pretty confident this will be one of my favorite records of 2023. 

{% iframe 'https://open.spotify.com/embed/album/6khHinRjZTyPU1uwwTDEzB?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B0CBSWNVJ9/?id=Uqf4oyxgWH&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/rio/1697362549' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/304747256' '100%' '96' %}