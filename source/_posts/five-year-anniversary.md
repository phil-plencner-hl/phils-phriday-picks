---
title: five-year-anniversary
date: 2024-03-28 19:34:43
tags:
- 5 year anniversary
- awesome
---

![](FiveCake.jpg)

Today marks the five year anniversary of Phil's Phriday Picks! 

What started as just an informal discussion group between a small group of my co-workers has grown into a blog, email newsletter, and Facebook group. 

The first post on the blog is dated March 29, 2019 exactly 5 years ago. The first pick? A compilation album called *Post Now: Round One - Chicago vs. New York* ([Blog](https://phil.share.library.harvard.edu/philsphridaypicks/2019/03/29/post-now-round-one/) | [Substack](https://philsphridaypicks.substack.com/p/post-now-round-one)) on the Skin Graft record label featuring artists like Cheer-Accident, The Flying Luttenbachers, Bobby Conn and Lovely Little Girls. It is still a fantastic record and was an ideal launching pad for 318 picks over the next half decade.

I've covered a lot of ground over the years. Genres such as pop, classic rock, heavy metal, country, progressive rock and more have been represented with a pick. 

The early picks were usually only a few sentences.  Over time the posts have grown more elaborate with historical footnotes, related videos and personal anecdotes. 

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

At one point near the beginning of the COVID-19 Pandemic in 2020 I was challenged to write a pick every weekday, instead of just on Fridays. This went on for six months (from March through August of that year).

I even gave a lightning talk about Phil's Phriday Picks at my department all staff meeting last year. The [slides are available online](https://docs.google.com/presentation/d/1TD-JLY6NaNHSWRR_daBi57KUelZBrWEYCTyQSVY6Od0/edit?usp=sharing) (thankfully I don't think there is any audio or video footage of my presentation).

[The Spotify playlist](https://open.spotify.com/playlist/730J0XZOp2SUfmAktPCE6G?si=2959ca950fd84bb9), consisting of one song from each pick, continues to grow. Currently you would need over 32 hours to get through the entire thing! Put it on during your next party and your guests will be surprised by your impeccable taste in music.

For this week, I'm going to highlight one pick from each previous year that is worth revisiting. Let's call them my top 5 picks! 

### 2019: Cheer-Accident - *Introducing Lemon*
* [Read the pick on the blog](https://phil.share.library.harvard.edu/philsphridaypicks/2019/07/12/introducing-lemon/)
* [Read the pick on Substack](https://philsphridaypicks.substack.com/p/introducing-lemon)

{% iframe 'https://open.spotify.com/embed/album/2wSplRfdnX7R0EXRP8U69i?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B01BMVQCHU/?id=Vbb7cN9G11&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/introducing-lemon/1082918431' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/111445332' '100%' '96' %}

### 2020: Frank Zappa - *The Roxy Performances*
* [Read the pick on the blog](https://phil.share.library.harvard.edu/philsphridaypicks/2020/03/18/the-roxy-performances/)
* [Read the pick on Substack](https://philsphridaypicks.substack.com/p/the-roxy-performances)

{% iframe 'https://open.spotify.com/embed/album/51cjNmDteqloO1E3O0wrfd?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B0786NZTFB/?id=4XClg8jhcb&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/the-roxy-performances-live/1440933680' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/83873501' '100%' '96' %}

### 2021: Izzy Stradlin and the Ju Ju Hounds - *s/t*
* [Read the pick on the blog](https://phil.share.library.harvard.edu/philsphridaypicks/2021/07/01/izzy-stradlin-and-the-ju-ju-hounds/)
* [Read the pick on Substack](https://philsphridaypicks.substack.com/p/izzy-stradlin-and-the-ju-ju-hounds)

{% iframe 'https://open.spotify.com/embed/album/0C5dXQc43DCHdGqTU857Wt?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B01ACI72QE/?id=yzsen65Qq0&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/izzy-stradlin-and-the-ju-ju-hounds/1443075882' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/77555627' '100%' '96' %}

### 2022: High Castle Teleorkestra - *The Egg That Never Opened*
* [Read the pick on the blog](https://phil.share.library.harvard.edu/philsphridaypicks/2022/06/24/the-egg-that-never-opened/)
* [Read the pick on Substack](https://philsphridaypicks.substack.com/p/the-egg-that-never-opened)

{% iframe 'https://open.spotify.com/embed/album/3BEEKfVCfGncTwDgsuG0jy?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B09ZHBGLTP/?id=aecqzNsoXM&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/the-egg-that-never-opened/1622329847' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/227434551' '100%' '96' %}

### 2023: Lil Yachty - *Let's Start Here* 
* [Read the pick on the blog](https://phil.share.library.harvard.edu/philsphridaypicks/2023/04/07/let-s-start-here/)
* [Read the pick on Substack](https://philsphridaypicks.substack.com/p/let-s-start-here)

{% iframe 'https://open.spotify.com/embed/album/6Per97deaWqrJlKQNX8RGK?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B0BSVMFB4Y/?id=nFkUulj6hZ&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/lets-start-here/1667414099' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/273619903' '100%' '96' %}

Obviously, these just scratch the surface of what I've covered over the past five years. Feel free to [browse through the archives](https://phil.share.library.harvard.edu/philsphridaypicks/archives/). There's plenty more to learn about and explore. 

I hope you enjoy reading my picks every week as much as I enjoy making them. Don't worry: I still have plenty of candidates for future picks in a running list. We are in no danger of running out of ideas any time soon. 

Here's to many more years of Phil's Phriday Picks! If you like them, I encourage you to tell your friends, [follow PPP on Facebook](https://www.facebook.com/philsphridaypicks) or [subscribe to the PPP Substack](https://philsphridaypicks.substack.com/subscribe). You can also [follow me on Instagram](https://www.instagram.com/beefheartfan/) where I mostly post photos of vinyl records on my turntable, if you're into that sort of thing.

Anyway, I heard that a new Beyonce album was released today...so I'm going to follow up on that rumor. 

Thank you so much for reading! 
