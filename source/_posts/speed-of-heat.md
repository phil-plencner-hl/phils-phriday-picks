---
title: Jeff "Skunk" Baxter - Speed Of Heat
date: 2022-07-08 08:13:36
tags:
- jeff baxter
- jeff skunk baxter
- skunk baxter
- steely dan
- doobie brothers
- 70s rock
- jazz rock
- yacht rock
---

![](SpeedOfHeat.jpg)

Jeff "Skunk" Baxter is one of those musicians that many people don't realize that they know. He's played on some gigantic hits with popular bands for close to 50 years.

His early success was as a member of Steely Dan. He was a member for all their early records (up to and including *Pretzel Logic*). He left when Steely Dan became less of a band and more of a studio concern.

Perhaps his most famous playing from that era was the incredible guitar on "Reelin' In The Years". Here he is wailing on that song for the *Midnight Special* TV show in the early 70s:

<iframe width="560" height="315" src="https://www.youtube.com/embed/2WTh_IEyU1w" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

After he left Steely Dan he joined The Doobie Brothers. A little while after he joined, the original singer Tom Johnston became ill. Jeff Baxter suggested a friend of his named Michael McDonald fill in on a tour. Of course Michael McDonald became the full time singer and keyboardist after that and was part of the Doobie Brothers biggest hits. A fine example is "Takin' It To The Streets". Here are The Doobie Brothers, with Jeff Baxter, blowing the roof off the place:

<iframe width="560" height="315" src="https://www.youtube.com/embed/i9_e0rRvyz8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

By 1980 Jeff Baxter left The Doobie Brothers to focus on studio work. He performed on tons of hits throughout the 80s.

Most interestingly, he also became interested in military missle defense in the mid-1980s! He eventually became a United States defense contractor and works regularly with congress on such things. Here is [an interesting article detailing his work there](https://old.post-gazette.com/pg/05144/509629.stm).

Eventually he started doing work for TV soundtracks and ended up collaborating with composer and keyboardist C.J. Vanston. C.J. Vanston encouraged him to work on his own material.

Surprisingly, after all this time Jeff Baxter has never released a proper solo album. Until now. He recently released *Speed Of Heat* with C.J. Vanston which is today's pick!

Among the originals, there are also excellent covers peppered throughout the album. One of those covers is "My Old School" by Steely Dan. Here he is performing it a few years ago with C.J. Vanston and you can tell he hasn't lost his fastball:

<iframe width="560" height="315" src="https://www.youtube.com/embed/NRXfBiQJpPQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Overall, Jeff Baxter's solo album is long overdue. It has been in heavy rotation here and hopefully it will be for you as well.

<iframe src="https://open.spotify.com/embed/album/5vW3gz4iSd2QKfMo9v7vez?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

<iframe id='AmazonMusicEmbedB09VG7GDYS' src='https://music.amazon.com/embed/B09VG7GDYS/?id=sJNfUFr8MN&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *; clipboard-write" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/speed-of-heat/1614175760"></iframe>

<iframe src="https://embed.tidal.com/albums/225412415" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>