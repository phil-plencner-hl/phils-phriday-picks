---
title: Nick Mason - Nick Mason's Fictitious Sports
date: 2023-10-20 10:39:42
tags:
- nick mason
- carla bley
- pink floyd
- progressive rock
- canturbury prog
- jazz rock
- soft machine
- michael mantlier
- robert wyatt
---

![](FictitiousSports.jpg)

Composer and pianist Carla Bley [passed away earlier this week](https://www.npr.org/2023/10/17/1206659408/carla-bley-obituary). Much has already been written about her amazing and prolific career spanning many decades and I will not attempt to encapsulate it all here.

An early touchstone of her lengthy discography is *Escalator Over The Hill*. The sprawling 3-LP jazz opera (or "chronotransduction" as its referred to on the album cover) would be an easy choice for my pick, but it would require a book-length discussion. Let's just say I was completely blown away the first time listening to it in the late 90s when I checked the box set out from the local library because I saw that musicians like John McLaughlin, Jack Bruce, Linda Rondstadt, Don Preston and Don Cherry (among many many others) all appeared on it. A life changing recording if there ever was one.

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

Instead I'll focus on a more obscure record. In the late 70s, after Pink Floyd wrapped up their extensive tour for *The Wall*, drummer Nick Mason was working on material for his first solo album. He ended up coming to New York City and meeting up with trumpeter Michael Mantler and Carla Bley and collaborating with them instead. Collaboration is an understatement, as Carla Bley wrote all the songs on the album, produced it and included many of the musicians who were playing in her band at the time. Looking at the liner notes of her solo albums released near the same time (*Musique Mecanique*, *Social Studies* and the awesome *Live!*) support this. Members include: Steve Swallow, Michael Mantler, Gary Valente and Vincent Chancey.

The resulting album was released under Nick Mason's name (likely to capitalize on his fame from Pink Floyd by the record company) and was called *Nick Mason's Fictitious Sports*, which is today's pick!

This is not to say that Nick Mason's involvement is minimal here. His unique drumming style propels all the songs forward. It results in an interesting mix of avant-garde jazz mixed with Pink Floyd's patented space rock.

In addition to Carla Bley's hand-picked musicians, the album also includes Robert Wyatt of Soft Machine / Matching Mole fame singing on all of the songs except the first one. This is a masterstroke, as his singing contributions really lifts the album. This is especially so on the lengthy last song "I'm a Mineralist". 

This is a great record for both Pink Floyd and Carla Bley completists. While it might not reach the grand heights of *Escalator Over The Hill*, it is still an incredible listen.

{% iframe 'https://open.spotify.com/embed/album/186h8hclIvxHoMR8q1x8Qq?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B07DGJV5FS/?id=eoFhYy2h8q&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/nick-masons-fictitious-sports/1393133766' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/93679120' '100%' '96' %}