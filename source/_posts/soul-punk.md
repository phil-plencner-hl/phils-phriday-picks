---
title: Patrick Stump - Soul Punk
date: 2023-01-27 12:23:23
tags:
- patrick stump
- fall out boy
- emo
- punk rock
- 2000s rock
- alternative rock
---

![](SoulPunk.jpg)

Chicago rock band Fall Out Boy has recently been receiving a lot of hype for their upcoming new album *So Much (For) Stardust*. Between their new music video starring Weezer's Rivers Cuomo for "Heartbreak Feels So Good" and their [last-minute show at The Metro](https://chicago.suntimes.com/2023/1/26/23572924/fall-out-boy-metro-chicago-channel-the-past-and-tease-the-future-music) they have been generating a lot of buzz and excitement:

<iframe width="560" height="315" src="https://www.youtube.com/embed/oGvLD7D2Xsg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

 Guitarist Joe Trohman (who actually recently announced he is taking a leave from the band) also recently put out a pretty interesting memoir, [*None of This Rocks*](https://a.co/d/7iG9wkr) , which is worth reading.

This has led me to revisit a more tumultuous period in Fall Out Boy's career. After they released *Folie A Deux* in 2008 with a thud compared to *Infinity on High* they went on a hiatus. Joe Trohman and drummer Andy Hurley joined forces with Anthrax's Scott Ian in the group The Damned Things and bassist Pete Wentz went off the electropop deep-end with Black Cards.

Singer / guitarist Patrick Stump went the completely solo route. He composed, sang, and played every instrument on his album *Soul Punk* which is today's pick!

As the name implies *Soul Punk* is heavily influenced by 80s / 90s soul and R&B combined with the pop punk that Fall Out Boy were known for. 

Patrick Stump acknowledged that Michael Jackson was a huge influence on it. In fact, as part of the promotion of the album he recorded an acapella medley of Michael Jackson hits:

<iframe width="560" height="315" src="https://www.youtube.com/embed/NYk1HZeA7SM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

As an additional pre-album hype he surprise released a vinyl-only EP called [*Truant Wave*](https://www.discogs.com/release/3085370-Patrick-Stump-Truant-Wave) that introduced the new style with songs that mostly didn't appear on *Soul Punk*.

*Soul Punk* is an impressive album. Very interesting songwriting, along with solid musicianship and stellar singing to match. There's not much else that sounds like it. 

The biggest single from the album was his tribute to Chicago called "This City" which also featured rapper Lupe Fiasco:

<iframe width="560" height="315" src="https://www.youtube.com/embed/aGGIQQKKD0Y" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Even though he played all the instruments on the album, he assembled a red-hot band and took the show on the road. Along with playing headlining shows and festivals (I caught him playing a side stage at Lollapalooza) he also opened for Bruno Mars.  Additionally, he promoted the album on TV with stops on Jay Leno and on Fuse where you can see and hear how awesomely tight the band sounded:

<iframe width="560" height="315" src="https://www.youtube.com/embed/EKe8y2a-9uY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/cqliRY0EFmY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Unfortunately, even with all this momentum, the album did not sell well. It didn't meet the lofty expectations of many Fall Out Boy fans. Patrick Stump wrote a pretty heartbreaking blog post about the album's aftermath ([We Liked You Better Fat: Confessions of a Pariah](https://ohnotheydidnt.livejournal.com/66936438.html)) and has yet to release another solo album.  

This is unfortunate because *Soul Punk* was easily one of my favorite albums of 2011 and I still play it regularly. I hope he follows it up someday, but with the recent re-emergence of Fall Out Boy it doesn't seem like that will be likely in the immediate future.

For now, all we have is *Soul Punk* and *Truant Wave*.

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/album/2BQbowoV5XjkHnGJ9mOEbI?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

<iframe id='AmazonMusicEmbedB005NMBIXS' src='https://music.amazon.com/embed/B005NMBIXS/?id=HhN9rxafxE&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *; clipboard-write" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/soul-punk-deluxe-edition/1440668530"></iframe>

<iframe src="https://embed.tidal.com/albums/35423506" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>