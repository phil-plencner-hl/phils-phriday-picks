---
title: Koenjihyakkei - Angherr Shisspa
date: 2022-06-17 12:25:34
tags:
- koenjihyakkei
- ruins
- zeuhl
- progressive rock
- avant-rock
- tatsuya yoshida
---

![](AngherrShisspa.jpg)

Japanese drummer Tatsuya Yoshida is one of my greatest influences on the drums. His complex, busy style has been something I have been trying to achieve ever since I first saw him play in The Ruins at the Empty Bottle in Chicago back in 2001. It was a crazy bill with Pak, Flying Luttenbachers and Cheer-Accident also playing that night. A "brutal-prog" overload if there ever was one.

The Ruins were a bass and drums duo that were known for their short tunes packed full of quick time signature changes, heavy music and Magma / Zeuhl-inspired vocals in a language of their own making.

Tatsuya Yoshida would expand the concept with other bands that were larger in scope. My favorite of these is Koenjihyakkei. Along with bass and drums, the group also has keyboards, occasional saxophone and multiple vocalists. Taking cues from Zeuhl music but pushing the speed and heaviness up even further than the classic 70s bands in that style.

Probably the best example of Koenjuhyakkei firing on all cylinders is their fourth album *Angherr Shisspa*, which is today's pick.

It was originally released in 2005, but Skin Graft Records re-released it in 2019 with some live bonus tracks. Just in case you don't believe they could pull the songs off in a concert setting. The whole thing is pretty mind blowing.

The Skin Graft promotional materials describe the album better than I can, so I will quote them here:

> *Angherr Shisspa*, the band’s landmark fourth album explodes with glittery keyboard lines, speedy bass/drum workouts, emotive reed respites, and operatic female vocals that take the listener from sheer exuberance to absolute apocalypse... all performed with superhuman technique in confoundingly catchy, complex arrangements. 

Sign me up!

As a bonus for today's pick here is a live video from 2006 where the band plays every song on *Angherr Shisspa* along with other classics peppered throughout the set. Incredible stuff!

<iframe width="560" height="315" src="https://www.youtube.com/embed/8Far9Pu7Gtw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

If you like this music, all of the other albums in their short discography are worth seeking out.

<iframe src="https://open.spotify.com/embed/album/2O03aMw5BYJbG0V01uzV11?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

<iframe id='AmazonMusicEmbedB07VBZQ61X' src='https://music.amazon.com/embed/B07VBZQ61X/?id=lSEJlkgPvz&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *; clipboard-write" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/angherr-shisspa-revisited/1473060276"></iframe>

<iframe src="https://embed.tidal.com/albums/113491785" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>