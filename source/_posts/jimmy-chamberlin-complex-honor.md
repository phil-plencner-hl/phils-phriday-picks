---
title: Jimmy Chamberlin Complex - Honor
author: Phil Plencner
date: 2020-09-25 16:29:01
tags: 
- Jimmy Chamberlin
- Jimmy Chamberlin Complex
- Smashing Pumpkins
- 90s Rock
- Alternative Rock
- Jazz Fusion
---
The Jimmy Chamberlin Complex released a new album today! I'm very excited. 

Jimmy Chamberlin, of course, was the drummer for The Smashing Pumpkins during their 90s heyday. His "Complex" band is entirely different as it's an instrumental jazz rock band....with his trademark powerful drumming fully intact. The new album is more focused than some of their previous efforts and is actually pretty catchy and melodic. 

I saw them perform one of their rare shows back when they made their live debut at [The Drum Pad's 20th Anniversary show](https://www.amazon.com/Drum-Pads-20th-Anniversary-Show/dp/B000MM0Y3Y) back in my Chicago days and even caught a quick chat with Jimmy along with Jeff Hamilton as they took a smoke break in the alleyway next to The Vic during the day long event. Nice dude, awesome drummer.

<iframe src="https://open.spotify.com/embed/album/5nA0OgUBdZS85pmJk2Moxw" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe> 

