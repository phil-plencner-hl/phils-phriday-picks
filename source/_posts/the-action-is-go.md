---
title: Fu Manchu - The Action Is Go
date: 2021-06-25 09:34:16
tags:
- fu manchu
- 90s rock
- alternative rock
- stoner rock
- kyuss
---

Fu Manchu had been kicking around for many years as part of the Southern California "Desert Rock" scene (along with Kyuss). They put out their first brilliant album in 1997 with *The Action Is Go*.

I think the reasons for it finally coming together for them center on the change in personnel. Right before the album original guitarist Eddie Glass and drummer Ruben Romano left to form the band Nebula (replaced by Bob Balch and Brant Bjork respectively). Brant Bjork is the secret sauce, as his previous experience playing in the recently dissolved Kyuss is key to this albums power and groove.

Additionally, Jay Yuenger, who famously played guitar in White Zombie during their heyday, produced the album. His fingerprints are all over the sound of the record. 

The album is an excellent example of the Desert Rock scene. A heavy, Black Sabbath-inspired metal with the fury of punk rock, with a heavy dose of psychedelic rock. "Evil Eye" comes out of the gate swinging, and the album basically doesn't let up until the extended space rock of "Saturn III". 

If you love this album as much as I do, Fu Manchu continued their winning ways on a trilogy of follow-up records with the same lineup (*Eatin' Dust*, *King Of The Road*, *California Crossing*) until Brant Bjork eventually left the band in 2003.

<iframe src="https://open.spotify.com/embed/album/7xYJinzZ19pVPVmEAakYv9" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>






