---
title: Megadeth - Rust In Peace
date: 2023-07-14 08:34:37
tags:
- megadeth
- dave mustaine
- marty friedman
- nick menza
- david ellefson
- metallica
- cacophony
- thrash metal
- 90s rock
- heavy metal
---

![](RustInPeace.jpg)

Heavy metal band Megadeth has quite a turbulent history. Guitarist Dave Mustaine is the only constant member of the group, and the lineup generally changes from album to album and tour to tour. Which is part of what made the band from 1990-1996 so remarkable. During these years the lineup was completely stable (the longest such stretch in Megadeth's existence). Megadeth consisted of guitarist Marty Friedman, drummer Nick Menza and bass player David Ellefson. It is considered their most "classic" and technically proficient lineup...and perhaps not coincidentally it was their most popular and successful as well.

Of course, this lineup didn't come out of thin air. The previous lineup, which included Mustaine and Ellefson along with guitarist Jeff Young and drummer Chuck Behler had a heavy drug and alcohol problem. While on their 1988 tour supporting *So Far, So Good...So What!* they basically blew apart. Chuck Behler was the first to go, but Nick Menza was specifically hired as the drum tech in case he was unable to play. So, this worked out nicely. 

After the tour, Dave Mustaine was arrested for driving under the influence and possession of narcotics after crashing into a parked vehicle occupied by an off-duty police officer. Soon afterwards he went to rehab and became clean.

Jeff Young was hardest to replace. They went through a series of auditions with famous players such as Slash (Guns N' Roses), Dimebag Darrell (Pantera), and Criss Oliva (Savatage) but nothing really came out of them. Eventually, Cacophony guitarist Marty Friedman was found. Amazingly part of his audition tape is available online!

<iframe width="560" height="315" src="https://www.youtube.com/embed/n5iWPDn120M" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

I just want to take a moment to point out that Marty Friedman didn't come completely out of nowhere. Cacophony was an incredible shred metal band who also had guitar virtuoso Jason Becker in the lineup. Jason Becker, sadly, developed a very severe case of ALS and can no longer perform or speak. But here they are at the height of their powers in 1989:

<iframe width="560" height="315" src="https://www.youtube.com/embed/Ys33hVWTgxc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

With this band assembled, mostly clean and sober, Megadeth worked hard to release their next record. It's hard to imagine it now, but thrash bands were hugely popular in the late 80s and early 90s. There was heavy competition between the bands to put out amazing, technically proficient records. In 1989 bands were releasing classic after classic: Metallica (*...And Justice For All*), Slayer (*South of Heaven*, *Seasons In The Abyss*), Exodus (*Fabulous Disaster*, *Impact Is Imminent*) and Anthrax (*State of Euphoria*, *Persistence of Time*)...it was a great time to be a thrash metal fan!

Megadeth wanted to make their album better than all of those, and I feel like more than succeeded with *Rust In Peace*, which is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

What still amazes me about the record is that it is practically a prog rock record, with so many parts and quick changes. Nevertheless the 9 song album clocks in at a tidy 40 minutes. How do they do that? The opening track "Holy Wars...The Punishment Due" feels like it’s a 20-minute epic with all its distinct sections and multiple solos...but is really just a hair over 6 minutes.

Capitol Records promoted the record heavily. They made two videos for the album, the first being "Holy Wars...The Punishment Due":

<iframe width="560" height="315" src="https://www.youtube.com/embed/9d4ui9q7eDM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

At this time, it was not unheard of for heavy metal bands to make a bunch of videos, since MTV played them regularly. The show *Headbangers Ball* was quite possibly the most-watched show on the channel in the early 90s... It was so popular they expanded the show from 60 minutes to 120 minutes and then added an additional hour-long show right after it called *Hard:60*...basically meaning they were playing metal videos for 3 hours straight! This was must-watch viewing for me in high school and college. I spent a lot of late nights watching Adam Curry (now famous for being one of the first podcasters!) and Riki Rachtman VJing *Headbangers Ball*.

The 2nd video that they released was "Hanger 18" which was probably the most straightforward song on the album. It’s practically mid-tempo and catchy even though it contains two crazed solos from Friedman and Mustaine. The video is wild with the special effects of government captured aliens and whatnot:

<iframe width="560" height="315" src="https://www.youtube.com/embed/rUGIocJK9Tc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Perhaps the ultimate highlight of the album is "Tornado of Souls". A runaway train of a song with complex guitar and bass parts played at high speed. Marty Friedman's solo is notorious for being very difficult to play. Naturally, when they went on tour for *Rust In Peace* they played it even faster than the record. Check out this bananas video for proof...Dave Mustaine strolling around the stage like it's no big deal...Marty Friedman nailing the solo, David Ellifson just blazing through the bass parts...and the crowd goes wild!!

<iframe width="560" height="315" src="https://www.youtube.com/embed/xeo0TlBnXcs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

The song was so popular with musicians that when Marty Friedman put out his 2008 solo album *Future Addict* he included a new version, with David Lee Roth Band / Mr. Big bassist Billy Sheehan. It’s a killer version of the song:

<iframe width="560" height="315" src="https://www.youtube.com/embed/PQeSPNT-7YI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

I would be remiss to not mention drummer Nick Menza's contributions to this record. He does not play straightforward beats, and frequently tries to match the rhythms of the guitar parts. Here is a drum-cam version of "Holy Wars...The Punishment Due" to illustrate what I'm talking about:

<iframe width="560" height="315" src="https://www.youtube.com/embed/s7u_Lpzmaik" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

While this lineup was the most stable of Megadeth it still wasn't made to last. The put out increasingly more commercial albums (*Countdown To Extinction* being the most successful) and eventually fizzled out with much slower tempos and generic hard rock (*Youthenasia* and *Risk* neither of which I recommend spending much time with). The band broke up and Dave Mustaine retooled Megadeth...going back to his thrash metal roots with relative and occasional success. 

They never again reached the heights of *Rust In Peace*. A front-to-back banger which I can still play on repeat and never get bored. 33 years after its release, that is saying a lot!!

{% iframe 'https://open.spotify.com/embed/album/4e6ML9RBhDyyKTaTwbiRZv?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B073LSR771/?id=DMhm87uFx3&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/gb/album/rust-in-peace/724648893' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/77622288' '100%' '96' %}

