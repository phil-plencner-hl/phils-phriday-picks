---
title: Phil Collins Big Band - A Hot Night In Paris
date: 2023-02-17 15:49:18
tags:
- phil collins
- george duke
- chester thompson
- daryl stuermer
- 80s rock
- genesis
- pop music
- big band jazz
- jazz
---

![](AHotNightInParis.jpg)

Everyone knows Phil Collins' singing and playing in Genesis along with his solo work from the 80s. He was ubiquitous and unescapable. 

Aside from the studio hits, the live concerts he put on were amazing. This is partly due to his own talent, but also with the excellent band he put together which included drummer Chester Thompson, bassist Leland Sklar and guitarist Daryl Stuermer.

As a fine illustration here he is with that band playing live in 1990 playing "Inside Out" from *No Jacket Required*. The band is absolutely on fire here, but especially take note of Leland Sklar's unreal bass playing late in the video:

<iframe width="560" height="315" src="https://www.youtube.com/embed/nIRBZ-PO2Os" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Along with being a great front man, Phil Collins is also a phenomenal drummer. Chester Thompson also played live with Genesis and handled most of the drumming, but Phil Collins would always play a drum duet with him every show. Here is a notable example from Genesis' 1992 tour:

<iframe width="560" height="315" src="https://www.youtube.com/embed/jiFvnNVyvcc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

By the mid 90s, Phil Collins had grown tired of being a member of Genesis and quit in early 1996. Aside from continuing to focus on his popular solo pop music, he also decided to form a big band jazz ensemble with him behind the drum set. He retained guitarist Daryl Stuermer (who was also working on a solo career with his soon to be released solo album *Live & Learn*) when he chose musicians to be in the group. 

They played mostly Genesis and Phil Collins songs arranged in the big band style, along with some jazz standards like "Birdland" and "Milestones". 

The BBC made a documentary near the beginning of their formation, and it is an eye-opening and insightful glimpse into how this band worked towards playing in concert:

<iframe width="560" height="315" src="https://www.youtube.com/embed/yYEhMddw1jg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Over time the band got even better. Later in 1996 they played at the Montreux Jazz Festival with Quincy Jones conducting. Here they are playing the Genesis song "Los Endos" complete with a super-cool drum intro:

<iframe width="560" height="315" src="https://www.youtube.com/embed/JT5UslpgPUA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

I had the pleasure of seeing them live when they played a larger world tour (including the United States) in 1998. They played a free show in Chicago's Grant Park as part of the Taste of Chicago. It was excellent!

On that same tour they played in Paris and recorded the concert for the live album *A Hot Night in Paris*, which is today's pick!

Along with the Phil Collins material and the jazz standards, they also played Average White Band's "Pick Up the Pieces" with George Duke on keyboards, which is a highlight. The whole concert and album are exciting front-to-back.

Unfortunately, the Phil Collins Big Band was short-lived. They broke up after the 1998 tour when Phil Collins started focusing more on writing songs for Disney movies like *Tarzan* and *Brother Bear*. So, *A Hot Night In Paris* remains the sole release from this stellar big band that has mostly been forgotten years later.

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/album/49MS0v62K967L2AJKTVflR?utm_source=generator" width="100%" height="352" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

<iframe id='AmazonMusicEmbedB07L19K9F7' src='https://music.amazon.com/embed/B07L19K9F7/?id=3CCGGWt1Mk&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *; clipboard-write" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/a-hot-night-in-paris-live-remastered/1445238503"></iframe>

<iframe src="https://embed.tidal.com/albums/102716917" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>