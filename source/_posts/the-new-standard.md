---
title: Herbie Hancock - The New Standard
date: 2023-09-15 21:46:04
tags:
- herbie hancock
- jack dejohnette
- dave holland
- john scofield
- michael brecker
- don alias
- jazz
- classic jazz
- jazz funk
- miles davis
---

![](TheNewStandard.jpg)

I am a huge fan of Herbie Hancock. There are distinct eras in his length career that people typically point to as some of his best work: The mid-60s with the Miles Davis Quintet, the early 70s with his wild avant-garde Mwandishi group, the mid 70s with his group The Headhunters and their heavy jazz funk and the mid 80s with his futuristic techno funk of "Rockit" all come to mind.

One time period of Herbie's music that usually gets unsung is the mid 1990s. He was basically spearheading the Acid Jazz movement at the time, releasing *Dis Is The Drum* in 1994. The album even included some rapping and DJ scratching! 

He took that sound on the road, playing to bemused crowds at jazz festivals. Here is at the Umbria Jazz Fest for example:

<iframe width="560" height="315" src="https://www.youtube.com/embed/V86lw5zaF-Y?si=oToAHuJ412HeTbYh" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

He also hooked up with drummer Jack DeJohnette, who through his association with ECM Records was working on a solo album with guitarist Pat Metheny. They put out an excellent record called *Parallel Realities* that stuck close to the script of Metheny's brand of electric jazz fusion. They played some live dates together with bassist Dave Holland as well. Here is an example of them playing Hancock's "Cataloupe Island" (originally from 1964):

<iframe width="560" height="315" src="https://www.youtube.com/embed/5KNL0_sis1k?si=ZsdlRBww3e2mPuxH" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Jack DeJohnette's long time tenure in pianist Keith Jarrett's "Standards Trio" (where they exclusively played old jazz standards but stretched them in wild and interesting directions) must have rubbed off on Herbie Hancock, because in 1996 he decided to do something similar. However, the songs chosen were not old chestnuts, but more contemporary rock and pop music. This became the aptly named album *The New Standard* which is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

Joining Hancock and DeJohnette on the record was Dave Holland, along with guitarist John Scofield (before his spaced-out funk collaborations with Medeski Martin and Wood) and saxophonist Michael Brecker. Both Scofield and Brecker were known mostly for their work in the electric jazz fusion realm so it was very surprising that they decided to perform exclusively on acoustic instruments.

The songs chosen for the album were popular, yet eclectic: "New York Minute" (The Eagles), "Mercy Street" (Peter Gabriel), "Love Is Stronger Than Pride" (Sade), "Thieves In The Temple" (Prince) and even "All Apologies" (Nirvana) were included!

Even though the melodies of the songs were originally straighforward, the versions this band performed turn them completely inside out. Sometimes it is even hard to recognize what song they originally started playing by the time they get to the end. The way they morph these "New Standards" into something completely original in their own right is pretty incredble and breathtaking. I think it ranks up there with the best work of all of the musicians involved.

The band itself was unfortunately short-lived. They only played a small handful of dates before the members all moved on to other projects. Fortunately there are some examples of them performing the material on the internet, such as this footage of them playing "New York Minute" in Japan:

<iframe width="560" height="315" src="https://www.youtube.com/embed/Cjmtb0wOXxo?si=zGGSGoQ0yX7tq2yg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

*The New Standard* was recently reissued on vinyl for the first time earlier this year. So it is a fine time to revisit this forgotten classic in Herbie Hancock's lengthy discography.

{% iframe 'https://open.spotify.com/embed/album/6MyskgWSf6Gg3wiMJzLCl3?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B073LSPW28/?id=JAc0x4QPxr&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/the-new-standard/1444013083' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/568486' '100%' '96' %}
