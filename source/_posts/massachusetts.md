---
title: Scud Mountain Boys - Massachusetts
author: Phil Plencner
date: 2020-11-06 09:58:13
tags:
- scud mountain boys
- country music
- country rock
- alternative country
- sub pop records
---

Today's pick is the excellent 1996 studio album from The Scud Mountain Boys called *Massachusetts*.

The Scud Mountain Boys were formed by Joe Pernice (who later went on to greater indie-rock fame as part of The Pernice Brothers and a bunch of consistently great solo albums). Obviously, they are from the great state of Massachusetts and the songs on this album are set in that area.

*Massachusetts* is where Joe's songwriting really came into focus. The earlier albums,*Pine Box* and *Dance The Night Away*, had their moments but this is where it all came into focus as a front-to-back masterpiece. 

Extremely detailed lyrics describing heartbroken, downtrodden characters....with an excellent backing band providing the perfect mood. 

I've been re-obsessing over this record this week and it still holds up the same as it did since the first time I heard it almost 25 years ago.

 <iframe src="https://open.spotify.com/embed/album/44fJ7ZBG2j6YPcu7FSolBL" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>