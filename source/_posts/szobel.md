---
title: Hermann Szobel - Szobel
date: 2024-07-19 14:59:04
tags:
- hermann szobel
- szobel
- progressive rock
- jazz fusion
- avant-rock
- frank zappa
---

![](Szobel.jpg)

The story of Hermann Szobel is so crazy that it defies reality. Hearing it for the first time you'd likely think it was unbelievable and not true. However, I assure you it is not the work of fiction.

Hermann Szobel is the nephew of music promoter Bill Graham. Graham is famous for his work starting in the 1960s with the clubs the Fillmore East and West along with major festivals like *Day on the Green* and the *US Festival*.

Hermann Szobel was born and grew up in Austria, but because of his association with Bill Graham he came to the United States in 1975 as a very technically accomplished seventeen-year-old piano virtuoso. He allegedly sat in with Roberta Flack in the studio when she was recording *Feel Like Makin’ Love* (thanks again to Bill Graham) and people in the studio were so blown away they let him assemble his own band to record a album.

The album was simply titled *Szobel* and is today's pick.

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

*Szobel* (released on the major label Arista Records in 1976) is a completely unique record. It is only 5 songs, spanning almost 40 minutes (a huge chunk of it consumed by the 12 1/2 minute "The Szuite*) and runs the gamut from heavy funk to blindly fast jazz fusion workouts that sound like unreleased Frank Zappa songs from the *Hot Rats* / *Waka/Jawaka* / *Grand Wazoo* era. Truly, mind blowing stuff.

The album starts off with "Mr. Softee" and is racing right out of the gate with a complex jazz-funk workout. 

Next is the aforementioned "The Szuite" which has a heavy Magma / Zeuhl influence along with more funk plus some improvised free jazz blowing from the saxophone.

"Between 7 and 11" follows with contains so many tricky stops and starts that it sounds like the band and their instruments are falling down a flight of stairs while continuing to play.

"Transcendental Floss" sounds like it could be a Tony Williams Lifetime outtake with its heavy almost metal take on jazz fusion, but it also includes some very Ruth Underwood / Zappa inspired vibraphone and marimba sprinkled on top. 

Finally, "New York City, 6 AM" starts out with a heavy drum intro but follows into a more slow burning tune...almost minimalist compared to the rest of the album!

The whole record is a front-to-back classic that has so many twist and turns that I've listened to it countless times and still hear new things each time.

Unfortunately, it was Hermann Szobel's only recording. The stories vary as to what happened: They range from he had a falling out with the other musicians while recording a follow up album to he hated his management team and grew frustrated with the music industry. Whatever was the case he suffered from a nervous breakdown and literally disappeared. Nobody has basically seen or heard of him since 1976. 

In fact, his mother filed a missing person's report in 2002. There have been rumors of his whereabouts (San Francisco and Jerusalem are two places he supposedly has been spotted) but nothing concrete has come out that definitively accounts for what he has been doing and how he has been living for the last 49 years.

Luckily, we have the *Szobel* record to confirm that he does exist and was an amazing musician and composer. Since he'll likely never record again, this is a crucial document of an obscure musician that should be of greater renown. 

 You can [listen to Hermann Szobel - *Szobel* on your streaming platform of choice](https://songwhip.com/hermannszobel/szobel) (including Spotify, Amazon Music, Apple Music, Tidal, YouTube Music and more!).