---
title: Michael Manring - Thonk
date: 2024-08-09 21:42:14
tags:
- michael manring
- alex skolnick
- tim alexander
- steve smith
- michael hedges
- primus
- journey
- testament
- jazz fusion
- jazz rock
- funk
---

![](Thonk.jpg)

Michael Manring is a virtuoso bass player, who is mostly known for his association with the Windham Hill record label and playing with guitarist Michael Hedges. Which is to say he's often pigeonholed as a "new age" musician. This isn't exactly fair.

Even when playing in those situations, he was playing some really wild stuff. Take for example this live footage from the mid 80s of him playing "Out on the Parkway" with Michael Hedges originally on the record *Watching My Life Go By*:

<iframe width="560" height="315" src="https://www.youtube.com/embed/cpn51s0-Kgc?si=w2JzHNZK6_d4eCvi" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

That being said, the music is definitely on the mellower side. Which is why it was surprising that in the early 90s he started playing with hard rock musicians.  One of the first examples of this was his 1994 albun *Thonk* which is today's pick!

*Innerviews* conducted [a very informative interview with Michael Manring from 1994](https://www.innerviews.org/inner/manring-1) that explains his approach and thought process on the record.

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

The musicians that play with him on *Thonk* include drummers Steve Smith (of Journey fame) and Tim "Herb" Alexander (of Primus fame). 

Steve Smith was originally with Journey during their superstar years: 1978 through 1985. (He rejoined Journey again in the late 90s).

Here he is playing a quick drum solo with Journey in 1981:

<iframe width="560" height="315" src="https://www.youtube.com/embed/bh2AKbsXZpk?si=4Wfgnk9qPKTNt0pH" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

He also had his own jazz fusion group called Vital Information, with Tom Coster on keyboards and Jeff Andrews on bass. Here they are in their prime in 1992:

<iframe width="560" height="315" src="https://www.youtube.com/embed/7Fu8hm7-KqA?si=8Dd7pL7f8q7XJhMd" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

He also excelled in playing more traditional jazz. He was one of the musicians picked by Neil Peart to play on the *Burning For Buddy* Buddy Rich tribute records and concert. Here he is absolutely nailing "Nutville" with the Buddy Rich Big Band:

<iframe width="560" height="315" src="https://www.youtube.com/embed/EQJ8sOnQKhk?si=IPUI2EA3U6Ln-MwG" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Tim Alexander's drumming was a huge part of the unique nature of Primus' music. His interesting polyrhythmic playing is intricate enough that it stands on its own. Here he is playing many Primus songs solo (along with songs from his side-project called Laundry):

<iframe width="560" height="315" src="https://www.youtube.com/embed/H8QpVMyENls?si=rleG3JwK3wlalcdx" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

The guitars on *Thonk* are performed by Alex Skolnick. At the time, he was the guitarist in thrash metal band Testament! So this was really outside-of-the-box thinking in 1994 (even though Alex Skolnick's career after he left Testament has focused almost exclusively on jazz and progressive rock). Here is some awesome footage of Alex Skolnick and the rest of Testament recording their classic record from 1989 *Practice What Your Preach*:

<iframe width="560" height="315" src="https://www.youtube.com/embed/x0MxtZlE9bM?si=_hFt-8epcAfr9Zzn" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

So what does *Thonk* sound like? It is heavy progressive rock mixed with a 90s alternative sound including a huge dose of Primus-inspired playing. 

The introduction to "Disturbed" especially sounds like it was a Primus outtake, but it goes into a more heavy metal direction for most of the tune before returning to the Primus style for the outro.

<iframe width="560" height="315" src="https://www.youtube.com/embed/YQecb-J8M18?si=lTkSG_wIn8Qrraqw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Songs that include Steve Smith (such as "Snakes Got Legs" and "On A Day of Many Angels" have a more jazz fusion influence.

There are also a few songs on the album that are just Michael Manring playing solo, including "Monkey Businessman", "My Three Moons" and "The Enormous Room". These songs are performed on an instrument called a Hyperbass. What is a Hyperbass? It is specially designed by Joseph Zon and Manring. Special tuning pegs and a special bridge allow instantaneous tuning change of single strings as well as of all strings simultaneously by the action of several tiny levers, a system like that of the TransTrem guitar. 

Here is Michael Manring playing "Monkey Businessman" on the Hyberbass at the NAMM Convention in 2005:

<iframe width="560" height="315" src="https://www.youtube.com/embed/QpI_nGUOrBo?si=3uWtSHZgetRvNf7d" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

This is a performance of Manring playing "The Enormous Room" in a living room concert:

<iframe width="560" height="315" src="https://www.youtube.com/embed/aY4Ra2KOyas?si=B5OY3eEcecRwj04e" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

I don't believe Michael Manring played live concerts with Alex Skolnick, Steve Smith or Tim Alexander. However, he did play solo concerts performing tons of music from *Thonk* in 1994. This is one example (It also includes a mind blowing performance of "My Three Moons" which is performed on 3 separate basses simultaneously):

<iframe width="560" height="315" src="https://www.youtube.com/embed/26LZC5CATsU?si=lBU6H1xjocvQ4QMi" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Michael Manring formed a group called Attention Deficit with Alex Skolnick and Tim Alexander after *Thonk*. They released a couple good records, but I don't think either of them matched the high levels achieved by *Thonk*. An incredible record that I have played dozens of times since I first bought the CD 30 years ago and it never gets old. A timeless classic!

{% iframe 'https://open.spotify.com/embed/album/0qJwlwmLJkQwGqcUYdKNSd?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B09LV814YN/?id=GfeG9Npz0v&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/pl/album/thonk/1595552826' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/205224497' '100%' '96' %}

