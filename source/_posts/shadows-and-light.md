---
title: Joni Mitchell - Shadows and Light
date: 2024-03-22 21:05:21
tags:
- joni mitchell
- jaco pastorius
- pat metheny
- charles mingus
- tom scott
- jazz fusion
- 70s rock
---

![](ShadowsAndLight.jpg)

Today Joni Mitchell announced that her music would [return to Spotify after a two year absence](https://pitchfork.com/news/joni-mitchell-returns-music-to-spotify-after-two-year-protest/). The music was available pretty much immediately after the announcement, which is welcome news to heavy Spotify users like myself.

This made me want to revisit some of my favorite Joni Mitchell records. Her beautiful record from 1972, *Blue*, gets plenty of accolades (and rightly so) but it isn't the one I reach for the most. Her association with Bob Dylan's *Rolling Thunder Revue* or The Band's *Last Waltz* also would be easy choices, but that's not how I roll.

It's probably not surprising that my favorite era of Joni Mitchell's music is in the mid to late 1970s when she ditched folk rock and went all-in on jazz fusion.

This started with *Court and Spark* in 1974, which features Tom Scott and the L.A. Express as her backing band. Tom Scott was a hotshot saxophone player who played on tons of studio sessions before hooking up with Joni Mitchell. Here they are backing Joni Mitchell on the supporting tour for *Court and Spark* in 1974 playing "Help Me":

<iframe width="560" height="315" src="https://www.youtube.com/embed/ix3O8iy3suE?si=Hlz69PinY-XhihY3" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

*Court and Spark* was the start of a long line of albums that Joni Mitchell recorded that we're basically wild jazz fusion records with an amazing singer on top (which was actually kind of rare and unique at the time): *The Hissing of Summer Lawns* (in 1975), *Hejira* (in 1976), *Don Juan's Reckless Daughter* (in 1977) and *Mingus* (in 1979). Along with Tom Scott and the L.A. Express, there were a ton of other great famous jazz players who recorded with Joni Mitchell during this time: Joe Sample, Larry Carlton, John Guerin, Airto Moreira and Herbie Hancock are notable examples. 

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

Two players who frequently played on the records and were a huge part of the sound and style was saxophonist Wayne Shorter and bassist Jaco Pastorius (who were both also in the band Weather Report at the time). You can hear them play on most of the albums from this period. 

As you might imagine the record called *Mingus* was originally supposed to be a full-on collaboration with the legendary bass player and composer. Unfortunately, he passed away before the recording took place. Nevertheless Joni Mitchell still pushed forward with the project recording songs that Mingus wrote specifically for the album along with the classic "Goodbye Pork Pie Hat".

The culmination of this period was the live record from a concert in 1979 at the Santa Barbara Bowl called *Shadows and Light*, which is today's pick!

*Shadows and Light* features an incredible band supporting Joni Mitchell (who sings and plays guitar in absolutely top form here). The musicians performing were Pat Metheny (guitar), Jaco Pastorius (bass), Don Alias (drums), Lyle Mays (keyboards) and Michael Brecker (saxophone). A couple songs also include a cappella group The Persuasions, who were the opening act that day.

The songs run the gamut of her jazz focused discography at the time, playing songs from all the records I previously mentioned. Along with the audio, it was also recorded as a video, which was later released on VHS and DVD. An incredible document of an amazing concert. I'll post a few highlights from the video below, which will hopefully whet your appetite for the entire performance. 

Here they are playing "Coyote" from *Hejira*:

<iframe width="560" height="315" src="https://www.youtube.com/embed/DHQfIwyEVzY?si=zSXRpDxwSGQjjVhn" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Another highlight was "Black Crow" also from *Hejira*:

<iframe width="560" height="315" src="https://www.youtube.com/embed/4GLJCZ5L2sQ?si=4qxsYdTtYIj7eMIU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Jaco Pastorius is featured on "The Dry Cleaner From Des Moines" off of *The Hissing of Summer Lawns*:

<iframe width="560" height="315" src="https://www.youtube.com/embed/ZbPorrmjWHM?si=GEev67TcSoYrCv4C" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Finally, here is Randy Brecker going wild on "Goodbye Pork Pie Hat":

<iframe width="560" height="315" src="https://www.youtube.com/embed/ARfUn2scZRE?si=vAs_scN5bdV4eqMg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

The stunning show is pretty much front-to-back awesomeness. I hope you are able to dive into the entire 90 minute recording.

{% iframe 'https://open.spotify.com/embed/album/0sk9dYm1TZbsxJ5hIEBuby?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B0045X8NLG/?id=rdrZSbvVGu&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/shadows-and-light-live/396470322' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/68708663' '100%' '96' %}
