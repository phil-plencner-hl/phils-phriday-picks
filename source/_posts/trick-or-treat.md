---
title: Trick Or Treat
date: 2019-10-25 14:07:40
tags:
- halloween music
- the cramps
---

Cherry Red / Rightous records recently released an excellent compilation of vintage / novelty records based around Halloween (Werewolves, Dracula, Zombies, Frankenstein's Monster etc) , curated by Lux Interior and Poison Ivy from the always excellent psychobilly band The Cramps. 

It flopped into my letter box earlier this month and has been a great (trick) or treat, getting plenty of repeated spins in Casa De Plencner. 

Sadly, it doesn't appear to be on Spotify, but I was able to piece together almost all the songs into a playlist for your enjoyment!! 

Happy Halloween!!

<iframe src="https://open.spotify.com/embed/playlist/4xiKaaZhlLWsDvGdmby82m" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>