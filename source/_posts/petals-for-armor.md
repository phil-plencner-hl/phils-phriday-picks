---
title: Hayley Williams - Petals For Armor
author: Phil Plencner
date: 2020-05-08 15:09:06
tags:
- hayley williams
- paramore
- pop music
- punk rock
- ska
---
Ex-Paramore singer Hayley Williams new album is finally fully out today. 

She has been putting it out piecemeal throughout 2020...but this is the complete album. Pretty different than what you might think of for "solo album from that singer from Paramore" and that's definitely a good thing. 

Happy Friday.

<iframe src="https://open.spotify.com/embed/album/4HXpQ5KQBVWN25ltjnX7xa" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>