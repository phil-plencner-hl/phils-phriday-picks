---
title: Estradasphere - Buck Fever
date: 2021-03-31 08:36:37
tags:
- estradasphere
- secret chiefs 3
- trey spruance
- 90s rock
- jazz fusion
- jazz rock
- progressive rock
- black metal
---

I first experienced the band Estradasphere back in the year 2000. I went to see Trey Spruance's (Mr. Bungle, Faith No More) weirdo music ensemble Secret Chiefs 3. Estradasphere was the opening act.

They were a large group (including 2 horn players, a violin player, guitar, bass, drums). Not only that, they had a bunch of additional performers with them (fire eaters, jugglers, the "death metal cheerleaders" etc). They played a wild mix of just about every kind of music you could think of...jazz, rock, metal, folk, dance, country etc. 

They only had one album out at the time (the excellent *It's Understood*) and I promptly bought it and became a rabid fan that day.

If you want to see some great footage of the band during this era, their old home video (literally a video...I bought it on VHS at one point) called [*These Are The Days* is available on YouTube](https://youtu.be/Ov7lulxTYGM).

The following year they put out what I think was their definitive musical statement: *Buck Fever*.

*Buck Fever* upped the anti in every possible way. The entire album follows a loose concept about a group of deer hunters. The music is extremely diverse, but all excellently played. There's not a single dull moment on the entire 70+ minute running time. At the time they marketed themselves with strange new self-invented genres such as  "Bulgarian Surf", "Romanian Gypsy-Metal", and "Spaghetti Eastern". I can't argue with those descriptions. 

They even put out a super-cool music video about a full-on black metal song on the album about an uprising of Elk!

<iframe width="560" height="315" src="https://www.youtube.com/embed/OhaTjL1foDY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Estradasphere put out a few more albums and eventually disbanded. Some of the members went on to become Amanda Palmer's backing band for a little while. Others continued with other bands following similar directions. Most recently [High Castle TeleOrkestra](https://highcastleteleorkestra.com) and [Red Fiction](https://www.redfiction.com) are two examples. 

*Buck Fever* remains my favorite to this day.

[Buy *Buck Fever* at Amazon](https://amzn.to/2QLBcji)

<iframe src="https://open.spotify.com/embed/album/5s3ETtgAx4HWqm7dWBoktN" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>


