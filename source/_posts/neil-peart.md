---
title: Neil Peart
author: Phil Plencner
date: 2020-01-13 12:10:17
tags:
- neil peart
- rush
- progressive rock
- 70s rock
- 80s rock
- hard rock
---

SPECIAL MONDAY EDITION OF PHIL'S PHRIDAY PICKS

As you likely heard over the weekend...Neil Peart, the drummer of the rock band Rush passed away from a 3 year battle with brain cancer at the age of 68. 

This hit me particularly hard as he was practically a life-long musical inspiration for me. It started when soon after I started learning to play drums at the age of 10. My cousin heard of my new-found hobby and made me a mix-tape that contained the Working Man drum solo from *All The World's A Stage* ("Ladies and Gentlemen, The Professor on the drum kit!"). Needless to say, I was hooked from that point on. 

A couple years later, after saving money from birthday cards I received for a couple years I bought the 2LP *All The World's A Stage* album [pictured here](https://www.instagram.com/p/B7KU8LrJJq7/) and away we go!
 
 Counting ticket stubs, I saw Rush in concert no less than 8 times, plus I even went to see a tour of his [DRUM SET](http://www.2112.net/powerwindows/transcripts/20050815neilpeart.htm) at one point along the way  . I also read all [his books about motorcycle riding and life](https://www.amazon.com/s?k=neil+peart&i=stripbooks&crid=3JTYLJK2LTK9C&sprefix=neil+peart%2Caps%2C167&ref=nb_sb_ss_c_2_10) all of which were pretty inspiring and inspirational, but a special shout-out goes to *"Ghost Rider"* if you only can read one. 
 
 You could say I was a pretty huge fan.

Also, my friend Hank wrote [an excellent piece in Rolling Stone](https://www.rollingstone.com/music/music-features/neil-peart-rush-drumming-tribute-936430/) in tribute to Neil Peart that is worth reading.

Anyways, here is a playlist I made several years ago that collected all of the instrumental songs by Rush (tracks 1-12) plus chronicled all of Neil's recorded drum solos (starting with aforementioned Working Man solo on track 13). 

Let's crank it up!

<iframe src="https://open.spotify.com/embed/playlist/7g3ou4bAZhYCJFlK7MJbVZ" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>