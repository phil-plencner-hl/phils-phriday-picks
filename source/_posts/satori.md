---
title: Lee Konitz - Satori
author: Phil Plencner
date: 2020-04-16 16:02:32
tags:
- lee konitz
- jazz
- swing
- dave holland
- jack dejohnette
---
Alto Saxophone legend Lee Konitz passed away yesterday, due to pneumonia / COVID-19 complications. 

His contribution to jazz is impossible to quantify at this point....but this particular album from the 70s is one of my personal favorites. 

It's hard to go wrong when your backing band consists of Martial Solal, Dave Holland and Jack DeJohnette.

<iframe src="https://open.spotify.com/embed/album/2TPsvg33u4ad2lLIlfvUDu" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>