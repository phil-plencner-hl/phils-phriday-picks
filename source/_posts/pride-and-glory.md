---
title: Pride And Glory - s/t
Author: Phil Plencner
date: 2020-09-04 14:44:20
tags:
- pride and glory
- 90s alternative rock
- zakk wylde
- ozzy osbourne
---

Happy Friday! 

Today's pick is the only album by the Zakk Wylde-fronted group "Pride & Glory". 

Zakk Wylde was a member of Ozzy Osbourne's band (he wrote the song "No More Tears" and was part of that album's overall monster success).  

Soon after the No More Tears tour in 1994, he quit Ozzy's band to start the more bluesy / laid back Pride & Glory and play clubs. The album is definitely a HUGE departure from his work with Ozzy and lead the way for the band he started next called Black Label Society that had more continued success for the next couple decades in metal circles (though obviously not Ozzy levels of fame). He occasionally rejoins and tours with Ozzy, but more as a sideman and not part of a functioning group.

Funny aside, when Zakk was touring behind *"1919 Eternal"* in 2002 he played the House of Blues in Chicago. I ended up interviewing him for the startup I was working for. He kept up a persona of the tough biker / metal guy through the interview. When it was over and the cameras were shut off....he invited me to hang around and watch *The Simpsons* with him and drink beers! A total persona shift as he basically giggled like a little kid through the whole *Simpsons* episode and goofed around with everyone in the room. 

Anyways! Pride & Glory!

<iframe src="https://open.spotify.com/embed/album/0zPLvNvzNmyJvH72Vq7oIm" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>