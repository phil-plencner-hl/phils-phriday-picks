---
title: Animal Logic - II
date: 2022-04-15 10:48:20
tags:
- animal logic
- the police
- return to forever
- stewart copeland
- stanley clarke
- deborah holland
- 90s rock
- pop music
- 80s rock
---

After The Police broke up, drummer Stewart Copeland was mainly focused on writing movie soundtracks, jumping off of his success of *The Rhythmatist*. However, he eventually got the itch to play rock again and looked into starting another band. He teamed up with virtuoso bass player Stanley Clarke, who was best known with his association with Chick Corea and Return To Forever. 

They agreed that they wanted the group to have a female singer, and after holding a long series of auditions chose the then unknown vocalist Deborah Holland. Not only was she a very gifted singer, reminiscent of Linda Ronstadt, she was also a great songwriter. This seemed to be a perfect fit.

They originally were called Rush Hour and convinced Andy Summers to play guitar. However, he thought it would be compared too much to The Police with his association, so it was short-lived. After he left, they became known as Animal Logic.

They never fully settled on a full-time guitarist, but Michael Thompson and Rusty Anderson appeared with them in concert and on their two studio albums.

The band had a pretty unique sound. Stanley Clarke's jazzy basslines, mixed with Stewart Copeland's funky and powerful drumming and Deborah Holland's poppy, country-inspired vocals on top is a pretty intoxicating combination.

Here they are appearing on The Late Show With David Letterman in 1989:

<iframe width="560" height="315" src="https://www.youtube.com/embed/GINB-5bfkiw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Copeland is really swinging for the fences here. I don't think he can ever play a subtle beat. I also love the ending with the massive single stroke drum rolls. A master.

Here is another great live clip of them performing on The Tonight Show With Johnny Carson in 1991. Same deal: Great song? Check! Beautiful singing? Check! Unbelievable bass? Check! Slamming drums with machine-gun drum roll ending? Check!

<iframe width="560" height="315" src="https://www.youtube.com/embed/fHvJhj7DeM4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Unfortunately, after their 2nd album they broke up. Stanley Clarke wanted to focus on his own soundtrack work (He worked on *Pee-Wee's Playhouse*, *Boyz 'N The Hood*, *Passenger 57*, and *What's Love Got To Do With It* in pretty quick succession after the breakup.) 

Deborah Holland went on to a solo career that had some solid moments, but never regained the popularity of Animal Logic.

Stewart Copeland, of course, remains awesome to this day.

Animal Logic's 2nd album *II* is this weeks pick.

<iframe src="https://open.spotify.com/embed/album/1WlOLrGthdWlH4suNLJPxT?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

<iframe id='AmazonMusicEmbedB001QSEE0C' src='https://music.amazon.com/embed/B001QSEE0C/?id=Y7F8ig4D0X&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/es/album/animal-logic-ii/716740799?l=en"></iframe>

<iframe src="https://embed.tidal.com/albums/2283174" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>