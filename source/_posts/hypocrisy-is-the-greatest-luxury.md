---
title: Disposable Heroes of Hiphoprisy - Hypocrisy Is The Greatest Luxury
date: 2022-06-10 10:50:37
tags:
- disposable heroes of hiphoprisy
- michael franti
- spearhead
- charlie hunter
- hip-hop
- rap music
- 90s rock
- industrial
---

![](Hypocrisy.jpg)

Before Michael Franti formed Spearhead and became well-known for his sunny, reggae influenced music, he focused on much harsher, more politically charged music.

The first widely known example of this was the industrial/hip-hop group called The Beatnigs. Drummer Rono Tse was also part of this heavy-hitting ensemble. 

Both Rono Tse later went on to form the more successful Disposable Heroes of Hiphoprisy, which took the concepts The Beatnigs had to a much larger scale.

Disposable Heroes of Hiphoprisy's most famous song "Television, The Drug Of The Nation" was initially a Beatnigs song. Here they are performing it in 1989, complete with power tools percussion:

<iframe width="560" height="315" src="https://www.youtube.com/embed/ZvLUXbS2a5s" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Along with Michael Franti and Rono Tse, Disposable Heroes Of Hiphoprisy also included jazz guitarist Charlie Hunter before he gained larger fame playing 8-string guitar in instrumental ensembles. 

It is interesting to hear him in this context. The crazed, Public Enemy / Bomb Squad style beats, Gil Scott-Heron inspired spoken word and lyrics encased in a  barrage of samples is pretty different than what he (and even Michael Franti) are known for nowadays.

Here's Disposable Heroes Of Hiphoprisy's take on "Television":

<iframe width="560" height="315" src="https://www.youtube.com/embed/hD9pJzZ1XGI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

They were also a powerful, live act. Here they are performing "Famous And Dandy (Like Amos And Andy)" (ironically?) on late night television:

<iframe width="560" height="315" src="https://www.youtube.com/embed/b7XMXuITf70" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Here they are performing their version of the Dead Kennedy's "California Uber Alles" at a club show:

<iframe width="560" height="315" src="https://www.youtube.com/embed/mkKDlfSApQU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Their live act was so good they eventually opened for U2 on a portion of their *Zoo TV* tour, along with Primus!

U2 was so into the group, that even after they were off the tour itself, their music was still incorporated into their massive show introduction. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/WfL-UAHcJME" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Disposable Heroes Of Hiphoprisy only put out two albums (the second one being the backing band for William S. Burroughs reading selections of his works). So their first album, *Hypocrisy Is the Greatest Luxury*, remains their definitive musical statement.

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/album/1FJ9XKn6aTTigSG5GTQaXZ?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

<iframe id='AmazonMusicEmbedB000V656PC' src='https://music.amazon.com/embed/B000V656PC/?id=H9r0Ew8EmN&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *; clipboard-write" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/hypocrisy-is-the-greatest-luxury/1442921405"></iframe>

<iframe src="https://embed.tidal.com/albums/35657787" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>