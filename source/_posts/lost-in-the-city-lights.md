---
title: Johnny Falstaff - Lost In The City Lights
author: Phil Plencner
date: 2020-03-27 15:59:44
tags:
- johnny falstaff
- country music
- texas music
---
How about some old school traditional sounding country music for a Friday? 

Texas country-music-lifer Johnny Falstaff recently put out a new album that is excellent. 

I look forward to the days where I too can become Lost In The City Lights again. 

Happy Friday everyone.

<iframe src="https://open.spotify.com/embed/album/1pMCMi9PaeA0Kngu84puek" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
