---
title: Chad Smith's Bombastic Meatbats - Meet The Meatbats
date: 2025-02-28 21:36:40
tags:
- red hot chili peppers
- deep purple
- chad smith
- jeff kollman
- kevin chown
- ed roth
- glenn hughes
- meatbats
---

![](MeetTheMeatbats.jpg)

Drummer Chad Smith is most well-known for his long time membership in the Red Hot Chili Peppers. He was not the original drummer of the band (that would be Jack Irons) but joined the group at the beginning of their widespread popularity. His first recording with the Chili Peppers was on 1989's *Mother's Milk*. This was also the first album with on and off again guitarist John Frusciante. Unlike Frusciante, Chad Smith has appeared on every single Red Hot Chili Peppers record since that time. 

The first single off of *Mother's Milk* was their absolutely ripping cover of Stevie Wonder's "Higher Ground". Talk about an amazing debut performance!

<iframe width="560" height="315" src="https://www.youtube.com/embed/X3mRasO5bvo?si=tQ1sTNc0YHUxNaZk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

I don't need to list the many highlights and accomplishments of Red Hot Chili Pepper's career. They are one of the most popular alternative rock bands of all time. Part of their lengthy success was due to the high level of musicianship from all the members, including Chad Smith. As an example, here is a cool drum solo from the *Stadium Arcadium* tour in 2007:

<iframe width="560" height="315" src="https://www.youtube.com/embed/ufeRFrC-Lhg?si=p8cCeJLXae24_yiB" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Apparently, even that level of success was not enough for Chad Smith. In 2003 he played on the song "Get You Stoned" on bassist / singer Glen Hughes solo record *Songs in the Key of Rock*. Glen Hughes was once a member of Deep Purple (from 1974-1976 appearing on *Burn*, *Stormbringer* and *Come Taste The Band*) so it seems like Chad Smith would be an odd pairing. But it actually works really well:

<iframe width="560" height="315" src="https://www.youtube.com/embed/LmPijqdn8Y0?si=W1Mdr3_MEUXjSCil" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

This actually turned into an ongoing collaboration. Chad Smith appeared on four more Glen Hughes solo records since then. Jane's Addiction guitarist Dave Navarro also joined Chad in Glen Hughes' band occasionally. One example is "Soul Mover" which was the title track of Glen Hughes' 2005 album:

<iframe width="560" height="315" src="https://www.youtube.com/embed/WkOPuCZwzsY?si=L3XdbA5oVmV5eRai" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

While recording material with Glen Hughes in 2007, Chad Smith joined other members of the band at the time in lengthy jam sessions that were heavily influenced by jazz fusion. Guitarist Jeff Kollman and keyboardist Ed Roth was the initial trio. They loved performing together so much that they started writing their own songs and also enlisted bassist Kevin Chown. This band came to be known as Chad Smith's Bombastic Meatbats. 

As an aside, Kevin Chown might be familiar to you as he was a part of Uncle Kracker's band and performed on their rendition of "Drift Away":

<iframe width="560" height="315" src="https://www.youtube.com/embed/bC22SvMXbRw?si=sTB-x9bTqL91EpYb" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

In 2008 Chad Smith's Bombastic Meatbats had enough material to record their own full length album. This album is called *Meet The Meatbats* and is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

The band had serious chops, but they did not take themselves very seriously (as if you couldn't guess from the band name). The instrumental songs often had goofy titles like "Oops, I Spilled My Beer". Here they are performing that ditty in 2011:

<iframe width="560" height="315" src="https://www.youtube.com/embed/UbOUSaKRDWk?si=uGxZI60AzANgW7M0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Great stuff! 

*Meet The Meatbats* comes out of the gates swinging with a high energy song called "Need Strange". It sounds like something that might be on a Jeff Beck record from the 1980s and features some great drum breaks and fills from Chad Smith. Killer stuff:

<iframe width="560" height="315" src="https://www.youtube.com/embed/9jRU6dp-G0E?si=MXBnlFwKjXDbLiNp" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Another blazing song is "The Battle For Ventura Blvd" which has some crazed keyboard parts throughout:

<iframe width="560" height="315" src="https://www.youtube.com/embed/RhRTK7f5PO8?si=HyUivUorG9JGW4MM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

One of the highlights of the record for me is "Into The Floyd". As the name implies, it is a tribute to Pink Floyd specifically the *Meddle* / *Dark Side of the Moon* / *Animals* era of the group. This song could definitely fit right in on one of those classic records:

<iframe width="560" height="315" src="https://www.youtube.com/embed/nGap9CEVATg?si=DWRG7EGrlHMgpRN8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

They also cover the Billy Cobham song "Stratus" (originally off of one of my favorite albums of all time: *Spectrum*). This definitely gives the original a run for its money. Old school jazz fusion fans take note!

<iframe width="560" height="315" src="https://www.youtube.com/embed/ETHMhUZiUHA?si=sjv3Y8xiShxEo3sZ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Chad Smith's Bombastic Meatbats only put out one other album (*More Meat* in 2010). Apparently, being in the Red Hot Chili Peppers (not to mention Chickenfoot the supergroup that includes Sammy Hagar, Joe Satriani and Michael Anthony) probably takes up a lot of Chad Smith's time and energy. Hopefully he finds an opportunity in the future to revive the Bombastic Meatbats. I would certainly welcome some new music from this incredible group!

{% iframe 'https://open.spotify.com/embed/album/12CV3gJNdx3Dfznk4UXcZY?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B018JHVQ4Y/?id=TgRPSaRqr8&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/meet-the-meatbats/1062413487' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/334326772' '100%' '96' %}