---
title: Various Artists - Lounge-A-Palooza
author: Phil Plencner
date: 2020-07-27 15:43:41
tags:
- lounge music
- lounge-a-palooza
- 90s swing
- lounge revival
- hollywood records
- 90s alternative rock
---

Remember in the mid-90s when there was a "Lounge Music" revival? Here is a fun compilation I re-discovered over the weekend that had Lounge singers covering 90s hits and 90s bands covering Lounge hits! Lounge-A-Palooza!

<iframe src="https://open.spotify.com/embed/album/20cXnXpRQyPV93VzaDRDh6" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>