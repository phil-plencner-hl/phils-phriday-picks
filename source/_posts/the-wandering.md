---
title: Caleb Dolister - Daily Thumbprint Collection 3, The Wandering
author: Phil Plencner
date: 2020-07-24 15:46:18
tags:
- caleb dolister
- the kandinsky effect
- progressive rock
- jazz rock
- cuneiform records
---

Today's pick is the amazing new album by the drummer from the Kandinsky Effect, Caleb Dolister. It's called *"Daily Thumbprint Collection 3, The Wandering"*. 

It's a pretty awesome progressive rock epic that was recorded over a period of 10 years, with dozens of musicians...travelling across the country to record them all separately and piece everything together. A truly inspiring work of art.

Caleb also [wrote an essay](https://medium.com/@calebdolister/the-story-of-daily-thumbprint-collection-3-the-wandering-99f6f962fb6d) about how he accomplished the writing and recording of the album over on Medium that is worth checking out to get the bigger picture.

<iframe src="https://open.spotify.com/embed/album/5dQst0kV19SG1UexPSWfFK" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>