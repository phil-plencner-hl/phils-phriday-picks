---
title: Ornette Coleman - Of Human Feelings
date: 2021-03-12 08:58:32
tags:
- ornette coleman
- bern nix
- jamaaladeen tacuma
- calvin weston
- jazz
- free-jazz
- 80s jazz
- funk
- jazz fusion
- jazz funk
---

Ornette Coleman would have celebrated his 91st birthday earlier this week if he didn't tragically pass away due to a heart attack in 2015. He is most renown for his groundbreaking work in the early 60s, pioneering the avant-jazz and free-jazz movements. 

However, my favorite music from Ornette comes from his 80s band Prime Time.

Prime Time was originally formed in the mid-70s with the release of the excellent, yet embryonic, *Dancing In Your Head*. It featured rock instrumentation (electric guitars! An excellent duo of Bern Nix and Charlie Ellerbee), rock drumming (PPP favorite Ronald Shannon Jackson was in Prime Time at this point but soon left to play with Cecil Taylor and eventually start his own band Decoding Society with Vernon Reid who went on to form Living Colour...but I digress). *Dancing In Your Head* is a great album in its own right, but the lack of sales and any record company support left Ornette disillusioned for many years afterwards.

Eventually he formed a new Prime Time band in the early 80s. He retained the two guitar players, but added young bass hotshot Jamaaladeen Tacuma and went with two drummers ("Grant" Calvin Weston along with Ornette's son Denardo).

This band explored a concept they called "Harmolodics" which basically means every instrument and whatever they might be playing is equally important. Ornette described it as "harmony, melody, speed, rhythm, time and phrases all have equal position in the results that come from the placing and spacing of ideas".

What this basically means is instead of playing free-jazz, the band is playing an amazing free-funk! There are melodic heads of the tunes, but most of the band is playing free and independent of that, while the drummers switch off between heavy funk beats and free-form soloing. 

Prime Time was so into this concept that they started their own record label (Harmolodic Records) and opened their own studio / nightclub in Texas (The Caravan Of Dreams) in order to promote this music. 

The first studio release of this concept was *Of Human Feelings*. All the songs are of very short length, with very melodic overtones, but mostly the band is playing his unhinged, cacophonous din!! They actually promoted it to jazz radio stations and thought it was going to take off with the average record buying public. It absolutely rules!!! However, it didn't take off like they expected. 

I discovered the album in a bargain bin in the early 90s. I bought it expecting 60s styled avant-garde jazz, and boy was I pleasantly surprised! It has become one of my favorite albums. It has been in regular rotation ever since that fateful day. 

I saw a reunited prime time band about 4 years ago in Philadelphia. They played a bunch of the songs off of this album. It was an incredible performance and all the members of the band hung out with the (unfortunately sparse) audience. We all ate buffet food prepared by their families (no joke!). It was without a doubt one of my all-time favorite live concert experiences. 

Best enjoyed at maximum volume!

<iframe src="https://open.spotify.com/embed/album/1VIFVF6TOsptlOxvAIA6Jj" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>