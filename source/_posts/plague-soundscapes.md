---
title: The Locust - Plague Soundscapes
date: 2022-05-06 16:46:02
tags:
- the locust
- cattle decapitation
- grindcore
- punk rock
- avant-garde
- brutal prog
- gabe serbian
---

![](PlagueSoundscapes.png)

Earlier this week, Gabe Serbian, drummer for San Diego grind-punk weirdos The Locust [passed away](https://pitchfork.com/news/gabe-serbian-drummer-for-san-diego-noise-punks-the-locust-has-died/). He was only 44 years old.

The Locust, and Gabe's drumming specifically, was a huge influence on me and my own playing in the early 2000s. I was completely immersing myself in what was casually called "Brutal Prog" around that time which was a combination of progressive rock with more heavy influences like punk rock, grindcore, and death metal. On the grindcore end of things, there were basically 3 bands I considered the pinnacle of this idea: Daughters, Agoraphobic Nosebleed and The Locust.  All 3 of them released excellent albums in 2002, coincidentally enough: Daughter's self-titled, 7" record, Agoraphobic Nosebleed's mind-boggling *Frozen Corpse Stuffed With Dope* and The Locust's magnum opus *Plague Soundscapes*. Given the recent circumstances *Plague Soundscapes* is this week's pick, but the other albums are well worth checking out if you're into this sort of thing.

The Locust's take on the sound involved very short bursts of songs, with heavy guitars, screaming vocals, very spazzy synthesizers with pummeling drums. Not for the faint hearted...this is not background music by any stretch of the imagination! An overwhelming tornado of sound with tons of details to unpack. When they played live, they also donned insect costumes to give them a pretty menacing appearance.

When Gabe Serbian first joined The Locust, he was playing guitar. However, he switched to drums in late 2001, and we're all better off for it. The man was an incredible drumming machine. Completely powerful, overwhelming and complex was the name of his game. When he played live, he put his entire body into it and would basically pass out from exhaustion at the end of their short, yet blistering sets. 

I saw them live twice during this time...and luckily both of the shows are preserved on YouTube!

The first one was at the More Than Music festival in Columbus, Ohio in 2002:

<iframe width="560" height="315" src="https://www.youtube.com/embed/L6_hyA9WTsY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

They shared the stage that day with Melt-Banana and Mastodon (*Remission* had just come out!). An unbelievable day of music.

The second show was at The Fireside Bowl in Chicago, IL the next summer in 2003:

<iframe width="560" height="315" src="https://www.youtube.com/embed/E-WfsXw6fWI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

In between those two shows *Plague Soundscapes* came out so it's very representative of their sound and approach at that time.

Later in the band's career they lengthened their songs and added more progressive elements (*Safety Second, Body Last* and *New Erections*). However, I think the *Plague Soundscapes* era was probably their high-water mark.

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/album/3IAeuXl1pBBbzA5mo0HuEz?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

<iframe id='AmazonMusicEmbedB07ZMH93PN' src='https://music.amazon.com/embed/B07ZMH93PN/?id=dupiLowJc4&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/plague-soundscapes/1485019846"></iframe>

<iframe src="https://embed.tidal.com/albums/121123192" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>