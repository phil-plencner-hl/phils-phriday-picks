---
title: Local H - Pack Up The Cats
author: Phil Plencner
date: 2020-08-19 14:55:50
tags:
- local-h
- 90s alternative rock
- pack up the cats
- grunge
---

Went down a nostalgic 90s rock wormhole today and ended up listening to a bunch of Zion, Illinois' finest: Local H! 

I love this particular album a lot...but before this "success" they bummed around the Lake County Illinois local rock scene.

<iframe src="https://open.spotify.com/embed/album/0v7rheoRfC87UP3A9737ih" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
