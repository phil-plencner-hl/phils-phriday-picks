---
title: The Conet Project
author: Phil Plencner
date: 2020-07-02 21:46:12
tags:
- the conet project
- number stations
- wilco
---
Oh man, it's late and most people are probably already starting their weekend...but I wanted to just throw out this PPP because I was reminded of it this morning when a co-worker sent that website for helping him sleep.

 One of the options in it was to listen to [Number Stations](https://en.wikipedia.org/wiki/Numbers_station) which were old short-wave radio stations what countries used in wartime to transmit coded messages. 
 
 Many of them are still running today. 
 
 Many years ago someone compiled 4 CDs worth of recordings of Numbers Stations, called it *The Conet Project* and released it. I happen to have the box set (because yeah, I collect recordings of literally everything apprently!) and it's pretty fascinating to listen to. Anyways, it's also on Spotify so join in the fascination!
 
 Also, The Conet Project's [pop-culture moment](https://www.wired.com/2004/06/wilco-pays-up-for-spycasts/).
 
 <iframe src="https://open.spotify.com/embed/album/7FVKMel4eooo1UhRpO2wuH" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>