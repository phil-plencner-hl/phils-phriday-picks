---
title: Robert Drasnin - Voodoo / Voodoo II / Voodoo III
date: 2025-01-31 22:27:11
tags:
- robert drasnin
- skip heller
- tiki music
- exotica 
- 50s jazz
- hawaii
---

![](Voodoo.jpg)

![](VoodooII.jpg)

![](VoodooIII.jpg)

I was summoned to jury duty today. As you're probably aware, this usually results in a bunch of downtime while you wait to see if you'll actually be seated in a jury for a trial. You basically bring snacks, some reading material, good headphones and wait to see what happens. 

What happened for me today was just the snacks, reading and music on headphones. I was not asked to perform the civic duty of being part of a trial.

One of the reading materials I packed for the day was the [latest issue of *Exotica Moderne*](https://houseoftabu.com/products/exotica-moderne-25). This is a quarterly magazine focused on tiki culture: Polynesian themed bars, cocktails and, of course, surf and exotica music. It was a nice escape from the cold and dreary January that we have been experiencing. *Exotica Moderne* is high quality: great writing, excellent photography and tons of information to chew on. 

Reading this periodical influenced what I was listening to on my headphones. I found myself queuing up the classic Martin Denny record...but I wanted to dig deeper into the exotica genre. Which lead me to the series of records Robert Drasnin released under the *Voodoo* banner. There ended up being three records in the series and they are all today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

In 1959, while working as an arranger for the tiny Tops Records he was asked to write his own take on the style of music Martin Denny was popularizing at the time. *Voodoo* was that record.  It didn't have a big splash at the time, but over the next several decades it became a cult favorite with a growing fanbase. In the mid-1990s during the neo-lounge music resurgence / craze (with bands like Combustible Edison) it was finally re-released on CD. This is when I first heard about it. It is a fantastic recording. The songs are laid back, yet catchy. The crystal clear production and arranging allows you to hear every instrument and every note. A very rewarding experience that bears a lot of fruit with repeat listening.

One of the strongest songs on the record is "Chant of the Moon" and it encapsulates what the whole record is about:

<iframe width="560" height="315" src="https://www.youtube.com/embed/B2cM1AL9J6w?si=b2Rky0NoiDwN-riM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Another great song is "Jardin De La Noche", which has a little more of a Spanish / Latin vibe:

<iframe width="560" height="315" src="https://www.youtube.com/embed/eZeWZRRgxNM?si=X-Au5A1bW4uyL4P1" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

In the mid 2000s, as Robert Drasnin and *Voodoo*'s popularity continued to grow he was invited to perform the material live at Tiki themed events. The band he assembled to perform the material certainly did the original songs justice. They sound incredible! Here they are playing "Chant of the Moon" at an event called *Tiki Oasis 8*:

<iframe width="560" height="315" src="https://www.youtube.com/embed/Nal4b9iJMKw?si=bMhl2HpzdxRO6kCb" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

These performances inspired the Robert Drasnin to record new material for a follow-up record called *Voodoo II*. PPP favorite Skip Heller was involved in the all aspects of the record: helping to produce and arrange the material as well as playing guitar. *Voodoo II* came out in 2007...48 years after the original *Voodoo* and very worth the wait!

Here is a cool promo video for *Voodoo II* featuring footage of the musicians in the studio:

<iframe width="560" height="315" src="https://www.youtube.com/embed/gVTUWlr6o8k?si=gnYCPKo-gMkdO3W9" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

One of the highlights of *Voodoo II* is "Tahitian Dream":

<iframe width="560" height="315" src="https://www.youtube.com/embed/DgA8FtwKZuQ?si=fD5S9Sr4Q4Y7FLsy" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Another song I really like from the record is "Sambalerro":

<iframe width="560" height="315" src="https://www.youtube.com/embed/sbCK_um0lb8?si=Ztk8C8nM4ILMwI-e" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

The group took the *Voodoo II* songs to the stage as well! Here is a live version of "Puente Doble" from that record at *Hukilau 2007*:

<iframe width="560" height="315" src="https://www.youtube.com/embed/Wk7TaxjeGmc?si=ZSLoqH1rKZ8fb6bj" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Around the time of *Voodoo II* this nice little summary of Robert Drasnin was produced. It does a good job of showing many of the highlights of his entire career. Worth watching to give you the bigger picture:

<iframe width="560" height="315" src="https://www.youtube.com/embed/GhLP96S2AmE?si=_nUMONcYyLeIQeCk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Unfortunately, Robert Drasnin passed away in 2015, in the middle of working on *Voodoo III*. Before he died, he asked Skip Heller to finish the record and release it. It's a great epilogue to the *Voodoo* series and the life of Robert Drasnin.

"Jobimiana" is one of my favorites on *Voodoo III*:

<iframe width="560" height="315" src="https://www.youtube.com/embed/-CKflVsRbKU?si=8DArnuogtKS5BpiP" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

I'm glad I had jury duty today which gave me a ton of time to reacquaint myself with the entire *Voodoo* series. It brightened my spirits and makes me realize that spring is right around the corner and better days lie ahead. I hope it gives you similar inspiration.

{% iframe 'https://open.spotify.com/embed/album/6SnfMkaU2WAvBmmiFpz892?utm_source=generator' '100%' '352' %}

{% iframe 'https://open.spotify.com/embed/album/5f2lr5T4AhHgzndOvzFqLT?utm_source=generator' '100%' '352' %}

{% iframe 'https://open.spotify.com/embed/album/14NlMCz4iiD3ka81uDTpY0?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B007CSF32S/?id=Q1pEdWnh3O&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://music.amazon.com/embed/B007CSAGQQ/?id=kunRwCCjJc&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://music.amazon.com/embed/B06XGP9KPC/?id=NDyS3X3w27&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/voodoo/504659145' '100%' '450' %}

{% iframe 'https://embed.music.apple.com/us/album/voodoo-ii/504139049' '100%' '450' %}

{% iframe 'https://embed.music.apple.com/us/album/voodoo-iii/1213090944' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/14254262' '100%' '96' %}

{% iframe 'https://embed.tidal.com/albums/14089302' '100%' '96' %}

{% iframe 'https://embed.tidal.com/albums/71219059' '100%' '96' %}
