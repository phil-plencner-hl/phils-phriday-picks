---
title: Hank Mobley - No Room For Squares
author: Phil Plencner
date: 2020-03-26 16:01:44
tags:
- jazz
- hank mobley
- classic jazz
- hard bop
- herbie hancock 
- philly jo jones
- andrew hill
- donald byrd
- lee morgan
---
I was reading a [new career overview of Hank Mobley](https://www.newyorker.com/culture/the-front-row/the-haunted-jazz-of-hank-mobley) over my morning coffee today and it reminded me of how excellent *"No Room For Squares"* is. 

Lots of jazz heavyweights in his band at the time (Herbie Hancock, Philly Jo Jones, Andrew Hill, Donald Byrd, Lee Morgan), some in early stages of their careers (1963). 

A total classic hard bop record!

<iframe src="https://open.spotify.com/embed/album/3qer0IWDm6mSLUJy4ji0Oi" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>