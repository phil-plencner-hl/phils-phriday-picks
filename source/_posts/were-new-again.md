---
title: Makaya McCraven - We're New Again
author: Phil Plencner
date: 2020-02-07 12:02:03
tags:
- makaya mccraven
- gil scott-heron
- jazz
- jazz fusion
- jazz rock
---
Today's Phil's Phriday Pick is the latest release by Chicago-based drummer Makaya McCraven. 

A "reimagining" of Gil Scott-Heron's last studio album "I'm New Here" called "We're New Again".  Using the original vocals, he puts new jazzy instrumentals underneath. Very cool and inspiring!

<iframe src="https://open.spotify.com/embed/album/7AiWJCZ2HArLkWG8HdPgIQ" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
