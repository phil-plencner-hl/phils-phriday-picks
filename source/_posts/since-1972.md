---
title: Josh Freese - Since 1972
date: 2023-05-26 11:38:36
tags:
- josh freese
- foo fighters
- nine inch nails
- devo
- a perfect circle
- danny elfman
- punk rock
- 90s rock
- alternative rock
---

![](Since1972.jpg)

Foo Fighters finally announced Josh Freese as their new drummer this week, following the unfortunate passing of long-time member Taylor Hawkins. They did so with an elaborate movie called [*Preparing Music For Concerts*](https://foofighters.com/news/foo-fighters-preparing-music-for-concerts). Its pretty amusing to watch the "tryouts" at the beginning and the excellent concert that follows. Josh's actual live debut is tonight at the [Boston Calling music festival](https://bostoncalling.com/).

The decision of Josh Freese joining the Foo Fighters is not a left-field choice by any stretch of the imagination. Since the early 90s he has been playing with a who's who of alternative rock bands (along with being a member of The Vandals). The list is impressive: Nine Inch Nails, The Offspring, Guns N' Roses, A Perfect Circle...and the list goes on and on.

To give you an idea of how well he performs with such an impressive list of bands, I'll highlight some live videos!

Here he is playing Nine Inch Nails’ "Wish" back in 2010:

<iframe width="560" height="315" src="https://www.youtube.com/embed/s0Rdn1n8yW4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

In 2014, Devo went on tour playing songs from 1974-1977 ("Hardcore Devo") and Josh was the drummer for that. Here they are playing "Uncontrollable Urge":

<iframe width="560" height="315" src="https://www.youtube.com/embed/O_gxbtxVrzw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Here is Josh performing with A Perfect Circle on Conan in 2000:

<iframe width="560" height="315" src="https://www.youtube.com/embed/iw5zjQ-6xyY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

And finally, here is more recent footage of Josh playing with Danny Elfman at Coachella last year:

<iframe width="560" height="315" src="https://www.youtube.com/embed/GwEQGKxu-OQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

In between all this activity, Josh Freese still manages to put out his own solo records! One of my prized possessions is my copy of the [picture disc of his first record *The Notorious One Man Orgy*](https://www.discogs.com/release/2870653-Josh-Freese-The-Notorious-One-Man-Orgy).

His second solo record from 2009, however, is my favorite. It's called *Since 1972* and is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

*Since 1972* does not mess around. It flies through 11 songs in less than a half hour, basically bulldozing everything in its path. A completely goofy, fun, melodic punk rock record that is so infectious that once I start listening to it, I usually put it on repeat for an additional 3-4 spins. Josh Freese plays all the instruments (with few exceptions. His brother Jason Freese plays some saxophone, Skerik lends support on keyboards and even Pearl Jam's Stone Gossard stops by playing bass on "Who Am I To Say, Really?"). His drumming is completely off the charts throughout. 

What makes this album even more bonkers is he sold it online with a name-your-price model with increasingly wild "rewards" for people who paid ridiculous amounts of money for it. Here is [the full list of rewards](https://www.nastylittleman.com/clients/josh-freese/), but I'll highlight a few good ones below.

- **$500** - Meet Josh in Venice, CA and go floating together in a Sensory deprivation tank (filmed and posted on youtube)
- **$1,000** - Josh washes your car OR does your laundry….or you can wash his car.
- **$5,000** - Josh gives you and a friend a private tour of Disneyland.
- **$10,000** - Twiggy from Manson’s band and Josh take you and a guest to Roscoe’s Chicken n’ Waffle in Long Beach for dinner.
- **$10,000** - Josh takes you and guest to “Club 33″ (the super-duper exclusive and private restaurant at Disneyland located above the Pirates Of The Caribbean) and then hit a couple rides afterwards (preferably the Tiki Room, Haunted Mansion and The Tower Of Terror).
- **$75,000** - -Josh will join your band for a month…play shows, record, party with groupies, etc…. If you don’t have a band he’ll be your personal assistant for a month (4 day work weeks, 10 am to 4 pm). Take a limo down to Tijuana and he’ll show you how it’s done (what that means exactly we can’t legally get into here).

[Wired Magazine did a really interesting interview](https://www.wired.com/2009/03/drummers-crazy/) with Josh Freese about the album and the rewards that is worth checking out.

He also released a video documentary, also called *Since 1972* which is a lot of fun:

<iframe width="560" height="315" src="https://www.youtube.com/embed/qhlHJsotz-Q" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Since I cannot make it to Boston Calling this weekend, I will instead crank up *Since 1972* and spend the next three days listening to that on repeat instead. 

(For some reason, *Since 1972* is not on Spotify, but it is on other streaming platforms! Sorry, not sorry to Spotify listeners this week.)

{% iframe 'https://music.amazon.com/embed/B0023CU9QS/?id=ZCyd4UHANw&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/since-1972/309771969' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/19138260' '100%' '96' %}
