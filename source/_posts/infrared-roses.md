---
title: Grateful Dead - Infrared Roses
date: 2022-07-15 15:18:06
tags:
- grateful dead
- drums
- 70s rock
- classic rock
- jazz fusion
---

![](InfraredRoses.jpg)

A regular feature of every Grateful Dead live show since the late 1970s was a sequence called "Drums/Space."

During this segment of their concerts, drummers Mickey Hart and Bill Kreutzmann (collectively known as The Rhythm Devils) played a lengthy drum solo. Not only including their drum sets, but usually predominant use of other percussion instruments as well.

Eventually that would segueway into the portion called "Space" where the rest of the band (and sometimes guests) would play very free-jazz / space-rock inspired improvisations. Much of the time, this included electronic noises and other unusual sounds.

It truly showcased Grateful Dead's most out-there inclinations.

For a more detailed description into what "Drums/Space" was, how it came to be and how it evolved this [blog post from 1999 by Michael Parrish is a great overview](http://deadessays.blogspot.com/2017/03/drums-and-space-guest-post.html).

Here is very early footage of "Drums/Space" from Oakland in August 1979:

<iframe width="560" height="315" src="https://www.youtube.com/embed/8Elm6ZBbcLw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

In late 1991, Grateful Dead released an album called *Infrared Roses* which compiled and morphed a bunch of recordings of "Drums/Space" from their 1989 and 1990 tours into an hour long head-trip (by Bob Bralove, the sound engineer from The Dead's tours from the mid-80s onward). It is very much one of the most bizarre, yet awesome, entries in their very lengthy discography. There are no real "songs" on this album, just the wild sounds of a massive, imaginary "Drums/Space". *Infrared Roses* is today's pick!

To see and hear how "Drums/Space" evolved over time, here is some incredible footage from Alpine Valley on the 1989 tour:

<iframe width="560" height="315" src="https://www.youtube.com/embed/sJhu8LC_k-s" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

About a year after *Infrared Roses* was released, Grateful Dead put out an accompanying video called *Infrared Sightings* which plays about 15 minutes of the music over some pretty amazing for its time (and still pretty trippy today) computer generated animation. You can watch it all online!

<iframe width="560" height="315" src="https://www.youtube.com/embed/gkhr23asO-M" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

While many people think drum solos at rock concerts are a fine time to take a bathroom break, I'm not one of those people. This is why *Infrared Roses* remains a favorite and fascinating record for me.

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/album/3GfAcOrfKnUH3HaQ361LOs?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

<iframe id='AmazonMusicEmbedB09LRNVH38' src='https://music.amazon.com/embed/B09LRNVH38/?id=DQbgC62EMH&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *; clipboard-write" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/in/album/infrared-roses-live/1595206826"></iframe>

<iframe src="https://embed.tidal.com/albums/204994847" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>