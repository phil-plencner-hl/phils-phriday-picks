---
title: Gong - Gazeuse! / Expresso
date: 2021-07-30 08:02:36
tags:
- gong
- daevid allen
- allan holdsworth
- pierre moerlin
- jazz fusion
- jazz rock
- progressive rock
---

Gong was always a weird band that had an ever-shifting lineup. The early albums were very much in a progressive rock / space rock vein. Especially with their seminal *Radio Gnome Invisible* trilogy of albums in the early 70s.

Guitarist Daevid Allen was the mastermind of the group. Which is what made it surprising when in 1975 he quit the group right before a concert because there was a "wall of force" that prevented him from taking the stage or continuing with the group!

Steve Hillage, the other primary songwriter and guitarist in the group, soon left bolstered by the success of his recent solo album *Fish Rising*.

This left Gong in even more disarray than normal. Drummer / Percussionist Pierre Moerlin ended up taking over control. He recruited jazz fusion guitarist Allan Holdsworth from Soft Machine and bassist Francis Moze from Magma. Gong then turned into an entirely instrumental, percussion-heavy jazz fusion group.

They only released one album with this particular lineup (now re-christened as "Pierre Moerlin's Gong") called *Gazeuse!*. For some reason the version released in the U.S. was called *Expresso*, but the music contained within is exactly the same.

This album ranks as possibly my favorite of all the Gong albums (the much later *New York Gong* is a close second for different reasons).

A highlight for me is the 10-minute long drum-centric piece "Percolations (Part I and II)". However, the rest of the album is also top-notch.

It includes a new version of the title track from Allan Holdsworth's solo album *Velvet Darkness* (adding further confusion by being renamed "Shadows Of"). There is also an awesome composition by Francis Moze called "Mireille" that definitely has shades of Magma / Zeuhl in it.

The lineup was short-lived with Holdsworth leaving soon after, but Pierre Moerlin pressed on until it finally fizzled out sometime in the '80s.

Daevid Allen created Gong offshoot bands in the meantime, including the already mentioned New York Gong and Planet Gong etc.

*Gazeuse!* used to be on Spotify, but due to the mysteries of the universe it is currently unavailable there for some reason. So YouTube will save the day this week.

<iframe width="560" height="315" src="https://www.youtube.com/embed/DaqUg23aKKM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
