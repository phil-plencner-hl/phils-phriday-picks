---
title: Dave Lombardo - Rites of Percussion
date: 2023-05-05 17:10:28
tags:
- dave lombardo
- slayer
- mike patton
- john zorn
- bill laswell
- fantomas
- heavy metal
- avant-garde rock
- punk rock
---

![](RitesOfPercussion.jpg)

Dave Lombardo is probably most known for his time as the drummer of Slayer, but he is so much more than that.

Of course, he was a founding member of Slayer. Appearing on all their albums from 1983's *Show No Mercy* through 1990's *Seasons in The Abyss*. Every Slayer album in that timeframe are groundbreaking classics, especially *Reign in Blood*.

I've attended many Slayer concerts over the years, but the first time is probably still my favorite. They opened for Motorhead at Chicago's Aragon Ballroom in 1988. For some reason, they set up folding chairs near the front of the stage for people to sit on during the Slayer show? Once Slayer hit the stage, the chairs instantly became flying projectiles. It was definitely dangerous...somehow, I survived to tell the tale.

Here is Slayer performing "Reigning Blood" and "Silent Scream" in 1988 to give you a sense of their intensity:

<iframe width="560" height="315" src="https://www.youtube.com/embed/9qERcmKx-50" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

My favorite era of Slayer was probably 1990-1991. For my money "War Ensemble", from *Seasons in The Abyss* is their best song. Check out the bonkers video for that song:

<iframe width="560" height="315" src="https://www.youtube.com/embed/jqnC54vbUbU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

In 1992 Dave Lombardo left Slayer. He occasionally came back for select shows and tours but has since focused on a wide variety of other bands and projects.

Soon after leaving Slayer, he formed the band Grip, Inc. They were still pretty much in a thrash metal wheelhouse but were pretty good. Here is their music video for "Ostracized" from *The Power of Inner Strength*:

<iframe width="560" height="315" src="https://www.youtube.com/embed/lnJqjZtXv5U" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Things really started to get interesting in the late 90s. Two very important things happened. The first is he appeared in the wild Matthew Barney movie *Cremaster 2*. He performed in a trio with Steve Tucker and a SWARM OF BEES. Crazy stuff:

<iframe width="560" height="315" src="https://www.youtube.com/embed/QU3IcaltbCw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

He also joined Mike Patton's new "supergroup" Fantomas along with Mr. Bungle's Trevor Dunn and Buzz Osbourne of The Melvins. 

I saw them at The Metro in Chicago in 1999, soon after their debut album came out. A bunch of confused Slayer fans were in the audience and didn't know what they were witnessing. Fantomas' songs are generally short with a ton of moving parts and moods. Straight-ahead song structures are not part of their M.O. Here they are performing a few songs during that era:

<iframe width="560" height="315" src="https://www.youtube.com/embed/rbl3yfc2TQI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Since then, Dave Lombardo has involved himself in a lot of unusual groups and projects: Dead Cross (along with Mike Patton and Justin Pearson from The Locust), Bladerunner (with saxophonist John Zorn and bassist Bill Laswell) the new version of Mr. Bungle (along with fellow thrash metal legend Scott Ian of Anthrax) and even collaborating with DJ Spooky and Public Enemy's Chuck D on a project called Drums of Death.

Here is Drums of Death's version of Public Enemy's "B-Side Wins Again":

<iframe width="560" height="315" src="https://www.youtube.com/embed/MkgjK1gOBj8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

During the 2020 pandemic Dave Lombardo like most people experienced a long stretch of downtime. Unable to tour or rehearse with the bands he was in he instead focused on writing and recording material for solo drums and percussion. 

Those songs finally see the light of day today on an album called *Rites of Percussion* which is today's pick!

Dave Lombardo [discussed the album in detail recently with The Guardian](https://www.theguardian.com/music/2023/may/04/i-worried-it-was-self-indulgent-dave-lombardo-on-slayer-going-solo-and-shaking-his-banana) and there are some great insights worth reading there.

He also released some cool music videos ahead of the album release that really whet my appetite for the full record:

<iframe width="560" height="315" src="https://www.youtube.com/embed/ypmc9WpH0-8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/IoDW7CCQBpQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Now that the full album is out, I can confidently say it is one of my favorite records released this year! Really amazing and intricate music that goes well beyond what you might typically think of as "drum solos". I will be spending a lot of time digging into it in the coming months.

{% iframe 'https://open.spotify.com/embed/album/3tdKf4z5N20gdgWfyifC9G?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B0BT1JLG7C/?id=q6E7l9w6pf&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/rites-of-percussion/1667727681' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/273301436' '100%' '96' %}
