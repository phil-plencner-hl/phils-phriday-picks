---
title: Genesis - Foxtrot
date: 2021-07-23 08:03:41
tags:
- genesis
- phil collins
- peter gabriel
- 70s rock
- progressive rock
- rush
- jethro tull
---

"Get Em Out By Friday! You don't get paid 'til the last one's well on his way."

Earlier this week I stumbled across this recently restored footage of Genesis live in France from 1973:

<iframe width="560" height="315" src="https://www.youtube.com/embed/WIZwg1t2iAo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The video that captures this event is absolutely stunning. Genesis is touring behind their brand-new album *Foxtrot*. This means we get a tantalizing clip of them playing part of "Supper's Ready" from that album, along with some other gems from the era like "The Musical Box" and "Return of The Giant Hogweed" from their previous album *Nursery Cryme*. 

The amount of technical effort that went into restoring this footage is incredible. The description in the above YouTube video goes into painstaking detail about it. There was also a [recent feature in Rolling Stone](https://www.rollingstone.com/music/music-news/genesis-concert-film-4k-1157723/) that is worth checking out.

When I was about 11 years old in 1985, I was big into the 80s variant of Genesis. The pop-rock trio of Phil Collins, Mike Rutherford and Tony Banks. I absolutely adored *Abacab* as well as the 1983 self-titled record. I also was heavily listening to Phil Collin's solo record *Face Value*....yet, I still wanted more.

I came across a Genesis album I had never heard of before in a bargain rack at Musicland called *Foxtrot* and convinced my parents to buy it for me. The album cover looked very different from anything I had seen before. It was an old looking cartoon with a bunch of strange characters on it. The song titles were very weird. Little did I realize I had unwittingly acquired my very first progressive rock album. It certainly would not be my last.

My tiny mind was blown away by what was contained within. What is this thing? There are long stretches of instrumental features without vocals. In fact, the vocals are by someone besides Phil Collins! There was an additional guitarist who was playing a bunch of really amazing parts! Again, I thought, what is this thing?

The lyrics are about topics besides love and relationships gone wrong. Most of it was way over my childish head. For example, "Get Em Out By Friday" is about landlords forcing out tenants, so they can redevelop the property for more profits. Also, "Watcher of the Skies" is about an alien's first encounter with earth. 

There was also Mellotron all over this album, which was, to me, a completely new and intriguing sound that was very enjoyable to my ear. Phil Collins' drumming was masterful. Very jazzy with lots of difficult to play parts. 

Of course there is also the focal point of the whole album: "Supper's Ready". 23 minutes long! Broken into movements like some sort of classical composition! A completely incomprehensible storyline in the lyrics!! I ate this all up as I listened to this non-stop for weeks trying to unlock its mysteries.

I still play it very regularly and it is now likely my favorite Genesis album.

Soon after my obsession with *Foxtrot*, my prog rock immersion continued when later that same year I discovered Rush's *2112* and Jethro Tull's *Thick As A Brick*...but those are probably tales for another day.

<iframe src="https://open.spotify.com/embed/album/3y67YB3vSbaopIg1VoAO1n" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>


