---
title: Urgh! A Music War
author: Phil Plencner
date: 2020-03-24 16:07:27
tags:
- urgh a music war
- 80s rock
- punk rock
- new wave
- alternative rock
---
Phil's Phriday Picks Video Edition!!

Does anyone here remember the punk rock concert film *Urgh! A Music War*? 

It was a staple on the USA Network in the late 80s/early 90s (especially during their late night programs). I taped it off of the TV back then and was delighted to find a semi-official DVD release several years ago which I also purchased. 

I was listening to the 2LP soundtrack last night and was disappointed to find that it is not on Spotify!! 

However, someone has created a playlist of the studio tracks that coincide with the live versions from the movie...they are not as good in many cases, so luckily there someone also made a [YouTube playlist](https://www.youtube.com/playlist?list=PLzRpTGQFkmxytTnjbe-l4wVcStNeFlZwa) of the concert film (that is MOSTLY there).  

The Police! Oingo Boingo! Devo! XTC! Klaus Nomi! etcetcetc!!!

<iframe src="https://open.spotify.com/embed/playlist/7rhhIbJ8zUd30oCKIy9saG" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
