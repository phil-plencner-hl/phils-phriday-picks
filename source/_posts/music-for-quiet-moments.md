---
title: Robert Fripp - Music For Quiet Moments
date: 2021-04-23 10:14:50
tags:
- robert fripp
- king crimson
- 70s rock
- progressive rock
---

For the past year, King Crimson mastermind Robert Fripp has been a very busy man. During the pandemic he's probably had the most notoriety for his [video series with his wife Toyah Wilcox](https://www.youtube.com/c/toyahofficial/videos). They've been putting out goofy / fun cover songs...this has been great, but he's also been serving up another regular series that has been flying under the radar. This series is called *Music For Quiet Moments*.

*Music For Quiet Moments* focuses on solo "Soundscape" live recordings that Fripp has performed and perfected since 1994.

"Soundscapes" are an extension of his concept of "Frippertronics" from the 1970s. Frippertronics used actual reel-to-real tapes to create looping textures for him to solo over. Soundscapes instead uses digital technology and has a much more modern / sleek sheen.

Here's a really cool example of Frippertronics from 1979:

<iframe width="560" height="315" src="https://www.youtube.com/embed/4Xjtm-RZaek" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Here's a very awesome example of Soundscapes from 2000 that Robert performed before a King Crimson show:

<iframe width="560" height="315" src="https://www.youtube.com/embed/fxfU2oe0gK0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

As a fun aside, I saw King Crimson on that tour as well. When I entered the venue early, as I'm wont to do, I went to the bar and ordered a beer. I heard ambient guitar enveloping the club and thought it might be Fripp. I looked at the stage and sure enough, there he was playing....and nobody was really noticing he was there!! I ran up to the stage right by his feet and watched him with about 2-3 other fanatics. It was mindblowing.

Anyways...*Music For Quiet Moments* released a song in this vein every Friday for the past year. It started exactly 52 weeks ago this week, and the final installment was posted today. 

I eagerly awaited each installment, so I'm not sure what I'm going to do next week.  I compiled them all into a running Spotify playlist, so you can hear the entire 8.5 hour collection of peaceful soundscapes. 

<iframe src="https://open.spotify.com/embed/playlist/0lKfImU9HavypScrCAmPUV" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

King Crimson did a similar year-long series when they celebrated their 50th anniversary a couple of years ago...a rare recording every week for 50 weeks. I collected those into a large playlist as well if people are interested in diving into 7 hours of the mighty King Crimson as well.

<iframe src="https://open.spotify.com/embed/playlist/3tN3PTaIsQwENPMbGJminH" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>


