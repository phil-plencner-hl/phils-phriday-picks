---
title: Case Of The Mondays
author: Phil Plencner
date: 2020-05-12 15:04:14
tags:
- 90s rock
- country rock
- heavy metal
---
Yesterday at the end of the work day, a PPP co-worker and I lamented the fact that we were having a "case of the Mondays". 

We determined the best way to solve this problem was to Slack each other music videos from YouTube in an attempt to vanquish the malaise. 

I compiled the shared videos and put them into a Spotify Playlist for group enjoyment. It's probably pretty easy (?) to determine who chose what songs in the playlist. 

Enjoy.

<iframe src="https://open.spotify.com/embed/playlist/2PXF4a84xVsHcnithsNxtn" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>