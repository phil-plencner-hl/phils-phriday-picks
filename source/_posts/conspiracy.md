---
title: Chris Squire / Billy Sherwood - Conspiracy
date: 2023-03-17 15:28:50
tags:
- chris squire
- yes
- jon anderson
- billy sherwood
- progressive rock
- bill bruford
- rick wakeman
---

![](Conspiracy.jpg)

Keeping track of the membership of the band Yes has always been somewhat confusing, but probably the late 1980s was the most convoluted.

Yes was still riding high on the success of "Owner of a Lonely Heart" when they released *Big Generator* in 1987 and went on an extensive tour. At the time the band included Jon Anderson (vocals), Trevor Rabin (guitar), Tony Kaye (keyboards), Chris Squire (bass) and Alan White (drums). They pushed themselves hard on the tour, scheduling close to 100 shows in 1987 and 1988. By the tour's end Trevor Rabin literally collapsed from exhaustion and Jon Anderson decided he had enough and quit the group.

Jon Anderson reunited with members of Yes from the "classic" era of 1971-1973: Steve Howe (guitar), Rick Wakeman (keyboards) and Bill Bruford (drums). The band they formed was aptly named Anderson, Bruford, Wakeman & Howe. The group also included Tony Levin on bass (of King Crimson and Peter Gabriel fame among countless others) though his name was not included in the band's moniker.

Anderson, Bruford, Wakeman and Howe played music more in the style of their 70s progressive rock heyday than of the 80s pop style.

The remaining members of Yes eventually decided to continue forward. Bassist Chris Squire befriended guitarist Billy Sherwood from the band World Trade. It was decided he would play rhythm guitar and sing for Yes.

However, things ended up not being that simple! Anderson, Bruford, Wakeman and Howe's music was not to their record company's liking...and Jon Anderson eventually reconciled with Trevor Rabin. The problem was both separate groups had written a bunch of new material in their different styles. (Including a song that Billy Sherwood wrote called "The More We Live - Let Go"). 

With urging from the record company, they all decided to join forces as a giant supergroup version of Yes! Everyone would be involved in the recording and playing of the album *Union*. Depending on who you ask it was either an interesting collaboration or a gigantic mess.

Billy Sherwood was not involved in the *Union* tour, but he remained close friends with Chris Squire.

The Yes supergroup imploded during the tour with Bruford and Wakeman being especially unhappy.  Yes scaled back to the *Big Generator* lineup for their follow-up *Talk*. After that, Billy Sherwood came back into the picture and officially joined Yes as an extra guitarist / producer and songwriter.

He ended up writing a few more yes songs such as "Open Your Eyes". Here they are performing the song in 1998:

<iframe width="560" height="315" src="https://www.youtube.com/embed/_bQNvJr2UT8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

He stuck around through *The Ladder* in 1999. Here he is along with the rest of Yes performing "Homeworld (The Ladder)" on that tour:

<iframe width="560" height="315" src="https://www.youtube.com/embed/ETmRMPHtcqk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

All the while Billy Sherwood continued to work directly with Chris Squire on a side-project. The band went under many names over the years including The Chris Squire Experiment and Chemistry before finally settling on Conspiracy.

That group finally released an album of their own material in 2000, which is today's pick! (Phew, I knew I'd get there eventually!)

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

The music is not a far stretch from what Yes was doing but was more pop oriented than the more prog rock direction Yes was heading back in at that time.

It includes a new version of Yes' "The More We Live - Let Go" along with "Open Your Eyes" and "Love Conquers All". The rest of the songs are of an equally high caliber. Highlights include the opener "Days of Wonder" and "Light In My Life". There is also an interesting collaboration with guitarist Steve Stevens (from Billy Idol's band) and drummer Michael Bland (from Prince's New Power Generation) called "Violet Purple Rose".

Conspiracy didn't perform much, but they did play some shows in 2006. Here they are playing "The More You Live - Let Go":

<iframe width="560" height="315" src="https://www.youtube.com/embed/ZSqkn7x67iE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

When Chris Squire passed away in 2015, Billy Sherwood re-joined Yes on bass as his replacement. He played with Yes for their 50th Anniversary Tour and subsequently their first post-Squire album *The Quest*. He will also be playing bass on the new Yes album *Mirror to The Sky* which will be coming out later this year.

*Conspiracy* remains an interesting Yes obscurity that doesn't get the same level of attention as some of their other offshoots, which is a shame. It is a fun album that deserves wider recognition.

{% iframe 'https://open.spotify.com/embed/album/3X8O50Wok21VpKhxHQkQB7?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B000QZVNQE/?id=Tby8n79Bb2&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/conspiracy/260209457' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/12858897' '100%' '96' %}
