---
title: Brian Eno - Apollo Atmospheres And Soundtracks
author: Phil Plencner
date: 2019-07-19 15:42:24
tags:
- brian eno
- progressive rock
- space rock
---

Since everyone (including the Houghton Library!) is celebrating the 50th anniversary of Apollo 11 moon landing this week, its only proper that I highlight Brian Eno's excellent "Apollo: Atmospheres and Soundtracks" album from 1983 (which not coincidentally was also released today in an expanded edition!).

<iframe src="https://open.spotify.com/embed/album/1Km58i317Pm5bQR3wPHKcO" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>