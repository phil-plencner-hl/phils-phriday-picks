---
title: Gary Numan - Dance
date: 2021-04-01 08:11:17
tags:
- gary numan
- tubeway army
- mick karn
- japan
- 80s music
- jazz fusion
---

Gary Numan was massively successful in 1981. He had 3 huge singles with "Down In The Park", "Complex" and especially "Cars". He was selling out arenas. 

This is why it was so shocking that he announced at that time that he was retiring from performing and broke up his band The Tubeway Army.

He ended this phase of his career by playing a sold out show at Wembley Area, with a huge stage show that cost over $150,000 (certainly not chump change in 1981!).

<iframe width="560" height="315" src="https://www.youtube.com/embed/Gc0KOW-w1KQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Afterwards he assembled a new "studio band" comprising Roger Taylor (from Queen) on drums along with members of the band Japan...Rob Dean on guitar and Mick Karn on fretless bass (who's fingerprints are ALL OVER the album they ended up releasing).

As an aside, I simply adore Mick Karn's playing. I think he is a vastly underrated player. Here is some killer footage of him doing what he does best in a live setting

<iframe width="560" height="315" src="https://www.youtube.com/embed/7IanvhpoGCA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


Anyways, the album they put out was the ironically-titled *Dance* because compared to Gary Numan's previous albums it was not very easy to dance to. It was a collection of mostly subdued, jazz fusion inspired rock music. 

The first side of the original album is practically perfect. Bookended by two songs that are nearly 10 minutes apiece ("Slowcar to China" and "Cry, The Clock Said"), this is an emotional slow burn. 

They did release a single from the album, "She's Got Claws" which did decent on the charts but certainly didn't reach the sales of "Cars".

Shortly after the album's release Gary Numan reneged on his claim that he would never tour again and hit the road.

He still however stuck to this sound for a few more albums (*I, Assassin* and *Warriors*) but in my opinion this was the best album of this era (mainly because it was the only one that Mick Karn played on!)

The deluxe album released later included some great b-sides, the "title track" from *Dance* that actually didn't appear on the album, plus the excellent non-album single "Stormtrooper In Drag".

The deluxe album is the pick for the day!

<iframe src="https://open.spotify.com/embed/album/0yZIhME5Q9kHEcRFMID47w" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>


