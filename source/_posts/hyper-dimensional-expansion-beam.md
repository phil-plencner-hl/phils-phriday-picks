---
title: The Comet Is Coming - Hyper-Dimensional Expansion Beam
date: 2022-09-30 13:31:09
tags:
- the comet is coming
- shabaka hutchings
- sons of kemet
- dan leavers
- max hallett
- sun ra
- alice coltrane
- jazz
- jazz rock
- avant-garde
- free jazz
---

![](HyperDimensionalExpansionBeam.jpg)

The Comet Is Coming is an excellent British jazz trio that has been around for close to a decade. Their members use pseudonyms and consist of saxophonist Shabaka Hutching (King Shabaka), keyboardist Dan Leavers (Danalogue) and drummer Max Hallett (Betamax).

Their style is based on the cosmic free jazz of Sun Ra or Alice Coltrane but with a more modern / electronic twist. Outer space is obviously a common theme in their visual aesthetic and song titles.

Here is excellent live footage of them performing on NPR's Tiny Desk Concert back in 2019 when they were supporting their then-new album *Trust in the Lifeforce of the Deep Mystery*:

<iframe width="560" height="315" src="https://www.youtube.com/embed/gpfpYTmohAk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Shabaka's sax is run through a bunch of cool effects that acts as a great foil to Dan Leavers' electronic noises. Max Hallett adds a powerful, funky beat to the proceedings. Wild stuff.

Shabaka Hutching is very prolific outside of The Comet Is Coming. He also fronts a group called Sons Of Kemet which plays in more of an afrobeat style with an unusual quartet lineup of sax, tuba and two drummers. Check out this great video of them performing live earlier this year:

<iframe width="560" height="315" src="https://www.youtube.com/embed/tjuc1tWy-EY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Additionally, Shabaka performs with a group called the Ancestors which consists of all South African musicians. Here they are playing the North Sea Jazz Festival back in 2017:

<iframe width="560" height="315" src="https://www.youtube.com/embed/TnkjcS_yTfA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Anyways, The Comet Is Coming recently released a new album called *Hyper-Dimensional Expansion Beam*, which is today's pick!

On this album they further hone and tighten up their unique style. The album is banger after banger. There is not a wasted second in its 43-minute running time. 

Here is the wild music video for "Technicolour" to whet your appetite before diving into the full album:

<iframe width="560" height="315" src="https://www.youtube.com/embed/VN5VtWncH0A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*Hyper-Dimensional Expansion Beam* will likely be one of my favorite albums of the year once 2022 ends!

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/album/1XPsJnIVMjOt04rczMWVO4?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

<iframe id='AmazonMusicEmbedB0B725LHKN' src='https://music.amazon.com/embed/B0B725LHKN/?id=jGw5jnwst4&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *; clipboard-write" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/hyper-dimensional-expansion-beam/1635377449"></iframe>

<iframe src="https://embed.tidal.com/albums/248705024" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>