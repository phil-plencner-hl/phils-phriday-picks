---
title: John Anderson - Years
author: Phil Plencner
date: 2020-04-13 14:53:10
tags:
- john anderson
- george strait
- country music
- 80s country
---
When people generally think back to 80s traditionalist country music, George Strait is probably the first, biggest star that comes to mind. 

Coming up at the same time was John Anderson. He had a string of huge chart-toppers in the early 80s (I'm Just an Old Chunk of Coal (But I'm Gonna Be a Diamond Someday), Swingin') and even had a decent resurgence in the pop-country boom of the early 90s (Straight Tequila Night, Seminole Wind, Money In The Bank). 

Last week he released his first album in 11 years(!!!) and it is really good. I hope this will lead to more frequent missives from his studio:

<iframe src="https://open.spotify.com/embed/album/7jZerY215GlyfhhlkRV4Xp" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>