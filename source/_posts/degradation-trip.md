---
title: Jerry Cantrell - Degradation Trip
date: 2023-09-22 10:56:55
tags:
- jerry cantrell
- alice in chains
- ozzy osbourne
- mike bordin
- robert trujillo
- 90s rock
- alternative rock
- grunge 
---

![](DegradationTrip.jpg)

Alice In Chains was one of the biggest hard rock bands in the country in the 1990s. Their 1992 album *Dirt* went 5x platinum (over 5 million copies sold) and the 1995 full-length follow-up self-titled record was double platinum (over 2 million copies sold). 

While Layne Staley was the singer of the group and front man, guitarist Jerry Cantrell was a huge part of their success. He wrote most of the songs, sang backup vocals and even occasionally lead vocals with the group. A fine example of his lead singing is "Grind" from *Alice In Chains*:

<iframe width="560" height="315" src="https://www.youtube.com/embed/83gddxVpitc?si=eVfy7mwfRWXPO4YO" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Unfortunately, the rampant drug use of every member of the band derailed any follow up records from the classic lineup. Layne Staley was the most notorious, but the other members (especially Jerry Cantrell) were not much better off. 

Alice In Chains essentially disbanded in 1996, with both Layne Staley and Jerry Cantrell living mostly reclusive lifestyles. Nevertheless, Jerry Cantrell did continue to write new songs and record them.

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

He put out his first solo record, *Boggy Depot* in 1998. For this album his band included Mike Inez and Sean Kinney from Alice In Chains on most of the songs, but also included unusual guests like Primus' Les Claypool plus Norwood Fisher and Angelo Moore of Fishbone fame. *Boggy Depot* stays pretty close to the musical template of Alice In Chains but does branch out in some country / folk directions from time to time.

One of the videos released from the album was "Cut You In" which is representative of the whole record:

<iframe width="560" height="315" src="https://www.youtube.com/embed/wsAki_cekfg?si=W5lF5iAAjutS73jy" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

It didn't sell very well, and critics didn't give it high praise. Soon after, he started writing songs with Ozzy Osbourne and his band. He was hoping to have his songs and guitar playing included on Ozzy's next record, *Down To Earth*, but that didn't ultimately happen. Both things certainly didn't help the self-esteem or reclusive nature of Jerry Cantrell. (Although, he did play on every song on Ozzy's album of covers, the aptly named *Under Cover* in 2005).

Jerry Cantrell basically locked himself in his house near the Cascade Mountains and isolated himself for 4 months. During that time, he wrote 30 songs. Most of these songs ended up being included on his next solo record *Degradation Trip* in 2002, which is today's pick!

To record the songs, he recruited players who were members of Ozzy Osbourne's band at the time: drummer Mike Bordin (who was also in Faith No More) and bassist Robert Trujillo (who later went on to join Metallica). 

Before the album was released that band went on tour, playing the new songs along with Alice In Chains favorites. This tour was derailed by two events: Jerry Cantrell broke his hand playing football during the tour and could no longer play guitar. Layne Staley also passed away during the tour, which obviously affected the overall mood, but they pressed on anyways.

There were positives that came out of the tour though. Roadrunner Records agreed to put out the album. Initially they balked at the long running time and only released 14 songs in June 2002. Eventually because of the high praise the album received the full 25 songs were released in November 2002 as *Degradation Trip Volumes 1 and 2*.

Additionally, one of the opening acts for the tour was Comes With The Fall. Their singer, William DuVall eventually became the lead singer for the post-Staley Alice In Chains in 2006 and is still with them today. Their latest record, *Rainier Fog* is awesome, although that is in no small part because of Jerry Cantrell...but I digress.

"Anger Rising" was one of the songs that was released as a single with a video for *Degradation Trip* and it is a fine choice:

<iframe width="560" height="315" src="https://www.youtube.com/embed/FGuJ3tvKgo8?si=YKSH0DTo3QXp8RAE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

With two dozen songs and close to 2.5 hour run time you would think that the songs on *Degradation Trip Volumes 1 and 2* would all start to sound the same after a while, but this is not the case. Each song has a unique sound and structure, many of them including odd time signatures and fascinating lyrics that describe Cantrell's descent into madness from the drugs and self-isolation. The album holds my interest throughout the long duration. It is basically a prog-grunge album, if that makes any sense, because of all the twists and turns in the songs. Unlike more typical progressive rock, it is extremely dark and brutal which gives it an exciting edge.

Despite its length and heavy subject matter *Degradation Trip* remains in pretty regular rotation for me, especially as summer turns to fall. This is a fine time to immerse yourself in this amazing album.

{% iframe 'https://open.spotify.com/embed/album/5ka4PEFAk6yi7aeDVTIbng?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B0011ZYR6G/?id=wYqkFPPC1R&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/degradation-trip-vols-1-2/214470632' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/702781' '100%' '96' %}