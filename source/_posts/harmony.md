---
title: Bill Frisell - Harmony
author: Phil Plencner
date: 2019-10-04 14:13:52
tags:
- bill frisell
- petra haden
- jazz
- jazz rock
---
A new album from Bill Frisell, one of my all-time favorite jazz guitarists,  is always reason for celebration. 

The fact that the album also features the amazing Petra Haden on vocals is icing on an already delicious cake.

<iframe src="https://open.spotify.com/embed/album/7wfij1EcITNfNQT4yWCGJl" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>