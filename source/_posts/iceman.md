---
title: I Signori Della Galassia - Iceman
date: 2024-02-16 11:09:09
tags:
- i signori della galassia
- italian music
- progressive rock
- space rock
- 70s rock
- funk
- disco
---

![](Iceman.jpg)

I Signori Della Galassia are obscure. Even on the internet, information on this Italian group is sparse. Nevertheless, they are a pretty incredible, unique band.

I Signori Della Galassia translated in English is "The Lords of the Galaxy" which is a pretty apt name for their sound and aesthetic. They mainly sound like 70s disco music, but it is mixed with healthy doses of progressive rock, space rock and Italian pop music. It is as bizarre as it sounds, but also compelling and intriguing.

They hail from Savona, Italy which is in the far Northwest part of the country, close to France. So it is no surprise they are influenced by bizarre, space disco like the French band Rockets which treads similar ground.

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

I Signori Della Galassia only released two albums, both long out-of-print. Luckily the Medical Records label recently re-released their first album, *Iceman*, which is today's pick!

"Archeopterix" was actually a minor hit in their home country, when it was released as a single in 1979 (with "Volcano" on the b-side):

<iframe width="560" height="315" src="https://www.youtube.com/embed/6vtLFJ17iTo?si=07rOBujbkm-EqUz0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Both songs appear on *Iceman*, so there is no need to seek out the single separately.

*Iceman* starts out with the 8-minute disco / progressive rock epic "Proxyma Centauri". They apparently promoted this because they made a crazed music video for it:

<iframe width="560" height="315" src="https://www.youtube.com/embed/gzJD0LKMnk4?si=oiRRevBkclMYbE7t" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

I could not find much live footage of I Signori Della Galassia except for this interesting interview footage of them on Italian TV, where they also perform "Proxyma Centauri":

<iframe width="560" height="315" src="https://www.youtube.com/embed/7LKdcGb9aTI?si=jq4WmYS2eFToMJqo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

After the release of *Iceman* they put out another single "Luce" backed with "Eliane" which are included on the recent re-release. They lean more towards Italian pop than the disco / prog rock mix but are still great.

*Iceman* is one of those records that is great to play in the winter months, because the cold, spacey and atmospheric music fits the mood of the season. I hope you enjoy this Italian obscurity as much as I do.

{% iframe 'https://open.spotify.com/embed/album/73bEB7AIAGrbdodihI3wcY?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B00D3U24YE/?id=m61vzhF5f7&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/iceman/656333246' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/20543321' '100%' '96' %}
