---
title: Nirvana - Live At Reading
date: 2021-09-24 07:57:43
tags:
- nirvana
- kurt cobain
- krist novoselic
- dave grohl
- 90s rock
- alternative rock
- grunge
---

September 24, 1991. 30 years ago today, Nirvana released *Nevermind*. It wasn't predicted to be a huge success  (generous estimations were that it could sell 500,000 copies total). Of course, it ended up being one of the best-selling albums of all time. 30 million copies sold and at one point even eclipsing Michael Jackson at the top of the charts and selling over 300,000 copies a week! It helped make the alternative rock explosion in the 90s what it was by no small measure.

Tons of people with more inside information than me have written a lot of words about *Nevermind* before today. One of the best of the bunch is this [amazing recent article by Michael Azerrad](https://www.newyorker.com/culture/personal-history/my-time-with-kurt-cobain) (who wrote the definitive Nirvana biography "Come As You Are: The Story Of Nirvana").

The closest I came to Nirvana was watching them perform at The Aragon in Chicago in 1993 (with Mudhoney opening!). Which is the inspiration for today's pick: Nirvana was also an exciting live band.

Michael Azerrad spends some time in his article recounting when Nirvana headlined The Reading Festival in 1992. 

A lot of the retrospective focus on that day was about how Kurt Cobain was rumored to be so strung out on heroin that he could not perform. The band made light of that rumor by having Kurt appear on stage in a wheelchair which he promptly discarded before launching into the set.

One thing that cannot be overstated was how huge Nirvana was at this point. They were so big that Kurt basically called the shots on who else would perform on the main stage of the festival that day. Looking at the lineup, [it's obvious that Kurt hand-selected the bands](https://en.wikipedia.org/wiki/Reading_and_Leeds_Festivals_line-ups#1992) (Melvins, L7, Mudhoney, Teenage Fanclub all appeared that day).

Thankfully, Nirvana's performance that day was recorded for prosperity. It really showcases how great a band they were at the height of their powers. 

The show was before they released their next album, *In Utero*. Nevertheless, they still peppered a few songs from that album throughout the set ("Tourettes", "All Apologies", "Dumb").

They even play some obscure covers by Fang and The Wipers.

Of course, the focus of the set is on the songs from *Nevermind* which are still completely awesome even when listening to it in 2021.

<iframe src="https://open.spotify.com/embed/album/01Z1nufhjxJVXVDuMRmNEM" width="100%" height="380" frameBorder="0" allowtransparency="true" allow="encrypted-media"></iframe>


