---
title: Charlie Watts Jim Keltner Project
date: 2021-08-26 21:54:16
tags:
- charlie watts
- jim keltner
- rolling stones
- classic rock
- 60s rock
- 70s rock
- blues rock
---

Obviously the biggest news in music this week was the sad passing of Charlie Watts, the drummer for The Rolling Stones.

He was with the band from the very beginning and performed on every album. Obviously, he was one of the most famous drummers in the world...and for good reason.

I could wax poetic about my favorite Stones albums or songs, but that has already been done ad nauseam. 

I could also brag about the one and only time I saw The Rolling Stones perform live. It was at the Aragon Ballroom in Chicago in 2002 (capacity: 5000...a tiny crowd by their standards). It was a "secret" rehearsal show before their actual tour. Dr. John and U2's Bono performed with the band. It was awesome, but I said I wasn't going to talk about it.

Instead, I'm going to pick an obscurity: *The Charlie Watts Jim Kelter Project*!

Jim Keltner is a famous drummer in his own right. He is a session musician who has recorded on literally hundreds of albums in his career. But he's most renown for his work with Bob Dylan, Ry Cooder, George Harrison, and The Traveling Wilburys all of whom he has longstanding professional relationships with.

In 1996 / 1997 The Rolling Stones were working on *Bridges To Babylon*. There were a ton of outside collaborators who were part of the sessions and one of them was Jim Kelter, who played percussion on many of the songs. Charlie Watts and Jim hit it off and decided to work together on a separate album.

It took several years, but was finally completed and released in 2000 on the very small CyberOctave label. In fact, probably the biggest artist they had on their roster before this was Buckethead!

It turned up on my radar because a promo CD was sent to the office I worked at, and I was naturally very intrigued. I became pretty obsessed with it at the time. I still have the promo and dug it out this week after hearing the news of Watts' passing. I'm starting to get obsessed with it again.

*The Charlie Watts Jim Keltner Project* is a collection of percussion-heavy songs that are each named after a different famous drummer. The songs don't try to mimic the playing of their namesakes, but instead are more like musical portraits of how they inspired Charlie and Jim.

Most of the actual drumming is performed by Charlie, and it's pretty great, but not uncharacteristic of his playing with the Stones. Solid, dependable, no frills, yet still exciting. Jim Keltner added a bunch of digital samples that gave it a modern, melodic sheen. He also dabbled in various percussion instruments as well.

Other musicians also lent a hand, including Mick Jagger who played piano on "Tony Williams" and Keith Richards adding some guitar to "Elvin Jones Suite".

In fact those are probably two of my favorite tracks. The ominous, dirge-like "Tony Williams" was likely inspired by the fact that Tony Williams passed away while they were recording the album. The "Elvin Suite" is a multi-part long ensemble piece (including a choir!) that certainly feels like the centerpiece of the album.

At the time of release, it got some interesting write-ups in musician-centric publications, but I don't think mainstream media really pushed it too hard, which was a shame. I did dig up this [interesting press kit](http://www.rosebudus.com/watts/CWJKProject.html) that was being distributed at the time that adds more detail and context to it than I ever could. Its worth reading.

Unfortunately this musical gem is not on Spotify! YouTube will save the day again this week, as someone uploaded the entire album as a playlist so that will be where you can go to hear this week's Pick.

<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLtGDN7i8dXA6TX81pawxUUdHieNbmpKFv" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>




