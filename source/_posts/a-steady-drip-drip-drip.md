---
title: Sparks - A Steady Drip Drip Drip
author: Phil Plencner
date: 2020-05-20 14:47:49
tags:
- sparks
- new wave
- glam rock
---

Glam-rock / New Wave pioneers Sparks have a new album out, and it lives up as another excellent missive in their overall high-quality body of work. 

For those not familiar with Sparks, I'm also going to include an old playlist I made (newly updated with songs from this album!) to today's posting as well. 

Sparks allegedly was an early inspiration for Queen (!!), they had a minor hit early on with "This Town Ain't Big Enough For The Both of Us". 

Their biggest brush with fame was 1983's collaboration with The Go-Go's Jane Wiedlin "Cool Places" which was in heavy rotation on MTV. 

As I mentioned their whole back catalog is awesome, and hopefully the playlist is a nice overview for the uninitiated.

<iframe src="https://open.spotify.com/embed/album/43DZQacT84CN8EYfKcmeSL" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

<iframe src="https://open.spotify.com/embed/playlist/3EZiUZD5wzqBwSHM15603u" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>