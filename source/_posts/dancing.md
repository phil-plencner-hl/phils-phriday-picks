---
title: Mike Keneally - Dancing
author: Phil Plencner
date: 2020-10-30 10:13:15
tags:
- mike keneally
- beer for dolphins
- frank zappa
- dancing
- jazz fusion
- progressive rock
---

Today's pick is the epic progressive rock release *Dancing* by Mike Keneally and the band Beer For Dolphins.

Mike Keneally first became renown for playing guitar in Frank Zappa's final touring band in 1988. He also participated in previous PPP Zappa's Universe show. Nowadays besides the MKB, he also plays with Devin Townsend, Joe Satriani, Dethklok and Andy Patridge. 

Afterwards he started his own band called Beer For Dolphins (now just called The Mike Keneally Band) that played Zappa-inspired progressive rock. Beer for Dolphins was originally a trio but by the time of 2000's *Dancing* it had expanded to an 8 piece band with horns and a marimba player! 

Nowadays besides the MKB, he also plays with Devin Townsend, Joe Satriani, Dethklok and XTC's Andy Patridge. 

Back to *Dancing*...Clocking in at 80 minutes, this album has it all. Goofy pop songs, instrumental epics, short bursts of sound and plenty of humor and social commentary. 

The song "We'll Be Right Back" is probably the centerpiece of the album for me and is now more relevant than ever with the United States Presidential Election fast approaching.

I was lucky enough to see this band when they toured behind the album at [Martyr's in Chicago](https://www.setlist.fm/setlist/mike-keneally-and-beer-for-dolphins/2001/martyrs-chicago-il-bdc4d2a.html)...it was an excellent *Dancing*-heavy set with lots of other fan favorites thrown in.

<iframe src="https://open.spotify.com/embed/album/5i7ilgY35eznrXUyikKAEV" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>