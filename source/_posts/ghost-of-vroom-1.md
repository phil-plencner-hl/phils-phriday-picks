---
title: Ghost of Vroom - Ghost Of Vroom 1
date: 2021-03-26 07:55:03
tags:
- ghost of vroom
- mike doughty
- andrew scrap livingston
- mario caldato jr
- soul coughing
- 90s rock
- alternative rock
---

More bacon than the pan can handle.

Mike Doughty was originally pushed into the spotlight fronting the band Soul Coughing in the 90s. It was a unique blend of rock and rap music. The lyrics Doughty penned were very witty and clever and brought him quite a bit of mainstream fame.

Eventually the fame became too much for him and he dissolved the band. He's put out a huge amount of solo records since then, most of them self released and in limited quantities / distribution....but he has kept up a large cult following. The solo albums drifted away from the jazz / hip-hop / rock sound of Soul Coughing.

I mostly fell out of touch with his solo work, even though I remain a huge fan of his work with Soul Coughing.

So I was excited that I recently came across a video for his new project called Ghost of Vroom. Here Mike Doughty embraces his old sound with a new set of musicians.

<iframe width="560" height="315" src="https://www.youtube.com/embed/apFIAf2CyFY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Ghost of Vroom is a collaboration between Doughty and bassist / cello player Andrew “Scrap” Livingston (The Panderers) along with drummer Gene Coye. This sounds great so far...but even more enticing is their new album was produced by Mario Caldato Jr (Beastie Boys). 

I was surprised to find they put out an EP last year that was completely missed by my radar.

Their new album just came out last week, and it's been in heavy rotation for me. 

[Buy *Ghost Of Vroom* at Amazon](https://amzn.to/39qiBjs)

<iframe src="https://open.spotify.com/embed/album/2ZWScOee1RMrgevPsNKn4A" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>


