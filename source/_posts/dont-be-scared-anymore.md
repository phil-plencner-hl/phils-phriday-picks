---
title: Paul Wertico Trio - Don't Be Scared Anymore
date: 2022-09-23 15:31:00
tags:
- paul wertico
- paul wertico trio
- john moulder
- eric hochberg
- pat metheny
- jazz fusion
- jazz rock
---

![](DontBeScaredAnymore.jpg)

Paul Wertico is an underrated drummer. He first came into prominence as part of the Pat Metheny Group. He joined that band in 1984, replacing original drummer Danny Gottlieb, for the album *First Circle*. He ended up staying with Pat Metheny for almost 20 years...touring around the world, selling boatloads of albums and winning Grammys along the way.

Here they are performing "Have You Heard" on the *Night Music* television show in 1989:

<iframe width="560" height="315" src="https://www.youtube.com/embed/tUfi1zSUkKY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Paul Wertico also played with other Pat Metheny related projects, such as this amazing ensemble with saxophonist Ernie Watts and bassist Charlie Haden:

<iframe width="560" height="315" src="https://www.youtube.com/embed/GKZJypgGygQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

In 2001, Paul Wertico grew tired of always being on the road and wanted to spend more time at home with his family. He ended up focusing his time in his hometown of Chicago.

He ended up becoming a [professor and faculty member at both Northwestern University and Roosevelt University](https://www.linkedin.com/in/paul-wertico-57704b93/). 

He also performed solo in drum clinics and released instructional videos. Here is a clip from *Paul Wertico's Drum Philosophy* (which I still have on VHS!):

<iframe width="560" height="315" src="https://www.youtube.com/embed/pjzZWpp4RCc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

He also formed the Paul Wertico Trio around this time. The song in the previous video, “Cowboys & Africans", is a song the trio would perform.

The Paul Wertico Trio consisted of guitarist John Moulder (who is also a [Catholic priest](https://news.wttw.com/2016/03/04/local-priest-music-festival-use-jazz-tackle-hunger)!) and bassist Eric Hochberg.

They played an intense, loud and aggressive, yet funky, version of jazz fusion. They played extensively throughout Chicago from 2002-2006 or so. I was lucky enough to be living in Chicago at that time and saw them every chance I got (probably dozens of times over the years). I saw them in The Green Mill, The Hot House, The Jazz Showcase and many other smaller clubs and coffee shops. They were a tremendous band and their sets varied from night to night.

Their debut solo album was *Don't Be Scared Anymore* and is today's pick. It is probably still my favorite recording of the group as it accurately reflects what they sounded like during that period. The album features all original tunes written by each of the members and has some great soloing throughout. For those who like their jazz to *rock* (like me), this is a highly recommended record!

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/album/1OnID6K2E8rG1yHT4WTtQ3?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

<iframe id='AmazonMusicEmbedB00H8TFCRM' src='https://music.amazon.com/embed/B00H8TFCRM/?id=tEAJPs8uEs&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *; clipboard-write" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/dont-be-scared-anymore/780198838"></iframe>

<iframe src="https://embed.tidal.com/albums/38847669" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>


