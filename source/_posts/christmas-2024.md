---
title: Christmas 2024
date: 2024-12-06 09:19:17
tags:
- christmas music
---

Another year has flown by (by my count there were 38 albums picked in 2024!) and that means the holiday season is upon us. That also means it's time for my annual Christmas playlist (now in its 16th year counting the time I used to make mix CDs back in ancient times).

Every year there is a huge pile of new Christmas records that come out.  I listen to a bunch of them (so you don't have to) and select some gems from the overall batch of coal to include in the playlist. I also pepper it with classics from Christmases past...some well-known and some more obscure. 

This year's playlist collects 32 songs and will entertain listeners for a bit over an hour and a half. I hope it brings some cheer and optimism to you over the holiday season.

One thing is for sure: Every year for the past 16 years of playlists there have been *no repeats*! The exact same performance of the same song by the same artist has never reappeared on one of my playlists. However, every song is collected in a giant "Megamix" that has now swelled to an overwhelming 367 songs filling 20.5 hours. That's a lot of Christmas cheer!! Put that monstrosity on shuffle and you'll feel like you're living in the North Pole!

Merry Christmas and Happy New Year!

{% iframe 'https://open.spotify.com/embed/playlist/057k5F86TOIVKvitP3Cs4K?utm_source=generator' '100%' '352' %}

- [20.5 HOUR MEGA MIX!](https://open.spotify.com/playlist/5NqMi2wSRDRXP5L4G5McRW?si=ZwmbuH3iTkyzRb3IihO9UA)
- [Christmas 2023](https://open.spotify.com/playlist/5bkhmh2jk3g5ian28iqm2K?si=2c096c478c864485)
- [Christmas 2022](https://open.spotify.com/playlist/0xcrzSFIKysMMFi6ogGxTG?si=87f9ac0ccf42403a)
- [Christmas 2021](https://open.spotify.com/playlist/6ZRGinlvxFJYSqqLR60mxj?si=d273d0565b274585)
- [COVID Christmas 2020](https://open.spotify.com/playlist/6ZEzVZLOLb7jEWAWwFMOeR?si=45ae978fa6f549c4)
- [Modern Christmas 2019](https://open.spotify.com/playlist/1NiCJLrp4eX6cGvSwYBd7I?si=EWPGwVPVR9qe4WWVUyUQaw)
- [It's Christmas Again? 2018](https://open.spotify.com/playlist/0KVsnggEkc7MftYJIRmmZl?si=cIhjjkEySeKKOC4g5AyCQA)
- [New Christmas Classics 2017](https://open.spotify.com/playlist/73rcF9IZMKENn9xoZ6aLE2?si=esHyVl4jTJOvE8950mxAcw)
- [Merry Christmas 2016](https://open.spotify.com/playlist/15Q28CMAlNDhnxvpdzPril?si=q7XgWr3hRRiyDtsDDxBD_Q)
- [Festive 2015](https://open.spotify.com/playlist/32yvyJJiGGcxAJVwf4VyOZ?si=HqEeppCZSvmFLVtK-fIB7w)
- [80s Christmas 2014](https://open.spotify.com/playlist/4YEulevL9dKg0uXmXgUfML?si=KojXWrunRk69fNoxHSNf_g)
- [Jazzy Christmas 2013](https://open.spotify.com/playlist/1WQE7qzJugPui8TsK4EngF?si=a_PCH1vJTBqLMSh9a6ik2A)
- [Sounds of Christmas 2012](https://open.spotify.com/playlist/13KoC1HiL5NOM153QjrJk8?si=2gKKSXoJQ0OrSx21LiIBLg)
- [cRaZy ChRiStMaS 2011](https://open.spotify.com/playlist/5t2qUwrQHDiMpakUa2j7KM?si=a_zUuWcOSAmUWcrZZKxeTQ)
- [Wacky Christmas 2010](https://open.spotify.com/playlist/3KPGMZ8X8J1MDZa4NM6XIE?si=pUxfVmnSQGKZuNlDsUSXjA)
- [Ultimate Christmas 2008](https://open.spotify.com/playlist/3AylbZDuVKYQH7g2ik5ciw?si=jtjSA8thTLyrbjf1AAkGYQ)
