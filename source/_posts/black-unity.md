---
title: Pharoah Sanders - Black Unity
author: Phil Plencner
date: 2020-06-02 22:40:28
tags:
- pharoah sanders
- stanley clarke
- normon connors
- billy hart
- free jazz
- jazz
---

If I were to pinpoint my favorite saxophone player, Pharoah Sanders would probably be near the top of the list. 

He reached prominence as a member of John Coltrane's gnarly final quartet. His playing was very dissonant and free until around 1970-1971 when he started incorporating more groove based music / African rhythms. 

This leads us to *Black Unity* which I think was the first full realization of the sound he would continue to refine for the next decade. 

It features a very young Stanley Clarke on bass and a dynamic drum duo of Norman Connors and Billy Hart. The album is also a very apt message for the past week.

<iframe src="https://open.spotify.com/embed/album/3luFuzmpqNIeQQM485wkI8" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>