---
title: Allan Holdsworth - Atavachron
date: 2024-06-28 20:28:53
tags:
- allan holdsworth
- tony williams
- chad wackerman
- gary husband
- jimmy johnson
- alan pasqua
- jazz fusion
---

![](Atavachron.jpg)

I'm a huge fan of guitarist Allan Holdsworth. He's been involved in tons of great bands and albums in his long 40+ year career. In fact, in the past I've picked at least two albums he's performed on: Gong's *Gazeuse / Expresso* and Return to Forever's *Romantic Warrior*. However, I have not picked one of his solo records before. 

One of the many bands Allan Holdsworth was in was The New Tony Williams Lifetime in the mid-70s (appearing on *Believe It* and *Million Dollar Legs*). 

Live recordings of the band are rare, but here's a quality recording from 1975:

<iframe width="560" height="315" src="https://www.youtube.com/embed/RJLqy3cMENo?si=WT3fOQWwYwd4pIAq" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Also part of this band was keyboard player Alan Pasqua. After The New Tony Williams Lifetime disbanded, Alan Pasqua stuck with Allan Holdsworth and continued to be a part of his bands (even while playing with more famous musicians like Eddie Money and Santana). His playing with Holdsworth ended up being a very fruitful collaboration.

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

Allan Holdsworth also liked keeping up with technology. In the mid-80s he started playing a weird instrument called the SynthAxe. The SynthAxe was a MIDI controller that was shaped like a guitar with two separate sets of strings and a bunch of buttons. It connects to synthesizers to make noise. Here is Allan Holdsworth discussing the SynthAxe and performing a little demonstration of the instrument:

<iframe width="560" height="315" src="https://www.youtube.com/embed/8aAYfZdkCA8?si=PB4DdIhUbVTPea93" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

In 1986, Allan Holdsworth teamed up with Alan Pasqua, along with drummer Gary Husband and bassist Jimmy Johnson to record his first of many records featuring the SynthAxe. The album was called *Atavachron* (named after a Star Trek time travel device) and is today's pick!

*Atavachron* is a wild record that manages to mix jazz fusion with progressive and symphonic rock. It also features some of Allan Holdsworth best playing, especially since he can expand the voice of the guitar into previously unheard realms with The SynthAxe.

Supplementing the core band is drummer Chad Wackerman on two songs "The Dominant Plague" and "All Our Yesterdays". Plus there is a mini New Tony Williams Lifetime reunion on "Looking Glass". Also appearing on "All Our Yesterdays" is the beautiful singing of Rowanne Mark.

The songs from *Atavachron* were not just a studio creation. Soon after the album's release Allan Holdsworth played many of the songs in concert (with Kei Akagi replacing Alan Pasqua since he ended up playing in John Fogerty's band for a little while). There's some footage of live *Atavachron* performances on the internet. Even though the quality is a little questionable (seemingly coming from a very old VHS tape), it's worth sharing because the playing is incredible and the songs are absolutely bonkers.

For starters, here is the album's opening track "Non Brewed Condiment":

<iframe width="560" height="315" src="https://www.youtube.com/embed/0s7i578n3Tk?si=RzkAphFHiACcyini" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Next is the the title song:

<iframe width="560" height="315" src="https://www.youtube.com/embed/-c0K8i-P5Vg?si=Gv1vAmSW-h_iqdV7" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Finally, here is "Looking Glass":

<iframe width="560" height="315" src="https://www.youtube.com/embed/Qdzqkk7o00c?si=0qBo5bV7_iYbaSfx" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

All of these examples showcase what an awesome record *Atavachron* is. It was way ahead of it's time technology-wise and was performed with a staggering amount of virtuosity and precision. Even today, it still sounds like it came from outer space.

You can [listen to Allan Holdsworth - *Atavachron* on your streaming platform of choice](https://songwhip.com/allan-holdsworth/atavachron-remastered) (including Spotify, Amazon Music, Apple Music, Tidal, YouTube Music and more!).
