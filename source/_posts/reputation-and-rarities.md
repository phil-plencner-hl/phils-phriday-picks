---
title: Dusty Springfield - Reputation and Rarities
date: 2024-05-03 22:57:55
tags:
- dusty springfield
- pet shop boys
- rupert hine
- andy richards
- frankie goes to hollywood
- 60s pop
- 80s music
- 80s pop
---

![](ReputationAndRarities.jpg)

Dusty Springfield was a huge sensation in the late 60s. Her blue-eyed-soul styled pop singing brought her many hits both in her native U.K. and in the United States including "You Don't Have To Say You Love Me" and "Son of a Preacher Man". This culminated in perhaps her most famous record: *Dusty In Memphis* which is still critically acclaimed today. 

Here she is performing "Son of a Preacher Man" on *The Ed Sullivan Show* in 1968:

<iframe width="560" height="315" src="https://www.youtube.com/embed/b4pYANUAJAI?si=8XQN6TEuY6apJwDE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

She continued to put out records in the 1970s and 1980s but with diminishing returns. She started to fade into obscurity. Then she was saved by...The Pet Shop Boys.

The Pet Shop Boys just released a new record called *Nonetheless* which has been getting [a ton](https://www.washingtonpost.com/entertainment/2024/04/25/pet-shop-boys-interview-new-album/3cad7a40-0321-11ef-8eac-39c6dcb59eb5_story.html) of [great press](https://www.washingtonpost.com/entertainment/music/2024/05/01/pet-shop-boys-nonetheless-review/) lately (well deserved, it is [another fantastic record](https://www.theguardian.com/music/2024/apr/26/pet-shop-boys-nonetheless-review-a-great-fan-pleasing-album) in their basically unblemished discography.) 

This led me to revisit their older albums and I am especially obsessed with their second record called *Actually* from 1987. The very same album that brought Dusty Springfield back into the spotlight with the huge hit single "What Have I Done To Deserve This?". People sometimes mistakenly think that the Pet Shop Boys sampled Dusty Springfield in the song, but she actually sang it with them. Here is the original music video for the tune:

<iframe width="560" height="315" src="https://www.youtube.com/embed/Wn9E5i7l-Eg?si=VE96uXrx_9NhzWMC" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

She even performed the song in concert with the Pet Shop Boys. A famous example was during the BRIT Awards in 1988:

<iframe width="560" height="315" src="https://www.youtube.com/embed/WRPhsLO1nl4?si=NzBQlkQ-7t-KQsDc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

The song was an enormous success. It rose to number two on the charts, only kept out of the top spot by George Michael's "Father Figure". 

To capitalize on the success, Dusty Springfield put out her first solo album in eight years. It was mostly produced by the Pet Shop Boys and half of the songs were written by them as well! It originally came out only in the UK (eventually reaching the United States 7 years later!) so it was unfortunately ignored here. The album was called *Reputation* and is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

The title track on the record was produced by Andy Richards who also performed with the Pet Shop Boys...you can hear his synthesizer work on "Its a Sin" and their Willie Nelson cover "Always on My Mind". Andy is also famous for playing on Frankie Goes To Hollywood's "Relax". I think the song "Reputation" ranks up there with all of these.

One of the songs on *Reputation* that was written by the Pet Shop Boys is "Nothing Has Been Proved". The lyrics are based on a British political scandal called the "Profumo Affair". A movie was made about it called *Scandal* and it was also part of the film's soundtrack. Additionally the orchestrations in the song were arranged by Angelo Badalamenti! They made a music video for it that includes clips from *Scandal*:

<iframe width="560" height="315" src="https://www.youtube.com/embed/5HOfaSSIVcE?si=Sm8QryUlZLJxB-ah" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Another highlight from *Reputation* was a cover of the Carole King song "I Want To Stay Here". They made an animated music video for it featuring a cartoon version of Dusty Springfield. Way cool!

<iframe width="560" height="315" src="https://www.youtube.com/embed/mPsedErYJAc?si=2MAbCdskFjeru-ku" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Dusty also covered the Rupert Hine song "Arrested By You" that was originally part of the soundtrack for the film *Better Off Dead*. An interesting choice that actually turned out better than the original.

It's unfortunate that *Reputation* didn't get much traction in the United States. The quality of the songs and performances rival anything else in Dusty Springfield's and the Pet Shop Boys catalogs. A forgotten classic! With the recent attention given to The Pet Shop Boys it is a fine time to also check out *Reputation* to hear what you have been missing.

You can [listen to Dusty Springfield - *Reputation* on your streaming platform of choice](https://songwhip.com/dusty-springfield/reputationandrarities) (including Spotify, Amazon Music, Apple Music, Tidal, YouTube Music and more!).
