---
title: Gergo Borlai - The Missing Song
author: Phil Plencner
date: 2020-06-01 22:42:47
tags:
- gergo borlai
- jazz
- jazz fusion
---
Today's pick is the new album by monster jazz fusion drummer Gergo Borlai. 

Each song is named after a famous drummer that inspired him (Billy Cobham, Gary Husband, Dennis Chambers, Keith Karlock, Steve Gadd, Kirk Covington, Peter Erskine, Terry Bozzio, Vinnie Colaiuta) and he plays the song in the style of that drummer pretending to "be" that drummer on the song. 

It's pretty amazing how he channels their styles and without knowning the concept and just playing the songs, you might believe the original drummers were actually the ones playing! 

Cool stuff...amazing drumming. Guest musicians are no slouches either.

<iframe src="https://open.spotify.com/embed/album/5jJpKVkozjaKZ2kAtCHPbY" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>