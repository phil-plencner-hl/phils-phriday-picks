---
title: Cheer-Accident - Introducing Lemon
author: Phil Plencner
date: 2019-07-12 15:44:16
tags:
- cheer-accident
- progressive rock
- noise rock
- alternative rock
---
Cheer-Accident is one of my favorite bands. 

Hailing from Chicago, they are part of an underground Avant/Progressive rock scene that's been having relative levels of success since the 90s. 

They are putting out a new album and going on tour next month...Boston date! August 12th at some place called ['Once' in Somerville](https://www.oncesomerville.com)

Introducing Lemon is my favorite album from the band. Released in 2003, back when I was part of the aforementioned avant/prog scene in Chicago. I probably saw them in concert dozens of times in that era. 

I also remember right before this album was released, drummer Thymme Jones played the thing for me on his car stereo after a show at maximum volume. After sitting silently for an hour plus being blown away he asked "What'd you think?" I was speechless. 

Enough babbling! Cheer-Accident's masterpiece: Introducing Lemon!!

<iframe src="https://open.spotify.com/embed/album/52AEvf6gVDGnOg6uU70AMH" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>