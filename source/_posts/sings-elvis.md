---
title: Danzig - Sings Elvis and Lullaby Wu Tang
author: Phil Plencner
date: 2020-04-24 15:43:55
tags:
- danzig
- elvis presley
- wu-tang clan
- rockabye baby
---
NEW MUSIC FRIDAY! Today is a  2-fer....I think the titles are pretty self explanatory.

<iframe src="https://open.spotify.com/embed/album/2lfdbxZXN3OU4Hz2ffJ36x" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

<iframe src="https://open.spotify.com/embed/album/7daFeFeJyRZP6NiEXWMsI8" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>