---
title: Chicago Underground Quartet - Good Days
author: Phil Plencner
date: 2020-03-31 15:55:31
tags:
- chicago underground quartet
- rob mazurek
- chad taylor
- tortoise
- isotope 217
- post rock
- chicago
- free jazz
- jazz
---
At the risk of tumbling down a Chicago fringe-jazz / post-rock rabbit hole today, I'm picking the new album from The Chicago Underground Quartet...perhaps aptly / optimistically called "Good Days". 

The Chicago Underground moniker is utilized by Rob Mazurek and Chad Taylor. They are both veterans of the aforementioned local Chicago music scene (Tortoise, Isotope 217, Exploding Star Orchestra etc). 

This new album maintains their high level of quality and has my seal of approval.

<iframe src="https://open.spotify.com/embed/album/6nD6cyeIZH3cZfScKl9qwj" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>