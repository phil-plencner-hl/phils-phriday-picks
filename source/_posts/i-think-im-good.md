---
title: Kassa Overall - I Think I'm Good
author: Phil Plencner
date: 2020-03-13 16:22:39
tags:
- kassa overall
- jazz
- jazz fusion
---
The new album from jazz drummer / DJ / MC Kassa Overall is excellent. You should listen to it.

<iframe src="https://open.spotify.com/embed/album/2FbHNeml2vjg4ksPIcsKMi" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>