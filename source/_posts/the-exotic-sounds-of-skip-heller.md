---
title: Skip Heller's Voodoo 5 - The Exotic Sounds of Skip Heller
date: 2024-05-31 22:12:48
tags:
- skip heller
- jazz
- frank zappa
- john hartford
- organ trio
- jazz guitar
- exotica
---

![](TheExoticSoundsOfSkipHeller.jpg)

Musician and composer Skip Heller is extremely prolific and immerses himself in a wide variety of genres. For approximately 30 years he has released tons of records running the gamut from organ jazz to country / blues to exotica. It's hard to condense the gigantic scope of his output into a tiny blog post, but I'm going to try.

Perhaps some of his historical biography can be best summed up by the man himself. So, here is a [somewhat recent interview (from 2019) with him](https://voyagela.com/interview/meet-skip-heller-musician-hollywood/) where he describes his modus operandi and some of the musical projects he was working on at the time.

I first encountered Skip when I was in college in the mid-90s. We were both members of an [email discussion group about John Zorn](https://browbeat.com/zornlist.html). Along with chatting about John Zorn in this virtual forum, we also discovered our mutual fanaticism for Frank Zappa, The Minutemen, John Hartford and tons more.

We've kept in touch ever since. Exchanging emails, following each other on every imaginable social media site (pretty sure we were connected on sites like Friendster and Myspace before modern-day applications like Facebook or Instagram were even a dream). We also exchanged mix CDs in the mail, hyping our recent discoveries and obsessions.

All along the way, whenever Skip released new music, I was listening. Some of my favorites are [*Couch, Los Angeles*](https://www.discogs.com/release/7539830-Skip-Heller-His-Orchestra-and-Chorus-Couch-Los-Angeles), [*Homegoing*](https://www.discogs.com/release/11312411-Skip-Heller-Quartet-Homegoing), and [*Mean Things Happening In This Land*](https://www.discogs.com/release/14875441-The-Skip-Heller-Trio-Mean-Things-Happening-In-This-Land).

I'll list a highlight from each record to give you an idea the albums are like....

On *Couch, Los Angeles* the original "Spy Perfume" shows his interest in noir jazz:

<iframe width="560" height="315" src="https://www.youtube.com/embed/eDsyQawR9tU?si=-5YOLnKxPDBnNoDi" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

*Homegoing* was a live organ trio record that was mostly instrumental but also included some vocal numbers. "Time After Time" is one example featuring Dave Alvin of The Blasters fame:

<iframe width="560" height="315" src="https://www.youtube.com/embed/ZjiVA9pEsMg?si=TQ7UA08JRCk6sqbH" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

The entire *Liberal Dose* album is excellent. Not a single bad track from beginning to end so it's hard to only select one example. However his arrangement of "Funeral March from The Mahler #5" is a great choice:

<iframe width="560" height="315" src="https://www.youtube.com/embed/iOptF3tZZyo?si=ngXNkniguCyWRyN0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

Some of his more recent ensembles have great footage available online. Carnival of Soul is one such case. They are a horn backed blues band with incredible vocals by Birdie Jones. Here they are playing "Busted":

<iframe width="560" height="315" src="https://www.youtube.com/embed/eYwdXDnv014?si=Vn4-2Q3xQ4GaBMZz" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

The Hollywood Blues Destroyers is another fantastic group. Here's the Blues Destroyers performing "I Hate You":

<iframe width="560" height="315" src="https://www.youtube.com/embed/B7xwG0iBQYw?si=3lgzajtTnBwcLL2O" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

One of his most recent bands is the Hollywood Film Noirchestra. Here they are performing earlier this year:

<iframe width="560" height="315" src="https://www.youtube.com/embed/Cck7nsBqoKc?si=ekgdp0NeosR-N4G2" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

For further context, here is Skip discussing the project on a recent Podcast called *Starlite Pulpcast*:

<iframe width="560" height="315" src="https://www.youtube.com/embed/9pQIuiRQFi4?si=-w80Ti4_mntRQIKH" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Exotica music has been a frequent passion for Skip Heller. Back in 2009 he released a full album of music in that style called [*Lua-O-Milo*](https://www.discogs.com/release/9193903-Skip-Heller-Lua-O-Milo) which turned out to be a precursor for his latest (and perhaps greatest) group: Skip Heller's Voodoo 5. Here they are playing "Hello Stranger" from a few months ago:

<iframe width="560" height="315" src="https://www.youtube.com/embed/fKyBMA5vnzU?si=dHK45q2xvlKif80Q" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Skip Heller's Voodoo 5 just released a new record called *The Exotic Sounds of Skip Heller* which is today's pick!!

The record is a great mix of originals along with covers. Some covers, such as "Miserlou" (popularized by Dick Dale) are well known. Others are more obscure like the Gerhard Narholz tune "Bossa Cubana". 

The originals rank right up there with covers, especially "The Collector" (also on *Lua-O-Milo*) and "Mirageband". They stay true to the classic exotica format and sound like modern day classics.

Stunning vocalist Lena Marie Cardinale sings throughout the whole record and she frequently steals the show. 

If you like what you hear, I recommend you [head over to Bandcamp to purchase your own copy of *The Exotic Sounds of Skip Heller*](https://skiphellersvoodoo5.bandcamp.com/album/the-exotic-sounds-of-skip-heller).

As a fun aside, even though I've known Skip Heller virtually for almost 30 years we never met in person....until last year! Skip came to town to visit family (he has relatives in Philadelphia as well as D.C. suburbs) and we were able to spend some time together (along with Lena Marie) talking music, movies and history. We traveled out to see the [Iwo Jima Memorial](https://en.wikipedia.org/wiki/Marine_Corps_War_Memorial) and had an incredible day. It was worth the wait!

![](SkipAndPhil.jpg)

You can [listen to Skip Heller's Voodoo 5 - *The Exotic Sounds of Skip Heller* on your streaming platform of choice](https://songwhip.com/skiphellersvoodoo5/the-exotic-sounds-of-skip-heller) (including Spotify, Amazon Music, Apple Music, Tidal, YouTube Music and more!).