---
title: Sex Mob - Solid Sender
date: 2021-09-09 20:21:31
tags:
- sex mob
- steve bernstein
- tony sherr
- briggan krauss
- kenny wollensen
- dj logic
- jazz
- avant-garde jazz
- free jazz
---

September 11, 2001. We all remember where we were that day, and what we were doing.

I was living in on the north side of Chicago, and working downtown. Let's just say it was a very stressful morning to be in a tall office building. I eventually walked home (to Lakeview! Not a short walk by any measure!), instead of cramming myself into the El with a bunch of other distressed commuters. 

Once there, I was glued to the footage on TV like everyone else. Obsessively watching to the point that by the next day I was completely at my wits-end and needed some fresh air.

Before the tragic events, I was looking forward to going to [Schuba's Tavern](https://www.instagram.com/lh_schubas/) on Wednesday to see a rock band called The Butcher Shop Quartet engaging in a complete performance of Stravinsky's "The Rite Of Spring".  Steve Bernstein's wacky jazz rock ensemble Sex Mob was the opening act. Here is the [write-up of the event in the Chicago Reader from that week](https://chicagoreader.com/arts-culture/in-performance-stravinsky-meets-fender-and-marshall/).

I wasn't sure if the show was still on. Nevertheless, I needed to get out of my apartment. Instead of calling to confirm, I figured I would walk the 20 minutes or so to Schuba's as showtime neared. If there was no show, at least I got out for a bit to clear my head.

Thankfully, there was a show that night. 

It was a very sparse crowd, maybe 10-15 people in attendance. The Butcher Shop Quartet's rendition of "The Rite of Spring" was spectacular. However, I want to focus on Sex Mob.

They are based in New York City. All the members are fixtures in the NYC jazz scene. Obviously, being on the road at that moment (away from friends and family) must have been incredibly difficult for them. They still managed to blow the roof off the joint that night with their joyous music. Steve Bernstein started out by dedicating the night to everyone in New York and then proceeded to play with an unmistakable fire. I don't specifically remember what songs they played, but the energy that night of their playing is what sticks in my mind 20 years later. 

So, who are Sex Mob? They are a jazz quartet who plays like a rock band. In fact, a hallmark of their sets are unusual covers of popular rock music. They consist of Steve Bernstein playing a unique slide-trumpet along with Tony Sherr on guitar and bass, Briggan Krauss on saxophone and Kenny Wollensen on drums. 

At the time of this performance, they were touring behind their then new album *Solid Sender*, which is this week's pick. Some popular tunes they tackle on the album (along with cool originals mostly written by Bernstein) are Nirvana's "About A Girl", Elvis Presley's "Don't Be Cruel", The Grateful Dead's "Ripple", The Rolling Stones' "Ruby Tuesday" and James Brown's "Please, Please, Please". They cover a broad spectrum.

DJ Logic also appears on the title track.

As an epilogue to the way I spent my evening on September 12, 2001, I want to also write about Malachi Ritscher. 

Malachi was regularly seen around town at jazz and experimental rock shows during this time. He brought his own professional recording equipment and, with permission from the bands playing, would record them. As a thank you, he would provide them with a copy. He allegedly made thousands of live recordings of bands playing in Chicago over the years.

He was one of the few in attendance that night. We talked between sets about the terrorist attacks. He was very vocal about his fears that it would likely lead to a long, brutal war. His passion about the topic is still very clear in my memory. 

Of course, he was absolutely right. Soon afterwards, he became a very active anti-war protester. 

As a final act of protest, Malachi [lit himself on fire during rush hour on the Kennedy Expressway in 2006](https://web.archive.org/web/20061118180838/http://www.infoshop.org/inews/article.php?story=2006110800212324).

This week's pick of *Solid Sender* goes out to everyone whose lives were effected by the events of September 11, 2001...especially the members of The Butcher Shop Quartet, Sex Mob and Malachi Ritscher.

<iframe src="https://open.spotify.com/embed/album/2V7riiQEmpKTl3G52mOa1t" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>