---
title: John McLaughlin - Tokyo Live
date: 2022-08-26 10:32:23
tags:
- john mchlaughlin
- dennis chambers
- joey defrancesco
- miles davis
- john scofield
- jazz fusion
- jazz rock
- mahavishnu orchestra
- steely dan
---

![](TokyoLive.jpg)

In the early to mid 1990s I was immersing myself heavily in jazz fusion, especially stuff featuring guitarist John McLaughlin. 

Primarily I was focused on his early band Mahavishnu Orchestra, which was an extremely explosive early jazz rock band.

I was also enjoying his work with Miles Davis (specifically the albums *A Tribute To Jack Johnson* and *Get Up With It*).

Additionally, I was also starting my long love affair with Steely Dan around this time. Steely Dan had recently reformed, and I saw them perform in the summer of 1994 (at the long since closed Poplar Creek Music Theater in Hoffman Estates, Illinois). Their drummer on this tour was Dennis Chambers, so I started also diving into his work with jazz guitarist John Scofield (another Miles Davis alum) on the albums *Blue Matter* and *Loud Jazz*. 

So I was very excited to learn that the two had teamed up in a new group called The Free Spirits with keyboardist / trumpeter Joey Defrancesco and were releasing a live album aptly titled *Tokyo Live*. It was recorded live on December 16 and 18, 1993 at the Blue Note Tokyo jazz club. I'm pretty sure I bought it the week it came out in the winter of 1994. *Tokyo Live* is this week's pick!

Even with my previous familiarity with 2 of the players, I was not ready for the unbelievable and exciting music performed on this concert! All three musicians in top form playing a bonkers mix of organ trio jazz and fusion. Especially prominent was the organ playing of Joey DeFrancesco who is absolutely playing out of his mind throughout the whole set. McLaughlin and Chambers also kept up with him playing at an extremely high-level.

Here is an excellent video of a different show of the same tour, so you can see as well as hear first-hand how red-hot this band was:

<iframe width="560" height="315" src="https://www.youtube.com/embed/Ox8eYBFszuA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

As far as I can tell, they never released a proper studio album. There was a single song "Thelonious Melodius" on John McLaughlin's mixed-bag album *The Promise*. There was also *After The Rain* where Elvin Jones replaced Dennis Chambers. Both albums were from 1995. That was it. The players had moved on. I doubt the studio could have properly captured the fire of this trio, but it does seem like a missed opportunity.

I just read yesterday on [drummer Steve Gadd's Instagram that Joey DeFrancesco passed away earlier this week](https://www.instagram.com/p/ChtDDbBLDsk/?fbclid=IwAR3BEkllnVOM6rsAnzidE2MxzlnZ5ayQn2xDsk0_U1aV8Qp0qDfAGWrkejM). So, this week's pick is particularly poignant. Now is as fine a time as any to crank up *Tokyo Live*. I'm still as floored listening to it today as I was when I first heard it 28 years ago.

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/album/4h2HmKSG3PJ0w0wxUFYChQ?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

<iframe id='AmazonMusicEmbedB000V657QU' src='https://music.amazon.com/embed/B000V657QU/?id=GY9n6JMpVu&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *; clipboard-write" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/tokyo-live/1442904691"></iframe>

<iframe src="https://embed.tidal.com/albums/3687331" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>