---
title: Dun - Eros
author: Phil Plencner
date: 2020-03-20 16:11:52
tags:
- dun
- magma
- 70s rock
- zeuhl
- progressive rock
- jazz fusion
- jazz rock
---
Dun was an amazing French band who only put out one album in the early 80s. They played a style of music called Zeuhl which is a pretty obscure offshoot of jazz rock that incorporates trance-like driving drums, throbbing basslines and (usually) choral vocals. 

Magma was the originator of this style of music and is still the band from which all others are measured (don't get me started on a long Zeuhl tangent!!!). 

Anyways, the album Eros was recently re-released with bonus tracks of their rehearsal tapes and it's awesome. 

PM me for more Zeuhl recommendations.

<iframe src="https://open.spotify.com/embed/album/1hN5JK7ET6vw4gQR5fnmj7" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>