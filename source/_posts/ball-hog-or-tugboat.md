---
title: Mike Watt - Ball Hog Or Tugboat
author: Phil Plencner
date: 2020-04-08 15:00:30
tags:
- mike watt
- minutemen
- firehose
- 80s punk rock
- punk rock
- 90s rock
- alternative rock
---
Today's pick is the almighty 1995 release by Mike Watt called *"Ball-Hog Or Tugboat?"*. 

Mike Watt, of course, is the bass player from seminal punk rock band The Minutemen (and later of fIREHOSE). If you want to choose one album as an amazing mid-90s alternative rock time capsule, this is it! Czech out the [Wikipedia page](https://en.wikipedia.org/wiki/Ball-Hog_or_Tugboat%3F#Personnel) to see the looooooong list of collaborators on the album that basically scream "90s alt rock!". 

Stylistically it runs the gamut too...including a pretty rad cover to Funkadelic's "Maggot Brain".  I first saw Mike Watt's band perform these songs opening for Primus in the summer of 1995 and was completely blown away. Soon afterwards, I ended up buying the limited-edition, [long-box version of the album](https://www.discogs.com/Mike-Watt-Ball-Hog-Or-Tugboat/release/1328518), which is still a prized possession. Later that fall, I saw Watt perform again at the much smaller Metro club in Chicago, with an all-star band...which was recently released as a live album if you'd like to [dig into that for extra credit](https://en.wikipedia.org/wiki/%22Ring_Spiel%22_Tour_%E2%80%9995).

<iframe src="https://open.spotify.com/embed/album/2deUSnHlt1JegsHSPS1bPS" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>