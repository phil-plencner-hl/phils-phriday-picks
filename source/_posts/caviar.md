---
title: Caviar - Caviar
date: 2024-09-06 21:02:54
tags:
- caviar
- fig dish
- local h
- alternative rock
- 90s rock
---

![](Caviar.jpg)

I recently learned that [Chicago alternative rock legends Fig Dish have reformed](https://illinoisentertainer.com/2024/08/cover-story-fig-dish-renewal-and-rejuvenation/). This is pretty cool news! They never really got huge during the 90s alt-rock boom, which is a shame because they were a very fun and talented group.

Their new record, *Feels Like the Very First Two Times* was released today and they are [playing a couple club shows in Chicago over the weekend](https://chicagoreader.com/music/concert-preview/fig-dish-gman-tavern/). I haven't had a chance to fully absorb the album yet, but here is the video for the single "Science Goes Public":

<iframe width="560" height="315" src="https://www.youtube.com/embed/hzGTafOIIGM?si=h7hDFUxVNf36x8GS" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Even though Fig Dish weren't a massive success outside of Chicago in their heyday, they were still signed to major label Polydor Records. Their first album, *That's What Love Songs Often Do*, was released in 1995. It featured the single "Seeds" which showcases their indie rock / power pop sound pretty well:

<iframe width="560" height="315" src="https://www.youtube.com/embed/U34A0wzqOpw?si=mKwYIHuJPk-8BUrZ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Their second album arrived two years later and was called *When Shove Goes Back To Push*. They were annoyed that the lack of support they received from Polydor so they self-sabotaged their big opportunities. The most notorious was wasting $80,000 on a basically unplayable on MTV risqué video for "When Shirts Get Tight":

<iframe width="560" height="315" src="https://www.youtube.com/embed/FJ8cMVcuqgI?si=NuXIvOUnr37IKrFn" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Consequently, they were dropped from the label. Fig Dish basically broke up, but still played infrequent shows in Chicago, most famously for their Halloween shows with their buddies in Local H (dressing like famous bands and playing their songs). I was lucky enough to catch one such show in 2002 at The Double Door. It was, to say the least, a wild night.

Two of the members of Fig Dish, Blake Smith (guitar & vocals) and Mike Willison (bass), regrouped. They became inspired by the growing popularity of electronica and decided to blend that with the heavy rock sound of Fig Dish and a big dose of irony (similar to Urge Overkill). They formed a band called Caviar. Caviar released their self-titled debut album in 2000 and is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

The most famous song on the record is "Tangerine Speedo" which was a minor hit, especially since it was featured on the soundtrack to the movie *Charlie's Angels*. It was a bonkers fusion of bossa nova and alternative rock (almost like a drunken version of The Killers). Here is the goofy music video for "Tangerine Speedo":

<iframe width="560" height="315" src="https://www.youtube.com/embed/MW11k4vTUGk?si=wx-XDj5tuByCbwhW" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

The rest of the record is even better than the single. For example, the album starts with the very heavy "Ok Nightmare" which sounds like a modern, sleazy take on Cheap Trick: 

<iframe width="560" height="315" src="https://www.youtube.com/embed/8_yKe8ymeag?si=P01lqpEzNiKxDWQ5" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Another highlight is "Flawed Like A Diamond" which features a big beat / techno influence:

<iframe width="560" height="315" src="https://www.youtube.com/embed/ZhC6MJTc_ZI?si=We4C8aziWMmCIMNv" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

I also really like "Automatic Yawns" which is a fun mid-tempo rocker that could fit comfortably on a latter day Rolling Stones record:

<iframe width="560" height="315" src="https://www.youtube.com/embed/hJO3Rbt9g-Q?si=8amQCCIqre3Alopj" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Caviar only put out one more album in 2002 called *Thin Mercury Sound* before calling it a day. It is not quite as good as their previous effort but it still has its moments. In the early 2000s it was common for record labels to send out EPKs (Electronic Press Kits) as promotions containing live footage of groups and their back story. Here is the EPK for *Thin Mercury Sound*:

<iframe width="560" height="315" src="https://www.youtube.com/embed/UbTjBjoEytk?si=Up8kacvptEWhczNx" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

It’s too bad Caviar didn't get larger recognition because they were an incredible band. Maybe they will reform like Fig Dish? One can only hope!

{% iframe 'https://open.spotify.com/embed/album/5iDueX318fjAdY2V9QXUp2?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B000VRIT82/?id=C9wOljLOCq&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/caviar/1443234752' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/35981868' '100%' '96' %}
