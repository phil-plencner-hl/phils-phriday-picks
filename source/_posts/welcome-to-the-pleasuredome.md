---
title: Frankie Goes To Hollywood - Welcome to the Pleasuredome
date: 2022-09-16 14:04:49
tags:
- frankie goes to hollywood
- trevor horn
- yes
- abc
- the buggles
- art of noise
- 80s rock
- progressive rock
- new wave
---

![](WelcomeToThePleasuredome.jpg)

Music producer Trevor Horn is a fascinating person. It’s hard to fathom how ubiquitous his handiwork was in the early 80s. Especially since most of the stuff he produced was so utterly bizarre yet became huge hits.

He first came to prominence as part of the creative duo The Buggles (along with Geoff Downes). They, of course, had the monster hit "Video Killed the Radio Star". 

Capitalizing on that success, they both joined Yes for the previous Phil's Phriday Pick *Drama*. Geoff Downes left soon after to start the band Asia. Instead of joining or forming another band Trevor Horn decided to just produce a bunch of bonkers records.

Consider the calendar year 1982-1983 alone. He produced the following records:

- ABC's *The Lexicon of Love* (including "The Look of Love" and "Poison Arrow")
- Yes' *90125* (including "Owner of A Lonely Heart")
- The Art Of Noise's *Who's Afraid Of The Art Of Noise?* (including "Beat Box" and "Close (To The Edit)")
- Malcom McLaren’s *Duck Rock* (including "Buffalo Gals" and "Double Dutch")
- Frankie Goes To Hollywood's *Welcome to the Pleasuredome* (including "Relax", "Two Tribes" and "Welcome to the Pleasuredome")

That is an incredible run! Pretty much every song and album are widely considered classics and are basically standards of 80s new wave.

Much of it at the time was very cutting edge. Trevor invested heavily in new technologies and was an early adopter of electronic instrumentation. For example, he was one of the earliest owners of a Fairlight CMI synthesizer and a LinnDrum drum machine. Instruments like these (along with lots of sampling and cut n' paste techniques) were cornerstones of his sound and aesthetic.

In my opinion the wildest of all of these records was and remains *Welcome to the Pleasuredome* which is today's pick!

"Relax" is well renown and comparatively tame pop music in 2022, but in 1983 it was very controversial.  It's references to LGBT culture (especially the original video) sent shockwaves through the record buying public at the time. In fact, the BBC originally banned the song and the video!

<iframe width="560" height="315" src="https://www.youtube.com/embed/Yem_iEHiyJ0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The anti-war anthem "Two Tribes" was also shocking in that era.

<iframe width="560" height="315" src="https://www.youtube.com/embed/pO1HC8pHZw0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The double-album featured a large share of unique cover songs too: "War" (originally by Edwin Starr), "San Jose (The Way)" (originally by Dionne Warwick penned by Burt Bacharach), "Ferry" (originally by Garry and the Pacemakers) and of course "Born to Run" (originally by Bruce Springsteen). Talk about running the gamut! 

The most audacious thing of all is the title track, which at almost 14 minutes long basically consumes the entire first side of the record. The song warns of the dangers of a lifestyle of debauchery. Trevor Horn even brought in Yes guitarist Steve Howe to play acoustic guitar.

They released it as a single in a tidy 4-minute version, but they also put out a video double that length, which is completely over-the-top!

<iframe width="560" height="315" src="https://www.youtube.com/embed/WzOGLKTmeuY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Trevor Horn basically performed much of the album himself, without the actual band-members involvement. So, it is actually more of a Trevor Horn solo record than one of an actual group!

All told, *Welcome to the Pleasuredome* is an album that is worthy of immersion in more modern times. It has a production that screams 1980s, but still sounds fresh. It’s a lot to chew on!

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/album/0FU4Eo42Oyg1We3eRrOf4m?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

<iframe id='AmazonMusicEmbedB07BSNCSHT' src='https://music.amazon.com/embed/B07BSNCSHT/?id=YU9jv1cVhx&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *; clipboard-write" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/welcome-to-the-pleasuredome/1365569059"></iframe>

<iframe src="https://embed.tidal.com/albums/86639807" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>