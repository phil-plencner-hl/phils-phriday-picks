---
title: Grimes - Miss Anthropocene
author: Phil Plencner
date: 2020-04-29 15:36:55
tags:
- grimes
- 2000s rock
- dance music
- electronic music
---
I recently bought some records from a local small business (Celebrated Summer Records), and they arrived yesterday. One of them is the new Grimes album. 

This slab of dark, apocalyptic electro-pop is a fitting soundtrack for our times.

<iframe src="https://open.spotify.com/embed/album/1ZKbjlrUC5REoa13uSH5KL" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>