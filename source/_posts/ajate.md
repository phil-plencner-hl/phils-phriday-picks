---
title: Ajate - Alo
author: Phil Plencner
date: 2020-06-11 22:22:32
tags:
- alo
- ajate
- japanese music
- african music
---

Today's pick is a fascinating album of cultural fusion called Alo by Japanse band Ajate.

Ajate blends African dance music with traditional Japanese music ("Ohayashi"). They are a large scale band (10 members) who's percussionists use hand-made bamboo instruments as well as traditional Japanese percussion.

Way cool!

<iframe src="https://open.spotify.com/embed/album/1iDroKjCkOMrap4NQWNW4s" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>