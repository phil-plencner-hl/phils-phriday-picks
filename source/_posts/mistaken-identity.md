---
title: Vernon Reid - Mistaken Identity
date: 2021-09-03 11:00:41
tags:
- vernon reid
- living colour
- 90s rock
- jazz fusion
- alternative rock
- ornette coleman
---

Vernon Reid is best known as the lead guitarist for Living Colour. However, he is so much more than your average player in a late 80s / early 90s alternative rock band.

Before Living Colour, he was playing with drummer Ronald Shannon Jackson's band Decoding Society. Ronald Shannon Jackson was fresh from his stint with Ornette Coleman's free-funk band Prime Time when he started the Decoding Society, and they are definitely cut from the same cloth. A wild hybrid of free jazz and funk rock. Vernon Reid fit in really well there with his sheets of sound and wild solos.

Here is some killer footage of that band from 1983:

<iframe width="560" height="315" src="https://www.youtube.com/embed/bZL88zT-LsM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Living Colour's biggest success was their debut album *Vivid*, especially the single "Cult Of Personality". While it is great, I think the band only got better and more daring afterwards. The subsequent albums *Time's Up* and *Stain* are a couple of my favorite rock albums of the 90s. Check out these two songs (That were released as singles!!) from those albums to see just how truly badass they were at that time.

<iframe width="560" height="315" src="https://www.youtube.com/embed/0YAA7nivQng" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/OYKxa_22BVk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Living Colour broke up after *Stain* and Vernon Reid formed a new band (that later became known as Masque) including turntablist DJ Logic, Clarinetist Don Byron, Curtis Watts on drums and Hank Schroy playing bass. 

Their first album was *Mistaken Identity*, which is today's pick.

It was produced by both Prince Paul (De La Soul) and Teo Macero (Miles Davis) which should give you a sense of the large range the album covers. 

It is an amazing album that really showcases Vernon Reid's fantastic guitar playing, along with the rest of the band. However, there are great songs to boot. They play everything from reggae to heavy metal to hip-hop to funk to jazz fusion with equal proficiency.

It's a fun record to crank up over a 3-day holiday weekend. I hope you enjoy it as much as I do.

<iframe src="https://open.spotify.com/embed/album/035uGTNvPWUhVigYQHvCQZ" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>