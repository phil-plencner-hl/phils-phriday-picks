---
title: Sun Ra - In The Orbit Of Ra
author: Phil Plencner
date: 2020-06-05 22:32:56
tags:
- sun ra
- free jazz
- avant jazz
- frank zappa
- marshall allen
---

arlier this week, I did a "virtual shopping session" with my buddy Jack who runs [El Supremo Records in Baltimore](https://www.baltimoresun.com/citypaper/bcp-setting-a-record-record-collecting-isnt-just-jack-moores-hobby-its-part-of-his-worldview-20140902-story.html)

During that time, we went into long-winded discussions on Frank Zappa (of course!) but we also went in-depth on freaky jazz pianist / composer Sun Ra...which lead to a couple records being added to my "shopping bag".

For the uninitiated, Sun Ra was supposedly an interstellar being from Saturn, and his Arkestra would play music from outer space. Sun Ra's discography is lengthy, and is convoluted as many of it was released on tiny independent labels, including Ra's own "Saturn Records" imprint (although he did have a brief stint on Impulse Records).  

So unless you're a hardcore collector, how do you even begin to dive into his works? Luckily for you longtime Arkestra saxophonist Marshall Allen (who still takes a version of the Arkestra on tour at the young age of 96!) put out a pretty comprehensive compilation a few years ago that is great for new and old fans alike. 

In the Orbit of Ra!

<iframe src="https://open.spotify.com/embed/album/2dkGXUsgCBXn5sbBtTtKFv" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>