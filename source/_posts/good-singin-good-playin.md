---
title: Grand Funk Railroad - Good Singin' Good Playin'
author: Phil Plencner
date: 2020-04-09 14:57:21
tags:
- grand funk railroad
- 70s rock
- classic rock
- frank zappa
---
If there was an award for Truth In Advertising for Album Titles, the 1976 record by Grand Funk Railroad called *"Good Singin' Good Playin'"* would win hands down.  This was the final album before their initial breakup, and it is quite a doozy. 

It was actually produced by PPP favorite Frank Zappa...and alert listeners might hear him rip out a gnarly guitar solo on "Out To Get You" as well. 

This album didn't contain any of Grank Funk's big hits, but in a perfect world every song here would be huge.

[Extra credit](https://www.youtube.com/watch?v=YXKmsvRXE4A)

<iframe src="https://open.spotify.com/embed/album/5iep9ZnQxhi2YzgTH7HmYU" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>