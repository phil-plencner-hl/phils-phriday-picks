---
title: Mike Keneally - Piano Reductions Vol. 1
date: 2021-03-24 08:17:50
tags:
- Steve Vai
- Mike Keneally
- Frank Zappa
- 90s rock
- hard rock  
- jazz
---

Mike Keneally and Steve Vai were both part of Frank Zappa's touring bands at one point, playing "stunt guitar" (basically meaning they perform the more virtuosic parts and solos). Vai played with Zappa from 1980-1983 and Keneally in 1988. 

Both went on to successful solo careers after being employed by Zappa. Renown not only for their guitar prowess, but also music composition and playing other instruments.

So it was very interesting to me that in the early 2000s Steve Vai reached out to Mike Keneally with an idea to re-record some of his songs on piano. Keneally loved the idea. He transcribed and learned a bunch of Vai's songs on solo Piano. 

The results are contained in *Piano Reductions Vol. 1*, which was released in 2004. Vai's originally hard-rock based songs are presented here in their sparsest form. This is not to say it is a quiet, cocktail jazz album. To the contrary, much of the playing is very powerful and forceful. It touches on many of the fan-favorites that Vai released up to this point in his career. Some of these songs I prefer in this state as opposed to the originals!

I can't be the only one that loves to hear "Kill The Guy With The Ball" played by Mike Keneally on solo piano, right??

<iframe src="https://open.spotify.com/embed/album/1VgrQUo318kwhPvu5oGhIf" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
