---
title: Kassa Overall - Animals
date: 2023-08-04 16:30:38
tags:
- kassa overall
- jazz
- jazz fusion
- hip-hop
- rap 
- nick hakim
- francis and the lights
---

![](Animals.jpg)

Kassa Overall is an extremely talented drummer and rapper. He has been putting out albums on small labels (including a previous PPP *I Think I'm Good*) which are worth checking out. 

He recently signed to Warp Records, which should bring him much larger exposure. The first record that is part of that collaboration is called *Animals* and is today's pick!

*Animals* whips by at a mostly furious pace (12 songs in 35 minutes). In that short time span he melds a diverse set of styles from jazz to hip-hop to rock. A lot of times it is hard to tell what is played organically and what is electronic. Similarly, its difficult to know what is structured or improvised. It flows so well, and the songs are so well written, orchestrated and produced that it doesn't hinder the enjoyment you get from listening to it.

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

The album also features many guests: Nick Hakim on "Make My Way Back Home", trumpeter Theo Croker on "The Lava Is Calm" and Francis and the Lights on "So Happy" and "Going Up" are just a few examples.

There were two videos produced for the record. One is "Make My Way Back Home":

<iframe width="560" height="315" src="https://www.youtube.com/embed/xMhmMEZdK2c" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

The other video is for the song "Going Up":

<iframe width="560" height="315" src="https://www.youtube.com/embed/4PkSuvr1L_w" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

There are a few strictly instrumental songs, but they are maddingly short ("No It Ain't" and "Still Ain't Find Me" zoom by in under 2 minutes each). This means the focus is on the vocal oriented songs. Kassa's lyrics mainly deal with themes of mental health and societal ills, which are interesting topics in today's day and age.  

Throughout the whole album Kassa's drumming is powerful and exciting. He frequently plays some blazing fast fills between the funky beats, and they bring a sense of urgency to many of the songs. 

Kassa Overall recently performed on NPR's *Tiny Desk Concert*. The short set shows him switching between drumming and rapping with features from the rest of his awesome live band. A couple of the songs are from *Animals* ("Ready To Ball" and "Make My Way Back Home") and the others are from earlier albums. This is worth checking out to see how these songs translate to a live setting:

<iframe width="560" height="315" src="https://www.youtube.com/embed/0kKct_AKz8s" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

*Animals* will likely end up as one of my favorite albums released in 2023. It’s been in heavy rotation since it came out back in May and I don't see that changing anytime soon! *Animals* is an incredible work featuring a band at the height of their powers.

{% iframe 'https://open.spotify.com/embed/album/6oRVOPA5BEzqm6j8RCQR19?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B0BWH6X766/?id=vpEeT851Hd&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/ua/album/animals/1673581769' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/278158608' '100%' '96' %}
