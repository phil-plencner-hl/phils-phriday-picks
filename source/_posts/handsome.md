---
title: Handsome - Handsome
date: 2022-09-09 10:34:58
tags:
- handsome
- helmet
- quicksand
- cro-mags
- 90s rock
- alternative rock
- hardcore punk
- punk rock
---

![](Handsome.jpg)

I posted some photos over on my Instagram account recently about the [Helmet / Faith No More show I attended 30 years ago this month](https://www.instagram.com/p/Ch9luhrurFj/?utm_source=ig_web_copy_link). Aside from the nostalgia of seeing the old photos from the concert, it also made me think back to bands in that style from that era.

Helmet was awesome in those days. Nowadays it's Page Hamilton and a bunch of hired guns, but the classic lineup in 1992 was devastatingly good. No further proof is needed than the "Unsung" video from *Meantime*:

<iframe width="560" height="315" src="https://www.youtube.com/embed/jBfygUiS50g" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Another band from 1992/1993 playing in a similar NYC-hardcore style was Quicksand. They had recently put out their album *Slip* featuring the single "Dine Alone":

<iframe width="560" height="315" src="https://www.youtube.com/embed/egzF66hDkFs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Before Helmet recorded *Betty* in 1994, guitarist Peter Mengede left the group over creative differences. 

After 1995's *Manic Compression*, Quicksand broke up. So, their guitarist Tom Capone and Peter Mengede decided to join forces in a new band.

They recruited former Cro-Mags drummer Pete Hines, bassist Eddie Nappi (who later went on to play with Mark Lanegan) and vocalist Jeremy Chatelain (who later went on to form Jets to Brazil). 

They called themselves Handsome (attempting to buck the trend of NYC-hardcore bands having tough sounding names) and released one brilliant self-titled album in 1997. That album is today's pick!

It is a shame they didn't get wider recognition. They opened for acts that were big at the time like Local H and Silverchair but never really built on that momentum. They released one single / music video for "Needles" which is a pretty accurate snapshot of their sound and style:

<iframe width="560" height="315" src="https://www.youtube.com/embed/YgX-HKXTb-M" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*Handsome* really is one of those underrated albums that flies under the radar even today. Their mix of heavy dissonant music and melodic vocals is worth checking out (or revisiting) today.

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/album/4Erk4K1hOnmQbfamo4xgid?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

<iframe id='AmazonMusicEmbedB00EYKP66K' src='https://music.amazon.com/embed/B00EYKP66K/?id=z75nRjHc0v&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *; clipboard-write" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/handsome/268442790"></iframe>

<iframe src="https://embed.tidal.com/albums/665305" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>