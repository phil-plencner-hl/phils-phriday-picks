---
title: Zach Hill - Astrological Straits
date: 2023-06-23 11:09:16
tags:
- hella
- death grips
- zach hill
- brutal prog
- prog rock
- progressive rock
- 00s rock
- avant-rock
 
---

![](AstrologicalStraits.jpg)

Zach Hill is one of my favorite modern-day drummers. I have previously highlighted some of his work in the past, [choosing Hella's *Hold Your Horse Is* as a PPP in way back in January 2021](https://phil.share.library.harvard.edu/philsphridaypicks/2021/01/22/hold-your-horse-is/).

Outside of Hella, Zach Hill has been very prolific. Releasing dozens of albums under different names with different musicians...it’s a deep rabbit hole to dive into. I'll keep today's post focused but encourage a deeper dive if the mood strikes.

In 2007, Hella expanded from its original duo format into a much larger band (complete with a singer) and released *There's No 666 In Outer Space*. Around the same time Zach Hill started a side-band called The Holy Smokes and put out two records: *Masculine Drugs / Destroying Yourself Is Too Accessible* in 2004 and *Talk to Your Kids About Gangs* in 2006. They pushed the mathy, brutal prog rock of Hella to the outer limits. Completely overwhelming, noisy and complex the records definitely catered to a small audience of drummers and avant-rock aficionados.

In 2008, Zach Hill took the best parts of the concepts introduced on those albums, added a bit more melody, recruited some high-profile guests and released his best solo record (in my opinion): *Astrological Straits* on Mike Patton's Ipecac Records.

Some of the bigger names on the album were Chino Moreno (Deftones, Team Sleep), Tyler Pope (!!!) and Les Claypool (Primus, Frog Brigade). Even with that cast of musicians it is still Zach Hill's sound and vision with completely crazy, psychedelic, high speed music with his constant-soloing style of drumming and pitch-shifted vocals mixed to the front. The drumming alone really pushes the limits of human endurance and are completely mind-blowing and awe-inspiring.

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

[Pitchfork's review of it at the time](https://pitchfork.com/reviews/albums/12127-astrological-straits) gave it a pretty apt description: "At its best, Astrological Straits is a mashup of Liars tribalism, Boredoms bombast, Smell-scene art-punk, Lightning Bolt repeti-grooves, and Frank Zappa prog-overload.". Obviously, this is right up my alley!

Ipecac pushed the record hard. They released two music videos from the album: "Dark Art" and "Hindsight Is Nowhere".

<iframe width="560" height="315" src="https://www.youtube.com/embed/M4kVOJfZg_E" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/Z4gJvhnubyc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

The album was originally released on 2 CDs. The 2nd CD was a 32-minute drum / piano free-jazz duet with Marnie Stern on vocals called "Necromancer" which is ridiculously awesome.

Once *Astrological Straits* was released, Zach Hill toured behind it as a solo act, playing "Necromancer" which is certainly an odd way to promote the record! Here is some video footage of what that was like:

<iframe width="560" height="315" src="https://www.youtube.com/embed/ptCf8NZY-H8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Also, around that time, Zach Hill recorded footage of him playing his drums on top of a mountain that was supposed to be used for a Vans shoe commercial. I don't believe the commercial ever aired, but there is some incredible raw footage of it on the internet:

<iframe width="560" height="315" src="https://www.youtube.com/embed/D8AzjOfwk4s" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

*Astrological Straits* was released 15 years ago, but still sounds completely fresh today. A truly timeless masterpiece from one of the world's greatest drummers. 

{% iframe 'https://open.spotify.com/embed/album/1O2yj7FlwfhoVbFGoZRkNz?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B00WNPLZ22/?id=w2btQwBz4D&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/astrological-straits/989430426' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/46153823' '100%' '96' %}
