---
title: The Band - The Last Waltz
date: 2020-11-25 10:03:53
tags:
- the band
- the last waltz
- classic rock
- 70s rock
- thanksgiving
- neil young
- dr. john
- muddy waters
- eric clapton
- joni mitchell
- paul butterfield
- van morrison
---

Special Thanksgiving reminder that should listen to / watch *The Last Waltz* this week (as it is a holiday tradition in Casa De Phil).

*The Last Waltz* was the farewell concert by The Band, filmed on Thanksgiving in 1976. It was directed by Martin Scorsese and is considered (quite correctly) as one of the best concert films of all time.

The concert features notable guests such as Eric Clapton, Neil Young, Muddy Waters, Dr. John, Joni Mitchell and Van Morrison. 

A few years ago it was re-released with extended footage and songs on Blu-Ray. If you have never watched or listened to this historical rock concert, what are you waiting for??

Happy Thanksgiving!

<iframe src="https://open.spotify.com/embed/album/7uqVE9qWdqjtDeLpdHyMxP" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>