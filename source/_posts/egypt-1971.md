---
title: Sun Ra - Egypt 1971
date: 2024-04-05 16:38:46
tags:
- sun ra
- free jazz
- avant jazz
- marshall allen
- egypt
---

![](Egypt1971.jpg)

I have been debating what would be an appropriate pick for this week since a full solar eclipse will be happening on Monday. I eventually decided that a Sun Ra record would be the best bet (since Sun Ra is named after the Egyptian god of the Sun) ...but with such a lengthy and convoluted discography what would be the best one to choose?

As part of my process, I have been playing selections from my large (but far from complete!) Sun Ra vinyl record collection, [which can be seen here](https://www.instagram.com/reel/C5ZQml3rwkj/?utm_source=ig_web_copy_link&igsh=MzRlODBiNWFlZA==).


One of my prized possessions in that pile of records is the [*Egypt 1971* box set](https://www.discogs.com/release/15579788-Sun-Ra-Egypt-1971) that I was able to snag on Record Store Day a few years back (now woefully out of print and going for a pretty penny online). 

It's a five-record set, consisting of three previously released albums and two unreleased records. Sadly, the stuff exclusive to the box is not available for streaming, but the other albums are available separately so I will select all of them as today's pick! 

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

The three albums are: *Dark Myth Equation Visitation*, *Nidhamu* and *Horizon* and altogether they paint a pretty clear picture of what they sounded like on what was likely an amazing trip to Egypt for what was at the time an Arkestra that was twenty-one members strong. the musicians that took part in the trip included:

    - John Gilmore tenor sax, percussion,
    - Danny Davis - alto sax, flute
    - Marshall Allen - alto sax, flute, oboe
    - Kwamw Hadi - trumpet, conga drums
    - Pat Patrick - baritone sax
    - Elo Omoe - bass clarinet
    - Tommy Hunter - percussion
    - Danny Thompson - baritone sax, flute
    - June Tyson - vocals
    - Larry Narthington - alto sax, conga drums
    - Lex Humphries - percussion
    - Hakim Rahim - alto sax, flute
    - Sun Ra - organ, moog, piano

Back in 2021 *The Nation* magazine [wrote a pretty interesting overview of the trip](https://www.thenation.com/article/culture/sun-ra-egypt-1971-review/) that is worth reading to get some background. Basically, they were already in Europe and had an opportunity to book a flight to Cairo and they jumped on it. Local drummer and Sun Ra fanatic Salah Ragab was able to supply them with musical gear and book the gigs, which ranged from legitimate theater shows to house parties to impromptu outdoor jam sessions.

Along the way they also filmed some home movies, a small clip of which is available online. What little footage there is really illustrates how bonkers they looked and sounded at that time:

<iframe width="560" height="315" src="https://www.youtube.com/embed/4DZXNe6Wuj0?si=x-1R_sWm8bpBCxj5" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

So, what do the records consist of?

*Dark Myth Equation Visitation* features a recording of a TV performance, including an interview, along with song recorded during a concert in Heliopolis near Cairo.

{% iframe 'https://open.spotify.com/embed/album/4lGSWQ11DLHBGg5GwDvKjl?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B08JH4GNP1/?id=rw4QBPMDHM&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/dark-myth-equation-visitation/1532399659' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/184837080' '100%' '96' %}

*Nidhamu* includes selections from various concerts in Cairo plus, oddly enough, a solo keyboard performance which was recorded a week earlier in Oakland, California.

{% iframe 'https://open.spotify.com/embed/album/6GMHPCxAfx3K1jCYZPm0ad?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B08JH6DWGB/?id=PCUvklF35T&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/nidhamu/1532396005' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/184817237' '100%' '96' %}

*Horizon* is focused solely on a concert held at the Ballon Theatre in Cairo.

{% iframe 'https://open.spotify.com/embed/album/050YbB6NQItIjmOwthAdzN?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B08JH3LV13/?id=sa4M29nDiN&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/horizon/1532402082' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/184811115' '100%' '96' %}

The playing throughout is wild and free, including lots of crazed blasts of organ from Sun Ra along with out of this world ensemble playing with tons of heavy percussion. 

These recordings are obviously a perfect fit for blasting while watching our sun disappear for a few hours on Monday enveloping our world in complete darkness! See you on the other side...
