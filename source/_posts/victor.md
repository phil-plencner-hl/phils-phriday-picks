---
title: Victor - s/t
author: Phil Plencner
date: 2020-05-05 15:16:20
tags:
- victor
- rush
- alex lifeson
- 90s rock
- alternative rock
---
I'm thinking of having some sort of recurring category of PPP called "unusual side projects" or something to that effect. With this in mind, here's a potential first installment! 

Back in the mid-90s, when Rush went on a hiatus, guitarist Alex Lifeson released an album by his heavy alternative rock alter-ego "Victor". 

There are definitely shades of Rush in it, but there are no progressive rock epics...and its definitely inspired by popular mid-90s heavy rock. There's even some Nine Inch Nails influence throughout.

<iframe src="https://open.spotify.com/embed/album/1HP7hZBF7hXvTVO4B4ZxgO" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>