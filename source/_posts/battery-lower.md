---
title: The Cell Phones - Battery Lower
date: 2020-11-24 11:18:48
tags:
- chicago
- grindcore
- pop rock
- alternative rock
- punk rock
- the cell phones
---

Special day for the pick this week, due to the Thanksgiving holiday.

Today I'd like to draw your attention to the excellent Chicago band called The Cell Phones. They are a drums / bass / vocals trio that plays a highly energetic mix of pop, punk and grindcore. 

The grindcore influence mainly comes from the drummer (Justin Purcell), as he peppers the songs with blast beats and other unconventional rhythms.  The bassist (Ryan Szeszycki) utilizes an acoustic bass, but runs it through a bunch of effects pedals making it sound like there is a guitarist involved when there isn't. Vocalist Lindsey Charles sings, screams and yelps on top of it all. It is basically a recipe for awesome.  

While they have been around for decades, they have remained under-the-radar. Here's hoping their new album *Battery Lower* is what finally gives them some wider exposure. It is a quick burst of energy (15 songs in a crisp 25 minutes) that should help propel you into the long weekend.

<iframe src="https://open.spotify.com/embed/album/7LVLQoM2CBAkFLbovIBtvA" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>



