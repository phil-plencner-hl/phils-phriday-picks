---
title: Ginger Baker Trio - Falling Off The Roof
date: 2024-01-19 17:35:12
tags:
- ginger baker
- charlie haden
- bill frisell
- cream
- blind faith
- feli kuti
- classic rock
- jazz
- jazz fusion
---

![](FallingOffTheRoof.jpg)

Ginger Baker is most famous for being the drummer in Cream and Blind Faith. He is a legend based on membership in those bands alone, but his musical career is much more than that.

But let's start with the aforementioned groups just because they are so awesome! Cream was a power trio with Eric Clapton and Jack Bruce. They definitely put the power in power trio.

A great example of Ginger Baker's drumming in Cream is the song "Toad". While there is a little bit of full band playing in the intro and outro it is essentially a drum solo. 

Here is a great version of "Toad" at the Royal Albert Hall in 1968:

<iframe width="560" height="315" src="https://www.youtube.com/embed/GDDPn4k10WU?si=QV20MhTROvjpDXw-" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

After Cream broke up, Ginger Baker joined Blind Faith with Steve Winwood, Ric Grech and Eric Clapton. They were even more short-lived than Cream but left a big impression with their one self-titled record.

At Hyde Park in London in 1969 they played perhaps their definitive concert. Luckily there is footage on the internet. Here is "Had To Cry Today":

<iframe width="560" height="315" src="https://www.youtube.com/embed/x_iLZXjhI-g?si=IN0rG8zqJ0BTJjQj" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Next, he started a jazz fusion band called Ginger Baker's Air Force. This was basically an expanded Blind Faith that was much jazzier. Awesome footage of them playing Wembley Stadium in 1970 is essential viewing:

<iframe width="560" height="315" src="https://www.youtube.com/embed/fITPJ2lkUso?si=4oZ5RIPN23j65Xz_" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

In the 1970s, Ginger Baker moved to Africa and immersed himself in the music culture there. He even built his own music studio in Lagos, Nigeria!

Most famously there he collaborated with Afrobeat legend Fela Kuti. If you want to see and hear some absolutely bonkers drumming, look no further:

<iframe width="560" height="315" src="https://www.youtube.com/embed/89sxJdr5B-I?si=ucX5yHxONMAabKYX" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

In the 1980s Ginger Baker essentially went into hiding. He still lived in Africa but lived a secluded lifestyle. Eventually he moved to the United States and was playing again with a vengeance. He collaborated with a ton of musicians at this time, including Bill Laswell, and even played in a reformed Cream. 

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

The best of the collaborations in my opinion was the trio he formed with bassist Charlie Haden and guitarist Bill Frisell. You might think this is a unusual grouping, but their playing together was absolutely magical. 

They played jazz standards, along with compositions from all three members. It's what I like to call power jazz. Ginger Baker does not hold back with his playing at all, Bill Frisell plays his crazed melodies and Charlie Haden on an upright bass keeps it all together. 

They play so fluid together, like they have been a working unit for decades. The chemistry they had was nothing sort of incredible.

Here is an example of their playing in Frankfort, Germany in 1995:

<iframe width="560" height="315" src="https://www.youtube.com/embed/Go8UgNMQ-Dg?si=zP6oOYkvVVGKH_sK" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

The Ginger Baker Trio, as they came to be known, only put out two albums. Both are great, but the second one is my favorite. *Falling Off The Roof* is today's pick!

The trio is expanded on this album to include fusion guitarist Jerry Hahn (on "Sunday At The Hillcrest") and Bela Fleck on banjo on 3 songs (including a great performance of Charlie Parker's "Au Privave"). Speaking of covers, there is also a rendition of Thelonious Monk's "Bemsha Swing" that is off the chain.  

The Ginger Baker Trio didn't last long, likely cut short because of the Cream reunion. 

Aside from Ginger Baker's fiery playing, he also is famous for his crazed personality and temper. To get an inside look at this I highly recommend the documentary called "Beware of Mr. Baker":

<iframe width="560" height="315" src="https://www.youtube.com/embed/9F9kH--atq0?si=Lgrov-JsaPZ-Vxpi" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Ginger Baker passed away in 2019 from complications of COPD. His music lives on, especially on masterpieces like *Falling Off The Roof*.

{% iframe 'https://open.spotify.com/embed/album/3VxJUjCc7JP2VG6phf0zNo?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B003A9COLQ/?id=FXD3gIFuEU&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/falling-off-the-roof/357867326' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/3460984' '100%' '96' %}
