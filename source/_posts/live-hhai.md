---
title: Magma - live / Hhai
date: 2021-03-23 08:19:43
tags:
- magma
- zeuhl
- 70s rock
- progressive rock
- jazz fusion
- jazz rock
---

French band Magma is a pretty unique group in the world of rock music. Magma is the brainchild of drummer Christian Vander and his powerful music and overall vision basically stands alone. They essentially invented a new subgenre of progressive rock called "Zeuhl" that was known for its driving / repetative drums, throbbing bass, operatic vocals and sci-fi inspired backstories.

Magma also invented their own fictional language called "Kobaïan" that all their songs are sung in.

Needless to say this is really bizarre stuff. 

It is hard for me to pick a single album by Magma, because in the early to mid 70s pretty much every album is excellent. I decided to go with their 1975 *Live / Hhai* album because it gives a great overview and shows the band at the height of their powers in a live setting (It is a recording of their performance in Paris in June 1975).

The album starts off with the complex two-part title track from *Kohntarkosz*. No warming up, they just jump headfirst into things and expect the audience to keep up. Insane stuff.

The closing of the album where they play Da Zeuhl Wortz Mekanïk and Mëkanïk Zaïn from *Mekanïk Destruktïw Kommandöh* is another astounding highlight. Of course, between these songs and *Kohntarkosz* we're already talking about close to an hour of music right there! The rest of the live album is just gravy after all of this.

I saw a reformed Magma perform in the early 2000s.  While they were still great, they didn't reach the ecstatic heights of this album. I'm certainly glad this document exists.

<iframe src="https://open.spotify.com/embed/album/78sy1qgqdfjea9lZHoVWRK" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>


