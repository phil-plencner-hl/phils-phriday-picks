---
title: Tony Allen - Rejoice
author: Phil Plencner
date: 2020-05-01 15:22:03
tags:
- tony allen
- afrobeat
- fela kuti
- damon albarn
---
Afrobeat drumming legend Tony Allen passed away this week. 

He was famous of being part of Fela Kuti's Africa '70 band, and eventually teamed up with Damon Albarn in The Good, The Bad and The Queen. 

He also put out a bunch of excellent solo albums...his latest which only came out a month ago. This is today's pick!

<iframe src="https://open.spotify.com/embed/album/061q5E43gIp25oJxVxvAav" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>-