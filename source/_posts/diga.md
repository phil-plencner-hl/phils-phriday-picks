---
title: Diga Rhythm Band - Diga
author: Phil Plencner
date: 2019-08-02 15:38:47
tags:
- diga rhythm band
- 60s rock
- classic rock
- grateful dead
---

In 1975-76, during a break from touring with the Grateful Dead, Mickey Hart assembled the percussion group Diga Rhythm Band. Here is there only album, which is pretty awesome. 

You can hear Jerry Garcia's guitar within the heady rhythms. Tabla player Zakir Hussain ended up continuing to collaborate with Mickey Hart through the decades as part of Planet Drum. 

Alert Deadheads will also note that the song "Happiness is Drumming" was later given lyrics by Robert Hunter and became the live favorite Fire On The Mountain.

<iframe src="https://open.spotify.com/embed/album/0iqEDwI3wY6kRkvJUxn7he" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>