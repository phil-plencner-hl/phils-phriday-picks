---
title: Electrocution 250  - Electric Cartoon Music From Hell
date: 2022-10-14 10:58:15
tags:
- electrocution 250
- jazz fusion
- cartoon music
- heavy metal
- technical metal
- lalle larson
- peter wildoer
- todd duane
---

![](ElectricCartoonMusicFromHell.jpg)

Since I highlighted actual music from cartoons for last week's pick, I decided to follow it up with a more contemporary take on the style.

In the early 2000s there was a short-lived trio of virtuoso musicians that went under the name Electrocution 250. They put out one album, the aptly titled *Electric Cartoon Music From Hell* in 2003. 

Electrocution 250 consisted of keyboard player Lalle Larson, drummer Peter Wildoer and guitarist Todd Duane.

Lalle Larson first came to my attention on the zany Dr. Zoltan Obelisk (aka Carl King) album [*Why I Am So Wise, Why I Am So Clever and Why I Write Such Good Songs*](https://shop.carlkingdom.com/collections/cds/products/copy-of-dr-zoltan-obelisk-why-i-am-so-wise-why-i-am-so-clever-and-why-i-write-such-good-songs-cd) album. I had been following Carl King through his association with Trey Spruance / Secret Chiefs 3. He released an epic album under a different pseudonym (Sir Millard Mulch) called [*How To Sell The Whole F#@!ing Universe To Everybody Once And For All!*](https://shop.carlkingdom.com/collections/frontpage/products/sir-millard-mulch-how-to-sell-the-whole-f-ing-universe-to-everybody-once-and-for-all) that I was obsessed with at the time. These two albums lead me to eventually diving into the discographies of the other musicians involved, including Lalle Larson.

More recently, Lalle Larson played on guitarist Terry Syrek's album *Story* along with drummer Marco Minnemann and bassist Bryan Beller:

<iframe width="560" height="315" src="https://www.youtube.com/embed/PVhrBoF4uro" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Todd Duane was previously part of the Shrapnel Records stable of "shred guitar" players. He released one self-titled album on the label and since then has been surprisingly [focusing on Christian worship music](https://toddduane.net/servant-rocks).

Peter Wildoer is probably most well known as the drummer of Swedish thrash band Darkane, but he also has a ton of side hustles with other metal groups. In fact, he also auditioned to be Mike Portnoy's replacement in Dream Theater in 2011:

<iframe width="560" height="315" src="https://www.youtube.com/embed/m2kIVOr1VPk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

So what does Electrocution 250 sound like? Well, if the album title doesn't clue you in it's basically a Carl Stalling inspired set of zany instrumental jazz fusion. The musicianship is beyond comprehension throughout all the songs. The energy level and speed are basically set to the maximum level for the duration of the album. Even the solo pieces (each player has one small track where only they appear) are engaging and interesting, which is hard to do.

Sadly, this was the only album they put out, but there is so much packed into it that it feels like it would be several albums from mere mortals.

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/album/17x2nV0SclurKbl8ybrZNQ?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

<iframe id='AmazonMusicEmbedB0013PIAYE' src='https://music.amazon.com/embed/B0013PIAYE/?id=etPWUCbj64&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *; clipboard-write" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/electric-cartoon-music-from-hell/18285034"></iframe>

<iframe src="https://embed.tidal.com/albums/27809716" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>