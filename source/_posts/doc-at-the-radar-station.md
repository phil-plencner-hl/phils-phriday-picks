---
title: Captain Beefheart - Doc At The Radar Station
author: Phil Plencner
date: 2020-07-20 16:08:48
tags:
- captain beefheart
- frank zappa
- doc at the radar station
- gary lucas
- eric drew feldman
- the pixies
- frank black
- jeff buckley
---

How have I been doing PPP this long and I haven't picked a Captain Beefheart album yet?? That deficiency ends today!! 

*Trout Mask Replica* is his most notorious album...but it's not on Spotify and is pretty confounding to the uninitiated. So I'll pick the 1980 album *Doc At The Radar Station*. This wasn't the "classic" magic band (although original drummer John "Drumbo" French drops by to play slide-guitar and drums on a couple tracks). However, this magic band is still pretty killer! 

It includes Eric Drew Feldman who was the touring keyboardist in The Pixies circa *"Trompe Le Monde"* (and later was a member of Frank Black's solo band along with Jeff Tepper who also appears here).   Guitarist Gary Lucas also is part of this band (who played on Jeff Buckley's *"Grace"* and also toured with Chris Cornell). 

The tunes are scorchers. High energy rock music with enough twists and turns to know it's involving nobody else but Beefheart. The lyrics are surreal and overall the package is a pretty great catchy raw rock record. It's hard to believe this was released on a major label (and the band also appeared on Saturday Night Live!). 

This will forever be one of my desert-island-discs.

<iframe src="https://open.spotify.com/embed/album/6JyNWU5OrHs00ayVrY896l" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>