---
title: Best Of 2023
date: 2023-12-21 19:35:49
tags:
- best of list
---

![](2023.jpg)

It's the end of another year. 2023 is quickly coming to a close, which means everyone's "Best Of" lists are coming in fast n' furious. I've been finding other people's lists fascinating lately, because there really doesn't seem to be much consensus (likely thanks to streaming and the internet in general). People are in their own microcosms. 

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

I, of course, am in my own unique bubble. I compiled my list before diving into other people's lists (so I wouldn't be influenced) and I was genuinely surprised that I didn't see many of my favorites show up in other lists! I hope this means my list will offer a unique perspective on 2023 and introduce you to tons of new music (even though alert readers should be familiar with some of the choices since they have been featured in previous Phil's Phriday Picks).

As in years past, I don't rank the albums. Instead, I make a listenable playlist of my favorites. In 2023, it consists of a whopping 94 songs spanning nearly 8 hours from what I consider the best albums of the year. This is intended as a digestible snapshot of what new albums I enjoyed the most this year. Additionally, it provides listeners an opportunity to dive further into whatever albums catch their ear the most. 

Likely surprising nobody, it covers a huge swath of styles and genres. Everything from country to free jazz to progressive rock to death metal there is literally something for everybody.

The playlist is available in Spotify for your listening pleasure.

{% iframe 'https://open.spotify.com/embed/playlist/1m5Ivl8iljzAbS1cbbCF2P?utm_source=generator' '100%' '352' %}

At the end of this post, I will write little blurbs about each album.

Aside from the songs in these playlists I want to highlight a few other albums that I listened to frequently this year that are not available on streaming services.

**Vomitatrix - Violently Ill**

The one and only "Acid Grind" band returns with another quick blast of chaos! 18 songs in less than a half hour zoom by in an evil blur.

<iframe style="border: 0; width: 100%; height: 42px;" src="https://bandcamp.com/EmbeddedPlayer/album=1910586828/size=small/bgcol=ffffff/linkcol=0687f5/transparent=true/" seamless><a href="https://vomitatrix.bandcamp.com/album/violently-ill">Violently Ill by Vomitatrix</a></iframe>   

**Behold The Arctopus - Interstellar Overtrove**

Progressive metal band makes a wild left turn. Turning off all the distortion and turning up all the weirdness. It sounds like Alan Holdsworth playing his Synthaxe while falling down three flights of stairs.

<iframe style="border: 0; width: 100%; height: 42px;" src="https://bandcamp.com/EmbeddedPlayer/album=4241689131/size=small/bgcol=ffffff/linkcol=0687f5/transparent=true/" seamless><a href="https://beholdthearctopus.bandcamp.com/album/interstellar-overtrove">Interstellar Overtrove by Behold The Arctopus</a></iframe>

**A Beginner's Guide to Sublime Frequencies Vol 1&2**

From the official press release of this compilation: "Thoughtfully curated by international music ensemble High Castle Teleorkestra (HCT), this SUBLIME FREQUENCIES authorized 2+ hour, 45 track, double-length compilation is a great point of entry to the daunting 120+ releases in the legendary SUBLIME FREQUENCIES record catalog." 

Essential listening!

<iframe style="border: 0; width: 100%; height: 42px;" src="https://bandcamp.com/EmbeddedPlayer/album=1555604748/size=small/bgcol=ffffff/linkcol=0687f5/transparent=true/" seamless><a href="https://highcastleteleorkestra.bandcamp.com/album/a-beginners-guide-to-sublime-frequencies-vol-1-2">A Beginner&#39;s Guide to Sublime Frequencies Vol 1&amp;2 by Sublime Frequencies (Various Artists)</a></iframe>

Phil's Phriday Picks will be taking a holiday break. It should be back in action on January 12th, 2024. Happy Holidays!!

As promised, here is the summary of my top 94 albums....

**Fall Out Boy - So Much (For) Stardust**

Their first new record in five years is a return to their classic form! Super-catchy punk / emo songs with their unique wit. 

**Oxbow - Love's Holiday**

The mighty Oxbow leaves long time label Hydra Head to the greener pastures of Ipecac for their latest release. Even though the theme of the record is love...that love is shown through the demented Oxbow lens. A furious, pummeling rock record that is just what I needed this year.

**Lydia Loveless - Nothing's Gonna Stand In My Way Again**

A recent Phil's Phriday Pick! This album showcases Lydia Loveless' amazing songwriting and lyrical chops with a killer backing band.

**Yussef Dayes - Black Classical Music**

English jazz drummer Yussef Dayes finally releases an album under his own name and it is a groovy one! Includes guests from the London jazz scene that Dayes plays in including Shabaka Hutchings (from The Comet Is Coming and Songs Of Kemet). This is a trippy funk fusion record that reminds me of early 70s Herbie Hancock.

**Tomb Mold - The Enduring Spirit**

Tomb Mold is probably the most interesting death metal band coming out of Canada nowadays. They've expanded their sound to include a hybrid of jazz fusion and prog rock pushing their classic death metal template into a unique modern form.

**Kurt Elling - SuperBlue: Guilty Pleasures**

Kurt Elling's collaboration with 8-string guitar player Charlie Hunter and drummer Nate Smith (under the moniker SuperBlue) delivers a fun covers record. The originals span a wide range: Anything from Eddie Money to AC/DC and Al Jarreau! Kurt Elling's amazing vocal chops with the top-notch musicians is once again a slam dunk. 

**Tee Vee Repairmann - What's On TV?**

A fun throwback sounding garage rock band from Australia that brings just enough unique twists to keep my interest high. Hook-filled songs, raging guitar, wild theramin outbursts...what else do you want?!

**Dave Lombardo - Rites Of Percussion**

One of my all-time favorite drummers releases a solo record of percussion heavy instrumentals. This couldn't be further from the sound of Slayer or Fantomas but that’s part of what makes it so amazingly awesome. I have a soft-spot for interesting drum solo records and this one definitely scratches that itch.

**Meshell Ndegeocello - The Omnicord Real Book**

The jazz and R&B genius Meshell Ndegeocello delivers another lengthy excursion to the outer reaches of the genres. An expansive work that takes many listens to wrap your brain around that is filled with big name guests such as Jeff Parker, Brandee Younger, Jason Moran, Joan As Police Woman, Joel Ross and many more. It is probably one of the wildest jazz-rock records that Blue Note has released to date.

**Lil Yachty - Let's Start Here**

What in the world happened here?! Trap rapper Lil Yachty ditches his usual formula to deliver a bonkers psychedelic space rock record that picks up where classic Pink Floyd left off. Usually these kinds of "rapper turned rocker" records are disappointing but this one wildly exceeds all expectations. The biggest and most welcome surprise of the year.

**The God In Hackney - The World In Air Quotes**

A super-strange mish-mash of art rock and new wave that really makes me question the band members sanity. There's even touches of jazz fusion and ECM-inspired jazz that seeps into their rock sound. If this is the direction that progressive rock is headed in the future I am all for it.

**Horrendous - Ontological Mysterium**

Philadelphia's progressive death metallers Horrendous have been putting out killer record after killer record for the past decade and this one is no exception. A wild ride from start to finish...so many riffs...so many twists n' turns...so much heaviness. Every time I play this I need to peel myself off the floor as I've been mowed down by their brutal assault.

**Joshua Ray Walker - What Is It Even?**

Joshua Ray Walker has been making a name for himself in Texas and beyond for his sound that is rooted in traditional country but is filled with modern twists with an incredible voice and great musicianship. This record is a series of covers of songs originally done by female artists! You haven't lived until you've heard his versions of Cher's "Believe", Lizzo's "Cuz I Love You", Sinead O'Conner's "Nothing Compares 2 U" and The Cranberry's "Linger" (the latter of which includes Kyle from Tenacious D as well).

**Liturgy - 93696**

Another completely off-the-wall black metal record from Haela Hunt-Hendrix. The term "extreme" does not even begin to describe this completely mind-melting record that punishes the ears one minute and sends you into a serene trance the next.

**Blink-182 - One More Time...**

Blink-182's triumphant return! If you liked what they did in the past, you won't be disappointed here. Ok, maybe they're a little less snotty and juvenile (not really). Travis Barker proves once again why he's one of the most exciting punk drummers out there as he rips his way through each song with a furious series of fills and high velocity beats.

**Irreversable Entanglements - Protect Your Light**

Modern day free-jazz that takes pages out of the McCoy Tyner playbook. Soulful, spiritual jazz that builds off of the past, but sounds futuristic.

**Lana Del Rey - Did You Know That There's A Tunnel Under Ocean Blvd**

Lana Del Rey delivers another epic winner that rivals my favorite albums from her catalog (which for the record are: *Lust For Life*, *Ultraviolence* and *NFR*). Taking the best elements of those and bringing them to new heights here. It's hard to see how she can top this one but I'm sure she will.

**Algiers - Shook**

In my opinion not enough people are aware of Algiers and their brand of political, heavy, post-punk. I urge you to crank up *Shook* as loud as your stereo allows and immerse yourself in their genius.

**Lamp Of Murmuur - Saturian Bloodstorm**

Black metal with a desolate and insane edge. I eagerly await each new release from the mysterious Lamp Of Murmuur and this is probably their best effort yet. An aggressively bleak masterpiece.

**David Murray / ?uestlove / Ray Angry - Plumb**

Another previous Phil's Phriday Pick. This one is a Miles Davis inspired space-funk workout.

**Kassa Overall - Animals**

Rapper / drummer Kassa Overall is an inspiration. *Animals* only disappointment is the fact that it is too short. Quietly pushing hip-hop into the future.

**Juliana Hatfield - Juliana Hatfield Sings ELO**

Juliana Hatfield's series of cover records continues with perhaps the best one yet. She performs most of the instruments herself, which is no small feat when trying to emulate the orchestral sounds of Jeff Lynne. 

**Brandee Younger - Brand New Life**

Harp player Brandee Younger's new record is a tribute to Dorothy Ashby. Jazz, funk and classical music collide into an inspiring supernova of sound. 

**Spencer Zahn / Dave Harrington / Jeremy Gustin - A Visit To Harry's House**

One of the more surprising records of the year! A jazzy reinterpretation of Harry Styles' last album *Harry's House*. Has to be heard to be believed.

**Dosh / Shahzad Ismaily / Tim Young = Dosh, Ismaily, Young**

Essentially a jazz-rock record featuring drummer Marty Dosh (Dosh, Andrew Bird, Fog), bassist Shahzad Ismaily (Marc Ribot’s Ceramic Dog, Secret Chiefs 3, Arooj Aftub), and guitarist Tim Young (Wayne Horvitz’s Zony Mash, David Sylvian, Michael White). Talk about a dream team! They work their way through a 10+ krautrock inspired freakout for starters! The rest is a wild ride of free jazz inspired space rock with a punk attitude. Basically all my favorite things!

**Queens Of The Stone Age - In Times New Roman...**

QOTSA return!! What more needs to be said? They haven't lost a step in their heavy desert rock sound. Mechanical and soulful at the same time.

**Sarmat - Determined To Strike**

Do you like weird death metal, but think Imperial Triumphant is too tame? Sarmat is for you! The record starts in a more traditional death metal fashion but gets progressively weirder and weirder as it goes on until it reaches the climax of the title track before kind of winding its way back down to earth.

**Boygenius - The Record**

The indie rock supergroup finally releases a full length record and it is worth the wait. Believe the hype: this is an incredible record with a lot of depth that showcases each individual member's strengths. 

**Young Fathers - Heavy Heavy**

Young Fathers are a wild avant-garde hip-hop band. *Heavy Heavy* is a soulful, poppy record with hooks for days with experimental edges. Its an addictive sound that frequently made me hit "play" as soon as it finished. 

**Chris Stapleton - Higher**

Another artist who has the hype machine behind him but it is completely justified. Chris Stapleton can seemingly knock out classic song after classic song in his sleep. He can hold my attention over the course of the hour long record and has a great band backing him up that likes to stretch out.

**Die Spitz - Teeth**

Die Spitz is a completely brutal heavy punk / garage rock quartet. No holds barred. Maximum destruction. Killer stuff. Check out their live footage on YouTube to confirm that they do not mess around.

**Jimmy Buffett - Equal Strain On All Parts**

Jimmy Buffett's last album, recorded shortly before he passed away this fall.  It is certainly not too outside-the-box compared to his other recent albums, but it is a fine closing chapter to his lengthly discography.

**Molly Tuttle - City Of Gold**

Highly proficient, virtuoso bluegrass! I hesitate to say "no frills" because the musicianship is basically "all frills" even if it doesn't stray outside of a traditional bluegrass sound. It also helps that it sounds like they are having fun doing it.

**Imperial Crystalline Entombment (I.C.E.) - Ancient Glacial Resurgance**

Almost 20 years since their last album, mysterious (band memebers include: Bleak, Blissered, Mammoth, and Icesickkill) gloomy black metal band Imperial Crystalline Entombment has finally returned. The new record picks up where they left off...absolutely ripping black metal with a vocalist that sounds like he has literally been summoned from hell to shriek at you. Welcome back, I.C.E.

**John Scofield - Uncle John's Band**

John Scofield has been regularly putting out high quality records for a long time. His batting average of is extremely high and *Uncle John's Band* raises it once again. Here Scofield is joined by Vicente Archer (bass) and Bill Stewart(drums). The album is named after the Grateful Dead song that closes the lengthy album (running time: 90 minutes). The record is half Scofield originals and half interesting covers ("Old Man" by Neil Young, "Mr. Tambourine Band" by Bob Dylan are a couple examples).

**L'Rain - I Killed Your Dog**

Multi-instrumentalist L'Rain's new record is another mind-bending experience. I hesitate to call this a pop record, because of its weird electronic flourishes, nods to psychedelic rock and free jazz and bizarre lyrical content....but that's basically what this is. A pop record from Mars.

**(Crosses) - Goodnight, God Bless, I Love U, Delete**

When Crosses (Chino Marino of the Deftones and Shaun Lopez from Far) released an EP called *Permanent Radiant* they promised is was a preview of a full length record that was coming soon. They fulfilled that promise with this excellent album. A gloomy, introspective rock record that takes a lot of time and effort to unpack, but it is worth it. Robert Smith (of The Cure) and El-P (of Run The Jewels) appear as guests.

**Angel Du$t - Brand New Soul**

A wild side project from Baltimore's finest band, Turnstile. Pushing the boundaries of what punk rock should be...this might be the weirdest hardcore record I heard all year.

**Mike And The Moonpies - Live At The Devil's Backbone**

The live record of the year, by far! Honky Tonk living legends Mike And The Moonpies play all their hits in front of a rowdy crowd. You can tell they are feeding off of the energy in the room.

**Trevor Rabin - Rio**

Guitarist Trevor Rabin (of Yes fame) releases his first solo record in 11 years and it is a full-blown prog rock masterpiece. Sounds like a long lost Yes record.

**Wadada Leo Smith - Fire Illuminations**

Trumpet player / composer Wadada Leo Smith's new band is an amazing ensemble: guitarists Nels Cline, Brandon Ross and Lamar Smith; bassists Bill Laswell and Melvin Gibbs; electronic musician Hardedge; percussionist Mauro Refosco; and drummer Pheeroan akLaff. If you like any of the aforementioned players, you will love this excursion into the outer reaches of jazz and rock. Wadada is 81 years old and sounds like he is not slowing down one bit.

**Mitski - The Land Is Inhospitable And So Are We**

Singer-songwriter Mitski has released her most epic record yet. A hauntingly beautiful pop record that is depressing and inspiring all at once. Much of it is recorded with a huge orchestra and those moments really push the album over the top with beauty and emotion.

**Gridlink - Cornonet Juniper**

Gridlink continues to show why they are the best grindcore band on planet earth. A runaway train with a bulldozer attached to the front. Get out of the way, or get smooshed. Drummer Bryan Fajardo is an absolute inspiration! Wild and creative blastbeats for days.

**Nick Shoulders - All Bad**

Country music with a punk attitude. Nick Shoulders is not afraid to break out into yodeling in between his songs that shine a spotlight on life in Appalachia. Hard living has never sounded so good. 

**Teezo Touchdown - How Do You Sleep At Night?**

Online viral sensation Teezo Touchdown put out a surprisingly good record. Modern day experimental hip-hop with just enough twists and turns to keep it interesting. He's also a surprisingly good singer.

**Cryptopsy - As Gomorrah Burns**

Cryptopsy has come a long way from 1994's *Blasphemy Made Flesh* and they haven't always made the best artistic decisions (I'm looking at you: *The Unspoken King*). The only member from the classic lineup is drummer Flo Mounier, but I'll let that slide when the music is so unrelentingly awesome. They're back to their classic sound and I couldn't be more excited.

**Raul Malo - Say Less**

Raul Malo, who rose to fame as the singer of The Mavericks. After their original breakup in 2000 (they have since reformed) he has been releasing quality solo records and appearing with members of Los Lobos in a group called Los Super Seven. As the title implies, on this album there are no vocals! An amazing instrumental record running through a wide variety of classic styles: country, Latin, surf, big band and more. Raul's guitar playing is awesome as well.

**Ghost of Vroom - Ghost Of Vroom 3**

Mike Doughty has been channeling the ghost of Soul Coughing for a while now in his new group Ghost of Vroom. If you like Soul Coughing, you'll like this.

**Jeff Rosenstock - Hellmode**

Jeff Rosenstock can apparently just crank out catchy, fun and rocking punk music effortlessly. This album is yet another example. 

**Speedy Ortiz - Rabbit Rabbit**

Indie rock that has a lot of clatter and noise...in a good way! The songs blaze by and at times the music is very dense, but it is still catchy and demands repeated listens. 

**Cory Hanson - Western Cum**

Cory Hanson is mostly known for his work with Ty Segall and the band Wand, but he also has been churning out solo records simultaneously. This particular record is a heavy alternative rock freakout! Alt rock is not dead, you just gotta know where to look.

**Incantation - Unholy Defication**

Longtime satanic death metal band Incantation release yet another banger. If you like that classic early 90s NYC evil death metal attitude and musicianship, this is the place to go to get it in 2023.

**Ashnikko - Weedkiller**

TikTok sensation Ashnikko finally releases a full length record and it is as bonkers as I hoped it would be. A rage-filled goth punk album that is kind of a concept album describing the end of the world or something. No matter, this thing is filled with dark humor and wild rock. Check out her videos to see her amazing visual aesthetic as well to get the full experience.

**Cindy Wilson - Realms**

Cindy Wilson of the B-52s put out an excellent solo record this year. Nobody seems to have heard it, which is a shame because it is a wild psychedelic ride of new wave and disco inspired rock.

**Kurt Elling - Only The Lonely Woman**

Along with the covers album I mentioned earlier, Kurt Elling and SuperBlue also put out an EP of originals that are also great.

**Lovely Little Girls - Effusive Supreme**

Chicago's wild circus prog band Lovely Little Girls returns with another demented set of songs that has you questioning the sanity of all the people involved in this thing. Highly recommended.

**Putridity - Greedy Gory Gluttony**

Putridity is a brutal death metal band that takes no prisoners. Part of the wild Italian metal scene that needs to get wider exposure...because there is a lot of brilliant stuff out there. Start here then start digging into the rest.

**Jack Irons - Walnut**

Drummer Jack Irons has played with huge bands (Pearl Jam, Red Hot Chili Peppers, The Wallflowers, Eleven), but shies away from the spotlight. He also puts out super cool solo records featuring drums and synths that are jazzy excursions into unknown realms well beyond a grunge / alt rock template.

**Snooper - Super Snooper**

Fun, frantic and noisy. This is the sound of Snooper and the genre they are apparently part of called "Egg Punk". Definitely inspired by early Devo. I discovered these guys through Third Man Records and *Maggot Brain* magazine, so kudos to them for helping me find the good stuff.

**Sunrot - The Unfailing Rope**

Heavy, droning, stoner metal. There's a lot of it out there, and it's hard to find the needle in the haystack of generic garbage...but this is certainly one of the needles. Essential soundtrack for the end of the universe.

**Sweeping Promises - Good Living Is Coming For You**

Boston's Sweeping Promises play a noisy, lo-fi punk rock style. Equal parts catchy and obtuse. The new record expands on what they've done in the past with a fuller, meatier sound.

**Pat Metheny - Dream Box**

Pat Metheny has been recording solo guitar songs while on tour with his other ensembles. This is a collection of them. Certainly not throwaways, there is some amazing music here that should become part of his broader repertoire. There are also some interesting covers peppered throughout like Miles Davis' "Blue In Green". 

**A.M.E.N. - The Book Of Lies - Liber 1**

I, Voidhanger records is a label I keep a close eye on because they always seem to find the most bizarre underground metal on the globe on the regular. A.M.E.N. is probably the best of the bunch I heard this year. A weird mix of grindcore and jazz (lots of flute!) that defies logic and good taste. The lyrics are all taken from the writings of Aleister Crowley...because it wouldn't be evil enough without that.

**Louise Post - Sleepwalker**

Louise Post put out a solo album but it sounds like the songs would be at home on a Veruca Salt record. Alternative rock meets power pop. You know the drill...and the drill is awesome.

**Rancid - Tomorrow Never Comes**

Rancid return after a 6 year hiatus! They have gotten rid of the ska influences and more poppy material and just focused on what they do best: pure, furious punk rock. 16 songs in a half hour. All killer, no filler.

**Foo Fighters - But Here We Are**

Foo Fighters rise out of the darkness of the death of Taylor Hawkins with a triumphant rock record. Sure, it sounds like Foo Fighters and doesn't really break any new ground...but why does it need to? If it ain't broke, don't fix it. 

**Blindfolded And Led Into The Woods - Rejecting Obliteration**

New Zealand's Blindfolded And Led Into The Woods (amazing name!) play a kind of progressive rock / math rock inspired death metal. Lots of twists and turns in their tunes of brutal despair. Highly technical instrumental parts that will make your head spin right off. 

**Sparks - The Girl Is Crying In Her Latte**

Vintage Sparks. 25 records into their career and they just keep releasing quality records. Pop rock with biting satire. If you love the Sparks sound, this is right up your alley. Another example of the if it ain't broke, don't fix it concept.

**Yes - Mirror To The Sky**

Yes has no business putting out great records like this one at this point in their timeline. Steve Howe is the only member left from the classic lineup (although Geoff Downes from the *Drama* era is still here). However, they are continuing to go full-on classic prog: The title track is 14 minutes long and another few tunes push 10 minutes. This is one for the long-time fans nostalgic for the classic era and it doesn't disappoint.

**Marty Stuart And His Fabulous Superlatives - Altitude**

Marty Stuart has long since abandoned his pop country career. He's now making these all-encompassing psychedelic country freakouts. Almost like prog-country. Seriously.

**Cattle Decapitation - Terrasite**

Cattle Decapitation are putting out the most punishing, brutal metal you'll hear all year. All rage, all velocity, all the time. It's almost too much to take in one sitting.

**Ultraphauna - No No No No**

Timba Harris (from Estradasphere) has a new group featuring Toby Driver (from Kayo Dot) called Ultraphauna. Their record *No No No No* is a chamber rock, movie soundtrack, prog metal hybrid that is equal parts baffling and inspiring. There's a lot of Magma and Gentle Giant influence here. 

**Dominic Miller - Vagabond**

Dominic Miller played guitar with Sting during his (in my opinion) best era: *Ten Summoner's Tales* through *Brand New Day*. He's still with Sting today, but let's just focus on the positives. Lately, he has been putting out quiet, contemplative solo records on the ECM record label and his latest one, while not veering far off the beaten path of that sound, is a very good record when you're in the mood for something slow and meditative.

**Terms - All Becomes Indistinct**

Terms is a duo consisting of members of bands I adore: guitarist Christopher Trull (from Yowie and Grand Ulena) and drummer Danny Piechocki (from Ahleuchatistas). Given those names, you pretty much know what this is all about. Manic, thorough-composed rock weirdness that is so precise you cannot believe it is humanly possible to play. But play it they do.

**Squid Pisser - My Tadpole Legion**

Squid Pisser is another duo, but this is more in the carnival, robotic grindcore realm, like The Locust or Deaf Club. The record ping pongs through all the songs in less than 20 minutes. Wacky and wild.

**Mudhoney - Plastic Eternity**

Mudhoney continues their winning ways. Noisy alt-rock that sounds like we're still in the early 90s.

**Ruston Kelly - The Weakness**

Ruston Kelly needs more recognition. He has been putting out country records with a dark outlook for years now and when I mention him to people they don't know who he is! The Weakness might be his best record yet, so it is as good a time as any to jump on board. 

**Blondshell - Blondshell**

Sabrina Mae Teitelbaum (Blondshell) recorded a throwback alternative rock record that hits the sweet spot. If you like Hole, PJ Harvey, Bikini Kill this is the record you want to hear ASAP.

**William Tyler And The Impossible Truth - Secret Stratosphere**

Cosmic country guitarist William Tyler teams up with steel guitar player Luke Schneider in a new group called The Impossible Truth. This live record shows what they are capable of. Long, exploratory instrumental journeys of sound that are a mix of jazz and country. A pretty unique vision. I hope they record a studio album.

**Rob Mazurek - Lightning Dreamers**

Trumpeter Rob Mazurek started the Exploding Star Orchestra as a homage to Chicago's avant-garde music scene such as the Art Ensemble of Chicago. Their new record is an immersive experience of spacey, free jazz. Awesome stuff.

**Arooj Aftab - Love In Exile**

Pakistan-born composer Arooj Aftab plays a meditative set of spiritual jazz with an all-star trio.  Quietly inspirational.

**The Fallen Prophets - Perpetual Damnation**

South Africa has a metal scene too! The Fallen Prophets are a deathcore band that has recently reached my radar and I'm obsessed. Energetic, melodic death metal...and impressive display of technical prowess without being too complex for its own good. They deserve to be bigger.

**Ralph Towner - At First Light**

Ralph Towner. Still recording for ECM. Still sounding like an ECM artist. This is not a bad thing.

**T-Pain - On Top Of The Covers**

I thought Lil Yachty would be the only big name rap artist taking wild left turn this year, but I was wrong. T-Pain put out an album of rock covers and it's as bonkers as it is fun! Black Sabbath, Frank Sinatra, Sam Cooke, Journey, David Allan Coe! Talk about running the gamut. It also sounds great too, you can tell he is serious about making it amazing.

**Mendoza Hoff Revels - Echolocation**

Mendoza Hoff Revels, is a new band fronted by Ava Mendoza with Devin Hoff, James Brandon Lewis and Ches Smith. Modern day progressive rock with lots of space for improvisation. Some heavy King Crimson vibes here. 

**Julian Lage - The Layers**

Guitar virtuoso Julian Lage put out *The Layers* as a companion piece to last year's *View With A Room*. Recorded during the same sessions it is another great collection of tunes with a fantastic cast of characters: Jorge Roeder (bass) and Dave King (drums). The always amazing Bill Frisell also drops by and is as incredible as ever. 

**Sam Grendel - Cookup**

Saxophonist Sam Grendel pays tribute to 90s / 00s R&B in his ambient style. Aaliyah, Erykah Badu, Boyz II Men and Beyonce all get reinterpreted in interesting ways here. 

**Overkill - Scorched**

38 years after *Feel The Fire* and Overkill is still going strong! It's still raging trash metal and it's still awesome. 

**Iggy Pop - Every Loser**

Iggy Pop puts out a new record, that sounds like an Iggy Pop record. This list of musicians that back him up here are very impressive  (including one of the last recordings of drummer Taylor Hawkins). If you like the idea of Iggy Pop backed by 90s alternative rock titans, this is your one stop shop.

**Dodheimsgard - Black Medium Current**

Black metal weirdos Dodheimsgard are back for the first time in 8 years. They actually turn down the weird and turn up the black metal here. Pedal to the metal in the best possible way. 

**Mohini Dey - Mohini Dey**

Ok, this is a bonkers jazz fusion record, mostly because Mohini Dey's bass playing is mind blowingly complicated and wild. It helps that she gets assistance from the likes of Simon Phillips, Marco Minneman, Steve Vai, Jordan Rudess, Ron "Bumblefoot" Tha and many more. A stunning accomplishment. I'm looking forward to hearing where Mohini's career goes next. 

**Kryptoxik Mortality - Interdimensional Calamitous Exterpation**

I don't usually listen to slam metal, but this is really good! 

**Andre 3000 - New Blue Sun**

Andre 3000 told Lil Yachty and T-Pain "Hold my beer" and put out the weirdest record of them all. A spiritual jazz epic with tons and tons of flute. On paper, it sounds like a disaster but in the real world it is astonishing. When it first came out, I couldn't stop listening to it over and over.

**Ambrose Akinmusire - Owl Song**

Trumpeter Ambrose Akinmusire signs with Nonesuch Records and immediately collaborates with Bill Frisell. It's as astounding as you'd imagine it is. It came out just a week ago, so it sneaks into my best of list at the wire.

