---
title: Banyan - Anytime At All
date: 2022-06-03 14:57:01
tags:
- banyan
- janes addiction
- stephen perkins
- mike watt
- nels cline
- buckethead
- jazz fusion
- 90s rock
- alternative rock
---

![](AnytimeAtAll.jpg)

Stephen Perkins is best known as the drummer of the 90s alternative rock bands Jane's Addiction and Porno For Pyros. However, his skills go beyond plain ol' rock n' roll.

After Porno For Pyros dissolved following their 1996 tour, Stephen Perkins started a more jazz and improvisation-based band called Banyan.

He recruited bassist extraordinaire Mike Watt to join (Watt also appeared on Porno For Pyro's *Good God's Urge*) along with guitarist extraordinaire Nels Cline (who is now with Wilco) and Money Mark (of Beastie Boys / Grand Royal fame). They put out a self-titled album with this membership in 1997, which is decent.

In 1999, Stephen Perkins expanded the Banyan concept, including a wider variety of styles (funk, metal, world music, hip-hop along with the jazz fusion) with a much larger ensemble and released *Anytime At All*, which is today's pick.

The musicians who appear on the album is lengthy and amazing. Along with Mike Watt and Nels Cline the following bigger names are also on board along with a smattering of less famous individuals:
- Rob Wasserman (bass)
- John Frusciante (guitar)
- Buckethead (guitar)
- Flea (bass)
- Martyn LeNoble (bass)

They still play shows and tour sporadically. Here is some bonkers live footage of Banyan playing in 2005, keeping the spirit alive:

<iframe width="560" height="315" src="https://www.youtube.com/embed/IAE0_Fbk1YM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

However, Banyan has not put out another studio album since 1999, so *Anytime At All* remains the best place to hear their music outside of a live context.

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/album/5KSgzaDQy5xdVNIWpQ2LgK?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

<iframe id='AmazonMusicEmbedB000TS2V32' src='https://music.amazon.com/embed/B000TS2V32/?id=Vfj0eOvhni&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *; clipboard-write" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/ve/album/anytime-at-all/723512619?l=en"></iframe>

<iframe src="https://embed.tidal.com/albums/1550440" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>