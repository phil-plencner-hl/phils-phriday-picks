---
title: Foghat - Foghat Live
date: 2021-05-14 15:32:17
tags:
- classic rock
- 70s rock
- blues rock
---

It's difficult to believe now, but the band Foghat was huge in the mid 70s. They had a particularly muscular take on blues / boogie rock. Early on the had a hit with a Willie Dixon cover ("I Just Want To Make Love To You") but eventually started writing their own popular material, which reached a peak in 1975 with *Fool For The City*. 

At this point they were touring non-stop and were a very popular attraction. So much so that when they released *Foghat Live* in 1977 it became (and I believe still is) their best selling album. 2 Million copies sold!

The great thing about *Foghat Live* is, unlike other live albums of the era, it is a short all-killer, no-filler album. No double album, no deep album cuts. 6 songs. 40 minutes...ending with a powerful version of "Slow Ride". BOOM! 

Also, it captures only a single concert, so you know that particular night they were on fire. May 10th (the date of my birth!), 1977 (not the year of my birth!). 

In fact, through all these years it has never been re-released in a "deluxe edition" with extra songs or outtakes. Why mess with perfection?

Fun fact about this era of Foghat: Nick Jameson played percussion with them live during this era and also produced *Foghat Live*. He is now most famous for portraying Russian President Yuri Suvarov in the popular tv show *24*.

<iframe src="https://open.spotify.com/embed/album/3WYfkaZSSomQpGUTTBdy35" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
