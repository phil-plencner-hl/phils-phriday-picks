---
title: My Life With The Thrill Kill Kult - s/t
date: 2023-10-27 21:03:35
tags:
- my life with the thrill kill kult
- ministry
- wax trax records
- wax trax
- industrial music
- disco 
- 80s rock
---

![](MyLifeWithTheThrillKillKult.jpg)

I have been considering what scary music to pick for the Halloween weekend. I decided to go in a little more unusual direction. 

Instead of picking a typical horror movie soundtrack or a heavy metal record, I chose to go with perhaps my favorite group that recorded as part of the classic Wax Trax Records roster: My Life With The Thrill Kill Kult!

My Life With The Thrill Kill Kult was formed by Groovie Mann (Frankie Nardiello) and Buzz McCoy (Marston Daley) in 1987. They have been the masterminds of the group until present day. 

Before forming the group, they toured with Ministry when the band was supporting the album *Twitch* (before they added the heavy guitars). 

It's hard to find footage of Ministry in that era, but here is some incredibly low-quality footage of them performing in Houston in 1987. It's hard to tell in the fog and darkness but I'll assume one or both Buzz McCoy and Groovy Man are involved here. Nevertheless, it is killer footage of early Ministry:

<iframe width="560" height="315" src="https://www.youtube.com/embed/GP6YP6ak1Yc?si=NACbUfr8hK9EHePe" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

My Life With The Thrill Kill Kult was originally supposed to be a film and they were just recording the soundtrack for it. The film was never completed and it instead became a band. For the uninitiated, in the early years they played a style of very dark industrial music with lyrics and imagery focused on horror themes and satanism. Which makes them a perfect selection for Halloween! 

Later on they added more disco and surf influences and had greater success (specifically with *Sexplosion!* in 1991 and the single "Sex on Wheels") but I always preferred their earlier material.

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

A lot of those songs were originally released on Wax Trax Records on a series of singles: *My Life With The Thrill Kill Kult* in 1987, *Some Have To Dance, Some Have To Kill* in 1988 and *Kooler Than Jesus* in 1989. Luckily, Rykodisc compiled all those singles and more into one handy self-titled compilation album in 2004. That is today's pick!

Along with the aforementioned early singles, it also includes their collaboration with no wave legend Lydia Lunch "A Girl Doesn't Get Killed By A Make-Believe Lover...'Cuz It's Hot" which is pretty incredible. There is also a remix of "Leathersex" from *Sexplosion!* and some other more modern material but the real meat of the compilation are the earliest stuff.

They made at least one music video during that time, "Kooler Than Jesus" which really encapsulates their sound and image pretty well. If you like this, you'll likely dig the entire compilation:

<iframe width="560" height="315" src="https://www.youtube.com/embed/WCkCXs9DaWU?si=0c7qj3lqZQE6QsZr" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Another song on the compilation "Nervous Xians" was later reworked into the song "After The Flesh" that was included in the soundtrack to *The Crow* in 1994:

<iframe width="560" height="315" src="https://www.youtube.com/embed/aF4hyVArBDc?si=op-IXCENaHQp-kYD" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Surprisingly, there's not a lot of early footage of My Life With The Thrill Kill Kult on the internet. I did uncover this great footage of them performing "A Daisy Chain 4 Satan" in 1990...which is probably my favorite song from the full-length record *Confessions Of A Knife*:

<iframe width="560" height="315" src="https://www.youtube.com/embed/UTc_PoW3xY4?si=bt1okA17sfcX9i-w" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

My Life With The Thrill Kill Kult is actually touring this year with a classics-heavy set. Here they are performing the same song, so you can see the difference. Still good, but not as dangerous I suppose:

<iframe width="560" height="315" src="https://www.youtube.com/embed/jY45IvgWVJ4?si=Z9af7MtF6Pv19vi9" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

At any rate, I'll be spending the Halloween weekend cranking up this compilation of vintage industrial music and scaring the neighbors. Perhaps you will do the same.

{% iframe 'https://open.spotify.com/embed/album/7mBds5LRXeaCPZUTjfZveQ?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B079LFTWHQ/?id=4Yui66mbSD&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/my-life-with-the-thrill-kill-kult/1344674924' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/84291765' '100%' '96' %}
