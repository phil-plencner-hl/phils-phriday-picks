---
title: Tanya Donelly And The Parkington Sisters  - s/t
author: Phil Plencner
date: 2020-08-17 15:00:49
tags:
- tanya donelly
- the parkington sisters
- belly
- throwing muses
- the breeders
- 90s alternative rock
- paul mccartney
---

PSA The new Tanya Donelly album is excellent. Includes a great cover of the Paul McCartney / Wings song "Let Me Roll It"

<iframe src="https://open.spotify.com/embed/album/5H4d1x9t38IDnA0P76Qyy0" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>