---
title: Lil Yachty - Let's Start Here
date: 2023-04-07 21:45:45
tags:
- lil yachty
- tame impala
- trap music
- rap music
- progressive rock
- alternative rock
---

![](LetsStartHere.jpg)

Since 2017 rapper Lil Yachty has been kind of a fun novelty. He put out albums and mixtapes in a trap music style with a heavy dose of humor. Songs like "Broccoli" were amusing but I honestly didn't follow his career too closely.

That is, until he teamed up with modern psychedelic rock band Tame Impala for a remix of their song "Breath Deeper" from their excellent album *The Slow Rush* back in 2022. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/TmeKpd8B15U" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Soon afterwards rumors were swirling that Lil Yachty was working on an album that was heavily influenced by progressive rock.

That album is *Let's Start Here*, which was released earlier this year. It was first teased on January 24th, with an odd video called "Department of Mental Tranquility":

<iframe width="560" height="315" src="https://www.youtube.com/embed/clq1cg_Tbnw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

A few days later the first single, "Say Something", was released. I was definitely intrigued! This was no longer the simplistic, goofy trap music Lil Yachty was known for.

<iframe width="560" height="315" src="https://www.youtube.com/embed/igFtut_1drQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

The album showed up on streaming services pretty immediately after the video and I quickly dived in. To say it is a complete reinvention is an understatement. A full hour of full-blown progressive space rock mixed with rap and autotuned vocals. The AI generated album artwork of crazed looking people in suits aptly illustrates how wild the music is.

The album kicks off with "Black Seminole" which sounds like prime-era Pink Floyd! Lengthy instrumental parts and a cool guitar solo set the tone for the rest of the album. 

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

Other highlights include the funky "Running Out Of Time", the thrashy hard rock of "I've Officially Lost Vision" and "Should I B?" which has a post-punk kind of vibe similar to what the band Algiers is doing. 

Soon after the album's release Lil Yachty assembled an all-female band to bring his vision to the stage. He premiered it at the *Rolling Loud* festival, with the first 20 minutes or so of the set focused entirely on songs from *Let's Start Here*.

<iframe width="560" height="315" src="https://www.youtube.com/embed/IkiBw2v4TK8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Most recently the same band performed "Black Seminole" on *Saturday Night Live* which is unbelievably awesome.

<iframe width="560" height="315" src="https://www.youtube.com/embed/XYdE4HAsXpI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

*Let's Start Here* is one of my favorite records of the year so far. I keep finding new things with every spin. Lil Yachty is taking the show on the road over the summer including Lollapalooza in Chicago and I am considering going to the festival specifically to see his set.

{% iframe 'https://open.spotify.com/embed/album/6Per97deaWqrJlKQNX8RGK?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B0BSVMFB4Y/?id=yEOa0fFogB&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/lets-start-here/1667414099' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/27361990' '100%' '96' %}
