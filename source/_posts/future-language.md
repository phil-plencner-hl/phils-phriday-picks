---
title: Von LMO - Future Language
author: Phil Plencner
date: 2020-04-06 15:05:49
tags:
- 70s rock
- von lmo
- red transistor
- avant duel
- no wave
- weird rock
- space rock
---
Today's pick is the mysterious Von LMO. He was originally part of the early NYC No Wave scene as a member of Red Transistor. 

Eventually he built up a persona that he was from outer-space and formed a space rock / hard rock band that is truly out of this world! The album was aptly called *Future Language*. 

Spotify has a re-release of the album that also includes the not-quite-as-strong 2nd album *Tranceformer* as well as a few tracks from *Red Transistor*. 

If you wanna dive even deeper Von LMO formed a new band about 10 years ago called Avant Duel that is a lot more dance / techno oriented but still pretty wild and psychedelic.

[Further reading](https://daily.bandcamp.com/lists/von-lmo-discography) 

<iframe src="https://open.spotify.com/embed/album/32RCq2mlmwuyyoPUlWEGeW" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>