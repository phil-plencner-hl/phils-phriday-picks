---
title: Photek - Modus Operandi
date: 2022-11-10 09:52:13
tags:
- photek
- squarepusher
- sound direct
- drum n bass
- house music
- jazz fusion
---

![](ModusOperandi.jpg)

Since Veterans Day is this Friday, today's pick comes early!

I recently stumbled across an excellent Drum N' Bass documentary that aired on Dutch TV in 1996 on YouTube called *Lola da Musica*:

<iframe width="560" height="315" src="https://www.youtube.com/embed/Z13ckF1ytYU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

It features 3 artists that were big in the United Kingdom Drum N' Bass / Jungle scene at that time: Squarepusher, Photek and Sound Direct. It shows each of these artists in their home studios. The Squarepusher segment also includes some cool live footage of Tom Jenkinson playing bass at a music festival.

I was really drawn to the Photek footage. It shows Rupert Parkes racing around the streets in his hometown (in his Lamborghini!!) and working in what appears to be Cubase constructing his songs. It's a fascinating look at his methods and process.

This brought me back to Photek's excellent debut album from that era: 1997's *Modus Operandi*, which is today's pick!

*Modus Operandi* is a very dark and murky album. The skittering beats are always inventive and keep the listener off kilter. It covers a lot of ground. There are the orchestral soundscapes of "Minotaur" and "Trans 7".  "Aleph 1" brings the spaced-out funk.  A more straight-ahead techno / house style is included with "124". Of course, there is a huge amount of jazz fusion influence throughout the whole album, but especially so on "Modus Operandi" and "KJZ".

Photek rarely DJed in live settings. Rupert Parkes eventually started focusing less on Drum N' Bass and more on House music then transitioned into scoring movies and TV shows. Here is some killer footage of one of his DJ sets that has surfaced:

<iframe width="560" height="315" src="https://www.youtube.com/embed/iV3oyrEkvWM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

With the sun setting earlier and the weather getting chilly, *Modus Operandi* is a perfect album to listen to this time of year.

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/album/20SMwxVrMXrevEy2qexGmm?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

<iframe id='AmazonMusicEmbedB0010YHNXW' src='https://music.amazon.com/embed/B0010YHNXW/?id=t6XSD6BlQR&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *; clipboard-write" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/modus-operandi/724526994"></iframe>

<iframe src="https://embed.tidal.com/albums/1477824" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>