---
title: Tool - Fear Inoculum
author: Phil Plencner
date: 2019-08-30 15:30:20
tags:
- tool
- 90s rock
- alternative rock
- progressive rock
---
It's August 30th 2019. 

The first new Tool album in 13 years is out today. 

That is all.

<iframe src="https://open.spotify.com/embed/album/7acEciVtnuTzmwKptkjth5" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>