---
title: Cristina - Ze Debut Redux Album
author: Phil Plencner
date: 2020-04-02 15:10:45
tags:
- cristina
- no wave
- disco
- ze records
---
The news lit up yesterday about the passing of Fountain of Wayne's Adam Schlesinger. A terrible loss for the rock world. I had the pleasure of hanging out with him (and the rest of FoW) right after Welcome Interstate Manager's came out. They were all super-nice guys. A lot of talent and fun. 

However, lost in the shuffle of that news was the passing of seminal No-Wave / Disco singer Cristina...who also passed away from complications from COVID-19. She was married to the founder  of Ze records, which was a pretty huge piece of the puzzle of the early NYC No Wave / Disco / Funk scene in the early 80s...Was (Not Was), Kid Creole & The Coconuts, Material etc etc. 

ANYWAYS, Christina's debut album for that label is an excellent timepiece of 80s NYC...Spotify has the reissue with bonus tracks from the same era including her swanky cover of Drive My Car. 

Blame It On Disco!

<iframe src="https://open.spotify.com/embed/album/22clmFFl9UH4WwMBuILjQ2" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>