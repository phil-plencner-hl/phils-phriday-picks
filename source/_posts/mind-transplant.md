---
title: Alphonse Mouzon - Mind Transplant
date: 2024-08-30 15:53:19
tags:
- alphonse mouzon
- tommy bolin
- billy cobham
- larry coryell
- weather report
- herbie hancock
- mcoy tyner
- jazz fusion
- jazz rock
- 70s rock
---

![](MindTransplant.jpg)

People who know me, along with long-time Phil's Phriday Pick readers, know that I am a jazz fusion fanatic. Especially heavy jazz fusion from the early to mid 1970s. The pinnacle of this style of music is Billy Cobham's 1973 masterpiece *Spectrum*. I'm surprised I haven't picked that album yet...but maybe soon. 

The (maybe not so) secret weapon on *Spectrum* was guitarist Tommy Bolin. He died too soon at the age of 25 in 1976, and at one point was a member of Deep Purple (appearing on the criminally underrated 1975 record *Come Taste the Band*) but before that he was playing really heavy jazz rock, such as *Spectrum*. 

For those unfamiliar I will showcase the scorching album opener from *Spectrum*, "Quadrant 4":

<iframe width="560" height="315" src="https://www.youtube.com/embed/unxshBHfVsY?si=7Z3Zbz7Znr8buFa8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Blasting out of the gate and taking no prisoners! That's what I'm talking about!

As much renown that *Spectrum* has there is another record that comes in a close second to that legendary album. It also features Tommy Bolin on guitar and is a solo record from another great jazz fusion drummer. The record I'm referring to is Alphonse Mouzon's *Mind Transplant*, which is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

Before *Mind Transplant*, Alphonse Mouzon already had an incredible resume. He was in the very first version of the band Weather Report, appearing on their self-titled debut in 1971. Here he is playing "Waterfall" with Weather Report during that short time he was in the group:

<iframe width="560" height="315" src="https://www.youtube.com/embed/-7hIPu0Cusg?si=BuqvB6bBcAiY5-gL" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Afterwards he joined pianist McCoy Tyner's band. McCoy Tyner is, of course, part of the legendary John Coltrane band that performed on *A Love Supreme*. His solo albums in the early 70s expand on that spiritual jazz sound and takes it to new heights. Alphonse Mouzon played on one of my favorite Tyner records *Sahara*. The whole thing is straight up fire, especially the 20+ minute title track. Speaking of fire, Mouzon also played with Tyner at the Montreux Jazz Festival in 1973 along with Azar Lawrence and this performance is almost too hot to touch!

<iframe width="560" height="315" src="https://www.youtube.com/embed/9RdHXui_SxA?si=4eT1QoWRLwsgxoZH" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

After that, Alponze Mouzon joined Larry Coryell's Eleventh House band. They were trying to give Billy Cobham, John McLaughlin and the rest of the Mahavishnu Orchestra a run for their money around that time (and they came close often). For example, here they are playing "The Other Side" in 1975:

<iframe width="560" height="315" src="https://www.youtube.com/embed/pnFU5RaKsBg?si=1HQC8_ME0i-Z1Mba" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Anyways, let's focus back to *Mind Transplant*. Along with Tommy Bolin the record also features Jerry Peters on piano and organ, Henry Davis on bass and additional guitar by a pre-smooth jazz Lee Ritenour! A tight, bombastic little quintet!

*Mind Transplant* starts off like a rocket launch with the title track. A funky, fusion burner that has a lot of great riffing from Bolin and an explosive drum solo near the end:

<iframe width="560" height="315" src="https://www.youtube.com/embed/FpA7WJrIk0A?si=2F6Y37Z2klgeD_2e" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Another highlight is "Carbon Dioxide" with its cool unison parts between the guitar and bass along with some very Billy Cobham-esque machine gun drumming throughout:

<iframe width="560" height="315" src="https://www.youtube.com/embed/mkeuPjLcJnY?si=4DFmzAMI8Hu1rEWu" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Perhaps my favorite song on the album is "Ascorbic Acid" which starts out with an awesome drum solo and only gets more intense from there:

<iframe width="560" height="315" src="https://www.youtube.com/embed/0QVbLgyWXY4?si=c3-dYucJroSUYNMX" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

By the late 70s and early 80s Alphonse Mouzon started playing more commercial R&B instead of wild jazz fusion. However, in the early 2010's he revisited that style again...including playing songs from *Mind Transplant*. One of his bands at the time included former Testament guitarist Alex Skolnick (whom I featured a few weeks ago on my pick of Michael Manring's *Thonk*).Here they are playing "Nitroglycerin", the closing song on *Mind Transplant*:

<iframe width="560" height="315" src="https://www.youtube.com/embed/PU4PgRJylsk?si=S932I_XI3aZPyMWu" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

He really didn't lose his speed or precision! As a further example, here he is playing the drum solo from "Mind Transplant" around the same era but with a different group:

<iframe width="560" height="315" src="https://www.youtube.com/embed/o90VhhudYRo?si=hjfAdUFClVo3DYaB" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Unfortunately, Alphonse Mouzon passed away in 2016 from a rare form of cancer. I never had a chance to see him perform, but I'm glad there are plenty of recorded examples of his playing to explore, especially *Mind Transplant* which still sounds powerful and heavy nearly 50 years after it was first released!

{% iframe 'https://open.spotify.com/embed/album/3vqm5iYWGQyCo1bhEQuk4t?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B004G7R2A4/?id=GwvVmPdvaq&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/au/album/mind-transplant/724032945' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/1464351' '100%' '96' %}
