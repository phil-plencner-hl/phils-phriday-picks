---
title: Frank Black - Teenager of the Year
date: 2023-08-25 14:45:22
tags:
- frank black
- pixies
- kim deal
- breeders
- eric drew feldman
- moris tepper
- lyle workman
- captain beefheart
- 90s rock
- alternative rock
- punk rock
---

![](TeenagerOfTheYear.jpg)

Frank Black was one of the founding members of alternative rock legends Pixies (although he was known as Black Francis at that time). Along with Kim Deal (bass), David Lovering (drums) and Joey Santiago (guitar) Frank Black and Pixies redefined what alternative rock is, and their influence is wide-ranging.

However, all good things come to an end. Pixies originally broke up in 1992 after touring behind the album *Trompe le Monde*. 

*Trompe le Monde* is also notable for being the first recorded collaboration between Frank Black and bass / keyboard player Eric Drew Feldman. Eric Drew Feldman first rose to fame as part of Captain Beefheart's Magic Band in the late 70s / early 80s (appearing on *Shiny Beast*, *Doc at the Radar Station* and *Ice Cream for Crow*). They met on a previous Pixies tour when Feldman was playing with opening act Pere Ubu and hit it off...so Feldman provided keyboards on *Trompe le Monde*.

You can see him playing with the band in the "Alec Eiffel" video:

<iframe width="560" height="315" src="https://www.youtube.com/embed/rsMLjaloyvI?si=Tj4mm8KcDXrxTf4X" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

After the band's breakup, Frank Black formed a new band with Eric Drew Feldman and drummer Nick Vincent. They originally intended to put out a covers album, but the concept was quickly abandoned when Frank Black started writing a ton of new material specifically for the band and album. Only one cover, "Hang on To Your Ego" originally by The Beach Boys, was eventually released on the album *Frank Black*. They even made a video for it, directed by John Flansburgh of They Might Be Giants fame:

<iframe width="560" height="315" src="https://www.youtube.com/embed/tJ2qi9KfEZ8?si=NCBNs2KvSfqyzjZd" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

The album mostly stayed within the wheelhouse of latter-day Pixies albums. For the next album, Frank Black expanded the scope (as well as the size of the band) running through a wide variety of styles and moods. That album is the 22 song double-record *Teenager of the Year* which is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

For *Teenager of the Year* the band also included guitarist Lyle Workman (who later composed and played on the amazing *Superbad* soundtrack and was in Beck’s *Midnight Vultures* band), Moris Tepper (Eric Drew Feldman's band mate from his time with Captain Beefheart) and Pixies guitarist Joey Santiago.

To call *Teenager of the Year* a masterpiece is really an understatement. To me, it ranks right up there with Pixies' *Doolittle* album. It really is that good.

The album starts off with a bang. The slow intro of "Whatever Happened to Pong?" quickly whiplashes into a fast-raging whirlwind. This then flows right into the next two songs, "Thalassocracy" and "(I Want to Live on an Abstract Plain)" in quick succession. 

They would play these songs all together live with no break. Here is an example of that on MTV in 1994:

<iframe width="560" height="315" src="https://www.youtube.com/embed/QaViidD5xqo?si=TEoLLi9Yh18LXPGf" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

There are almost too many highlights to list on the album. Some of my other favorites are the mid-tempo tunes "Vanishing Spies" and "Speedy Marie". Another is the bonkers "Two Reelers" that switches tempos and time signatures in a way that would make Captain Beefheart proud. Also notable is the catchy "White Noise Maker" in which Frank Black just yearns for a little peace and quiet. 

There are also funny little jokes spread throughout the record, like the guitar solo in the weird reggae song "Fiddle Riddle" and many countless circus-like keyboard flourishes.

There is also the incredible multi-part mini suite of "Freedom Rock". The live band would tear through this complex tune like a well-oiled machine. Check them out playing it on Conan O'Brien's show in 1995:

<iframe width="560" height="315" src="https://www.youtube.com/embed/9KMfX5p7gQk?si=1EAhfwGoD46q3qAx" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

There was only one music video made for *Teenager of the Year*. The homage to The Beach Boys called "Headache":

<iframe width="560" height="315" src="https://www.youtube.com/embed/ytQhL0-3FtM?si=gkf0047vjrIIy53O" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

For reasons unknown to me, the album was not very well received at the time of release. It has grown in stature nowadays, but at the time it was not met with the expected commercial success. Frank Black ended up parting ways with Eric Drew Feldman and starting a more stripped-down rock band called The Catholics with Lyle Workman being the only one that stayed on board.

Frank Black never reached these heights again (not even with the recently reformed Pixies) but he came close when Eric Drew Feldman rejoined his bands for the run of albums between 2001-2003 (*Dog in the Sand*, *Black Letter Days*, *Devil's Workshop* and *Show Me Your Tears*). All those records had brilliant moments but none of them are the front-to-back banger that is *Teenager of the Year*. 

{% iframe 'https://open.spotify.com/embed/album/2VVAqbG2AFeXNiyDjjARaM?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B007N0943E/?id=vTeR1BfnkP&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/teenager-of-the-year/513245412' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/58080242' '100%' '96' %}
