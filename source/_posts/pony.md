---
title: Orville Peck - Pony
author: Phil Plencner
date: 2019-05-17 16:00:19
tags:
- orville peck
- country music
- country rock
---

Today's pick is the awesome new album from mysterious cowboy Orville Peck! 

Do you want to hear an amazing blend of old school country and shoegaze? Look no further...

<iframe src="https://open.spotify.com/embed/album/7Jn1h8E5aT96pdyrPxrLWi" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
