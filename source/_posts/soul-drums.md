---
title: Bernard Purdie - Soul Drums
date: 2022-11-04 15:29:05
tags:
- bernard purdie
- drums
- funk rock
- james brown
- steely dan
- aretha franklin
- soul music
---

![](SoulDrums.jpg)

Bernard Purdie is one of the most funky drummers you will ever hear. He has worked with everyone from James Brown to Steely Dan to Aretha Franklin (He literally played on hundreds of albums throughout his career).

His signature beat is the "Purdie Shuffle" which is a half-time shuffle beat with very distinct ghost notes running throughout. It is extremely difficult to master...especially if you want the groove to sound smooth.

Here he is explaining the Purdie Shuffle for the New York Times in 2009:

<iframe width="560" height="315" src="https://www.youtube.com/embed/aRIWH4HCoz8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

His most famous recordings with Steely Dan were "Home At Last" (on *Aja*) and "Babylon Sisters" (on *Gaucho*). Here are Walter Becker and Donald Fagen talking about his playing, along with some classic footage:

<iframe width="560" height="315" src="https://www.youtube.com/embed/_ldtieSEyQM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

He also played extensively in Aretha Franklin's band. Here is a full concert of them at the height of their powers:

<iframe width="560" height="315" src="https://www.youtube.com/embed/7nQpRhGdPZE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Even with all this session work, Bernard Purdie still found time to put out his own solo albums! The first one is called *Soul Drums* and it is today's pick!

The band on the album includes pianist Richard Tee, Eric Gale and Billy Butler on guitars, bassist Bob Bushnell, and Seldon Powell playing tenor saxophone.

Each song is super funky, drenched out in a huge amount of echo and reverb for maximum impact. The drums are some of the best funk drumming ever committed to wax and the band is playing out of their mind as well.

The album was originally released in 1967, but was re-released in 2009 with 8 additional bonus tracks. All of the bonus tracks are essential!

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/album/5McRW15qqelkCpwrx1ccGd?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

<iframe id='AmazonMusicEmbedB00397DF4Y' src='https://music.amazon.com/embed/B00397DF4Y/?id=8lIP0CwLs0&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *; clipboard-write" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/soul-drums-expanded-edition/356908468"></iframe>

<iframe src="https://embed.tidal.com/albums/3422441" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>