---
title: David Sylvian - Gone To Earth
date: 2024-03-01 21:22:17
tags:
- david sylvian
- robert fripp
- mick karn
- japan
- king crimson
- new wave
- art pop
- 80s music
- progressive rock
---

![](GoneToEarth.jpg)

Musician David Sylvian just celebrated his 66th birthday last week, so it seems like a fine time to revisit his vast back catalog. He first rose to fame as the front person of the new wave / art pop group Japan. Their sound was a very peculiar, moody and subdued form of new wave. David Sylvian's singing and guitar playing along with Mick Karn's slippery fretless bass helped define their sound. 

Japan only lasted about 5 years, from 1979 to 1982 before breaking up due to tensions between all the band members. This is partially a shame because they were sounding phenomenal in 1982 but it was also a blessing in disguise as David Sylvian's solo career took his music to even greater heights.  Japan's brilliance can be seen and heard in this clip from the television show *Old Grey Whistle Test* shortly before the band dissolved in 1982:

<iframe width="560" height="315" src="https://www.youtube.com/embed/uaSak_o9IEQ?si=bVTIA-GhRSZAzkxH" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

By 1984 David Sylvian had hooked up with Holger Czukay (from the German group Can) and trumpet players Mark Isham and John Hassell. They put out an record called *Brilliant Trees* which was pretty great. It took the art pop sound of Japan and took it to much jazzier and spacey places. 

Here is some incredible footage of the recording sessions that resulted in *Brilliant Trees*:

<iframe width="560" height="315" src="https://www.youtube.com/embed/v-X36Xi8i58?si=8RYDoRI4B3JWW1mu" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

After those sessions, David Sylvian connected with Robert Fripp. They worked together with Holger Czukay on the soundtrack for the brilliant short film *Steel Cathedrals*:

<iframe width="560" height="315" src="https://www.youtube.com/embed/yUTzW51a2_w?si=DWvS9_TmVR455Yhu" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

This was the beginning of a long partnership with David Sylvian and Robert Fripp (along with other members of King Crimson). The first of these collaborations was *Gone To Earth* in 1986 which is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

*Gone To Earth* was originally released as a double-vinyl set. The first record was more pop oriented and the second record was entirely instrumental and mostly performed by David Sylvian solo. The record label didn't want to pay for all the instrumentals, so David Sylvian paid for the recording of those songs himself (he must have been pretty passionate about those songs).

Other musicians featured throughout include guitarist Bill Neilson (Be-Bop Deluxe), saxophonist Mel Collins (King Crimson) and flugelhorn player Kenny Wheeler (who plays on a ton of classics released on the ECM Records label). 

*Gone To Earth* is a beautiful record that merges the best elements of pop rock, jazz and progressive rock into a wholly original sound. While Fripp's presence is definitely large (he even plays some of his famed "Frippertronics" on some songs) he's not the star of the show and is more of an ensemble player.

To give you an idea of the overall sound, here is David Sylvian playing "Taking The Veil", the opening song from *Gone To Earth*. The band includes guitarist David Torn replacing Fripp and a reunion with Mick Karn on bass!

<iframe width="560" height="315" src="https://www.youtube.com/embed/yxwalBjsYKA?si=VdmU3G1ShJAUclty" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

As I mentioned earlier, this was not the last time David Sylvian collaborated with Robert Fripp. They put out other records such as *The First Day* in 1993 and even toured together in the 90s. In fact, the band at that time included drummer Pat Mastelotto and Chapman Stick player Trey Gunn who both became part of the "Double Trio" lineup of King Crimson soon after. 

That Pre-King Crimson band did play songs from *Gone To Earth* on the tour including "Wave" and the title track.

<iframe width="560" height="315" src="https://www.youtube.com/embed/mUqqnEyWs3g?si=LwxB05TDvCrxTI_j" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/BYz61lyGA6g?si=WdPWQ5hND5t07u7N" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Robert Fripp also invited David Sylvian to be a member the reformed King Crimson, but he declined. Oh, what could have been! 

{% iframe 'https://open.spotify.com/embed/album/3CRipGCmcoBTKdadiYIMOc?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B07NRQPSYD/?id=WU8qzm74HD&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/gone-to-earth-remastered-2003/1451899979' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/104159185' '100%' '96' %}
