---
title: Slayer - Decade Of Aggression
author: Phil Plencner
date: 2020-06-04 22:35:46
tags:
- slayer
- heavy metal
- death metal
- thrash metal
---

I neglected yesterdays pick, so short and (un)sweet: Slayer's *"Decade of Aggression"* the completely raw and awesome live album from their 1990-1991 shows, which coincides with the first time I saw Slayer in concert. 

The show for me was life-altering. Aragon Ballroom, Chicago (affectionately known among local metal fans as the Aragon "Brawlroom").  The people organizing the show decided it was a good idea to set up folding chairs near the stage for people to quietly sit and enjoy their Slayer concert.  

By the time Motorhead finished their set and Slayer hit the stage the chairs had all become weapons / projectiles! 

I watched from a safe-ish distance, which is why I am still here today! 

SLLLLLLAAAAAAAAAAAAYYYYYYYYYYYYYYEEEEEEEERRRRRRRRRRRRRRRR!

<iframe src="https://open.spotify.com/embed/album/0x6sQPn26dKLzsmfLbXUG2" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>