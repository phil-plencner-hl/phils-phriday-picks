---
title: Stevie Wonder - Journey Through The Secret Life Of Plants
author: Phil Plencner
date: 2020-05-13 15:01:53
tags:
- stevie wonder
- 70s rock
- soul music
- r and b
---
Stevie Wonder is 70 today! Let's celebrate by listening to his bonkers double album based on the book *"The Secret Life of Plants"*. 

It was an ahead-of-its-time album...one of the first recorded completely digitally and containing a lot of brand new synthesizers. It's unlike anything Stevie did before or since.

<iframe src="https://open.spotify.com/embed/album/3LSgLZrSXELqWt5eqb6XMY" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>