---
title: Matthew Sweet - Altered Beast
date: 2024-02-02 21:20:47
tags:
- matthew sweet
- richard lloyd
- robert quine
- fred maher
- mick fleetwood
- power pop
- television
- punk rock
- no wave
---

![](AlteredBeast.jpg)

Matthew Sweet's career as a musician took a little while to take off. He moved to Athens, Georgia in the early 80s from Nebraska and became part of the same music scene as R.E.M. At one point he was even in a band with Michael Stipe called Community Trolls but that didn't go anywhere.

Eventually he became a member of The Golden Palominos, presumably through a connection with Stipe since Michael sang on the album *Visions of Excess* in 1985. Matthew Sweet sang on the following album *Blast of Silence*. The Golden Palominos had a rotating cast of characters, mostly from the New York City punk and no wave scenes. While in this group Matthew connected with guitarist Robert Quine (who played with Richard Hell & The Voidoids, Lou Reed, Material and tons more), drummer Fred Maher (of Massacre as well as Lou Reed and Material) and guitarist Richard Lloyd (of the band Television). These people formed the core of Matthew Sweet's band for most of the 1990s.

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

The first album they released together was *Girlfriend* in 1991, which was a big hit. The title track was played frequently on the radio and the video was in heavy rotation on MTV:

<iframe width="560" height="315" src="https://www.youtube.com/embed/n12OBlcHx9E?si=Ugtyt1w4NFKpk5NP" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

For the follow-up record Matthew Sweet wanted to expand the sound and scope of their material. This record was 1993's *Altered Beast* which is today's pick!

Along with the musicians mentioned already, guests on *Altered Beast* also included pianist Nicky Hopkins (who played with The Rolling Stones, The Kinks, The Who etc), drummer Pete Thomas (who played in Elvis Costello & The Attractions), and Mick Fleetwood (from Fleetwood Mac). To say this record is star-studded is an understatement!

The easiest way to showcase the range of music on *Altered Beast* is to listen to the two versions of "Ugly Truth". The first one is a country song and the second one is a heavy rock tune (complete with a sample from the film *Caligula* as the intro). They made a video for the rock version, which was also played frequently on MTV:

<iframe width="560" height="315" src="https://www.youtube.com/embed/tRDTyE_HF80?si=V-1QrkHFRIWWOYLb" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Another highlight is "Dinosaur Act" which flies out of the gate opening the album. There's also mellower, more sensitive material such as "Someone To Pull The Trigger", "Reaching Out", "What Do You Know" and "Time Capsule". They also made a video for the latter:

<iframe width="560" height="315" src="https://www.youtube.com/embed/7EJPzWg8cYQ?si=bcdn1zmxh0EmBjqM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

My favorite song on the record is probably "Knowing People" which both musically and lyrically I can relate to a great deal.

*Altered Beast* is one hour of power pop with an emphasis on the word "power". This is quite possibly Matthew Sweet's definitive statement.

The core group also recorded the albums *100% Fun*, *Blue Sky On Mars* and *In Reverse* which all have excellent songs and playing on them. However, they do not match the front-to-back classic that is *Altered Beast*.

{% iframe 'https://open.spotify.com/embed/album/5LQS2bRAVaSAdkYKajKvhk?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B0013AWZRM/?id=JDT6e1rsOT&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/altered-beast/304761427' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/3001072' '100%' '96' %}
