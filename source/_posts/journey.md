---
title: Journey - s/t
author: Phil Plencner
date: 2020-06-17 22:13:27
tags:
- journey
- classic rock
- 70s rock
- progressive rock
- santana
- frank zappa
---

Speaking of rock band debut albums that sound nothing like what they eventually became popular for: Let's talk about the first Journey album! 

Journey first formed as an instrumental prog rock / jazz fusion band with ex-members of Santana's backing band (Neal Schon of course plus Gregg Rolle on vocals(!) before he got downgraded to just the keyboardist  once Steve Perry arrived for *"Infinity"*). 

The band also originally included Aynsley Dunbar on drums, fresh from his stint in Frank Zappa's Mothers. Like I mentioned, the first album is mostly prog rock based, and doesn't really reach for pop hits like the Steve Perry-fronted group ended up doing. 

The first song "Of A Lifetime" is a 7 minute epic that sets the tone right out of the gate. There's even two instrumentals ("Kohoutek" and "Topaz") that veer into Pink Floyd-esque space rock. 

In fact, they recorded a few more instrumentals in this vein that weren't included on the album (worth seeking out). 

Overall a pretty solid overlooked catalog album.

<iframe src="https://open.spotify.com/embed/album/28NkOYgAmfDMjecduDX7lL" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>