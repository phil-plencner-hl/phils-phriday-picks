---
title: Hank III - Cattle Callin'
date: 2022-12-09 11:09:24
tags:
- hank iii
- hank williams iii
- country music
- punk rock
- death metal
- honky tonk
---

![](CattleCallin.jpg)

Hank Williams III is the son of Hank Williams Jr, which makes him the grandson of the original Hank Williams.

This would lead many to believe he just followed in the footsteps of his family and just played traditional or outlaw country music. This could not be further from the truth.

While he does perform his fair share of music in that style (See albums such as *Lovesick, Broke & Driftin'* or *Straight To Hell* to hear him in that context) he also performs a lot of punk rock and death metal music! In fact, at one point he was playing bass guitar in former Pantera singer Phil Anselmo's band Superjoint Ritual. 

Hank III calls his punk rock / metal band Assjack. He released one self-titled album under that band moniker, but most of it is released as Hank III albums (*Damn Right, Rebel Proud* and *Hillbilly Joker* are two good examples).

But none of this compares to the album he released under the band name of Hank 3's 3 Bar Ranch called *Cattle Callin'*. This was a full-on death metal album (complete with distorted guitars and tons of double bass drumming) all completely performed by Hank III. What makes this even more crazy is that instead of having a traditional vocalist, he used recordings of cattle auctioneers instead. Their high-speed speaking style works great within the death metal context. He ended up calling this unique style "Cattle Core". *Cattle Callin'* is today's pick!

Hank III released *Cattle Callin'* during a period of extreme productivity and creativity in 2011. The same day he released *Cattle Callin'* he also released a 2CD set called *Ghost To A Ghost / Gutter Town* (a mix of traditional country and Assjack) and *Attention Deficit Domination* (which is a really cool doom metal project). Soon after in 2013 he released two more albums: *Brothers of the 4x4* (traditional country) and *A Fiendish Threat* (traditional punk rock). 

During all this activity he managed to also tour heavily. Instead of focusing on one style or band, he would play a lengthy concert with 3 sets: a traditional country set, a punk rock / "hellbilly" set and then a "cattle core" set! Each set had their own band.

Here is video of a country set from that era:

<iframe width="560" height="315" src="https://www.youtube.com/embed/S0pUv2n4EQI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Here is a good example of a typical hellbilly set from that same time:

<iframe width="560" height="315" src="https://www.youtube.com/embed/BLIXT0Zfdpo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Here is awesome footage of him playing "cattle core" that has to be seen to be believed:

<iframe width="560" height="315" src="https://www.youtube.com/embed/Kyi3mXMb9fU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The drummer is Phil Cancilla, who later went on to play in Malevolent Creation. Here is some killer up-close footage of his playing with Hank III:

<iframe width="560" height="315" src="https://www.youtube.com/embed/-Tyq5NqUkT0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

There is really nothing else out there that sounds like *Cattle Callin'*. Even if you are not into the style generally, you must appreciate the audacity and unique nature of the project.

Unfortunately, after 2013 Hank III basically removed himself from the spotlight and has rarely been heard of or seen playing music. 

Recently, he has been posting some new demos on his Facebook page (along with a ton of country, punk and metal recommendations) so perhaps he will soon be making a much-anticipated comeback.

In the meantime, we can continue to crank up *Cattle Callin'*. 

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/album/52ts9eS1AWMesepZWq1Q60?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

<iframe id='AmazonMusicEmbedB005LIH3KQ' src='https://music.amazon.com/embed/B005LIH3KQ/?id=aWYbSMp0ZI&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *; clipboard-write" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/cattle-callin/455126617"></iframe>

<iframe src="https://embed.tidal.com/albums/7717226" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>