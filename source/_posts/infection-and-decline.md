---
title: Flying Luttenbachers - Infection And Decline
author: Phil Plencner
date: 2019-05-24 15:58:05
tags:
- flying luttenbachers
- progressive rock
- noise rock
- zeuhl
- magma
---

Today's pick: One of my all-time favorite Chicago bands:  The Flying Luttenbachers!! 

Masterminded by drummer/composer Weasel Walter (of whom I became friends with over the years), this album (*"Infection and Decline"*) is the start of their "Brutal Prog" phase. 2 bass players (1 tuned high, 1 tuned low and fretless) combined with frantic death-metal inspired drumming. 

I probably saw this band perform 20 times during this era. Killer cover of Magma's "De Futura" as well (Perhaps Magma should be a future Phil's Phiday Pick?? Hmmmmm!)

<iframe src="https://open.spotify.com/embed/album/3ZmqW2k1IpLDVYAHevEj2Y" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>