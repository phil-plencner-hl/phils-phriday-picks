---
title: Mats & Morgan - 35th Anniversary Collection
author: Phil Plencner
date: 2020-07-01 21:50:06
tags:
- mats & morgan
- progrssive rock
- jazz fusion
- frank zappa
- swedish rock
---
Today's pick is Swedish rock group Mats & Morgan. The group is named after the two principal instrumentalists / composers: Mats Öberg (keyboards) and Morgan Ågren (drums). 

They met as children and have had a lifelong bond playing music. Their music is a wild mix of jazz, rock and classical that is pretty hard to classify. They first came under my radar when they were part of the all-star band that Frank Zappa assembled for the *Zappa's Universe* shows in 1993 (right before Zappa passed away...he was originally supposed to participate in the tribute shows but was too ill). 

Most of Mats & Morgan albums are on small, independent labels (most recently awesome DC-based Cuneiform Records) and are generally not on Spotify. 

However, their "best-of" collection is! So I'll pick that for today, as it is a pretty great overview of their body of work.

Bonus footage: The complete [TV broadcast of the Zappa's Universe concerts](https://www.youtube.com/watch?v=sWmo2je1bhM) are on Youtube.

<iframe src="https://open.spotify.com/embed/album/0bbkXwei2cdhudkDcrkUlV" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>