---
title: Brandee Younger - Brand New Life
date: 2023-09-01 14:08:14
tags:
- brandee younger
- makaya mccraven
- joel ross
- kassa overall
- stevie wonder
- dorothy ashby
- jazz
- jazz fusion
- jazz harp
- alice coltrane
---

![](BrandNewLife.jpg)

Harp in a jazz context is seemingly in a resurgence nowadays. Alice Coltrane got a [nice feature in the *The New York Times*](https://www.nytimes.com/2022/09/07/arts/music/alice-coltrane-jazz-music.html) last year and Dorothy Ashby was [profiled in *The New Yorker* in June](https://www.newyorker.com/culture/listening-booth/how-dorothy-ashby-made-the-harp-swing). She was also on the cover of the [latest issue of *We Jazz* magazine](https://wejazzrecords.bandcamp.com/merch/we-jazz-magazine-summer-2023-shadow-shapes). 

The New Land record label recently released a [comprehensive box set of Dorothy Ashby's solo albums](https://newland.ochre.store/release/367003-dorothy-ashby-with-strings-attached-1957-1965?lang=en_US), which is likely why magazines are taking notice. Whatever the reason, it is a well deserved spotlight on a mostly obscure artist. 

I say, "mostly obscure", because you have likely heard Dorothy Ashby's playing on Stevie Wonder's "If It's Magic" off of *Songs in the Key of Life*:

<iframe width="560" height="315" src="https://www.youtube.com/embed/fX36mGEqfw4?si=zYth7Hfxt2J5Bu3M" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

A more contemporary example of jazz harpist that has been getting a good amount of exposure lately is Brandee Younger. She has come across my radar in the last few years because of her association with drummer Makaya McCraven (appearing on previous Phil's Phriday Picks *Universal Beings Sides E&F* and *I'm New Here*) and Kassa Overall's *I Think I'm Good* (another previous PPP!). She is also prominently featured on the new Meshell Ndegeocello album *The Omnichord Real Book*.

She also had a nice story in [*The New York Times*](https://www.nytimes.com/2016/03/01/arts/music/brandee-younger-a-harpist-finding-her-way-to-jazz.html) a few years ago.

She also recently released her own new record called *Brand New Life* which is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

Most of *Brand New Life* is a tribute to Dorothy Ashby. 5 of the songs on the album were written by Dorothy Ashby and there is also a cover of Stevie Wonder's "If It's Magic".

Makaya McCraven plays drums throughout and Meshell Ndegeocello also sings on "Dust". So, there is a lot of mutual respect between all of these players.

Overall *Brand New Life* is a creative mix of contemporary jazz, modern R&B and hip-hop. On the hip-hop side of things Pete Rock and 9th Wonder produce much of the music. 

One of the ways Impulse Records promoted *Brand New Life* was through a story on *Good Morning America* back in March. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/8srsXzyggeQ?si=mzDlw8z3MmvBfH8L" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Additionally, she recently appeared on NPR's *Tiny Desk Concert* and a KNKX *Studio Session* (where she even covers "Turiya & Ramakrishna" by Alice Coltrane).

<iframe width="560" height="315" src="https://www.youtube.com/embed/lW2cUz9Gz94?si=EkWeQFr9tN-8TT4n" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/MSLlz34IFjE?si=qCG6FqrqozWYpJxD" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

My only wish is that *Brand New Life* would be a little longer. It's 35-minute run time is too short...but maybe this means she'll release another album in the near future.

{% iframe 'https://open.spotify.com/embed/album/2bF9vZG92EocmLQdS1M1Ro?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B0BVGNB44C/?id=n4Vt1UplHR&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/brand-new-life/1671212000' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/286308904' '100%' '96' %}
