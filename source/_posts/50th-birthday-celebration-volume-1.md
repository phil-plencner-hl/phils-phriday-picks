---
title: Masada String Trio - 50th Birthday Celebration Volume 1
date: 2023-09-29 22:14:06
tags:
- john zorn
- masada
- tzadik records
- joey baron
- greg cohen
- dave douglas
- bill laswell
- mike patton
- fred frith
- milford graves
- mark feldman
- erik friedlander
- masada string trio
- avant-garde jazz
- free jazz
- classical
---

![](50thBirthdayCelebration.jpg)

Composer and saxophonist John Zorn recently turned 70 years old. To celebrate, he staged a series of concerts over 5 days at Great American Music Hall in San Francisco. 

During the shows, there was a surprise announcement that John Zorn's record label: Tzadik would finally release their entire catalog to streaming services. Tzadik has long been a holdout to add their vast discography (over 400+ titles going back to 1995) to streaming sites, so this was surprising, but very welcome news.

Since the announcement, I have been wondering what I could pick from the Tzadik archives. When John Zorn turned 50, he released a series of 12 CDs from live performances at Tonic in New York City that were part of that celebration. Back in 2004-2005 I eagerly awaited every release in this series and collected them all (I hope there will be a similar series for the 70th celebration).  Something from the 50th Birthday Celebration series seems like a great choice. But which one? Solo saxophone solos tagged as *Classic Guide to Strategy Volume 3*? Maybe one of the wild duo recordings with Fred Frith or Milford Graves? Perhaps the monstrously heavy Painkiller recording with Bill Laswell, Hamid Drake and Mike Patton? All good possibilities...

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

I eventually decided to choose the first volume in the series: A recording by the Masada String Trio! *50th Birthday Celebration Volume 1* is today's pick!

Masada is a long-running band that John Zorn fronts. Initially envisioned as a set of 100 "songs" notated within a limited number of staves and confined to specific modes or scales, Zorn's Masada project would eventually total 613 compositions divided into three specific "books". Those compositions were also performed by a wide range of other groups. Two notable examples are Electric Masada (playing the songs in a heavy prog rock style) and Masada String Trio (playing the songs in a classical trio format).

Masada String Trio consists of Greg Cohen on bass, Mark Feldman on violin and Erik Friedlander on cello. All three are virtuosos and their interpretations of the compositions are nothing short of amazing. This is likely because they have been playing the songs for many years prior to this live recording so they know the songs inside and out.

Here is an example of them playing live:

<iframe width="560" height="315" src="https://www.youtube.com/embed/XT_yyYXska8?si=C_f9afc4Lxhn19vc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

*50th Birthday Celebration Volume 1* is an exciting example of this band in full flight. Beyond the original compositions, they also add interesting improvisations that bring interesting new elements to the material.

I'll likely be diving into the Tzadik catalog in the coming months...reacquainting myself with old favorites along with new discoveries. I hope you do the same and *50th Birthday Celebration Volume 1* is a great place to start.

{% iframe 'https://open.spotify.com/embed/album/7jRkC8HzBGvFxDlEA2PJtl?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B01N0VWZYC/?id=ArVXr7qIU5&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/50th-birthday-celebration-vol-1/1188838505' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/318473523' '100%' '96' %}
