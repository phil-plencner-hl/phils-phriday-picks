---
title: Medeski, Martin & Wood - It's A Jungle In Here
date: 2023-04-21 09:07:57
tags:
- medeski martin and wood
- john medeski
- billy martin
- chris wood
- jazz
- jazz funk
- organ trio
- avant-garde jazz
---

![](ItsAJungleInHere.jpg)

Bonus pick this Friday since I missed last week's pick!

Record Store Day is tomorrow. The annual event has grown larger and more overwhelming each year and I find it less exciting as labels seem to flood the market with junk (Does anyone really want Macho Man Randy Savage's *Be A Man* on vinyl??).

One thing on this year's release list that caught my interest is the re-release of Medeski, Martin & Wood's *It's A Jungle In Here*. The album is celebrating its 30th anniversary this year (initially released in August 1993) and this will be the first time it is pressed on vinyl. I'm excited about that, so *It's A Jungle In Here* is today's bonus pick!

*It's A Jungle In Here* is Medeski, Martin & Wood in their early stages. This is before the heavy hip-hop influences of *Shack Man*. It is before they opened for Phish and became darlings on the jam band circuit. It is before they played on John Scofield's *A Go-Go* and became Scofield's backing band for a while. It is certainly before the heavy electronic, jazz fusion hybrids of *The Dropper* and *Uninvisible*.

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

*It's A Jungle In Here* is almost entirely acoustic, although John Medeski does run his organ and piano through some effects pedals. They play more like an avant-garde jazz trio than the futuristic funk band they later became. The set of covers on the album reflects this: "Moti Mo" originally by King Sunny Aide, "Syeeda's Song Flute" originally by John Coltrane and a cool Thelonious Monk and Bob Marley medley "Bemsha Swing/Lively Up Yourself". 

The originals are also great...especially the heavy funk of "Shuck It Up" and the title track.

While I probably won't get up early and fight the crowds and long lines for Record Store Day tomorrow, I am hoping that when I stop in to my local shop they still have a copy of *It's A Jungle In Here* waiting for me.

{% iframe 'https://open.spotify.com/embed/album/1FfTGkGrNCAl83mTPSY697?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B00415B6JE/?id=10Wat9bmyY&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/its-a-jungle-in-here/160783013' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/338336' '100%' '96' %}
