---
title: Squarepusher - Be Up A Hello
author: Phil Plencner
date: 2020-01-31 12:03:58
tags:
- squarepusher
- dance music
- drum n bass
- electronic music
- jazz
---
Today marks the release of a new album from Squarepusher! 

The jazz / edm maestro returns with a heady album that's going to keep me grooving this weekend.

<iframe src="https://open.spotify.com/embed/album/1zcfHeYC0sEQs3D3fCAr4m" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>