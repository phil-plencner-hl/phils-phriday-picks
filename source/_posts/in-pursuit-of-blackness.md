---
title: Joe Henderson - In Pursuit Of Blackness
date: 2021-06-17 07:52:37
tags:
- joe henderson
- jazz fusion
- classic jazz
- jazz funk
- 
---

Saxophone player Joe Henderson was already a living legend by the time he signed a record deal with Milestone Records in the late 60s. 

For starters, he released a series of highly acclaimed bop albums for Blue Note, including  a set of excellent records with the same rhythm section that appeared with Miles Davis on *Kind of Blue* (Wynton Kelly, Paul Chambers and Jimmy Cobb). 

He also appeared on many other seminal records for Blue Note as a sideman (appearing with Horace Silver, Kenny Dorham, Grant Green and Andrew Hill just to name a few).

You would think he could rest on his laurels at this point. However, he decided to persue a completely different direction once joining Milestone.

He assembled an electric fusion band which included the great George Gables on electric piano, trombone virtuoso Curtis Fuller, funky drummer Lenny White (before he joined Return To Forever) among many others. 

This lineup released what I consider one of his best albums *In Pursuit Of Blackness* in 1970.

*In Pursuit Of Blackness* is a mix of studio and live recordings. The live recordings were from a performance at The Lighthouse Cafe near Los Angeles. 

Much like last week's pick (*Electric Byrd*) this record is bursting with a powerful blend of heavy funk and outsider jazz played by a red-hot band of excellent musicians.

The closing track "Mind Over Matter" is a 15-minute epic highlight showcasing all that this band was capable of.

Fantasy Records (who bought Milestone Records later in the 70s) re-released this album as a two-fer with *Black Is the Color*, which is the version that appears on Spotify.

*Black Is The Color* is in a similar vein, but with a completely different ensemble (Ron Carter and Jack DeJohnette are the amazing rhythm section present here). It however also expands on the concepts, because at this point Joe Henderson started getting way into overdubbing. The result is a very dense sound where you can hear him playing multiple instruments simultaneously (tenor and soprano saxophone along with flute). "Terra Firma" is the lengthy highlight here, but the whole album is mostly excellent (though I think *In Pursuit Of Blackness* is the slightly stronger effort of the two).

After leaving Milestone Records in the 80s Joe Henderson signed with Verve and completely changed his persona again. His repertoire focused on classic jazz standards, and he embraced an elder statesman persona. I don't find this period nearly as interesting as his years at Milestone, but that might be an unpopular opinion. 

<iframe src="https://open.spotify.com/embed/album/096imWJdJPgNkdrRzcw6qe" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

