---
title: Smiley Winters - Smiley Etc.
author: Phil Plencner
date: 2020-06-08 22:30:57
tags:
- jazz
- smiley winters
- free jazz
---

Over the weekend a friend of mine reminded me of this very obscure free jazz record by drummer Smiley Winters called *"Smiley etc"*. 

The first 3 songs are total free jazz onslaught with the drums at the forefront. A total barn burning session!! 

The rest are more straightahead blues and jazz that's closer aligned to what was typically put out on the Arhoolie Record label that this was released on. 

It also ends with a pretty nice little drum / bass duet. Super-obscure and super-cool (both of which are what Phil's Phriday Picks as all about!)

<iframe src="https://open.spotify.com/embed/album/21MwrpSwiFTeJVWgQVR5OV" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>