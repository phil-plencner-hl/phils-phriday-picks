---
title: Emerson Lake And Palmer - Emerson Lake And Palmer
date: 2022-04-01 11:37:45
tags:
- emerson lake and palmer
- keith emerson
- greg lake
- carl palmer
- 70s rock
- progressive rock
---

Emerson, Lake and Palmer were pretty confounding in a lot of ways. They were pioneers in the early 70s of adapting classical music into a rock context. Their songs were complex, with tons of room for long-winded instrumental sections and virtuoso playing.  

The fact that they were hugely popular is pretty amazing. 1973's *Brain Salad Surgery* (which featured the 30 minute epic "Karn Evil 9") sold well over 500,000 copies and charted just outside the top 10! The tour for the album included an appearance at California Jam in front of 350,000 fans and was aired on ABC!

<iframe width="560" height="315" src="https://www.youtube.com/embed/SZK_fFtwKs0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Its hard to believe, but the bulk of their best material was released in a very short period of time. From their self-titled first album in 1970 to *Brain Salad Surgery* they released 5 albums. 

Afterwards they basically lost the plot and never matched the quality nor quantity of their material. I'm a pretty big ELP fanboy, so I can find things to like on pretty much all their albums since (with maybe the exception of their last album *In The Hot Seat* which was dreadful front-to-back), but there is no question that their first 5 albums are beyond reproach and have not been bested. "Works Volume 1" might come close to keeping pace with the others if it wasn't for Greg Lake's awful side of the album filled with sappy ballads. 

It's hard to believe so much great material was made in such a short span of time before flaming out. The situation is muddied further by the fact that those first 5 albums have been re-released and repackaged in countless ways since (Greatest Hits, Box Sets, Anthologies, "Definitive" Editions, "Deluxe" Editions, re-titled albums with the same material and on and on). So it looks like their catalog is impenetrable, but if you're just diving in any of their first five studio albums are all you need.

If there is one album I go back to most frequently, it is probably their self-titled debut album, which is today's pick. Why? Because it's not too overblown and showcases all their strengths in easy to digest pieces.

It starts off with "The Barbarian" which takes no prisoners right out of the gate. Carl Palmer's machine-gun drumming, Greg Lake's fuzzed out heavy bass and Keith Emerson's organ and keyboards dancing all over it. It shows that Bela Bartok can rock if you let him.

I tend to think that Emerson, Lake and Palmer were basically a keyboard-fronted punk band at this time intensity-wise and "The Barbarian" reflects that. 

To further bolster my case check out this version of Dave Brubeck's "Blue Rondo a la Turk" from 1970, where Keith Emerson famously stabs his organ with actual knives and throws it all over the place:

<iframe width="560" height="315" src="https://www.youtube.com/embed/d6Qi04W-PBA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

It is followed by Greg Lake's ballad "Take A Pebble" in a nice concise version (later live performances stretched it out to marathon lengths). 

They get back to heavy morphing of classical pieces with Leos Janacek's orchestral piece "Sinfonietta" titled "Knife Edge". Probably one of my all-time favorite ELP tunes.

On side two of the original record they stretch out with their first epic called "The Three Fates" which leads into the Carl Palmer penned "Tank" which is basically a drum showcase.

It ends with perhaps Greg Lake's most famous and popular ballad "Lucky Man" which you can still hear in rotation on classic rock playlists.

Overall it is, in my opinion, one of the greatest rock band debuts of all time.

<iframe src="https://open.spotify.com/embed/album/4Q8vDc5lmB9SW9cgMDeQAJ?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

<iframe id='AmazonMusicEmbedB01JTI6IP4' src='https://music.amazon.com/embed/B01JTI6IP4/?id=mEEYLKNQK2&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/emerson-lake-palmer-2012-remaster/1141484161"></iframe>

<div class="tidal-embed" style="position:relative;padding-bottom:100%;height:0;overflow:hidden;max-width:100%"><iframe src="https://embed.tidal.com/albums/68650812?layout=gridify" allowfullscreen="allowfullscreen" frameborder="0" style="position:absolute;top:0;left:0;width:100%;height:1px;min-height:100%;margin:0 auto"></iframe></div>