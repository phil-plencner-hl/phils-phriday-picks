---
title: Vinnie Colaiuta - s/t
date: 2021-02-12 09:10:20
tags:
- vinnie colaiuta
- frank zappa
- sting
- jazz fusion
- 70s rock
- 90s rock
---

Drummer extraordinaire Vinnie Colaiuta turned 65 last week. 

Back in the early 90s, I was an avid reader of Modern Drummer magazine. I would eagerly await the new issues and would devour every page. In October 1993, Vinnie Colaiuta was on the cover, and my life was never the same.

Today I discovered that particular cover story / interview with Vinnie Colaiuta is [available in full online](https://www.vinniecolaiuta.com/Interviews/ModernDrummer1993)!

The interview mainly focused on his work with Sting and the incredible drum performance on *Ten Summoners Tales*. However, it also put a huge emphasis on his time with Frank Zappa...specifically on *Joe's Garage*. 

I went out and bought *Joe's Garage* soon after and that started my decades-long obsessive fandom with Frank Zappa...but avid readers and people who know me know all about that already.

The next year, in 1994, Vinnie Colaiuta put out his first (and I believe only) solo album in which he is the leader. By the time that came out I was in the middle of a full-blown Vinnie Colaiuta bender, so naturally I bought the CD the day it came out. This is today's pick.

The album displays a wide range of styles within rock and jazz, and obviously the drumming is completely over-the-top. I'd have to say some songs on here and probably the best drum performances Vinnie ever recorded.

Opener *I'm Tweaked / Attack of the 20 Ft. Pizza* comes out with guns blazing. A super-heavy riff, with Vinnie playing with the time...skipping beats and basically making it sound like the CD is skipping while still keeping a locked in groove is ear candy to me. The massive drum solo near the end of the song is icing on the cake.

There are only a couple other full-on rockers on the album (notably *Bruce Lee* with another wild solo and the hard funk of *Slink*)....the rest is stuff that actually was pretty spacy and out-there. 

Sting guests on the nightmarish *Chauncy* and if you want to hear him play some monsterous bass for a change, this is a great track.  Other high-profile guests include Herbie Hancock on techno-ish *Momoska (Dub Mix)* and Chick Corea (RIP! Probably a tribute pick coming someday soon) on the beautiful ballad *Darlene's Song*

This is probably one of my favorite albums of all time in any genre, and it surprises me that it took so long for me to make it an official pick! I hope you enjoy it as much as I do.

<iframe src="https://open.spotify.com/embed/album/7zSk9L3eO8mJmwbMDIu1Qy" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>