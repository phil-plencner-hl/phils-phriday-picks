---
title: Best of 2021
date: 2022-01-07 10:08:23
tags:
- best of list
---

Another year is in the books! 2022 is here, which means it's time to wrangle up my favorite albums from the past year. Because I mostly stayed indoors again this year, there have been plenty of opportunities to continue to immerse myself in new music.

This year, I have whittled my favorites down to a sixty song playlist (The "Super 60"), spanning a little over 5 hours. Much like the pandemic playlists from the past year, it whiplashes between styles...anything from the pure pop of St. Vincent to hardcore country from Mike And The Moonpies to the full metal assault of Stormkeep. They are not ranked, but sequenced in a way that I find pleasing to listen to.

<iframe src="https://open.spotify.com/embed/playlist/1a4RbqFeGxLasIFKks2nBw?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

Additionally, for those whos ears perked up when I said "full metal assault", I also made a different playlist of only heavy metal music. This one is not for the weak of heart...it's another 56 songs (some repeats from the "Super 60" because they are so good). 4.5 hours with the pedal to the metal!

<iframe src="https://open.spotify.com/embed/playlist/0b2HyzNyjoxaPwQ2mUBCOU?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

I hope everyone has a good 2022 full of musical discoveries both old and new. It's gotta be better than 2021, right??
