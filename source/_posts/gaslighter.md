---
title: The Chicks - Gaslighter
author: Phil Plencner
date: 2020-07-17 16:12:17
tags:
- dixie chicks
- the chicks
- gaslighter
- 90s country
- country music
---

It's Friday, which means new music. The excellent new album by The Chicks is here!

<iframe src="https://open.spotify.com/embed/album/1YV5Rh6n8dLOycCqWcUSq4" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>