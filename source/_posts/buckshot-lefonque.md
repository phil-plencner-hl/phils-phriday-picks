---
title: Buckshot LeFonque - Buckshot LeFonque
date: 2022-11-18 15:39:54
tags:
- buckshot lefonque
- sting
- branford marsalis
- kevin eubanks
- kenny kirkland
- dj premere
- jazz rock
- funk
- hip-hop
- jazz
---

![](BuckshotLeFonque.jpg)

In the late 1980s to the early 1990s Branford Marsalis was seemingly everywhere.

He came to popular prominence as part of Sting's band after The Police broke up. He played on *Dream of the Blue Turtles* and many subsequent Sting solo albums. But he really shined in the live setting, as documented in the excellent *Bring on The Night* documentary from 1985:

<iframe width="560" height="315" src="https://www.youtube.com/embed/MUm5KWSlbC0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

He also associated himself with The Grateful Dead. He never officially joined the band but played shows with them on a regular basis. Here he is performing one of my favorite Dead compositions: *Help On The Way / Slipknot / Franklin's Tower* in 1991: 

<iframe width="560" height="315" src="https://www.youtube.com/embed/IdX_UL0ddC4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Of course, his most ubiquitous presence was as the bandleader of *The Tonight Show with Jay Leno*. He brought several hot players from his own jazz quartet (including guitarist Kevin Eubanks who would later take over the band and pianst Kenny Kirkland who would tragically pass away in 1998). Here is a fun promo of the show featuring Branford:

<iframe width="560" height="315" src="https://www.youtube.com/embed/djN5l6ze400" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

In 1994, Branford decided to form a new band that fused jazz with hip-hop called Buckshot LeFonque. Their first self-titled album (of two that they released) is today's pick!

The album is produced by DJ Premier (of Gang Starr fame!) and features his stellar turntable work.

The band also features a who's who of great jazz players such as Kevin Eubanks (guitar), Roy Hargrove (Trumpet), Jeff "Tain" Watts (drums), Albert Collins (guitar) and Victor Wooten (bass) among many others.

Here is a video from one of the singles they released from the album, "Breakfast @ Denny's", to give you a better idea of the style:

<iframe width="560" height="315" src="https://www.youtube.com/embed/SMrKGkKdKYU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The album covers a decent amount of ground fusing jazz with hip-hop, some reggae and some rock. A fun party record.

The album also includes a cool cover of Elton John's "Mona Lisas and Mad Hatters" in a very funky style.

Unfortunately, Buckshot LeFonque was short-lived as their popularity never eclipsed the hype and they broke up in 1997.

Phil's Phriday Picks will be on holiday next week. The next pick will be December 2nd.

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/album/7c2fqypN7hrI0CyYxTFgCZ?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

<iframe id='AmazonMusicEmbedB0013AT3LS' src='https://music.amazon.com/embed/B0013AT3LS/?id=gWh2abjuUh&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *; clipboard-write" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/buckshot-lefonque/192759425"></iframe>

<iframe src="https://embed.tidal.com/albums/5009385" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>