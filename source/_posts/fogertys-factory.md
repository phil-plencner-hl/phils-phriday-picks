---
title: John Fogerty - Fogerty's Factory
author: Phil Plencner
date: 2020-07-07 21:38:47
tags: 
- john fogerty
- fogerty's factory
- creedence clearwater revival
---
"Fogerty's Factory" is the band name that John Fogerty (of Creedence Clearwater Revival fame) uses for his new project involving his kids (Tyler, Shane, and Kelsy)! 

He runs a very popular [YouTube channel](https://www.youtube.com/results?search_query=%23fogertysfactory) of their performances. They have also performed for Stephen Colbert, NPR's Tiny Desk Concert, XM/Sirius and more. 

They recently put out a compilation album on streaming services such as Spotify with highlights from all their online content. They also re-create the CCR *"Cosmos Factory"* album cover. A totally fun album!

<iframe src="https://open.spotify.com/embed/album/22uwZnRVxLkQ5FUQxLzajh" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>