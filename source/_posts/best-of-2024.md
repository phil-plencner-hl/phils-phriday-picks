---
title: Best Of 2024
date: 2024-12-13 21:36:05
tags:
- best of list
---

![](2024.png)

2024 is rapidly coming to a close. As I was compiling my list of favorite records this year, other people's "best of" lists were already showing up online and coming to my attention. 

At first glance of other lists I saw, I was initially surprised that hardly any of my favorites were showing up on other lists! There are obvious exceptions with high profile records like The Cure, Blood Incantation and Mdou Moctar...but most of my list seemed out of touch with other listeners. However, even between other people's lists there wasn't much crossover beyond the aformentioned high profile examples. 

It made me realize that we're reaching a breaking point of siloed music listening. In the current state of personalized playlists, unique social media feeds, niche podcasts and the entire history of music at our fingertips everyone's in their own little worlds. No longer are there cultural zeitgeists like MTV, mainstream radio stations or magazines. Even previous online juggernauts like Pitchfork and Stereogum are no longer universal.  Sure, Taylor Swift is still huge and there was Brat Summer etc but those are generally out of my wheelhouse (I did like *Brat*, for what it's worth). 

It made me wonder: who are these year end lists really for? Are people really diving into them to find things they missed? Maybe it's just to document for future generations? I dunno, but I'm still making my list anyways. I, for one, have made a separate note of albums I missed that showed up on other "best of" lists I want to check out...but I suspect I am the exception and not the rule.

At any rate, below is my list. I can confidently say the top 20 records are definitely my favorites of the year, generally ranked in the order or how much I loved them. After that, the math becomes fuzzier but they are all worth hearing and I heard them all multiple times throughout the year. I'll likely keep coming back to most of them in the future. They each have links to the full albums in Spotify. At the top of the list I'm also embedding a playlist of 1 song from each album for those who want a "quick" (if you think 7 hours is quick) breeze through them all. You could potentially deep dive from there if you choose. I'm not gonna write about each of the 80 albums I selected individually...I think this year I'll let the music speak for itself. 

Phil's Phriday Picks will be going on holiday break for the rest of the year. I'll return in January 2025 with our regularly scheduled weekly selections. Have a Merry Christmas and Happy New Year! 

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/playlist/1317CT3n7vTw97XwoUPeQh?utm_source=generator" width="100%" height="352" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

1. Chaser - *Planned Obsolescense* [Spotify](https://open.spotify.com/album/5hp0VYfYM7hyRewDymcBAz?si=4a91f9792d4c474d)
2. Isaiah Collier & The Chosen Few - *The World is on Fire* [Spotify](https://open.spotify.com/album/3dEmEoutUFwiwc427SFkMK?si=59235e14bc7c421b)
3. Eric Slick - *New Age Rage* [Spotify](https://open.spotify.com/album/1XkNIi0AU9nSlv1p9ycwGU?si=1b94e1cb23234922)
4. Blood Incantation - *Absolute Elsewhere* [Spotify](https://open.spotify.com/album/7hriIeLMviZKpNfcXgpsd8?si=7ffca58f91b74ff5)
5. Sleepytime Gorilla Museum - *Of The Last Human Being* [Spotify](https://open.spotify.com/album/21sPynlIngwnuIOcltyRjv?si=1201100313e24d5e)
6. The Cure - *Songs of a Lost World* [Spotify](https://open.spotify.com/album/4wjxmqXnSQvBZWL3IbYngX?si=814afea9d4644092)
7. Mdou Moctar - *Funeral for Justice* [Spotify](https://open.spotify.com/album/1fJajwWIysMm9TqWzFcHwq?si=38306dae1bed45d9)
8. Skip Heller's Voodoo 5 - *The Exotic Sounds Of...* [Spotify](https://open.spotify.com/album/1I4wVBkBLfaeXu2Q9nr4Ww?si=fc62976fc9ab4960)
9. The Messthetics and James Brandon Lewis - *s/t* [Spotify](https://open.spotify.com/album/3hewGriGKlgvZGvnZgR85A?si=d43a17b15fcd4af9)
10. Bedsore - *Dreaming the Strife for Love* [Spotify](https://open.spotify.com/album/3KFaCfyta9b7t08QAXuI5P?si=7e29e80ef0094138)
11. Bill Frisell - *Orchestras* [Spotify](https://open.spotify.com/album/4EoWs7EVlk6Nv7GNNlYjLE?si=90e191c299c749ef)
12. David Murray Quartet - *Francesca* [Spotify](https://open.spotify.com/album/2EoiYFmvj6j8d7jINBTDaJ?si=3fb8fe6e0a934411)
13. Arooj Aftab - *Night Reign* [Spotify](https://open.spotify.com/album/2JdE3ilolUGhsNkW1oQfvf?si=acbf3346a1244606)
14. Green Day - *Saviors* [Spotify](https://open.spotify.com/album/4AIeqAMDyIT884A9uA2A0i?si=7ea4ff9a83bb4656)
15. Louis Cole - *Nothing* [Spotify](https://open.spotify.com/album/1hD0obPJIc7yi3RwwREblP?si=8bf458d50cfe4b41)
16. Joshua Ray Walker - *Thank You For Listening* [Spotify](https://open.spotify.com/album/78NZknn4k0XrQ6xw2mZ0FX?si=7458921971c94f18)
17. Mary Halvorson - *Cloudward* [Spotify](https://open.spotify.com/album/7pyMVbIw7GW24pEge8ufbC?si=d12f0bee99494a8c)
18. Jon Anderson - *True* [Spotify](https://open.spotify.com/album/3ON1es6pWr8pBYYRoMJa9R?si=cc03ab193a794a00)
19. Meshell Ndegeocello - *No More Water* [Spotify](https://open.spotify.com/album/2ba5r7icQRIInoWJYUrsSJ?si=f9945f3ab3d940f0)
20. The Jesus Lizard - *Rack* [Spotify](https://open.spotify.com/album/5Fsh82tpjAPMM3AUTil1WD?si=299674e3ea3e42a5)
21. Jack White - *No Name* [Spotify](https://open.spotify.com/album/4j6OkbZmVIqJYDLJbiWHbX?si=8cd23ab9f7034f5c)
22. Upright Forms - *Blurred Lines* [Spotify](https://open.spotify.com/album/5bDGj1xL06aChn3ixHM0nV?si=96529282b06a4015)
23. Nubya Garcia - *Odyssey* [Spotify](https://open.spotify.com/album/1ZoZu4AeEVIKybGiGgOYdd?si=5ddb679142114902)
24. Kronos Quartet - *Outer Spaceways Incorporated* [Spotify](https://open.spotify.com/album/6R5LUMUWQfeWcRGewFAxBA?si=fd96552a59f84e90)
25. Tony Levin - *Bringing It Down to the Bass* [Spotify](https://open.spotify.com/album/7Ki6ZgBTFAChYkimwU72XM?si=90210ad77be74626)
26. McCoy Tyner - *Forces of Nature* [Spotify](https://open.spotify.com/album/3chmmA509fybGQ6ZYWzjsA?si=9d4fa7c64ccb4d7e)
27. Judas Priest - *Invincible Shield* [Spotify](https://open.spotify.com/album/0SgF4c7VeFBoATEdpTvgG8?si=7a9872c7822544ec)
28. Amaro Freitas - *Y'Y* [Spotify](https://open.spotify.com/album/6uau7dao159A94pxtGIoMj?si=2ebc2b8d34724a32)
29. Yussef Dayes - *The Yuessef Dayes Experience (Live From Malibu)* [Spotify](https://open.spotify.com/album/3PGNm2ibnsl8rldfXjQ1wB?si=99571efde492432e)
30. Knoll - *As Spoken* [Spotify](https://open.spotify.com/album/2dTY8QU6yuAngXCGNzxnYj?si=1695117bf5374551)
31. Madi Diaz - *Weird Faith* [Spotify](https://open.spotify.com/album/40hflv8qjnNzLA2ZrFA3yv?si=3e8e773e0cdb444a)
32. Joel Ross - *Nublues* [Spotify](https://open.spotify.com/album/25Dgs9rR8ETpGCwD0wUv0q?si=a5b7a50074044343)
33. Mean Jeans - *Blasted* [Spotify](https://open.spotify.com/album/2W7Xa2zh9NQFeBxA4hQG7a?si=080d3c57125b4994)
34. Shabaka - *Perceive Its Beauty, Acknowlege Its Grace* [Spotify](https://open.spotify.com/album/6vIwPo3D1kZ3ZmlR1fyjm7?si=39e9edd83b4843d7)
35. Necrot - *Lifeless Birth* [Spotify](https://open.spotify.com/album/05kOE4UkPAJW5a0FuRxs4X?si=e72e7baffb844e04)
36. Isaiah Collier & The Chosen Few - *The Almighty* [Spotify](https://open.spotify.com/album/0DucTRALVpidCnE04dmhQe?si=9c711b7f076b486d)
37. Shabazz Palaces - *Exotic Birds of Prey* [Spotify](https://open.spotify.com/album/3PqRZV34l1ePPXXAHYOQYi?si=47a5bbc67e0b4c67)
38. Shellac - *To All Trains* [Spotify](https://open.spotify.com/album/09SFqMvmXLpfG62LjeSorU?si=9f8b30089f464991)
39. Dysrhythmia - *Coffin of Conviction* [Spotify](https://open.spotify.com/album/6LSrtj6Bn4LirMcqGggejy?si=0e2e8c9842ad4e0a)
40. Squid Pisser - *Dreams of Puke* [Spotify](https://open.spotify.com/album/4V4opR2houWJ5eKQ0gfyvM?si=92aeb10e58d44701)
41. Previous Industries - *Service Merchandise* [Spotify](https://open.spotify.com/album/2UuKBgS1IW6XscZCOBGKjZ?si=c16ae248e6da468d)
42. Dave Harrington - *Skull Dream* [Spotify](https://open.spotify.com/album/7rnr8dYQEdyCDKNdCaVQwy?si=68882a8c30284619)
43. JPEGMAFIA - *I Lay Down My Life For You* [Spotify](https://open.spotify.com/album/1ezs1QD5SYQ6LtxpC9y5I2?si=78e23f6215374970)
44. Cobranoid - *s/t* [Spotify](https://open.spotify.com/album/1xUII5eMiyXtccRPpIaawr?si=4d3216ce24fa43df)
45. Astro Can Caravan - *Astral Projections* [Spotify](https://open.spotify.com/album/47BhRzoa8PwTSp8MPsinJO?si=4535b9205ede4a01)
46. The The - *Ensoulment* [Spotify](https://open.spotify.com/album/4vWR7ZQwmXPOSo3lAXS6MA?si=3029d0c5d89640cb)
47. Molchat Doma - *Belaya Polosa* [Spotify](https://open.spotify.com/album/60B6wBzA7SFksiyiNqlYhn?si=5307e21947cf4427)
48. Fig Dish - *Feels Like the Very First Two Times* [Spotify](https://open.spotify.com/album/3SDsBnDhjX1DGZeE6njjBG?si=2ed7e08118eb4c4e)
49. Etran de L'Air - *100% Sahara Guitar* [Spotify](https://open.spotify.com/album/5FhPKVuqLiewzW2W6ds1s9?si=a52d7fe668ed4d7d)
50. Nick Lowe & Los Straightjackets - *Indoor Safari* [Spotify](https://open.spotify.com/album/3wwl5XK7iRAejuGRLLxn5y?si=2f129f907759424d)
51. Darius Jones - *Legend of e'Boi (The Hypervigilant Eye)* [Spotify](https://open.spotify.com/album/2fWR2M8izdIHpX3YwYG2Dy?si=00649a44aafc4ef1)
52. Chat Pile - *Cool World* [Spotify](https://open.spotify.com/album/3k7GhMNdQ0KXGkC84qVRf2?si=bec6cbf0c0e84403)
53. Bunuel - *Mansuetude* [Spotify](https://open.spotify.com/album/6YwnTDVLniYCdelDzDdi9s?si=0292b19b7e154f29)
54. Brigitte Calls Me Baby - *The Future is Our Way Out* [Spotify](https://open.spotify.com/album/2BOwAv2hKNPJW06P3RHScT?si=e6dec1effc804a2e)
55. Cursive - *Devourer* [Spotify](https://open.spotify.com/album/3OjLsoZkOL0HrHQbwdxjJK?si=bb7e427dcb784cc4)
56. Fievel is Glauque - *Rong Weicknes* [Spotify](https://open.spotify.com/album/6PPLCyHL015tBNHY0GQuc0?si=9fd7921017b44641)
57. Body Count - *Merciless* [Spotify](https://open.spotify.com/album/4BMssodRLk2w2YFGGxxGmv?si=15d60c8f73a74188)
58. Oranssi Pazuzu - *Muuntautuja* [Spotify](https://open.spotify.com/album/6cRCD79pIJvu5aeKWVtskd?si=d41fe7bf9ef74e16)
59. Kim Deal - *Nobody Loves You More* [Spotify](https://open.spotify.com/album/6MygjJ2a8narXmZnk9ifKp?si=723ea0badd674279)
60. Opeth - *The Last Will and Testament* [Spotify](https://open.spotify.com/album/1f2Q8urIhVZiuPSiNLcQG6?si=80c27a38c6304bc1)
61. Patricia Brennan - *Breaking Stretch* [Spotify](https://open.spotify.com/album/7AFqBcQrNtRtVEl5HYZKo0?si=9a50bf3aad6749f0)
62. Shabaka - *Possession* [Spotify](https://open.spotify.com/album/1ut2nbC8iiNz4wyd5ftVkl?si=c100ccfbaede4276)
63. Wand - *Vertigo* [Spotify](https://open.spotify.com/album/6sjjo0VgBM2jFk5mIcnGPX?si=0ad159ae49c04be4)
64. Painkiller - *Samsara* [Spotify](https://open.spotify.com/album/4g45iL44GvKLfe2mHNrhkB?si=b7ad0790bcc84e43)
65. Frank Zappa - *Live at the Whiskey A Go Go 1968* [Spotify](https://open.spotify.com/album/0BPJEezMF4wjvW420TParb?si=dfb648885cfe4903)
66. Ed Schrader's Music Beat - *Orchestral Hits* [Spotify](https://open.spotify.com/album/5Qtrzpv1A66fUmaA4Upoje?si=53cc7bdc25bf4322)
67. Kimmi Bitter - *Old School* [Spotify](https://open.spotify.com/album/5y2kLsLCD92fHBH8ze3lYW?si=293f24f89c4c47e7)
68. Jeff Parker - *The Way Out of Easy* [Spotify](https://open.spotify.com/album/3NBhIanqLMEjpwhzLWqHsN?si=IY6GibTxSXGitVXX5rCWYg)
69. Dwight Yoakam - *Brighter Days* [Spotify](https://open.spotify.com/album/6Xc07KxNf7b8vNEfDeWUs3?si=b8ecd606e1634b4a)
70. Kit Downes - *Breaking the Shell* [Spotify](https://open.spotify.com/album/7imWOWOlNlyNaKvOeRFA4G?si=1f4eb4dc9483442a)
71. David Gilmour - *Luck and Strange* [Spotify](https://open.spotify.com/album/5ds8DFWVozMIxRP3qr5Vii?si=aec24aa22b144fde)
72. Ben Arsenault - *Make Way For This Heartache* [Spotify](https://open.spotify.com/album/7G6xv13hUTdJRPo1ClqRCU?si=c51a28c5e8964acb)
73. X - *Smoke & Fiction* [Spotify](https://open.spotify.com/album/2flrGhu7n9y0IMlfmqRQ1F?si=c89bd1e0a98c43b5)
74. Silverada - *s/t* [Spotify](https://open.spotify.com/album/43TqKT6iNf3nYg8WMGnNda?si=135f3f226e6e4857)
75. Diskord & Atvm - *Bipolarities* [Spotify](https://open.spotify.com/album/60xgifi3BEOHG3wBhIjU84?si=86d57dd5f2c44bc3)
76. Nada Surf - *Moon Mirror* [Spotify](https://open.spotify.com/album/50SMq1CBNnS9pykwHqUfCd?si=5b2b0b52c6f84846)
77. Pet Shop Boys - *Nonetheless* [Spotify](https://open.spotify.com/album/4kRlRxwCZ0SeRxvjGgVVaj?si=e2116d478837449a)
78. Thou - *Umbilical* [Spotify](https://open.spotify.com/album/1WILkP95BuckkTIm4OIQRm?si=513ff6b55f424b8e)
79. Vince Staples - *Dark Times* [Spotify](https://open.spotify.com/album/30BFY5VHii4PyWqkTubUWX?si=604dc15227ee4348)
80. The Black Keys - *Ohio Players* [Spotify](https://open.spotify.com/album/4nL9a6F9au7kYG9KuswdNo?si=8309cbef03204506)
