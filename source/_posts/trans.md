---
title: Neil Young - Trans
date: 2020-11-13 10:05:03
tags:
- neil young
- trans
- 70s rock
- new wave
---

Today's pick is the 1982 album from Neil Young called *Trans*.

*Trans* started out as a standard Crazy Horse album, but turned into something much weirder quickly. Neil's son, Ben,  was born with cerebal palsy. Neil believed he could "speak" to Ben through a vocoder. He also recently discovered the [Synclavier](https://en.wikipedia.org/wiki/Synclavier), which was a computer where you could compose music and have it played back to you in a very 80s synthesised way (Frank Zappa also discovered the Synclavier around this time and put out many albums soley using it.) Needless to say, the Crazy Horse band was pretty pissed about how the album eventually ended up turning out.

If you only know Neil Young for his folk-rock or his ragged classic rock sound, you are going to be pretty surprised by how *Trans* sounds! Songs like "Computer Age" and "We R In Control" almost sound like Kraftwerk. 

Neil took this wild sound even further by later teaming up with the band Devo and re-recording "Hey Hey My My" for his completely bonkers movie [*Human Highway*](https://www.amazon.com/Human-Highway-Directors-Cut-DVD/dp/B01CODVHR0), which I also highly recommend.   

Eventually the record label ended up suing Neil soon for putting out records that were "deliberately uncommercial and unrepresentative work". Neil was undaunted at least for a little while and put out other bizarre albums like *Everybody's Rockin* (50s rock), *Landing On Water* (more synth-rock but without the vocodor) and *This Notes For You* (Blues Brothers inspired jump blues) before finally level-setting in the 90s. This is probably my favorite period of Neil Young music though.

Anyways, here's were all that madness began....enjoy *Trans*.

<iframe src="https://open.spotify.com/embed/album/6kHeF6gWAzKji20zAe7e16" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>