---
title: Damu The Fudgemunk - Ccean Bridges
author: Phil Plencner
date: 2020-05-22 14:40:06
tags:
- damu the fudgemunk
- archie shepp
- raw poetic
- free jazz
- hip hop
- rap
- jazz
---

I got a PPP 2-fer for today, since I apparently missed yesterdays pick! Free-jazz icon Archie Shepp just put out a new improv album with with rapper nephew Raw Poetic and vibraphonist Damu The Fudgemunk (I am not making this name up). The album is really hitting the spot today.

Further reading about the project [here].(https://www.washingtonpost.com/entertainment/music/how-jazz-legend-archie-shepp-his-nephew-raw-poetic-and-a-cast-of-dc-musicians-teamed-up-for-an-experimental-improvised-album/)

And as part of my 2-fer here is a playlist I made a while back (originally on [8tracks.com](8tracks.com)! with America themed songs! Happy Memorial Day everyone.

<iframe src="https://open.spotify.com/embed/album/1F7Z8pkrcZrgtpMFOGviH6" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

<iframe src="https://open.spotify.com/embed/playlist/4Zfu1wlyrlKldgUXbSraD8" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>