---
title: Royal Crown Revue - The Contender
date: 2025-01-10 15:20:53
tags:
- royal crown revue
- swing music
- swing revival
- squirrel nut zippers
- big bad voodoo daddy
- jazz
- classic jazz
---

![](TheContender.jpg)

It's hard to believe 30 years after the fact, but in the mid 1990s jazz swing music was immensely popular. There was a full-on "swing revival" with a style of music from the 1930s being played on modern rock radio stations and on MTV.

One of the earliest examples of a band that contributed was Royal Crown Revue. They formed in 1989 (well before the popular 1990s resurgence). They released their first album *Kings of Gangster Bop* in 1991 and basically nobody noticed.

That all changed when they were featured in the Jim Carey movie *The Mask* in 1994. Here is the pivotal scene in the movie of Jim Carey dancing to "Hey Pachuco!" with the band:

<iframe width="560" height="315" src="https://www.youtube.com/embed/iqjq2s_bHPA?si=frP8pReZP-XY3psw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

This basically kick started the "swing revival" trend. Other bands like Big Bad Voodoo Daddy, Brian Setzer Orchestra and Squirrel Nut Zippers soon followed suit (in their zoot suits). It reached critical mass by 1996 when the movie *Swingers* was released and then was on a downward trend. By 1999 the revival was essentially over. 

Royal Crown Revue was there every step of the way. They released their 2nd album *Mugzy's Move* around the same time as *Swingers*. It included a re-recorded version of "Hey Pachuco!" along with "Zip Gun Bop" from *Kings of Gangster Bop*. The latter of which they made a music video for:

<iframe width="560" height="315" src="https://www.youtube.com/embed/lqvLsLew-3Y?si=E6YGigwf1tlh1W2B" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

It also included the bonkers song "Barflies on the Beach" which is about exactly what the title implies. They made a wild music video for that too, which includes a ton of funny footage of the band frolicking on a beach plus footage of them playing in concert (showing people crowd surfing, of all things!):

<iframe width="560" height="315" src="https://www.youtube.com/embed/pIB8pk48cfY?si=EOQMmjaSqG5PHs3t" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

While the band is musically great, there was a lot of kitsch in their approach (see: "Barflies on the Beach").

For their follow-up record in 1998, *The Contender*, they brought a more serious approach. That album is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

I don't believe they made any music videos for the record, because by then major label interest in swing was waning which is a shame. The title track is a high-energy, badass swing burner. Here they are performing it soon after the release of *The Contender*:

<iframe width="560" height="315" src="https://www.youtube.com/embed/IGHEVpXpCb8?si=RNGG9dqH4wu36t7X" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Other highlights of the album include their covers of "Stormy Weather" and "Salt Peanuts" which showcase how they were trying to seriously pay homage to the original swing era.

<iframe width="560" height="315" src="https://www.youtube.com/embed/vOCLzf1hXm0?si=YUEipH5ID3DgEaIW" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/vLLkQqHcsoY?si=4LAreIv9HqSSmM0D" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

The original songs were also great. One example is the powerful lament about early closing times at the bar called "Big Boss Lee". I can certainly relate to their plight!

<iframe width="560" height="315" src="https://www.youtube.com/embed/SZ3Y1wi_x00?si=dTeavlHAO2MTaxMW" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Another highlight was a mid-tempo shuffle of "Walking Like Brando". This brings back some of the aforementioned kitsch but I think it keeps it mostly in check:

<iframe width="560" height="315" src="https://www.youtube.com/embed/X3P6Er1CLRE?si=rUQqEzJP-hVd4sLC" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

They supported *The Contender* by taking part in the Van's Warped Tour. Their drummer Daniel Glass has an excellent podcast and [a couple of the episodes goes into detail about their time on the Warped Tour](https://www.drummersresource.com/338-daniel-glass-show-life-warped-tour-pt-1/).

Royal Crown Revue only put out one more album (1999's *Walk On Fire* on the independent record label SideOneDummy). It didn't capture the magic like *The Contender* did and they stopped releasing any new material. They toured for a while afterwards on the festival circuit but eventually disbanded.

Speaking of Daniel Glass, he continues to regularly perform with his own groups (in fact, he is part of the house band at the Birdland Jazz Club in New York City). He is an excellent drummer, and he was a big part of why I enjoyed Royal Crown Revue's music the most out of all the swing revival bands. 

Here is Daniel Glass with his band performing in Germany in 2023:

<iframe width="560" height="315" src="https://www.youtube.com/embed/DUi7Qq6KblM?si=cm3UR3A7o4SHSgwg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Here is an awesome drum solo performance that's part of an instructional video he released:

<iframe width="560" height="315" src="https://www.youtube.com/embed/4QOoam_yJUc?si=hjktC_bbQjxD4tfk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

With the current cold, dark winter days seemingly never ending, I find the music of Royal Crown Revue, especially *The Contender* warming me up and keeping my mood positive. Hopefully it will do the same for you as well!

{% iframe 'https://open.spotify.com/embed/album/3QPXxTgPpNzLREqSs9WtyX?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B00PKXBQAA/?id=0DwhlYfhAJ&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/ca/album/the-contender/281705950' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/1785391' '100%' '96' %}

