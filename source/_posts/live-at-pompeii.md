---
title: Pink Floyd - Live At Pompeii
author: Phil Plencner
date: 2020-04-23 15:47:06
tags:
- pink floyd
- 70s rock
- classic rock
- space rock
---
Right before Pink Floyd started recording *Dark Side Of The Moon* (and becoming mega mega mega rock stars), they were a really wild space-rock band. It culminated with their live performance in the ruins of Pompeii in 1971. The [documentary  / concert film](https://en.wikipedia.org/wiki/Pink_Floyd%3A_Live_at_Pompeii) is one of my favorite documents of a live band in action in their prime.

About 12 years ago I had the opportunity to visit Pompeii and made a point to spend time just sitting in the old amplitheatre there and just imaging what is was like there when Pink Floyd filled the air with their music.  (I guess other historic stuff happened in Pompeii too).

It's worth tracking down the concert film, but Pink Floyd also had a massive rarities boxed set release series a few years ago and one of the sets had the complete audio recording of Live In Pompeii. 

This is today's pick (Disc 2 of the set).  

Disc 1 is also great because it contains studio sessions from the excellent Obscured By Clouds movie soundtrack.

<iframe src="https://open.spotify.com/embed/album/5xhBf9DKzndQhi1CSu0W7q" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>