---
title: Rick Wakeman - White Rock
date: 2022-02-04 08:27:13
tags:
- rick wakeman
- yes
- winter olympics
- progressive rock
- 70s rock
- soundtracks
---

Since the 2022 Winter Olympics opening ceremony is today, it’s a fine time to listen to *White Rock*: Rick Wakeman’s soundtrack to the film of the same name showcasing the 1976 Winter Games in Austria.

After Rick Wakemen left Yes in 1974, he was enjoying success with his high-concept solo albums such as *The Six Wives Of Henry VIII* and *Journey To The Centre Of The Earth*. However, he was also hired to score movie soundtracks such as the absolutely bonkers *Lisztomania* in 1975 and *White Rock* in 1977.

The film *White Rock* was narrated by James Coburn and directed by Tony Maylam. It depicts footage from the Winter Olympics focused on six of the events.

Nowadays the film is hard to find, but there are some snippets of it on YouTube such as this one:

<iframe width="560" height="315" src="https://www.youtube.com/embed/K2Jhe95HFJA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Tony Maylam also directed a concert film of the band Genesis called *Genesis: In Concert*. In the U.K. it was released as a double-feature with *White Rock* in theaters. That's a lot of entertainment!

As far as the soundtrack, it is almost entirely performed solo by Rick Wakeman using basically a warehouse full of pianos and synthesizers. Along for the ride is a choir and Wakeman's long-time drummer and buddy Tony Fernandez. It is uniformly excellent, especially if you're a fan of Rick Wakeman's other albums from the period.

The album is also difficult to find (on Spotify it is part of the 3-disc compilation *The Journey*) so I'll just include the *White Rock* songs here in a playlist by themselves.

My [well-worn copy of the original vinyl](https://www.instagram.com/p/CZjj2wcu7hs/) is what I am enjoying today.

<iframe src="https://open.spotify.com/embed/playlist/0BgKVOjS1Hn3nmgdErpRx4?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>








