---
title: Billy Cobham - Inner Conflicts
author: Phil Plencner
date: 2020-05-18 14:52:42
tags:
- billy cobham
- jazz fusion
- miles davis
- mahavishnu orchestra
- ruth underwood
- john scofield
- shiela e
- frank zappa
---
Over the weekend one of my favorite jazz-rock drummers Billy Cobham celebrated his 76th birthday. He first came into national prominence appearing in Miles Davis' awesome fusion bands in 1970 (*Get Up With It*, *Tribute to Jack Johnson*) but really blew the roof off when he joined the Mahavishnu Orchestra. (maybe a future PPP!) 

Soon after he left MO, he assembled amazing bands and released solo records. His first album, *Spectrum*, remains the high water mark and is one of my all-time favorite jazz-rock albums. However, during the course of my celebrating his recorded works this weekend I revisited *Inner Conflicts* for the first time in years and it blew my mind!!  

The first track is a weird out-of-character space-rock odyssey with drums and crazy synths that he played / programmed solo. Worth the price of admission right there! 

But the other tracks are burning fusion tracks featuring all-star lineups including  Zappa band member Ruth Underwood, guitarist extraordinaire John Scofield (a previous PPP!) and pre-Prince Shelia E!!! Don't sleep on this one. It's a banger.

<iframe src="https://open.spotify.com/embed/album/4P07dFFEn9DI61mkB3PFOG" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>