---
title: Grand Ulena - Gateway to Dignity
date: 2023-02-24 13:33:49
tags:
- grand ulena
- gateway to dignity
- wilco
- dazzling killmen
- yowie
- jeff tweedy
- math rock
- progressive rock
- avant-garde rock
- noise rock
---

![](GatewayToDignity.jpg)

In the 1990s and early 2000s the Midwest was a breeding ground for a ton of math rock and progressive rock bands. This was especially so in St. Louis Missouri. 

Dazzling Killmen was a big part of that scene. Their hybrid of progressive rock, punk and jazz was highly influential if not exactly well known. They broke up shortly after putting out their masterpiece *Face of Collapse* in 1995. 

Their bassist, Darin Gray, was a huge part of their heavy sound. One of the projects he started after Dazzling Killmen was a very bizarre instrumental power trio of sorts called Grand Ulena.

Grand Ulena also consisted of drummer Danny McClain and guitarist Chris Trull. Their music is self described in the following ways, which I think is pretty accurate:

"Grand Ulena is disjunct rhythms played at high velocity. Disjunct beats played under broken and jagged melodies. Complex structures combined with complete and utter failure. Failed soloing combined with rigid riffery. Extended instrumental techniques forged into song."

Their music may sound completely random and improvised, but this was far from the truth. Their music was almost completely thorough composed with very little repetition.  

After a period of very intense rehearsals, they hit the road. They played Chicago pretty frequently and I was lucky enough to witness their astonishing music firsthand several times, including this show at The Fireside Bowl in November 2002:

<iframe width="560" height="315" src="https://www.youtube.com/embed/TfGbnKLbZAY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

In early 2003, they released their only studio album *Gateway to Dignity* which is today's pick! It also happens to be one of my favorite rock albums of all time, so I'm kind of surprised it took me so long to make this an official PPP!

The album contains much of the same material that was performed live. It is an incredible document of a unique vision by three people completely committed to the concept. The amount of force power and precision they put into their playing (especially the drumming) is something seemingly from another world. 

The Flying Luttenbacher's drummer Weasel Walter describes the sound of the album in this way:

"Grand Ulena masterfully bridge the gap between the uber-angular non-repetitive hyperstructuralism of the most rigid, martial modern classical composition and the traditional rock and roll power trio format while leaving everybody wondering what the hell just happened."

After *Gateway to Dignity* Grand Ulena went into another series of intense writing and rehearsing sessions. Reportedly practice sessions lasted up to ten hours, and months of writing and rehearsing resulted in less than a minute of new material. 

Sadly, they never continued their vision. Bassist Darin Gray went on to perform with a wide range of artists, notably including Wilco guitarist Jeff Tweedy's solo group and with Wilco drummer Glenn Kotche in the group On Fillmore. Guitarist Chris Trull went on to join the bands Yowie and Terms who continue to push the limits of what a rock band can do. Unfortunately, [drummer Danny McClain passed away in 2011](https://www.riverfronttimes.com/music/danny-mcclain-grand-ulenas-relentless-drummer-played-hard-and-died-young-2493545). 

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/album/7FU0Re4NQPLudOcLTx20ug?utm_source=generator" width="100%" height="352" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

<iframe id='AmazonMusicEmbedB00IFMV80G' src='https://music.amazon.com/embed/B00IFMV80G/?id=9dofG9nUp1&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *; clipboard-write" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/gateway-to-dignity/820819483"></iframe>

<iframe src="https://embed.tidal.com/albums/26417118" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>