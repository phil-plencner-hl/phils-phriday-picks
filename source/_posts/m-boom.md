---
title: Max Roach - M'boom
date: 2022-05-27 10:45:05
tags:
- max roach
- m'boom
- art blakey
- jazz
- hard bop
- 1960s jazz
- 1970s jazz
- percussion ensemble
---

![](mboom.jpg)

Max Roach has primarily been remembered as a ensemble player with various hard-bop jazz bands (including playing with Charlie Parker, Sonny Rollins, Miles Davis, Theloneous Monk and probably dozens more not to mention his own ensembles). However, he is also notable for his focus on percussion-based music and melodic drum solos, often using a bare-bones setup to perform complex, yet memorable, compositions.

This was pretty unique and forward thinking in the 1950s and 1960s. Drummers were mostly relegated to the background until players like Buddy Rich, Art Blakey and himself came along. They even soloed together in "drum battles" to showcase their skills!

To whet your appetite for today's pick, I'm going to showcase a few examples.

In 1959, Buddy Rich and Max Roach recorded *Rich Vs. Roach*. Along with some ensemble songs there was this drum battle called "Figure Eights":

<iframe width="560" height="315" src="https://www.youtube.com/embed/sOmMTGI9KkY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

In 1965 Max Roach released *Drums Unlimited* which included perhaps his most famous solo called "The Drum Also Waltzes":

<iframe width="560" height="315" src="https://www.youtube.com/embed/_AS71j5nysw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Here is is performing it much later in 1994, preceded by a very musically interesting drum solo on Italian Television:

<iframe width="560" height="315" src="https://www.youtube.com/embed/gVdMbbrnv2A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The internet is full of incredible footage of Max Roach in action. Here is another classic drum battle from 1968 with Elvin Jones and Art Blakey:

<iframe width="560" height="315" src="https://www.youtube.com/embed/a24coCirun4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

And finally, in the less is more category, here is Max Roach performing a solo with only a high-hat in 1994:

<iframe width="560" height="315" src="https://www.youtube.com/embed/FHbqzX1zokk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

In the mid 1970s Max Roach went all-in on this concept by forming a percussion ensemble called M'Boom. Not just featuring drum sets, the group also utilizes vibraphone, marimba, xylophone, timpani, gongs and much more. Members of this all-star group included a who's who of jazz drummers such as Roy Brooks, Joe Chambers, Omar Clay, Fred King, Warren Smith and Freddie Waits along with Max Roach. They all wrote material along with playing their instruments.

Here is some live footage of them in their prime in 1979:

<iframe width="560" height="315" src="https://www.youtube.com/embed/G5KbIUq1L7M" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Around that time is when they released their self-titled second record...it is what I consider to be their best album. It is today's pick!

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/album/0uEFetfiIICfto8mXTWhvj?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

<iframe id='AmazonMusicEmbedB001BKLN7W' src='https://music.amazon.com/embed/B001BKLN7W/?id=a7Ha7uMHiJ&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *; clipboard-write" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/mboom/192761853"></iframe>

<iframe src="https://embed.tidal.com/albums/5011844" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>





