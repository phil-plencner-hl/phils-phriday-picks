---
title: Captain Beefheart & His Magic Band - Trout Mask Replica
date: 2021-08-13 07:39:47
tags:
- captain beefheart
- trout mask replica
- frank zappa
- ahmet zappa  
- john french
- drumbo
- bizarre straight records
- third man records
---

*Trout Mask Replica*. This is a Phil's Phriday Pick that has been a long time coming.

It is finally at the top of my queue because after being a long-time digital music holdout, Ahmet Zappa [recently announced it would finally be coming to streaming services](https://www.rollingstone.com/music/music-features/trout-mask-replica-captain-beefheart-streaming-1193345/)!

Of course, *Trout Mask Replica* is notorious for being an unlistenable mess. However, I retort that while it is very impenetrable to the first-time listener (It doesn't help that the album opens with "Frownland" basically throwing you right into the lion's den...nor does it help that is is a double-album, 80+ minute listening experience), it is actually a very complex work with tons of amazing lyrics and musicianship throughout.

Much has been written about *Trout Mask Replica* over the years. A lot of it good, and a lot of it bad. It is well-documented that Captain Beefheart (aka Don Van Vliet) was very much a cult leader / tyrant who ruled the Magic Band with an iron fist during the rehearsing and recording of this album. Drummer John French's (aka Drumbo) book [*Beefheart: Through the Eyes of Magic*](https://www.amazon.com/Beefheart-Through-Magic-John-French/dp/0992806232/ref=sr_1_1?dchild=1&keywords=beefheart+through+the+eyes+of+magic&qid=1628855656&sr=8-1) is a harrowing look at his entire time as a member of The Magic Band and specifically has a lot of gory details on the Trout Mask Replica period. It is worth taking the deep-dive for the interested listener.

Guitarist Bill Harkleroad (aka Zoot Horn Rollo) also wrote an interesting book on the period called [*Lunar Notes*](https://www.amazon.com/Lunar-Notes-Captain-Beefheart-Experience/dp/1908728345/ref=sr_1_fkmr1_1?dchild=1&keywords=bill+harkelroad+beefheart&qid=1628855752&sr=8-1-fkmr1) that is worth checking out.

Of course, I own copies of both of these books and more and return to them often for inspiration and deep research.

This is quite possibility my favorite album of all time. In fact, the entire Captain Beefheart catalog has been a decades long obsession of mine (my longtime personal email address actually references Captain Beefheart).


How did my infatuation come to be? It all started back in high school in the early 90s, where my buddy Ben had a cassette called [*Captain Beefheart At His Best*](https://www.discogs.com/Captain-Beefheart-At-His-Best/release/3298812). He hated it and gave it away to me (Famous Quote: "If This is him at his best, I'd hate to hear him at his worst!").

*Captain Beefheart At His Best* I later found out was a budget (bootleg?) re-release of The Magic Band's first album *Safe As Milk*. 

I enjoyed it immensely. A really warped blues band, with slide guitar (by a young Ry Cooder!) and drums that sometimes sounded like they were falling down the stairs. Topped by a signer with a humungous voice! I was hooked!!

Later, I attended a party with an older music obsessive who had a vinyl copy of *Trout Mask Replica*. He refused to play it, but went at great length about how strange and "out" it was. I needed to hear this!!

A couple years later, after I went to college I found [a copy of the original pressing on Frank Zappa's Straight Records](https://www.instagram.com/p/ByxYCK9AgcQ/) at a flea market. I haggled the seller down to $4 and took it home with me. 

I couldn't believe my ears when I heard what came out of my speakers that fateful afternoon. It was a completely over-the-top spectacle. Many of the elements of *Safe As Milk* remained, but were taken to their absolute extremes. Plus there was wild, free-jazz inspired saxophone blurting throughout the whole thing. Also, there were little spoken-word interludes and studio chatter which added to the mystery of the whole thing.

Ever since that day in 1994, a week probably hasn't gone by where I didn't listen to songs from this album.

I'm so obsessed that at one point I transcribed all the drum parts on the album and learned to play most of it in hopes of starting a *Trout Mask Replica* cover band (it sadly never materialized). John French's solo album [*O Solo Drumbo*](https://open.spotify.com/album/2JLv2CLR99A6GQfmEGEeTJ?si=74m8sWf_SOeRgGSckiqxPQ&dl_branch=1) definitely helped in my efforts at that time. 

I also have since bought the album on CD (twice...one an original release, one a remastered re-release). I also recently bought the amazing [re-release put out by Jack White's Third Man Records that came with all kinds of cool extras](https://www.instagram.com/p/BkoLXjbnYKe/) like a bag and a cardboard mask! (The re-release sounds awesome, and I believe that is the same version that Spotify has online).

I also have the [*Grow Fins* box set of outtakes](https://www.discogs.com/Captain-Beefheart-His-Magic-Band-Grow-Fins-Rarities-1965-1982/release/819304) (including full-on rehearsal tapes which are mindblowing) that I listen to on the regular. 

I listen to it so much, I own the [*Trout Mask Replica* rehearsals portion of the box on vinyl](https://www.instagram.com/p/BnduRRfnW9i/) as well!

I even have a [cookbook with a Captain Beefheart inspired recipe](https://www.instagram.com/p/BhUfImtHawF/) (although I never made the food).

Ok, enough of me going through my *Trout Mask Replica* collection, you NEED to hear the album if you haven't already. Dive in, and maybe you'll become as obsessed as I am! 

<iframe src="https://open.spotify.com/embed/album/4dgAnIHFpnFdSBqpRZheHq" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>