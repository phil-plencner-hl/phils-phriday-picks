---
title: David Murray, Ray Angry, Questlove - Plumb
date: 2023-08-11 09:13:14
tags:
- david murray
- ray angry
- questlove
- the roots
- world saxophone quartet
- elvis costello
- hip-hop
- jazz fusion
- avant jazz
- jazz rock

---

![](Plumb.jpg)

Questlove is someone who basically needs no introduction. Aside from his role as drummer and musical director of The Roots (including being part of Jimmy Fallon's house band) he is also a famous DJ, [podcaster](https://en.wikipedia.org/wiki/Questlove_Supreme), record producer and [Soul Train aficionado](https://www.nytimes.com/2020/03/03/arts/music/questlove-favorite-things.html) to name a few of his many accolades.

What is perhaps lesser known is his association with people in the jazz avant garde. Sure, there are jazz musicians featured on records by The Roots (especially early records like *Do You Want More?!!!??!*) but he also plays on records that are strictly in the jazz realm.

One earlier example was an album called *The Philadelphia Experiment*. This album featured a trio including pianist Uri Caine and bassist Christian McBride. An excellent record that saw them playing originals along with cool covers of Sun Ra ("Call For All Demons"), Elton John ("Philadelphia Freedom") and Grover Washington ("Just The Two Of Us"). 

<iframe width="560" height="315" src="https://www.youtube.com/embed/mjeZ-VSFbqI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

So it should come as no surprise that a new collaboration album has been recently been released featuring Questlove playing with another jazz great, saxophonist David Murray, and keyboardist Ray Angry. The record is called *Plumb* and is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

David Murray is most famous for being a founding member of the World Saxophone Quartet, which formed in the late 70s. They were an incredible ensemble. Here is some classic footage of them in 1987: 

<iframe width="560" height="315" src="https://www.youtube.com/embed/ypn-NCvN9qY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Of course, David Murray also has long been associated with The Roots. One song from *Illadeph Halflife* is named after him and obviously features him: "Dave Vs Us":

<iframe width="560" height="315" src="https://www.youtube.com/embed/AhsuCE3lRcI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Ray Angry has been a member of The Roots since about 2010, featuring heavily on all their albums since then. This includes their collaboration with Elvis Costello *Wise Up Ghost* in 2013. Here is a cool video of them playing "Wake Me Up" from that album: 

<iframe width="560" height="315" src="https://www.youtube.com/embed/ytx7gHTspqw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

So what does *Plumb* sound like? To me it sounds like heavy jazz fusion from the early 1970s. Think of Miles Davis' *Get Up With It* or *Tribute To Jack Johnson* and you're in the right ballpark.

The band really stretches out on *Plumb*. In fact, the 4 of the first 5 songs are approximately 15 minutes apiece! The whole record clocks in at a massive 2 hour and 15 minute runtime...so there is definitely a lot to absorb here. 

Along with songs playing as the complete trio, there are also shorter pieces which feature solos from one member. "Pleiades" is solo synths from Ray Angry, "Love" is a Questlove drum solo, and "False Dawn" is a sax solo to name a few examples. This really helps showcase each individual member of the group and highlight what they bring to the overall ensemble.

JMI Recordings is releasing *Plumb* as [a massive 4-LP box](https://www.jmirecordings.com/the-jmi-record-store/david-murrayquestloveray-angry-plumb-4-lp-box-set) set early next year and I am seriously considering purchasing this. 

In the meantime, I'm going to just immerse myself in their heady sound universe. There is certainly a ton to explore throughout the entire album. 

{% iframe 'https://open.spotify.com/embed/album/6VB4Hd7UflAqmq8eH4CKsf?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B0C9NXPV7G/?id=RitYSVGlyK&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/plumb/1694800690' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/302231084' '100%' '96' %}
