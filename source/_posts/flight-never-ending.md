---
title: Mingo Lewis - Flight Never Ending
author: Phil Plencner
date: 2020-07-21 15:50:01
tags:
- mingo lewis
- flight never ending
- jazz rock
- progressive rock
- jazz
- santana
- al dimiola
- john mclaughlin
---

Today's pick is a pretty amazing (yet pretty obscure) 70s jazz fusion record called "Flight Never Ending" by percussionist Mingo Lewis. 

Mingo came into prominence by appearing on Santana's *"Caravanserai"* album in 1972. This is when much of the original Santana band split because it was becoming too fusion focused (Neal Schon went on to form Journey soon after, for example). 

Mingo stuck around with Carlos Santana as he went further into wild jazz territory with *"Love, Devotion, Surrender"* (the John Coltrane tribute collaboration with guitarist John McLaughlin). 

After this more "mainstream" (lol) stint Mingo joined Al Di Meola's band (who had just quit Chick Corea's Return To Forever band..one of my all-time favorites and potentially a future PPP). They put out several albums together, that are all fusion classics...but when Mingo quit THAT band (confused yet?) he quietly released "Flight Never Ending".  

A completely killer slice of 70s jazz rock that takes even the stuff Di Meola did futher out in to outer space (a couple songs Mingo composed that were played by Al Di Meola were re-recorded here and are better versions imho). The other players on the album are kind of shrouded in mystery as they may have been aliases of more famous musicians who were locked into other record contracts. ANYWAYS, enough obscure jazz rock history for today...enjoy this slice of awesomeness!

<iframe src="https://open.spotify.com/embed/album/7MZVT9btUJPVl7G1bXb85r" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

