---
title: Luke Schneider - Altar Of Harmony
author: Phil Plencner
date: 2020-08-04 15:17:17
tags:
- luke schneider
- altar of harmony
- pedal steel guitar
- country music
- third man records
- jack white
- margo price
- robert fripp
---

Luke Schneider is the current pedal steel guitar player in Margo Price's band. 

As part of that newfound success, he released an amazing solo album on Jack White's Third Man Records imprint called *"Altar Of Harmony"*. It's played on a 1960s pedal steel guitar, but sounds all ambient and spacy. Like a Robert Fripp Soundscapes album or something! Totally awesome!!

<iframe src="https://open.spotify.com/embed/album/5sQtsTwdInp00bd7qqOZSJ" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>