---
title: Mac McAnally - Once In A Lifetime
author: Phil Plencner
date: 2020-08-28 14:48:41
tags:
- mac mcanally
- jimmy buffett
- kenny chesney
- the coral reefer band
- americana
- country rock
---

Programming note: I have switched the format of PPP back to its original intent. Friday's only! Today is Friday, so here comes the pick!

Mac McAnally released a new album this month, which always gets my attention. For the uninitiated he is primarily a songwriter and writes songs for popular country stars ("Down The Road" by Kenny Chesney is probably the most well-known example). 

He has also been a member of Jimmy Buffett's Coral Reefer band since the early 90s...and has been Buffett's songwriting partner during that whole time.

His solo albums are few and far between but uniformly excellent. *"Once In a Lifetime"* is no exception.

<iframe src="https://open.spotify.com/embed/album/3ehKE8T72sMLO0epU7OiEc" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>