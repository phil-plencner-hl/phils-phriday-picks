---
title: Woodstock 1969 Complete
author: Enrique Diaz
date: 2019-08-16 15:34:57
tags:
- woodstock festival
- classic rock
- 60s rock
---

This weekend marks the 50th anniversary of Woodstock, when more than 400,000 fans convened at Max Yasgur’s farm near Bethel, New York, for a music festival that would come to define not only the era, but the entire ethos of music festivals to come. A mammoth 38-CD/1-Blu-ray “Definitive Archive” called Back to the Garden was released recently to [considerable acclaim](https://www.theguardian.com/music/2019/aug/15/woodstock-50th-anniversary-box-set-38-discs), but until you can get your hands ears on that, here's a Spotify playlist of the proceedings.

<iframe src="https://open.spotify.com/embed/playlist/6Bt59OkgCZI5I2sOK2QHCP" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>