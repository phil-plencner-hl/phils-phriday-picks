---
title: Shudder To Think - Pony Express Record
date: 2022-03-04 13:32:50
tags:
- shudder to think
- craig wedren
- anna waronker
- nathan larson
- yellowjackets
- 90s rock
- hardcore
- alternative rock
- dischord records
---

Because of all the recent hype about the Showtime series *Yellowjackets* and the musical score by Craig Wedren and Anna Waronker I have spent time revisiting Wedren's previous rock band Shudder To Think.

Shudder To Think were a post-hardcore band from D.C. that was signed to Ian MacKaye's (Fugazi) Dischord Records for a while in the early 90s. They gained a lot of acclaim and popularity in that scene over the course of 3 albums.

For me, it really started falling into place when guitarist Nathan Larson joined the group. As part of the alternative rock boom of the era, Epic Records ended up singing Shudder To Think and they released the excellent *Pony Express Record* soon afterwards.

The first time I heard anything by Shudder To Think was on MTV when they aired the video for "X-French Tee Shirt". It was a pretty wild song and video to get that kind of exposure.

<iframe width="560" height="315" src="https://www.youtube.com/embed/0eLnhDzf6p8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The song has strong dynamics, stuttering drum patterns, a crooning singer and an overall weird vibe. It also boasted a very catchy chorus that they basically repeated into infinity at the end of the song.

I quickly went out and bought *Pony Express Record*. Overall, it is a very strange, yet intoxicating variant on the mid-90s alternative sound. It is practically math-rock with the amount of unusual time signatures contained in the songs. Mix that with unprededictable, dissonant guitar scronk and funk, plus operatic vocals and it all tied together. To say this was a mind-bender is an understatement. How Epic thought it would be able to sell boatloads of this record remains a mystery to me, but I'm glad they funded this masterpiece.

The album starts of on the right foot with "Hit Liquor" and basically careens around for nearly an hour. I didn't even know there was a video for "Hit Liquor" until recently, so here's that!

<iframe width="560" height="315" src="https://www.youtube.com/embed/V5VLhwR4Y14" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

They even included a completely bizarro cover of "So Into You" originally by Atlanta Rhythm Section just to make sure you have no idea where they are coming from.

Basically, *Pony Express Record* is a criminally underrated musical feast, which is why it is today's pick.

Unfortunately, Shudder to Think only put out one more record after this one before breaking up.

Nathan Larson and Craig Wedren have both moved on to very successful careers in movie and television soundtracks. 

<iframe src="https://open.spotify.com/embed/album/3uHtYF4M8rF1KuDgQpwIrH?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

<iframe id='AmazonMusicEmbedB00138F8WS' src='https://music.amazon.com/embed/B00138F8WS/?id=BVF85TBjvO&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/pony-express-record/181613925"></iframe>