---
title: Ministry - Live Necronomicon
author: Phil Plencner
date: 2020-03-25 16:04:51
tags:
- ministry
- bill reiflin
- r.e.m.
- nine inch nails
- king crimson
- 90s rock
- alternative rock
- industrial rock
---
It was announced late yesterday that Bill Reiflin, another major drumming influence of mine, passed away after a long battle with cancer.
 
 He had quite a lengthy and wide-encompassing career. Playing in Ministry, R.E.M., Nine Inch Nails, and most recently King Crimson. 
 
 As a rebellious youth, I was slack-jawed watching the Ministry home video *"In Case You Didn't Feel Like Showing Up"* which documented their 1989 tour. I couldn't believe the power and precision from the two drummers, including Reiflin. 
 
 A year or so later, I went to see a Ministry side-project (let's just call them RevCo) which also featured him and it was a life altering experience! 
 
 The last time I saw him perform was in 2014 during his last U.S. tour with King Crimson.

Today's pick is the fairly recently released live album by Ministry, which includes the full set of the 1989 Chicago show originally released in truncated form on *"In Case You Didn't Feel Like Showing Up"*.

<iframe src="https://open.spotify.com/embed/album/3zrVq89B44Nm50F1T67m0Y" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>