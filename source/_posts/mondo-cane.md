---
title: Mike Patton - Mondo Cane
author: Phil Plencner
date: 2019-06-14 15:49:24
tags:
- mike patton
- faith no more
- italian music
- progressive rock
- alternative rock
- ennio morricone
---
Since I'm heading to Italy next week, it feels appropriate to revisit the Mike Patton (Faith No More / Mr. Bungle / Fantomas etc) record Mondo Cane today. 

Amazing covers of '50s & '60s Italian pop songs with a 40 piece orchestra! 

Bellissimo!

<iframe src="https://open.spotify.com/embed/album/5uxM1YuInVCNCFKKx18qKj" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

BONUS: Mr. Bungle's excellent album [California](https://open.spotify.com/album/6QAKD1wqCrmkBYw1AsZfEy?si=alagQq7_RPqkqI1qa9QPSg) turns 20 years old, so let's revisit that masterpiece as well

BONUS #2 (Since I there will be no Phil's Phriday Picks for the next two weeks!): I'm celebrating the fact that my all-time favorite band, King Crimson, has finally been releasing their albums on Spotify! 
So, I'd like to highlight their excellent 2003 album [The Power To Believe](https://open.spotify.com/album/37yMnX5TiSEOZWTwWBSgf2?si=MNCeB7w-QBKLtxAdquUWGA), which features the "double duo" lineup of Fripp / Gunn / Mastelotto / Belew. Mathy and aggressive progressive rock, just how I like it. Newly remastered for maximum impact!
