---
title: Yes - Drama
author: Phil Plencner
date: 2020-10-16 08:45:22
tags:
- yes
- the buggles
- 70s rock
- progressive rock
---

Did you know that Yes included members of The Buggles (after they had recorded "Video Killed The Radio Star"!) for a short period of time?  

After the *Tormato* tour in 1979 singer John Anderson and Keyboardist Rick Wakeman disagreed with the direction of the band and quit. The roles were filled by Trevor Horn and Geoff Downes by way of a mutual acquaintance, manager Brian Lane. 

Yes' new sound was a really wild mixture of progressive rock, punk and new wave...and resulted in the *Drama* album. They never really sounded like this since...and until 2011's *Fly From Here* it was the only Yes album in which Jon Anderson didn't sing.

This video, really ecapsulates the sound and era of Yes. If you enjoy this, you'll likely enjoy the whole album:

<iframe width="560" height="315" src="https://www.youtube.com/embed/vzxZzIiO84Y" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>  

After touring behind Drama Trevor Horn questioned his singing ability compared to Jon Anderson and decided to strictly go into music production. Howe and Downes went on to form Asia, effectively ending the band. That is, until Jon Anderson met up with Trevor Rabin to form Cinema (which turned into Yes) and had a huge hit with "Owner of A Lonely Heart". But that's a topic for another day.

Enjoy the unique slice of Yes / Progressive Rock history that is *Drama*!

<iframe src="https://open.spotify.com/embed/album/7pIdphNSHXEjdXdThmgOOb" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

