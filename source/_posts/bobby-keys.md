---
title: Bobby Keys - Bobby Keys
date: 2023-11-12 19:42:39
tags:
- bobby keys
- rolling stones
- keith richards
- george harrison
- the beatles
- classic rock
- blues rock
- instrumental rock
---

![](BobbyKeys.jpg)

Bobby Keys was a saxophone player who appeared with a who's who of classic rock musicians over the years, but he was mostly known for his contributions to The Rolling Stones albums from the early 70s (*Let It Bleed*, *Sticky Fingers*, *Exile On Main Street*, *Goats Head Soup* etc). 

He first met The Rolling Stones in the mid 60s when they played a show together (Bobby Keys was touring with singer Bobby Vee). He really hit it off with Keith Richards and they became fast friends. This eventually lead to him playing with The Rolling Stones on record and on tour.

One of his most famous solos is the one on "Brown Sugar":

<iframe width="560" height="315" src="https://www.youtube.com/embed/Bar7SzNLnY0?si=PpV6fff4QvNUpP_z" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Probably my favorite Bobby Keys solo with the stones was on "Can't You Hear Me Knocking":

<iframe width="560" height="315" src="https://www.youtube.com/embed/nkQ0LhcTNsY?si=wwae4rceAwe1gleS" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Part of the reason why he got along so well with Keith Richards was because they were both hard partiers. The most famous example of their antics was when Bobby Keys and Keith Richards threw a hotel TV out of the window:

<iframe width="560" height="315" src="https://www.youtube.com/embed/SOA3cJ8Y2As?si=R6vEeSIe3UEZGd3c" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Along with The Rolling Stones, Bobby Keys recorded with dozens more artists, most notably George Harrison, John Lennon, Joe Cocker, Harry Nilsson and Eric Clapton.

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

Bobby Keys was part of the famous "Apple Jam" sessions that appeared on George Harrison's *All Things Must Pass*, especially on "Out Of The Blue":

<iframe width="560" height="315" src="https://www.youtube.com/embed/VVO8g_OOSaQ?si=LHFthbA2-4XCOHxm" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

In fact, around the same time as the "Apple Jam" Bobby Keys recorded his own solo record with many of the same players who appeared there. It was his only solo record, released in 1972. His self-titled album is today's pick!

Though uncredited at the time of release, we now known that there were plenty of famous musicians playing on the record: George Harrison, Ringo Starr, Eric Clapton, Nicky Hopkins (who really shines throughout) and Klaus Voorman. 

So what does it sound like? Completely instrumental rock / funk with really amazing horn parts, fantastic solos (especially from Clapton, Hopkins and Keys) and an overall sense of fun. You can tell everyone there was really enjoying playing the tunes. The only problem is, at 33 minutes, the record is way too short! You have to wonder what gems were left on the cutting room floor.

Bobby Keys passed away in 2014. One of his public performances was at Rock and Roll Hall of Fame's tribute to The Rolling Stones in 2013. Here is his solo from "Can't You Hear Me Knocking" with Public Enemy's Chuck D being his hype man!

<iframe width="560" height="315" src="https://www.youtube.com/embed/ODdU-dVuQmU?si=Co9cKCfk9ibfQUcT" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Bobby Key's solo record is a obscure, yet fun and enjoyable record that really deserves wider exposure.

{% iframe 'https://open.spotify.com/embed/album/5DQ6sZG6r7LmkQYLY9G2le?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B00I1R2WGY/?id=HzngdDFiQg&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/bobby-keys/972378867' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/42864054' '100%' '96' %}
