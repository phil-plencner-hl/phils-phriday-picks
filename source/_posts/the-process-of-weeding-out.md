---
title: Black Flag - The Process Of Weeding Out
date: 2021-03-25 08:20:34
tags:
- black flag
- henry rollins
- greg ginn  
- 80s rock
- punk rock
- jazz rock
---

Black Flag are most famously known for their beginnings as a DIY punk band. Masterminded by guitarist Greg Ginn they were definitely pioneers in everything from the musical style, to touring, to running a record label (Greg Ginn started SST Records as a way to distribute Black Flag records but eventually expanded out to other bands).

The band went through a ton of lineup changes but eventually stabilized with a lineup of Ginn on guitar, Bill Stevenston on drums, Kira Roessler on bass and Henry Rollins on vocals. This band travelled pretty far away from the original punk rock sound and eventually became more of a sludgy hard rock / heavy metal band. Albeit a metal band with a crazy guitarist playing atonal jazz-like solos over the top of the doomy riffs. This is explored on albums like *Slip It In* and *Loose Nut*. At the time, this did not go over well with their original punk fanbase.

In 1985 they released what turned out to be the swan song of this lineup *The Process Of Weeding Out*. This EP took the sound even further out. Henry Rollins does not appear at all (My original pressing of the record has a giant "INSTRUMENTAL! $6.98 list" warning sticker affixed to the front). 

It starts of with the almost 9-minute long "Your Last Affront". Keri plays a repetitive circular bassline while Greg goes wild over top. Bill tries to hold down a groove but eventually gets carried away in the din. 

The rest of the album follows suit. A unique blend of instrumental rock / metal with a ton of influence from free jazz and hard funk. 

This album basically broke up the band. Ginn went on to form the band Gone which continued in a similar style. Unfortunately the two albums they recorded with this mindset (*Let's Get Real, Real Gone for a Change* and *Gone II – But Never Too Gone!*) are not currently available on Spotify. 

I wonder if the title of the album was meant as some sort of truth-in-advertising statement. This is how they weed out the true Black Flag fans from the bandwagoners. It could also just be about smoking dope.

Nevertheless, I tend to side with the first notion, especially considering the Greg Ginn quote on the large sticker affixed to the back of my original record, which I will include below. You decide.

> The revolution will not be televised. But I don't have a T.V. and I'm not gonna watch. With talk of rating records and increased censorship it may be getting difficult for some to speak their minds. Black Flag already has enough problems with censorship coming from the business sector. Some record stores have refused to stock and/or display certain Black Flag records because of objectionable cover art and/or lyrical content. Now, with additional government involvement, the "crunch" is on. Hope does lie in the fact that fortunately these straight pigs show little ability in decoding intuitive data. For example, even though this record may communicate certain feelings, emotions and ideas to some, I have faith that cop-types with their strictly linear minds and stick-to-the-rules mentality don't have the ability to decipher the intuitive contents of this record. Of course, there may be a problem in that much of the public, most of whom comply with the whole idea of hiring the pigs in the first place, seem equally unable to intuitively feel and listen to music. Still, here it is: "The Process Of Weeding Out" -- `Greg Ginn`

[Buy *The Process Of Weeding Out* from Amazon](https://amzn.to/2QIntK5)

<iframe src="https://open.spotify.com/embed/album/1ANAXq3jO95foxWRhnoybY" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>


