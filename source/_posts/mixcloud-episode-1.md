---
title: Mixcloud Episode 1
author: Phil Plencner
date: 2020-04-03 15:08:36
tags:
- 90s rock
- alternative rock
- avant rock
- jazz
- jazz rock
---
Today, I'm trying something a little different. 

I made a few mixes of songs from my personal music library that are (mostly) not available on Spotify. Here is the first episode, I have 3 more that are ready to upload to my profile (hopefully later this afternoon & evening). 

[Enjoy the weird tunes!](https://www.mixcloud.com/phil-plencner/episode-1/)

