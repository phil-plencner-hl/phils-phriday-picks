---
title: Rollins Band - Weighting
date: 2023-06-02 15:25:33
tags:
- henry rollins
- rollins band
- melvin gibbs
- sim cain
- chris haskett
- black flag
- ornette coleman
- ronald shannon jackson
- 90s rock
- alternative rock
- free jazz
---

![](Weighting.jpg)

Without a doubt, one of my favorite groups from the early 90s was The Rollins Band. 

This, of course, is the name of Henry Rollins' musical group once Black Flag blew up in the mid 1980s. During Henry Rollins' time in the group, they went from a more straightahead punk rock group into something that tried to morph together heavy metal and free jazz. It alienated a lot of fans over time which led to the breakup of the group.

Near the end of that band's original existence, guitarist Gregg Ginn started an instrumental band Gone with Sim Cain as the drummer. Here they are performing in 1986:

<iframe width="560" height="315" src="https://www.youtube.com/embed/bvlC3M33Yss" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Simultaneously Henry Rollins was doing spoken word performances and toying with the idea of a new musicial project. This eventually led to his first solo album called *Hot Animal Machine*. Included in this band was guitarist Chris Haskett. 

Eventually Rollins poached Sim Cain from Gone and recruited bass player Andrew Weiss (who later when on to fame with the band Ween) and Chris Haskett to form the Rollins Band. 

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

They were a decent group, playing punk rock but with a heavy blues edge. The crowning achievement of this version of the group was probably 1992's *The End Of Silence* which found them playing a heavy metal / jazz fusion hybrid with extended song lengths. The song "Low Self Opinion" is a pretty good encapsulation of what they sounded like:

<iframe width="560" height="315" src="https://www.youtube.com/embed/o28dyt7w3As" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Andrew Weiss left under the punishing conditions of touring behind that album and was replaced with avant-jazz bass virtuoso Melvin Gibbs. Melvin Gibbs previously played with drummer Ronald Shannon Jackson (of Ornette Coleman's Prime Time fame) in his group Decoding Society. Here they are blowing the roof off the place in 1983:

<iframe width="560" height="315" src="https://www.youtube.com/embed/bZL88zT-LsM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

This is the band who put out the album *Weight* in 1994. An absolute masterpiece in 90s angst-rock. I present to you Exhibit A which is the completely punishing video for "Liar" from that album:

<iframe width="560" height="315" src="https://www.youtube.com/embed/awY1MRlMKMc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

They were a live force to be reckoned with. Check out this live video of "Civilized" for proof. No sleepwalking through this tune...playing the song like their life depended on it!! Also note Henry Rollins' introduction to the song, which still unfortunately rings true in 2023 as much as it did in 1994:

<iframe width="560" height="315" src="https://www.youtube.com/embed/EF5qnwtPKMQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

The recording of *Weight* was interesting because there were a bunch of tunes they ended up not using for the album, including some lengthy jam sessions they recorded with free jazz tenor saxophone player Charles Gayle. These recordings, along with some live documentation from the *Weight* tour was eventually released in 2003 as the album *Weighting* which is today's pick!

The first few songs ("Fall Guy", "I See Through", "Right Here Too Much") were not included on *Weight* but eventually released as part of movie soundtracks like *Tales From The Crypt Presents: Demon Knight* and *Johnny Mnemonic*. They could have easily been included in the original album as they are very high quality.

Next is almost 40 minutes of recordings from the Charles Gayle sessions which are incredible. 90s alt rock mixed with free jazz and Henry Rollins ranting on top of it is definitely an awesome fusion.

The album ends with 4 live cuts which really showcases how incredible The Rollins Band was in their prime. 

This version of the Rollins Band only put out one more album (*Come In And Burn*) which pushed their experimental tendencies to the limit. It didn't sell as well as *Weight* and Henry Rollins disbanded that group. He formed a different Rollins Band years later, but they played more straight-ahead punk rock, removing all the innovation and excitement that the *Weight*-era Rollins Band possessed. 

*Weighting* remains a crucial document of that era that still surprises and excites almost 30 years later.

Programming note: Phil's Phriday Picks will be on hiatus next week. I shall return on June 16th. Remain calm.

{% iframe 'https://open.spotify.com/embed/album/3Hzbsa12dN6IO6Rnv53j89?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B00B1KOL4E/?id=ERgjM9vk44&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/weighting/593747200' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/287748509' '100%' '96' %}
