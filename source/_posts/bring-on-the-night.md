---
title: Sting - Bring on the Night
date: 2024-05-17 21:36:41
tags:
- sting
- branford marsalis
- wynton marsalis
- omar hakim
- daryl jones
- kenny kirkland
- dire straits
- david bowie
- 80s pop
- 80s rock
- jazz
- jazz fusion
---

![](BringOnTheNight.jpg)

There was not many bands bigger in 1983 than The Police. When they broke up at the height of their popularity after touring behind *Synchronicity* many wondered what each of the individual members would do next. This was especially so for singer / bassist Sting.

He played a series of solo shows (literally "solo" often performing by himself) for a while but was itching to prove that he was a more "serious" musician than his previous band. In order to do this, he had to recruit "serious" musicians to be part of his new band.

During Sting's early solo period, he appeared on the Miles Davis record *You're Under Arrest*...he had a spoken word part on the song "One Phone Call / Street Scenes":

<iframe width="560" height="315" src="https://www.youtube.com/embed/2IpaeTAdtac?si=LZ6pBgPDYjaRNz1T" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

The bass player on *You're Under Arrest* was a young hotshot player named Daryl Jones. Sting was able to poach him away from Miles.

He also ransacked trumpeter Wynton Marsalis' band. They were a very popular jazz ensemble around that time. Included in that group were his brother Branford Marsalis on saxophone and Kenny Kirkland on keyboards. Here they are performing in 1983:

<iframe width="560" height="315" src="https://www.youtube.com/embed/3QA4D-0E1Dc?si=UeXzcxARZ1Y7If0a" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Sting was able to convince both Branford and Kenny to join his growing ensemble. This actually made Wynton very sour about rock music in general. He gave the genre a lot of attitude for a long while after this move. 

All Sting needed was a drummer.

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

Omar Hakim was that drummer. Omar was riding high in the mid 80s. He performed with David Bowie on the albums *Let's Dance* and *Tonight* but didn't tour with Bowie so he was behind the scenes. Tony Thompson also appeared on the records and was part of Bowie's live band. In fact, here is the video for "Modern Love" where Omar Hakim was the performer but he doesn't appear in the video at all. The cameras purposely never focus on the drum set:

<iframe width="560" height="315" src="https://www.youtube.com/embed/HivQqTtiHVw?si=kYPqFhQvF02_dyVV" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Omar Hakim also performed on the entirety of *Brothers In Arms* by Dire Straits, even though he was never an actual member of the group. Originally Dire Straits' real drummer, Terry Williams, played on all the songs but Mark Knopfler was not satisfied with his performance so Omar Hakim re-recorded almost all the songs. The only exception was the famous drum intro to "Money For Nothing". Hilariously, this video of "So Far Away From Me" predominantly features Terry Williams but he is not actually playing on the song! It's all Omar Hakim.

<iframe width="560" height="315" src="https://www.youtube.com/embed/YIHMPc6ZCuI?si=hhm-LUaNG1w8G1gH" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Having now assembled his band, Sting recorded his first solo record *Dream of the Blue Turtles* which was a huge success. One of the reasons why was the hit single "If You Love Somebody, Set Them Free". Here is the video for it featuring the all-star band:

<iframe width="560" height="315" src="https://www.youtube.com/embed/LSGl3d4KOMk?si=H5PmBoPeUpODRBit" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

They toured heavily behind the record. Luckily the shows were recorded both in audio and video form. These recordings were compiled into the album *Bring on the Night*, which is today's pick!

*Bring on the Night* was a double record, recorded so pristine that you'd think it was actually a studio record. It includes both songs from *Dream of the Blue Turtles* along with old chestnuts by The Police. Sometimes the solo and Police songs were combined brilliantly into medleys. One such example is "Dream of the Blue Turtles / Demolition Man":

<iframe width="560" height="315" src="https://www.youtube.com/embed/2JV0rnkvh_g?si=cmJiz3UtgRIfaFYU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Along with the record, a full length movie was also released simultaneously (also called *Bring on the Night*). It includes tons of concert recordings, along with band interviews and studio footage. It's an amazing snapshot of this very creative time for Sting. Luckily the entire film is on YouTube!

<iframe width="560" height="315" src="https://www.youtube.com/embed/aqwhGoEGy6E?si=dHZUwsXpICcv4pbM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

The whole band didn't stay together. Sting continued recording with Kenny Kirkland and Branford Marsalis for a few years before completely revamping the his band for *Ten Summoners Tales* in 1993. The others left after the *Bring on the Night* tour which makes it a very unique slice of Sting's career. Out of all the Sting solo records (and I love most of them), *Bring on the Night* is probably the album I return to the most. 

You can [listen to Sting - *Bring on the Night* on your streaming platform of choice](https://songwhip.com/sting/bringonthenight) (including Spotify, Amazon Music, Apple Music, Tidal, YouTube Music and more!).
