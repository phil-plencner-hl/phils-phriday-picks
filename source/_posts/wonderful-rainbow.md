---
title: Lightning Bolt - Wonderful Rainbow
author: Phil Plencner
date: 2019-07-26 15:40:39
tags:
- lightning bolt
- punk rock
- noise rock
---

It's Friday! Let's party to the loudest dance band I ever witnessed....Lightning Bolt!!

<iframe src="https://open.spotify.com/embed/album/4DsHBZ9Cw8D5cE2gmjoaXu" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>