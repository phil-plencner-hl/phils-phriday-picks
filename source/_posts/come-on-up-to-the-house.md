---
title: Come On Up To The House -  Women Sing Waits
author: Phil Plencner
date: 2019-11-26 13:56:08
tags:
- tom waits
- 70s rock
- country music
- folk music
---
Today's holiday week pick is the glorious new Tom Waits tribute album. 

You don't have to be a Waits fan to appreciate the beauty of these interpretations.

<iframe src="https://open.spotify.com/embed/album/4xyavB5BZ4HZeAFwmothzx" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>