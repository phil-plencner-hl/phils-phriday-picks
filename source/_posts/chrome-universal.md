---
title: Chrome Universal -  A Survey of Modern Pedal Steel
date: 2023-02-10 12:44:49
tags:
- chrome universal
- luke schneider
- spencer cullum
- susan alcorn
- bj cole
- elton john
- jack white
- steel guitar
- country music
- ambient music
---

![](ChromeUniversal.jpg)

The Tompkins Square record label, run by Josh Rosenthal, mainly focuses on archival releases of country, gospel, and blues music. Since 2005 the label has been releasing records as part of a series called "Imaginational Anthem" that highlights unique guitar players sometimes solely curated by Josh Rosenthal and sometimes with outside help. 

For the eleventh item in the series Tompkins Square worked with pedal steel guitar wizard Luke Schneider (who's *Altar of Harmony* record on Jack White's Third Man Records was [a previous Phil's Phriday Pick!](https://phil.share.library.harvard.edu/philsphridaypicks/2020/08/04/altar-of-harmony/)) to compile *Chrome Universal: A Survey of Modern Pedal Steel*.

You might think that the record would sound like country or americana music, but this could not be further from the truth. Most of it is very jazzy, experimental, and ambient in nature. Which makes it right up my alley!

Players on the album include B.J. Cole (who has previously played with a who's who of classic rock artists. In fact, that's his steel guitar you hear on Elton John's "Tiny Dancer"), Spencer Cullum (who plays with Dolly Parton, Miranda Lambert, and others), Baltimore native Susan Alcorn (who along with releasing her own experimental albums works with jazz players like Ken Vandermark, Evan Parker, Mary Halvorson etc.) and of course Luke Schneider himself. 

For those who like physical media, I also recommend [buying the vinyl record](https://tompkinssquare.bandcamp.com/album/luke-schneider-presents-imaginational-anthem-vol-xi-chrome-universal-a-survey-of-modern-pedal-steel) as it comes with a pretty informative booklet about all the artists along with a history of the pedal steel guitar that is well worth reading.

The music throughout the album sounds shimmering, beautiful, and glacial. It makes for a perfect listen during the chilly winter months.

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/album/5Yp1cJb9OpCQP4rrcj9PDZ?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

<iframe id='AmazonMusicEmbedB0B5XZ58HS' src='https://music.amazon.com/embed/B0B5XZ58HS/?id=ITm72vpKJc&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *; clipboard-write" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/luke-schneider-presents-imaginational-anthem-vol-xi/1633239564"></iframe>

<iframe src="https://embed.tidal.com/albums/236713897" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>