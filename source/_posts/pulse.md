---
title: Greg Phillinganes - Pulse
date: 2024-11-08 15:59:47
tags:
- greg phillinganes
- quincy jones
- michael jackson
- steely dan
- donald fagen
- yellow magic orchestra
- eric clapton
- 80s music
- pop music
---

![](Pulse.jpg)

Welcome to the triumphant return of Phil's Phriday Picks! I have been on hiatus for the month of October...so in the immortal words of David Lee Roth:

"I've heard you missed us, we're back! I brought my pencil! Gimme something to write on, man!"

Now that I got the obligatory Van Halen reference out of the way, let's get back to business.

The music world has been saddened this week by the announced passing of Quincy Jones There’s rightfully high praise for Quincy Jones’ most popular works (*Off The Wall*, *Thriller*, *We Are The World*, *In Cold Blood*, *Back on the Block*,  “It’s My Party”, “Sanford And Son”, “Fly Me to the Moon”….the list seemingly is endless!), and many people on the internet have already detailed his incredible career. 

This got me thinking about all the musicians involved in all his projects, especially during the 80s with Michael Jackson. One of the musicians that was a constant presence in Michael Jackson's band (both in the studio and on tour) was keyboard player Greg Phillinganes. 

Greg Phillinganes first big break was with Stevie Wonder. He was part of Wonder's band in the late 70s. Stevie Wonder introduced Greg Phillinganes to The Jackson's when they were recording the *Destiny* album in 1978. He helped with song arrangements and added some keyboard playing. This continued with 1980's *Triumph*. In fact, you can hear his playing on one of the big hits from that record, "Can You Feel It":

<iframe width="560" height="315" src="https://www.youtube.com/embed/lrKZNqIR2U0?si=VXOb53mRIOuw5Bqi" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Greg stuck with Michael Jackson when he went solo and worked with Quincy Jones on *Off the Wall*, *Thriller* and *Bad*. 

Here is an excellent interview with Greg where he discusses the recording of *Thriller*:

<iframe width="560" height="315" src="https://www.youtube.com/embed/XeZIjkPdca4?si=dOhlYTR6DRraZPev" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

You can tell Michael Jackson and Greg Phillinganes really had a deep bond, because Michael always made sure to specially introduce Greg during every concert. Here is a montage of some examples:

<iframe width="560" height="315" src="https://www.youtube.com/embed/S0NjbUDS0i4?si=yB1rxjr5IhMSwKza" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

During the recording of *Thriller*, Michael Jackson recorded a demo of a song called "Behind the Mask". It was originally written and recorded by a Japanese group called Yellow Magic Orchestra. Jackson took their original song, added some additional lyrics and flourishes and made it his own. Greg Phillinganes helped with the arrangement. For whatever reason, Quincy Jones wasn't thrilled with the song, and it was never fully completed for the *Thriller* record. 

Here is a great performance of the original song by Yellow Magic Orchestra:

<iframe width="560" height="315" src="https://www.youtube.com/embed/kcfJkH2gbcc?si=uM6RsMSCEigTkKHJ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

And here is the Michael Jackson demo of the song from the *Thriller* sessions:

<iframe width="560" height="315" src="https://www.youtube.com/embed/9Bfe8QAw33U?si=ADSOezQl7-Y9IrzD" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

After the smashing success of *Thriller* and the accompanying tour, Greg Phillinganes released a solo record which included his own version of "Behind the Mask". That record from 1984 is called *Pulse* and is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

Here is the music video for Greg's version of "Behind the Mask". I love how over-the-top the whole thing is! There are four (count 'em!) keyboard players on the stage...including two Keytars! That's a lot of synthesizers! 

<iframe width="560" height="315" src="https://www.youtube.com/embed/4SreNsrMu7k?si=2EmbRCPsLd2Jv9L1" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Other highlights of *Pulse* include a modernized version of an old doo-wop tune called "Countdown to Love". It was even included in the soundtrack to the movie *Streets of Fire*:

<iframe width="560" height="315" src="https://www.youtube.com/embed/jZn26OLS0lM?si=7Yc29Dn74dKTVVDp" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

The song even made an appearance in the movie itself:

<iframe width="560" height="315" src="https://www.youtube.com/embed/P5FLjAs2nQI?si=Yfdah1S8xs-QJh3Z" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Another highlight from *Pulse* is the song "Lazy Nina" which was written by Donald Fagen of Steely Dan fame. Fagen originally wrote it for one of his own solo records but ended up never recording it himself.

<iframe width="560" height="315" src="https://www.youtube.com/embed/TJMelsJukC0?si=KMWRiyrGlZ_o88Tm" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

What is the connection between Donald Fagen and Greg Phillinganes? Why would Fagen give him this excellent song? I'm glad you asked!

Greg played on nearly every song on Donald's 1982 record *The Nightfly*! This includes the song "I.G.Y." which is still a fan favorite today for Steely Dan aficionados. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/Ueivjr3f8xg?si=PGiaZgCl9eJmzs06" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

I want to take a slight detour here, because I wanted to also show this completely bonkers version of "I.G.Y." that Donald Fagen performed on the television show *Night Music* in 1988. None other than Patti Austin sings the song with Fagen and David Sanborn plays a killer saxophone solo. You got to see / hear this to believe it!

<iframe width="560" height="315" src="https://www.youtube.com/embed/IzaA3h5sI6A?si=ZnoG_wcT2zvZ-R8V" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Anyways, back to the *Pulse* album.

The third single from the record was "Signals" which is a great slice of 80s electronic R&B. It should have been a bigger hit!

<iframe width="560" height="315" src="https://www.youtube.com/embed/tfcacXQ22cA?si=D8J-t6gln9dZlYl4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

All told, *Pulse* is a great record. Even though it sounds a little dated with the 80s production techniques it still contains a ton of great music. The production is part of its charms, in my opinion. It is worth revisiting 40 years later!

Speaking of revisiting: Greg Phillinganes joined Eric Clapton's band in 1985 (appearing on *Behind the Sun*, *August* and *Journeyman*). Greg introduced Eric to "Behind the Mask" and he recorded his own version of the song on *August*! That song sure got a lot of mileage.

<iframe width="560" height="315" src="https://www.youtube.com/embed/2bY1WosGRXI?si=TjtbkaqGQNC94Vj2" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

One more thing I'll highlight (you might tell, I've missed writing PPP this past month) is a video showcasing the virtuosity of Greg Phillinganes. While this is basically an infomercial for Keyscapes it is an incredible performance:

<iframe width="560" height="315" src="https://www.youtube.com/embed/svX6WRWBP8o?si=A6JDSICO6Iogrf5m" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

I promise to be back on a regular weekly cadence! Until then, enjoy *Pulse*.

{% iframe 'https://open.spotify.com/embed/album/2keWhx31zk5DnCvRCD5Lg4?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B01KBVU6C4/?id=0vjphSCxdc&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/pulse-expanded-edition/1140702950' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/63639431' '100%' '96' %}
