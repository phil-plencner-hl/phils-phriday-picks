---
title: Louis Cole - Nothing
date: 2024-08-16 23:36:59
tags:
- louis cole
- clown core
- knower
- steve vai
- mike keneally
- frank zappa
- elvis costello
- progressive rock
- jazz fusion
---

![](Nothing.jpg)

Louis Cole is an incredible musician and composer. First and foremost he is known for his proficiency behind the drum set. Here he is performing a drum solo on top of some prerecorded music that makes me realize I really need to practice my drumming more:

<iframe width="560" height="315" src="https://www.youtube.com/embed/RuxzWqkyo0k?si=CUMqe9YblEtxeB6F" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

One of my favorite albums from 2022 was his solo record *Quality Over Opinion*. Though he is primarily a drummer he also plays other instruments such as keyboards and guitar. His style of playing is generally frantic and precise. He also has a wacky sense of humor. All of this is apparent in every single song on this album. He released a video for the single "I'm Tight":

<iframe width="560" height="315" src="https://www.youtube.com/embed/u9XrWB-u1vc?si=c3UTKNJZNGHTKxL0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

He also performs in numerous bands. One of them is called Knower which is a collaboration with musician Genevieve Artadi. They put out a record last year called *Knower Forever* which included this fun song called "I'm the President":

<iframe width="560" height="315" src="https://www.youtube.com/embed/tuhe1CpHRxY?si=4x4nbRzAoGKrKeHn" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

He is also part of the grindcore / free jazz duo called Clowncore with saxophone player Sam Gendel. Their music reaches the outer edges of sanity. They released a series of videos of them playing in an old minivan in clown costumes (and released an accompanying album simply called *Van*) back in 2021. Here is "Earth" from *Van*:

<iframe width="560" height="315" src="https://www.youtube.com/embed/sR_rPd_ufK4?si=M3WyTYPeu7K_3tqc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Louis Cole's latest project is even larger in scope. He has collaborated with the Dutch orchestra Metropole Orkest on an album called *Nothing*, which is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

The Metropole Orkest has been collaborating with rock musicians for decades, so this is not a new concept for them. There are dozens of recorded examples of them performing with many popular artists. For example, here they are playing "For the Love of God" with Steve Vai all the way back in 2004:

<iframe width="560" height="315" src="https://www.youtube.com/embed/XZUB4k9bQeo?si=tJt6WL6GMu2HwwDg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

They also performed a concert with Elvis Costello that was released as a album called *My Flame Turns Blue* in 2006. Here is "Clubland" from that concert:

<iframe width="560" height="315" src="https://www.youtube.com/embed/f2O9Rx8c_R8?si=-B2bthkeJGEPwQ2f" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

I think Louis Cole has taken those earlier concepts and really pushed them to the next level. The integration of rock music and the orchestra is very unique. One of the songs performed is "Things Will Fall Apart" which is a reworking of "I'm The President":

<iframe width="560" height="315" src="https://www.youtube.com/embed/mZOkLMXXax4?si=xF7aXd3wCuILP6nf" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Many of the songs on *Nothing* also have videos. I'll showcase a couple more examples. Here is "These Dreams Are Killing Me":

<iframe width="560" height="315" src="https://www.youtube.com/embed/Nv83lZfp7Zg?si=a8_HJqJhF-1VTdvO" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Finally, here is "Cruisin' For P":

<iframe width="560" height="315" src="https://www.youtube.com/embed/y1Tl4Uop96M?si=Qxd7CzmQKmR5j1_d" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

The rest of the songs on *Nothing* are equally as strong. There is not a bad moment throughout the entire hour long record. This will likely end up as one of my top 10 favorite records of 2024 when it is all said and done. It really is an incredible achievement that will be hard to top!

{% iframe 'https://open.spotify.com/embed/album/1hD0obPJIc7yi3RwwREblP?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B0D2NZM9FJ/?id=2TD6UCSmLm&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/nothing/1739105659' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/372109680' '100%' '96' %}