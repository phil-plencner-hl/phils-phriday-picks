---
title: Elliott Carter - Double Concerto For Harpsichord And Piano
author: Phil Plencner
date: 2020-08-10 15:08:54
tags:
- classical music
- 20th century classical
- elliott carter
---

Classical Music Hour! 

Today I'm highlighting this excellent recording of Elliott Carter's *"Double Concerto for Harpsichord & Piano"*. 

Conducted by Gutstav Meier this is a pretty masterful performance.  The melodic keyboards are set against a very busy and dissonant orchestra with a great percussion section that deserves to be played loud and rattle the windows. 

Igor Stravinsky called this composition a "masterpiece" so who am I to argue?

<iframe src="https://open.spotify.com/embed/album/0l5fZQf8I5vwBbZpU0rH8z" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>