---
title: Jack White - No Name
date: 2024-08-02 21:47:57
tags:
- jack white
- blues rock
- alternative rock
- third man records
- white stripes
---

![](NoName.jpg)

Sorry for the later than usual post tonight. It's been a pretty busy day, including diving into a ton of enjoyable new releases that came out today including Smashing Pumpkins - *Aghori Mhori Mei*, Orville Peck - *Stampede*, X - *Smoke & Fiction*, Meshell Ndegeocello - *No More Water* and Chrystabell / David Lynch - *Cellophane Memories*. They are all worth checking out...but none of these are today's pick.

I've been on a bit of a Jack White / White Stripes kick lately.  This is partly because I recently traveled to Nashville for my birthday and this trip included a stop at the Third Man Records location there.

![](ThirdMan1.jpg)

While browsing the racks there I saw one of my favorite albums of all time for sale there:

![](ThirdMan2.jpg)

They also had a ton of cool, unique records on display such as this one:

![](ThirdMan3.jpg)

I took a behind the scenes tour of the warehouse. It was awesome but they didn't allow photos on the tour unfortunately.

I also caught a show at the adjacent Blue Room. The aptly named space is basked in blue light with blue walls. In fact, the photos for album covers of many records released on Third Man were taken there.

Here's me looking very blue there:

![](BlueRoom1.jpg)

They have an actual elephant head on the wall, which is pretty wild:

![](BlueRoom2.jpg)

The headliner that night was Eric Slick. His band was incredible:

![](BlueRoom3.jpg)

Anyways, my current Jack White / Third Man Records obsessions have come at a good time, because Jack White recently released a surprise record called *No Name*, which is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

It was originally released for free only to customers at Third Man Record stores as an unmarked white vinyl record. There was a lot of mystery and hype about the record but [Jack White encouraged people to share it online](https://consequence.net/2024/07/jack-white-encourages-fans-to-rip-his-new-album/). Local Detroit radio station WDET even [played the album in full](https://wdet.org/2024/07/19/in-the-groove-jack-white-shadow-dropped-a-new-album-so-wdet-gave-the-world-premiere/)!

Soon afterwards Jack White played a small show at a Nashville American Legion Hall and played a few songs from the record. He also played some old White Stripes Classics. One such song is "Fell In Love With A Girl":

<iframe width="560" height="315" src="https://www.youtube.com/embed/3wfoZ7nlwwY?si=9GmNkEfxLocr9_jH" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

He also played "Ball & Biscuit" as an encore:

<iframe width="560" height="315" src="https://www.youtube.com/embed/ZDTbkwZTXeY?si=tN5lPvY1OwReUZUj" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

He definitely sounds in fine form, really getting back to his heavy punk blues roots. Which is exactly what *No Name* sounds like! A stripped down, no nonsense banger. 

Thankfully, the album is getting wider distribution. In September it will be released in a regular black vinyl format, but today he released it in an exclusive blue vinyl version sold only at Third Man Records and "select indie stores" (along with on the major streaming platforms). The Third Man Records in Nashville where I recently was [had a pretty long line](https://www.instagram.com/reel/C-Iq_9lPM2k/?utm_source=ig_web_copy_link&igsh=MzRlODBiNWFlZA==)!

A local Baltimore store had a few copies for sale and I was able lucky enough to grab a copy. It sounds incredible cranked up [on my turntable](https://www.instagram.com/p/C-LaS8qSoiJ/?utm_source=ig_web_copy_link&igsh=MzRlODBiNWFlZA==)!

I've already listened to it a few times and its definitely going to be on my list of best albums of the year. It really is that good. 

{% iframe 'https://open.spotify.com/embed/album/4j6OkbZmVIqJYDLJbiWHbX?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B0DBK5MCD8/?id=CQnaG5GO0O&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/bt/album/no-name/1760057427' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/378186214' '100%' '96' %}