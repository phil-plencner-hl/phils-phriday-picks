---
title: King Crimson - Cat Food
author: Phil Plencner
date: 2020-06-15 22:18:05
tags:
- king crimson
- keith tippett
- 70s rock
- progressive rock
---
Jazz keyboardist Keith Tippett passed away over the weekend. 

While he had a lengthy career playing in all manner of wild avant / free jazz ensembles, he's best known to me as the keyboardist in King Crimson from *"In The Wake Of Poseidon"* through *"Islands"* 

During that stint, KC recorded 'Cat Food' and released it as a single.  This is a pretty bizarre thing for rock band to release as a "hit single" even in 1970! 

The b-side "Groon" is even nuttier. They recently re-released the single including a live rendition by the current Crimson band as well as a remixed original single. 

Worth revisting.

<iframe src="https://open.spotify.com/embed/album/3BeGNkhLM7V8oT4HZkYAnA" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>