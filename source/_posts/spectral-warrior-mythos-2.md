---
title: The Flying Luttenbachers - Spectral Warrior Mythos 2
date: 2024-06-07 21:32:50
tags:
- the flying luttenbachers
- weasel walter
- progressive rock
- brutal prog
- math rock
- free jazz
- avant-garde rock
- abhorrent expanse
---

![](SpectralWarriorMythos2.jpg)

The Flying Luttenbachers have long been one of my favorite bands. In fact, I have previously picked a couple Flying Luttenbachers albums: On May 25, 2019 my pick was *Infection and Decline* and on July 9th, 2021 I picked *Negative Infinity*. This week will be a rare "pick trifecta"!

The Flying Luttenbachers only constant member is Weasel Walter. While the lineup has not been stable over the years, the group's commitment to extreme music, velocity and musical complexity has always remained. 

The current version of the band includes Weasel Walter on guitar, (which he switched to from drums back in 2021), Luke Polipnick on bass and James Paul Nadien on drums. 

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

Luke Polipnick also plays in the band Abhorrent Expanse, who play a very wild hybrid of death metal and free jazz. They recently put out a great record called [*Gateways to Resplendence*](https://abhorrentexpanse.bandcamp.com/album/gateways-to-resplendence). Here is the epic "Arcturian Nano Diamonds from the Tranquil Abyss" from that album:

<iframe width="560" height="315" src="https://www.youtube.com/embed/45QZCTaiYsc?si=J84eG1qZE-oZPlNG" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

James Paul Nadien also has a free jazz background. Here is a show he played just a couple months ago with bassist Caleb Duval and tenor saxophonist Lao Dan:

<iframe width="560" height="315" src="https://www.youtube.com/embed/XiAZp6kTLJo?si=ALSkWp4kDwVVQfew" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

This configuration of The Flying Luttenbachers is currently on tour with Melt-Banana, and I was able to [catch them earlier this week in Cambridge Massachusetts](https://www.instagram.com/p/C70Y076MhdK/?utm_source=ig_web_copy_link&igsh=MzRlODBiNWFlZA==). If you didn't see one of their shows, it's too late because the last show in the tour is tonight. You snooze, you lose. 

Maybe you don't completely lose, because fans have been recording videos of some of the concerts. 

Here they are in Minneapolis, MN on May 30:

<iframe width="560" height="315" src="https://www.youtube.com/embed/au6u7-Pn2e4?si=B0zdeZwbO95GUD3w" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

The next night they were in Madison, WI:

<iframe width="560" height="315" src="https://www.youtube.com/embed/g5I261TyZeQ?si=4rSRzVx8nwdvkCyd" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

After that, they played in Chicago, IL:

<iframe width="560" height="315" src="https://www.youtube.com/embed/v2BMdyjT_c0?si=DbwlHyfxxw9tViKQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

The Flying Luttenbachers recently released a new demo-style EP called *Spectral Warrior Mythos 2*, which is today's pick!

The songs on *Spectral Warrior Mythos 2* are the same ones they are currently playing on the road, so it is a great representation of what the band sounds like nowadays (even though the drummer, Charlie Werber, is different). If you really want to hear what you missed, then look no further!

You can [listen to a few songs from *Spectral Warrior Mythos 2* on Bandcamp](https://theflyingluttenbachers.bandcamp.com/album/spectral-warrior-mythos-2). If you like what you hear, you are in luck because you can also by the album at the same place!
