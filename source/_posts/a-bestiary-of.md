---
title: The Creatures - A  Bestiary Of
author: Phil Plencner
date: 2020-05-28 14:30:17
tags:
- siouxsie sioux
- siouxsie and the banshees
- 80s rock
- post-punk
- the creatures
---
A co-worker enlightened me that Siouxsie Sioux is celebrating a birthday this week. Rather than trotting out old Banshees chestnuts today, I'll pick her side-project The Creatures! 

This compilation combines their first EP (*Wild Things*) and first full length album (*Feast*). A strange mix of art rock and exotica music with Siouxie's unmistakable vocals.

<iframe src="https://open.spotify.com/embed/album/5EuJn55QHhPT2jNlfyunej" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>