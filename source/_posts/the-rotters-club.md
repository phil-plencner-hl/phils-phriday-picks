---
title: Hatfield & The North - The Rotters Club
author: Phil Plencner
date: 2020-10-02 16:09:47
tags: 
- progressive rock
- hatfield & the north
- canterbury prog scene
---
Today's pick is *The Rotters Club* by Hatfield And The North. 

Hatfield And The North were part of the "Canterbury Scene" offshoot of progressive rock in late 60s / early 70s England. The scene obviously revolved around the Canterbury area and consisted of bands like Soft Machine, Gong and Caravan. In fact, Hatfield and The North consisted of members who recently left Soft Machine. 

They perfected a mixture of ulta-complex jazz rock instrumental prowess ("Mumps") and quirky, humorous lyrics and song titles ("Didn't Matter Anyway", "Fitter Stoke Has A Bath"). 

If I were to pick one album to hear from the Canterbury Scene it would likely be *The Rotters Club* (although Caravan's *In The Land Of Grey and Pink* comes close). 

Soon after they toured behind *The Rotters Club* the band broke up and some of them went on to form National Health which focused more on lengthy instrumentals.

Coincidentally after I decided on this pick earlier this week, a brand new article with a nice overview of the overall Canterbury Scene posted online yesterday, for those who want to dig deeper:
[
Features
The Canterbury Scene: How A Bunch of Bookish Bohemians Became The Monty Pythons of Prog](https://www.udiscovermusic.com/stories/canterbury-scene-prog/)

<iframe src="https://open.spotify.com/embed/album/6NqP2oaumNCKtIoxIpgPKj" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>