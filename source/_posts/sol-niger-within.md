---
title:  Fredrik Thordendal's Special Defects - Sol Niger Within
date: 2021-10-15 11:10:55
tags:
- fredrik thordendal
- meshuggah
- morgan agren
- mats oberg
- mats & morgan
- progressive rock
- jazz fusion
- heavy metal
- djent
---

Fredrik Thordendal is a Swedish guitarist, who is most famous for being a member of Meshuggah. Meshuggah pinoneered a strange offshoot of heavy metal that came to be known as Djent. 

Djent is known for it's downtuned riffs (many times with 7-string guitars), mathy polyrhythms and lots of palm muting. 

Meshuggah really defined this sound starting with their 3rd album *Chaosphere*. Between their 2nd album (*Destroy, Erase, Improve*) and *Chaosphere*, Fredrik Thordendal put out a wild solo album called *Sol Niger Within*.

*Sol Niger Within* is Latin for "Black Sun Within" and I think that is a pretty apt description of what this album sounds like. It's very dense and contains a lot of strange, virtuoso music.

Fredrik Thordendal's guitar solos are definitely inspired by 70s jazz fusion. Some of it sounds like Steve Vai or Allan Holdsworth at their most experimental. Really awesome, inspired stuff!

One of my favorite drummers, Morgan Agren plays on all the songs and he's as much of a star of the show as Fredrik. Some of the stuff he plays has to be seen to be believed. Luckily, Swedish television filmed the duo playing together that is available on YouTube for your jaw-dropping pleasure.

<iframe width="560" height="315" src="https://www.youtube.com/embed/AgwJEvasJlM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The entire album is one long composition. The CD I have has it split up into 28 tracks, but they all seamlessly segued into each other. The version on Spotify is just one long 43 minute track.

Also appearing on the album, playing creepy church organ, is Mats Oberg. He is the blind keyboard player who is the other half of the duo of previous Phils' Phriday Pick Mats & Morgan.

There is also some free jazz inspired saxophone peppered throughout. 

All the sums of these parts means this album is right up my alley. I hope is it up yours as well.

<iframe src="https://open.spotify.com/embed/album/0vNCfKKhLqPC5ovta0EM9X" width="100%" height="380" frameBorder="0" allowtransparency="true" allow="encrypted-media"></iframe>
