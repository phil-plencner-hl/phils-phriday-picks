---
title: Cheer-Accident - Here Comes The Sunset
date: 2022-02-18 16:43:04
tags:
- cheer-accident
- chicago
- progressive rock
- noise rock
- alternative rock
---


One of my favorite bands of all time, Cheer-Accident, released a new album today called *Here Comes The Sunset*. It has already reached heavy rotation on my stereo as I have listened to it a half-dozen times already. The 30 minute running time certainly helps with that statistic.

As with the other 20+ Cheer-Accident albums, it covers a wide range of stylistic ground. Everything from instrumental progressive rock to poppy alternative rock (including an excellent cover of Cheap Trick's "Dream Police" that manages to be both of those things at once!)

For the uninitiated, [Chicago Reader has a nice writeup](https://chicagoreader.com/music/chicago-cheer-accident-here-comes-the-sunset/) on the new album including a bit of historical context. 

Regardless of your previous familiarity, I urge you to dive into what might be one of the best albums you'll hear all year.

<iframe src="https://open.spotify.com/embed/album/55tSrqipkP40MbjEPSOvAZ?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

<iframe id='AmazonMusicEmbedB09NVJ7VVC' src='https://music.amazon.com/embed/B09NVJ7VVC/?id=D7tKCXoMSL&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/here-comes-the-sunset/1601147205"></iframe>