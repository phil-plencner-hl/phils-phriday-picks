---
title: Ray Charles - A  Message From The People
date: 2024-07-05 08:56:34
tags:
- ray charles
- jazz
- r&b
- patriotic music
---

![](AMessageFromThePeople.jpg)

Happy Fourth of July! I'm just writing a very quick pick for this week since, like many of you, I am enjoying a very long weekend. 

Today's pick is Ray Charles' awesome 1972 album *A Message From The People*!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

In my opinion this is one of the most patriotic records ever made. The album starts with "Lift Every Voice and Sing" and ends with an absolutely stunning version of "America The Beautiful". In between these two classics are eight more perfectly chosen and impeccably performed songs (many arranged by Sid Feller and entirely produced by Quincy Jones). Highlights include covers of Melanie's "Look What They Done To My Song, Ma", John Denver's "Take Me Home, Country Roads" and Dion's "Abraham Martin and John". 

There is also a great version of "Hey Mister" which is an awesome protest song about the government ignoring the needs of poor people.

Listening to this album always makes me feel proud to be an American. Hopefully it does for you as well. How can this not?

<iframe width="560" height="315" src="https://www.youtube.com/embed/46j1eX5Ehwk?si=FQQKHBrJYAv0stt6" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

You can [listen to Ray Charles - *A Message From the People* on your streaming platform of choice](https://songwhip.com/ray-charles/a-message-from-the-people) (including Spotify, Amazon Music, Apple Music, Tidal, YouTube Music and more!).
