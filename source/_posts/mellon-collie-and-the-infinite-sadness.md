---
title: The  Smashing Pumpkins - Mellon Collie And The Infinite Sadness
date: 2024-01-12 18:50:16
tags:
- smashing pumpkins
- billy corgan
- jimmy chamberlin
- james iha
- d'arcy wretzky
- alternative rock
- 90s rock
- grunge
---

![](MellonCollie.jpg)

The Smashing Pumpkins recently announced that they are [holding open tryouts for a new guitar player](https://www.yahoo.com/entertainment/ever-wanted-play-guitar-smashing-221534125.html). While this is weird for a band of their size and acclaim, it is not the first time they held open auditions. They also [held open auditions for drummers back in 2009](https://www.mtv.com/news/7n8208/smashing-pumpkins-to-hold-open-auditions-for-new-drummer) when Jimmy Chamberlin left the band (he has since returned). Billy Corgan and The Smashing Pumpkins have always done things out of the ordinary and these are just more examples.

One of the biggest examples was how they followed up their 1993 album *Siamese Dream*. That record was, of course, a huge success. Selling over 6 million copies behind hits like "Today", "Disarm" and "Rocket". 

Naturally the safest choice would be to make a *Siamese Dream II*: rehashing their mix of alternative rock and shoegaze...but they did not do that. 

Instead they decided to go full-on progressive rock. They released a double-album that ran through a wide variety of styles from hard rock to jazz and beyond. While all the songs didn't really work together as a "concept album", they did split the songs into themes of "day" (the first disc) and "night" (the second disc). The record company was obviously worried, but the record was an even bigger success than *Siamese Dream*! The resulting album was *Mellon Collie And The Infinite Sadness* (selling 5 million copies but because it was a double CD it technically sold 10 million copies). *Mellon Collie And The Infinite Sadness* is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

The album spans 24 songs in two hours...and while the hits are uniformly excellent (and I'm sure you know them all: "Bullet With Butterfly Wings", "1979", "Zero", "Tonight Tonight" and "Thirty-Three") the rest of the songs are just as high quality, which is rare for a record of this size and scope. The scary thing is, there were dozens of B-sides that were also just as great as the proper album (documented on the box set *The Aeroplane Flies High* that came out the following year). Smashing Pumpkins at this time were extremely prolific and captured lightning in a bottle. 

I especially like the progressive rock epics like "Porcelina of the Vast Oceans", "Galapogos" and "X.Y.U", where they really get to stretch out and show off their instrumental prowess. The heavy rockers such as "An Ode to No One", "Tales of A Scorched Earth" and "Here Is No Why" also burst at the seams with power and energy. 

The pre-album hype was off the charts. The week before the album's release MTV broadcast a 20 minute "rockumentary" interviewing the band and showing behind the scenes footage of them in the studio:

<iframe width="560" height="315" src="https://www.youtube.com/embed/vPWmpQjPdwA?si=hD0GdfbejI5LXKuk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

A day before the release, they played a hometown show at The Riviera in Chicago playing mostly new material and broadcast on radio stations nationwide:

<iframe width="560" height="315" src="https://www.youtube.com/embed/GY4AJK-la74?si=FTBRCQIkJl8Ux1xF" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Needless to say, when the album was finally released it was a huge deal. I remember going to a midnight sale of the album in the tiny college town I was living in at the time. The line was so long outside the record shop that I swore most of the campus population was there.

A few weeks after the album came out, they blew the roof off of the Saturday Night Live stage:

<iframe width="560" height="315" src="https://www.youtube.com/embed/kzPTFzJPmq0?si=xFpG2SHcdNz8M-xH" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

The resulting tour was a grueling one for the band. The pinnacle of their shows at that time was probably their rendition of "Tonight Tonight" on the MTV Video Music Awards in June of 1996, complete with full orchestra. An incredibly moving performance that showed the band at the height of their powers:

<iframe width="560" height="315" src="https://www.youtube.com/embed/PJDpdzOMy2Y?si=VO19L0GdbYkArXe3" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Unfortunately it all came crashing down a month later when [their touring keyboard player, Jonathan Melvoin, died of a heroin overdose](https://www.washingtonpost.com/archive/politics/1996/07/13/drug-overdose-kills-musician-on-smashing-pumpkins-tour/ef0c9b5e-2713-457c-a39b-6f49df3e4a5b/). Drummer Jimmy Chamberlin was with him at the time. Jimmy was subsequently kicked out of the band. They stylistically changed their sound with the more subdued *Adore* in 1998 with mostly drum machines instead of live drumming.

Smashing Pumpkins with various casts of characters as members (Billy Corgan being the only constant) continues on to this day, but they never reached the heights of *Mellon Collie And The Infinite Sadness* again. I've liked some of the other latter-day records (*Zeitgiest*, *Shiny and Oh So Bright, Vol. 1: No Past. No Future. No Sun* and *Cyr*). They all have their moments but aren't front-to-back bangers like *Mellon Collie And The Infinite Sadness*. It still sounds tremendous: literally bursting out of the speakers almost 30 years later. A timeless classic.

{% iframe 'https://open.spotify.com/embed/album/4bPT6Q8ppaSNppk1kbEbLl?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B00AOMUCU2/?id=L8PImGQkhs&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/mellon-collie-and-the-infinite-sadness-remastered/721224313' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/79279089' '100%' '96' %}