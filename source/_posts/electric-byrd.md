---
title: Donald Byrd - Electric Byrd
date: 2021-06-10 22:05:06
tags:
- donald byrd
- john coltrane
- herbie hancock
- ron carter
- classic jazz
- jazz fusion
- space rock
---

Donald Byrd was a trumpet player who first came to prominence playing with Art Blakey in the Jazz Messengers during the mid 1950s. He also appears on a bunch of John Coltrane's albums on the Prestige label. He was one heck of a hard bop player. 

He was also close friends with Herbie Hancock and encouraged him to pursue his solo career away from the shadow of Miles Davis.

Late in his career he became known for a more smoothed out funk and soul style.

However, the era in between the bop and soul successes is what I find the most fascinating. In the early 70s he put out a series of electric jazz fusion albums that were a pretty heady mix of space rock along with the jazz style pioneered by Miles Davis' *Bitches Brew*. 

The best of these albums is probably *Electric Byrd*.

The group he assembled for this album was huge...11 pieces! There are two players I'd like to specifically point out:

Airto Moreira plays latin-based percussion before he became a member of Chick Corea's Return To Forever. 

Ron Carter, who was with Miles Davis' classic 60s quintet is all over this thing, laying down some pretty heavy funk.

Most of the songs exceed 10 minutes. There is plenty of room for the players to stretch out and improvise. 

Estavanico is a pretty neat mixture of soothing tones that occasionally erupts into noisy bursts of an almost free jazz nature.  

The Dude is another one of the stronger songs, with its blend of electric bop and robotic funk.

Don't sleep on this underappreciated gem in the Blue Note catalog!

<iframe src="https://open.spotify.com/embed/album/3I3wHHxGI7jFOzMja07ZcS" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>