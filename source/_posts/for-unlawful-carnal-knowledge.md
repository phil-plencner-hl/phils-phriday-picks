---
title: Van Halen - For Unlawful Carnal Knowledge
date: 2024-07-12 21:31:37
tags:
- van halen
- eddie van halen
- sammy hagar
- hard rock
- 90s rock
---

![](ForUnlawfulCarnalKnowlege.jpg)

Van Halen was one of the biggest hard rock bands of their time. Their first six records (all with singer David Lee Roth) are bona fide classics. Each album contained multiple hits and sold millions of copies each, culminating with *1984* which sold an incredible ten million copies in the United States alone. When Roth left the band soon after that, many wondered if Van Halen would be able to continue. 

Obviously they were able to continue. They recruited singer Sammy Hagar and continued to put out extremely successful records...*5150* still sold a very respectable six million copies and *OU812* sold four million more. 

One of the reasons David Lee Roth left the group was Eddie Van Halen's reliance on using keyboards and synthesizers in the songs. This trend continued after he left, especially on ballads like "Dreams" and "When Its Love".

Long time fans balked at these new directions of the group and there was much criticism and pressure for Van Halen to return to their roots. So they went into the studio with that mindset: No ballads, no synths (acoustic pianos were used instead) and no B.S. They also brought back producer Ted Templeman who the band had not worked with since *1984*. The result was *For Unlawful Carnal Knowledge* which is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

As much as I love the Roth-era classics, *For Unlawful Carnal Knowledge* is actually my all-time favorite Van Halen Record. It's basically relentless from beginning to end...all hard rock power! The closest thing they came to a ballad was probably the ode to 1-900 number commercials "Spanked". 

The album probably has some of the most complex parts and solos Eddie Van Halen ever performed. Even the kind of corny idea of playing his guitar with a power drill on the intro to "Poundcake" doesn't sound ridiculous and actually adds to the ambitiousness of the song. (Even if he did steal the concept from the band Mr. Big and their song "Daddy, Brother, Lover, Little Boy").

Here is the music video for "Poundcake" complete with the power drill intro:

<iframe width="560" height="315" src="https://www.youtube.com/embed/1uF5xMhfp1c?si=Q9E99rRQqdAHg0DO" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Soon after the record came out, Van Halen played "Poundcake" on the MTV Music Awards. You'd be hard pressed to find a better hard rock performance from that era. They absolutely knocked it out of the park:

<iframe width="560" height="315" src="https://www.youtube.com/embed/N3WCTsMi6hM?si=1ZzFHQe7UprkMEDt" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

On the way to selling over six million copies of *For Unlawful Carnal Knowledge* they released two more singles / music videos. "Top of the World" was the second one:

<iframe width="560" height="315" src="https://www.youtube.com/embed/JG0H4Zp9oqw?si=5-YsXm6d4mCD93-U" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

The third single was the iconic "Right Now" which still sounds fresh and relevant today:

<iframe width="560" height="315" src="https://www.youtube.com/embed/gU7d2EHV_OQ?si=MlZx9ST2umdJVolu" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

The reason why I am picking and revisiting *For Unlawful Carnal Knowledge* this week is because a new deluxe edition of the record was released today...and I could not be more excited about it! Along with the completely remastered original album (which absolutely leaps out of the speakers even at low volumes) it also includes a Blu-Ray of a complete concert performance in Dallas, Texas on December 4, 1991. 

It wasn't just any concert. On their last tour (supporting *OU812*) Van Halen played at the Cotton Bowl outside Dallas and Sammy Hager was suffering from a vocal ailment. He could not sing well at all, so they cut the performance short after only a few songs. Sammy Hagar promised they would come back to Dallas soon and play a free concert...and that is exactly what they did in front of a crowd of 80,000 people on December 4, 1991.

The concert and Blu-Ray capture a band in their prime! Here is "Poundcake" from that show:

<iframe width="560" height="315" src="https://www.youtube.com/embed/AMGUAZM-qb8?si=41g4VOyp-8lPj3k5" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

"Top of the World" was another highlight from that concert:

<iframe width="560" height="315" src="https://www.youtube.com/embed/MRGTDETEytE?si=N3Cf5LIhJDo63GTI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

I cannot possibly calculate how many times I have played *For Unlawful Carnal Knowledge* for the past 33 years since its original release. However, I can tell you that number is going to exponentially grow because of today's re-release. If you haven't heard this album for a long time (or if you haven't hear it at all!) I urge you to crank this thing up to maximum volume and let it rip! Your neighbors need to hear it too!

You can [listen to Van Halen - *For Unlawful Carnal Knowledge* on your streaming platform of choice](https://songwhip.com/van-halen/for-unlawful-carnal-knowledge) (including Spotify, Amazon Music, Apple Music, Tidal, YouTube Music and more!).
