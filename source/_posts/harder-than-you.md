---
title: 24-7 Spyz - Harder Than You
date: 2025-01-17 21:50:42
tags:
- 24-7 spyz
- p fluid
- living colour
- bad brains
- janes addiction
- black rock coalition
- punk rock
- hard rock
- heavy metal
- funk rock
---

![](HarderThanYou.jpg)

This week there was a horrific incident in the world of hard rock. Original vocalist of the band 24-7 Spyz, [P. Fluid (Peter Forrest) was found dead in an apparent homicide](https://www.rollingstone.com/music/music-features/p-fluid-247-spyz-singer-dead-obit-1235237150/). He was working for an ambulette company in New York City, and when co-workers realized he wasn't making his pickups went to look for him and discovered his body in the vehicle. An awful tragedy.

This made me go back and listen to the first couple 24-7 Spyz records, when P. Fluid was still in the band: *Harder Than You* from 1989 and *Gumbo Millennium* from 1990. These albums were in heavy rotation for me back in high school and brought back a lot of nostalgia for those days. 

Nostalgia aside, these records are incredibly diverse and the band was firing on all cylinders at the time. They could tackle a wide variety of styles from punk to funk to reggae and thrash metal. *Harder Than You* is a front-to-back banger and is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

There were not many African American bands at that time playing hard rock and metal so they were pushing boundaries. Living Colour and Bad Brains are obvious comparisons, but 24-7 Spyz had their own unique sound that is impossible to replicate. Plus, even though they were part of the same scene as Living Colour and were early members of The Black Rock Coalition, there was [apparently bad blood between the two groups](https://www.latimes.com/archives/la-xpm-1989-06-27-ca-4493-story.html).

The first big break for 24-7 Spyz was their cover of Kool & The Gang's "Jungle Boogie". They made a pretty fun video for it that got some airplay on MTV's *120 Minutes*:

<iframe width="560" height="315" src="https://www.youtube.com/embed/v5TC6vPO2P4?si=x32sau87XJxCcJjl" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

That wasn't the only cover on *Harder Than You*. They also recorded "Sponji Reggae" (originally by reggae group Black Uhuru). Incredible stuff:

<iframe width="560" height="315" src="https://www.youtube.com/embed/QL3ybmfWdCk?si=g4afPlSpzE-iytzB" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Their record label at the time, In-Effect was mostly focused on hardcore and metal bands such as Agnostic Front, Prong and Madball. They knew 24-7 Spyz was something special though so they promoted *Harder Than You* with a couple more videos...sadly they didn't get much distribution. I remember seeing "Jungle Boogie" back then, but I certainly didn't see the other videos which is a shame because they rule!

The album opener, "Grandma Dynamite", was the 2nd video which shows the band doing their thing in a concert setting:

<iframe width="560" height="315" src="https://www.youtube.com/embed/r25E-OSrqUU?si=ZOystnviBvZV1-s_" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

The final video made for the album was the protest song "Ballots Not Bullets" which is an interesting fusion of hard rock and reggae. Plus, there is some incredible vocal harmonies near the end of the tune. It should have been a hit!

<iframe width="560" height="315" src="https://www.youtube.com/embed/pJvF8rBHEMk?si=F2uQeK8wr-8BIC-S" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

24-7 Spyz also frequently played with high speed an intensity. One of my favorite songs on *Harder Than You* is "Spill My Guts" which gallops along at a pretty furious pace. Drummer Anthony Johnson plays some blazing double-bass and is really walloping the drums here:

<iframe width="560" height="315" src="https://www.youtube.com/embed/oGgL8XRfwWw?si=KHRuHPNYfs7Ngw7K" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Another cool song is "Spyz Dope" which was one of the songs written solely by singer P. Fluid and gives the heavy funk rock of Red Hot Chili Peppers a run for its money:

<iframe width="560" height="315" src="https://www.youtube.com/embed/oOZ440qkh8I?si=Biltt6Tegtewi0jn" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

"Social Plague" is also great. It is a hard rock funk shuffle that sounds like something Infectious Grooves would do a couple years later. No surprise as Infectious Groove's singer Mike Muir went on tour with 24-7 Spyz around this time with his other band Suicidal Tendencies. He was obviously very influenced by what they were doing here:

<iframe width="560" height="315" src="https://www.youtube.com/embed/VVKSgpBU_t0?si=ShP1tllC8ltuf9iV" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Speaking of touring: 24-7 Spyz appeared to be a very awesome live act. Here is some incredible footage of them playing at the *Poppark Festival* in Rotterdam, Netherlands in 1989 (also on the bill that day was fIREHOSE, Drivin' N' Cryin', Stereo MCs and Urban Dance Squad):

<iframe width="560" height="315" src="https://www.youtube.com/embed/9U6GSOytcWo?si=emsp94k1-JO5ztdV" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Speaking of touring, in 1990 they opened for Janes Addiction when they were promoting *Ritual de lo Habitual*. They were getting a lot of momentum but that unfortunately came crashing down at the end of the tour. Vocalist P. Fluid quit and drummer Anthony Johnson soon followed. 

This is one of those "What If?" moments in my mind. What if 24-7 Spyz was invited by Janes Addiction to be part of the original Lollapalooza festival?

Instead, the rest of the band floundered around for a couple years, eventually getting a new vocalist, Jeff Brodnax, and drummer Mackie Jayson (who previously played with Cro-Mags and Bad Brains). They streamlined their sound focusing mostly on hard rock and metal and signing to East/West Records. They put out a couple records that had some mild success but they never really gained the wide exposure that Living Colour or Red Hot Chili Peppers did. I lost interest in the group around that time because they became more serious and less freewheeling. They broke up in 1998. 

In the mid-2000s guitarist Jimi Hazel attempted many times to reform the group with a revolving door of other musicians (occasionally including P. Fluid and Anthony Johnson). They never recaptured the lighting in the bottle they did in 1989-1990. 

The current lineup is just a trio: Jimi Hazel and original bassist Rick Skatore with new drummer Tobias Ralph (who previously was in a band with Adrian Belew!). Here they are performing a few years ago. They definitely sound tight and heavy, but it doesn't pack the punch and excitement of the old days:

<iframe width="560" height="315" src="https://www.youtube.com/embed/w8nKRHuJypw?si=LdlZW_SJz0AQiSDX" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Which brings me back to *Harder Than You*. Before this week, I haven't heard the album in at least a decade...but I feel like I should have gotten back to it sooner! It is a heavy and innovative (yet fun) album that command attention for the entire 40 minutes. It still sounds pretty fresh 36 years after its original release!

{% iframe 'https://open.spotify.com/embed/album/716nSqxL0T9rERiCW7BFRh?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B09BBXY4QM/?id=4iRXIcH3T6&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/be/album/harder-than-you/1578595523' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/192456568' '100%' '96' %}
