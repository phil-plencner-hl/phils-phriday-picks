---
title: Extreme - III Sides To Every Story
date: 2022-03-18 13:42:44
tags:
- extreme
- 80s rock
- hair metal
- hard rock
- nuno bettencourt
- mike mangini
- dweezil zappa
---

Hard rock / hair metal band Extreme was always more cerebral than other bands in the same genre. While their first album was a Van Halen-inspired affair (guitarist Nuno Bettencourt could have definitely given EVH a run for his money) they soon started widening their horizons.

Their 2nd album, *Extreme II: Pornograffitti* had kind of a storyline, but to the average listener it just sounded like a collection of hair metal and funk metal songs. They ended up having a couple humongous hits with the ballads "More Than Words" and "Hole Hearted". So naturally, when it came time to record their follow-up....they decided to record a full blown progressive rock concept album!

One of the inspirations for this might have been Nuno working with Dweezil Zappa on his solo album *Confessions*. He contributed vocals and guitar to many of the songs on the album, including an absolutely bonkers cover of "Stayin' Alive". It contains no less than 6 guitar solos by Dweezil Zappa, Zakk Wylde, Steve Lukather, Warren DeMartini, Nuno Bettencourt and Tim Pierce. It needs to be heard to be believed:

<iframe width="560" height="315" src="https://www.youtube.com/embed/F4CMGbs1-rI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Anyways, back to the next Extreme album, which was called *III Sides To Every Story*. It was broken into three sections: "Yours", "Mine" and "The Truth" (hence the name of the album). 

The "Yours" section is mostly hard-rockers, many songs with a political message. "Rest In Peace" was one of the album's singles, but the rest of the songs in this section are even better and basically piledrive the listener the whole time.  "Warheads" is heavy and clever and "Peacemaker Die" even includes a snippet of Martin Luther King Jr's "I Have A Dream" speech. 

The "Mine" section contains the introspective ballads including two of the other singles from the album: "Stop The World" and "Tragic Comic". Even when making an over-the-top progressive rock album, they knew what paid the bills.

Finally, "The Truth" section is when they completely go all-in on the concept. A 22-minute suite called "Everything Under The Sun" which is broken up into three movements, features a 70 piece orchestra, shifts through many time signatures with the whole band (especially Nuno) playing their butts off (in true prog rock fashion)! Its another one of those instances where I cannot believe a major label backed and paid for this, but I'm glad they did. A truly stunning tour de force.

Somehow, this album wasn't as successful as *Pornograffitti*. Go figure.

Drummer Paul Geary left the band soon after. For their tour in 1994, they hired former Annihilator drummer Mike Mangini (who later went on to be a member of Dream Theater). I saw them on tour with Bon Jovi around this time. They played like their hair was on fire for this tour, as evidenced by this awesome clip of "Warheads":

<iframe width="560" height="315" src="https://www.youtube.com/embed/dGhy6HlgRBU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

As a funny aside, Mike Mangini was also at one time known for being the "World's Fastest Drummer". He held the record of playing 1203 single strokes in 60 seconds for a while. Here is some hilarious footage of that, where he's basically playing lightly on a little drum pad in a feat of endurance and pretty much no musical value: 

<iframe width="560" height="315" src="https://www.youtube.com/embed/e7TzWrKso4I" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

I saw him attempt to break this record at a NAMM Convention one year and it was about as non-exciting as this video in-person.

Anyways, because the album didn't sell as well and grunge was taking over the world, Extreme scaled back their ambitions for their next album *Waiting For The Punchline* and went on hiatus soon after. 

Singer Gary Charone became the singer of Van Halen and Nuno Bettencourt put out solo albums, played in Perry Farrell's Satellite Party (who I caught at Lollapalooza in 2007 which was surprisingly good) and was the touring guitarist for Rihanna. 

Sadly, none of this reached the heights of *III Sides To Every Story* and I believe this remains the band's high-water mark.

<iframe src="https://open.spotify.com/embed/album/2TV8JqednqRKb2injBMYGd?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

<iframe id='AmazonMusicEmbedB004EF1NJO' src='https://music.amazon.com/embed/B004EF1NJO/?id=TpzTaCQi6H&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/ca/album/iii-sides-to-every-story/1442833247"></iframe>

<div class="tidal-embed" style="position:relative;padding-bottom:100%;height:0;overflow:hidden;max-width:100%"><iframe src="https://embed.tidal.com/albums/35614918" allowfullscreen="allowfullscreen" frameborder="0" style="position:absolute;top:0;left:0;width:100%;height:1px;min-height:100%;margin:0 auto"></iframe></div>


