---
title: Nik Turner - Xitintoday
date: 2024-03-08 20:56:57
tags:
- nik turner
- hawkwind
- steve hillage
- gong
- space rock
- progressive rock
- psychedelic rock

---

![](Xitintoday.jpg)

Nik Turner was an originator of what became known as space rock. Space rock is a particularly trippy sub-genre of progressive rock that is much more psychedelic and trance-like. Hawkwind is probably the most famous of the early space rock groups and Nik Turner was a founding member, playing saxophone and flute. 

Lemmy, a bass player who went on to form the long-running hard rock group Motorhead was also an early member of Hawkwind. 

Unfortunately there is not too much video footage existing of the band in their 1970s peak. Here is one of the few examples you can find of them "Playing" on the BBC show *Top of the Pops*. The video is from their performance at Dunstable Civic Hall on July 7, 1972 but the audio is just the studio version of the song "Silver Machine". The video footage is pretty killer though, so I'll take what I can get:

<iframe width="560" height="315" src="https://www.youtube.com/embed/qiduSVQabAk?si=kminLoZcCWvGL8E2" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

If you want to hear Hawkwind at their best, I recommend checking out their studio albums *In Search of Space*, *Doremi Fasol Latido* and *Hall of the Mountain Grill*. Most essential though is the record *Space Ritual* which is one of the best live albums ever made. Nik Turner's playing is all over these things and is pretty incredible.

Recently the record label Cherry Red Records released a video of the song "Born To Go" from *Space Ritual* with an incredible animation of the band performing the song. While it’s not real live footage of the band it is still pretty wild...and again, I'll take what I can get of the classic era of Hawkwind:

<iframe width="560" height="315" src="https://www.youtube.com/embed/QAlw83pm0NY?si=vb_R-kMOmamyB89B" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

By the late 70s Hawkwind dissolved. Soon afterwards Nik Turner traveled to Egypt to explore the Great Pyramid of Giza. He brought his flute with him and was allowed to play it inside The Kings Chamber.

He took those recordings back to Wales. With the help of some members of the band Gong (Steve Hillage on guitar, Miquette Giraudy on keyboards and Mike Howlett on bass) they fleshed out the flute recordings into full blown songs. They also brought in a ton of percussionists, playing everything from congas, timpani, bells, vibraphone and drum set. Additionally, Nik Turner added recordings of him reciting passages from the Egyptian *Book of the Dead*. These recordings, produced by Steve Hillage, became the record *Xitintoday* which is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

*Xitintoday* is pronounced "Exit Into Day".

This is a pretty fascinating recording because of the mixture of the recordings from the Pyramid and the more modern recording studio. There really wasn't anything else quite like this released in the late 1970s. It's very atmospheric and subdued yet it has a dark mood. I still get a sense of peaceful enlightenment out of it. It is awesome to crank up and maximum volume and just soak it all in.

There is a [great interview with Nik Turner from 2014](https://hit-channel.com/interviewnik-turner-hawkwindspace-ritualsphynxinner-city-unit-71548/) where he recounts the recording of *Xitintoday*:

"I was just sitting in on the side of the Sarcophagus, playing my flute, visualizing the Egyptian pantheon of Gods. I did it spontaneously and I didn’t know what I was going to play...I was musically portraying Egyptian Gods and the Pyramids in time and space. It’s about a dude that goes into a Great Pyramid on Venus, and then came out in Egypt. The Egyptian Gods are the crew of the Pyramid/Inter-Dimensional Portal and he’s meeting Anubis, Thoth, Horus, Osiris, Isis and Nephthys. It was an awesome experience to be inside the Great Pyramid, playing my flute and trying to portray the Egyptian Gods musically for myself."

Nik Turner called this new group Sphynx and took it on the road in late 1978, mostly playing festivals. It sounds like it was pretty bonkers because he supposedly had a giant replica of the pyramid on stage and the band played inside of it!

Another notable piece of music history related to *Xitintoday* was this lead to the formation of the band The Police (believe it or not). Nik Turner, Steve Hillage and Mike Howlett also recorded a short, more commercial (if you could call it that) song during the same sessions called "Nuclear Waste". They found an unknown singer named Sting to sing on the tune:

<iframe width="560" height="315" src="https://www.youtube.com/embed/_9Z00hKM8nY?si=Jv_71_jJTNAobuLA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Sting and Howlett continued after this as a band called Strontium 90 (keeping with the nuclear energy theme). Guitarist Andy Summers joined the group. Originally Chris Cutler, drummer of the band Henry Cow, was supposed to be in the band but they couldn't make the schedule work. So, Sting brought in a young drummer he knew named Steward Copeland. 

Anyways, Nik Turner continued to make wild space rock with his crazed free jazz inspired saxophone and flute playing in various configurations of musicians and evolving band names such as Inner City Unit, Nik Turner's Fantastic All Stars, and Space Ritual well into his 80s. He sadly passed away last year. 

While there are plenty of highlights throughout his lengthy discography worth exploring (especially the absolutely heavy and freaky *Space Ritual Live 1994*) I often come back to where his solo career all started: *Xitintoday*.

{% iframe 'https://open.spotify.com/embed/album/4F7Y5ZbUqcLtkOm9to2AKc?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B07BT1P2FN/?id=wamHGcohhi&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/xitintoday/1365929383' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/86707760' '100%' '96' %}

