---
title: Circle - Paris Concert
date: 2023-02-03 21:32:21
tags:
- chick corea
- anthony braxton
- dave holland
- barry altschul
- circle
- miles davis
- free jazz
- jazz fusion
- wayne shorter
- jack dejohnette
---

![](ParisConcert.jpg)

Pianist Chick Corea is most well-known for two distinct periods of time: His stint in Miles Davis' band (from 1968-1970 including albums *In A Silent Way* and *Bitches Brew*) and his jazz-rock fusion bands (such as Return to Forever and his Elektric Band).  However, the time in-between these two eras he was exploring very "out" free jazz.

He was already tending towards that more avant-garde direction while with Miles. He played electric piano run through a bunch of effects. Here is a great example of that band (which also included bassist Dave Holland, Saxophonist Wayne Shorter and drummer Jack DeJohnette) at the height of their powers in 1969 playing "Directions":

<iframe width="560" height="315" src="https://www.youtube.com/embed/YHx1-BPgkkM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Dave Holland left the band along with Chick Corea and they hooked up with drummer Barry Altschul and recorded [the album *A.R.C.*](https://www.discogs.com/release/1242174-Chick-Corea-David-Holland-Barry-Altschul-ARC) (which stood for "affinity, reality, communication"...a reference to Scientology as Chick Corea became influenced by that belief system around this time) on the ECM label. ECM later became known for more sedate, new-age jazz but early on they released a lot more free jazz influenced records. This was one of them. It included a great version of Wayne Shorter's "Nefertiti" among other highlights.

Around this same time, Saxophone player Anthony Braxton was just starting to get his career off the ground. He associated himself with the AACM (Association for the Advancement of Creative Musicians) and released his completely bonkers solo record *For Alto*, which stretched the limits of Alto Saxophone techniques at the time.

He hooked up with the players from *A.R.C.* to form the band Circle in 1970. In a very short period of time they recorded a couple self-titled albums and played extensively in Europe in early 1971. 

Here is some incredible footage of them performing in Paris on April 19, 1971:

<iframe width="560" height="315" src="https://www.youtube.com/embed/XPPvJyw8siY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

You can see and hear that each player is really stretching out using extended techniques. Barry Altschul has an arsenal of percussion that he switches between that is showcased pretty heavily here. 

A couple months earlier they also played Paris on Febrary 21st, 1971. That show was released as *Paris Concert* on ECM in 1972. That album is today's pick!

They continue to include "Nefertiti" and also play the standard "There Is No Greater Love" (though not in a very standard way!) along with some wild originals written by everyone in the group. It is a fascinating time-capsule for all the players, but especially Chick Corea as he rarely explored anything like this ever again.

Circle was short-lived. By 1972 Chick Corea had formed Return To Forever. Dave Holland and Barry Altschul continued to play with Anthony Braxton for many years, though they tended to explore Braxton's compositions instead of the more free playing of Circle.

While the music of Circle is certainly very challenging I find it to be fascinating and actually a lot of fun to listen to. Deciphering their mindset and getting lost in their unique world of sound has held my interest for many years...there is seemingly always something new with every listen. I hope *Paris Concert* provides the same entertainment for you as well.

<iframe src="https://open.spotify.com/embed/album/7ETtMBL93DdOuZym37xFl6?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

<iframe id='AmazonMusicEmbedB000VHPZL6' src='https://music.amazon.com/embed/B000VHPZL6/?id=1CIPVXO0Vu&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *; clipboard-write" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/paris-concert-live/1443233637"></iframe>

<iframe src="https://embed.tidal.com/albums/77646182" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>