---
title: John Scofield - A Go Go
author: Phil Plencner
date: 2019-05-03 16:02:19
tags:
- john scofield
- medeski martin and wood
- jazz
- jazz rock
- jazz funk
---

Today’s selection is inspired by a cool jazz group I watched at Les Zygomates in Boston earlier this week!
 
 John Scofield’s first collaboration with organ trio Medeski, Martín & Wood.
 
 <iframe src="https://open.spotify.com/embed/album/08JMKtDS0StcwDUgdxKIw3" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>