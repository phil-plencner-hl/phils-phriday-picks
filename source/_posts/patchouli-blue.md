---
title: Boren & Der Club Of Gore - Patchouli Blue
author: Phil Plencner
date: 2020-05-04 15:19:09
tags:
- boren & der club of gore
- jazz
- doom rock
- ipecac records
---
German band Bohren & Der Club of Gore started out as a doom metal band, and eventually morphed into an extremely slow, doomy minimalist jazz band. 

They first came across my radar in 2002 when Mike Patton's Ipecac label released "Black Earth" which was in very heavy rotation in Casa De Plencner that year (Sadly, not on Spotify!). 

They put out a new album earlier this year called "Patchouli Blue" that is worthy of more heavy rotation here...and is a fine compliment to our current world situation. 

Dim the lights, and put this on.

<iframe src="https://open.spotify.com/embed/album/57Jc5ixqmNuGudtV51M5aG" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>