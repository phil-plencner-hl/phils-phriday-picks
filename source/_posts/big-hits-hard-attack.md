---
title: MX-80 - Big Hits / Hard Attack
date: 2022-01-14 08:31:59
tags:
- mx-80
- atavistic records
- ralph records
- the residents
- avant-garde
- punk rock
- progressive rock
---

The underground rock scene lost a giant this week when guitarist Bruce Anderson passed away. He was a founding member of the seminal avant-garde rock group MX-80. 

They were originally known as MX-80 Sound, but shortened their name pretty early on.

I first discovered MX-80s truly unique sound when I lived in the Lakeview neighborhood of Chicago in the early 2000s.

I lived just a few short blocks from [Evil Clown Compact Discs](https://www.chicagorecordstoreproject.com/Evil-Clown). This was a fantastically curated music store that didn't stock much from the major labels and focused on smaller, independent labels.

As a side note, while I lived in the neighborhood I had a monthly ritual (usually on a Saturday) where I walked from my apartment to the downtown Tower Records (a 5-6 mile journey!) hitting every record store along the way (probably close to a dozen stores). Evil Clown was always my first stop. I miss those days. Anyways, I digress.

One label I was deep-diving into at the time was [Atavistic Records](https://en.wikipedia.org/wiki/Atavistic_Records). They were mainly focused on free jazz, but they occasionally dabbled in weird rock bands. Evil Clown stocked a ton of their releases. One day I discovered *Big Hits / Hard Attack* by MX-80 sound in the Evil Clown rack. I was immediately intrigued by the wild cover. I bought it without knowing who MX-80 was, simply trusting Atavistic Records and Evil Clown in their choices.

I'm glad I did! It was a completely over-the-top rock explosion with free-jazz and no wave inspired saxophone, two drummers pounding their kits into oblivion, a singer yelping and the absolutely wild guitar playing and soloing of Bruce Anderson.

MX-80 started in Bloomington, Indiana and originally released these two albums on an independent label. Afterwards they moved to San Francisco, hooked up with The Residents and signed to their Ralph Records. They continued to release a couple more albums and appear on Ralph Records compilations. In fact, soon after I bought *Big Hits / Hard Attack* I also bought the [Subterranean Modern](https://www.discogs.com/release/242385-Various-Subterranean-Modern) compilation because of The Residents songs and was surprised to find there were MX-80 tracks on there too!

Here is an incredible music video of the band from that era:

<iframe width="560" height="315" src="https://www.youtube.com/embed/tIHBehzE5l8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Famous record engineer Steve Albini has written [probably the definitive history of MX-80](https://www.bynwr.com/articles/long-distance-information/), so I'll defer to him for his historical and personal insight of the band through the years.

Instead, I'll just recommend playing *Big Hits / Hard Attack* at maximum volume!

<iframe src="https://open.spotify.com/embed/album/5qIAGr56lIjdK14jOosSFb?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>
