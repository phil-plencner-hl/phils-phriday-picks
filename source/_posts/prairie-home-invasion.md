---
title: Jello Biafra & Mojo Nixon - Prairie Home Invasion
date: 2024-02-09 20:53:24
tags:
- jello biafra
- mojo nixon
- punk rock
- country music
- 90s rock
---

![](PrairieHomeInvasion.jpg)

Mojo Nixon [passed away this week at the young age of 66](https://consequence.net/2024/02/mojo-nixon-dead-cruise/), after performing on a cruise ship. This is a huge loss in the music community. 

You can read the details about his professional career in the many obituaries floating around the internet, so I won't rehash it all here. 

Obviously his biggest and longest enduring hit is "Elvis Is Everywhere". The original video is great, but this crazed performance of it on The Arsenio Hall Show in 1987 is even better:

<iframe width="560" height="315" src="https://www.youtube.com/embed/QAo1Ingszws?si=wCQ5M2U3LTAOefG3" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

The first Mojo Nixon album I bought was 1989's *Root Hog Or Die*. "Debbie Gibson Is Pregnant With My Two Headed Love Child" is the most notorious song on the record (in fact, MTV refused to air the video). However, I think the video for "(619) 239-KING" (yet another homage to Elvis) is even better:

<iframe width="560" height="315" src="https://www.youtube.com/embed/DtGb0lQovZU?si=frnbF9Nla3f8vWwE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Early on, Mojo performed and recorded exclusively as a duo with Skid Roper. Eventually he expanded to a full-band (dubbed The Toadlickers). Another notorious tune that he performed with this group was "Don Henley Must Die":

<iframe width="560" height="315" src="https://www.youtube.com/embed/TndeMd31ZTM?si=-nXXDTGFvHN0BPe7" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Aside from the comedic music, Mojo Nixon was also active politically. Here he is arguing about parental advisory stickers on *Crossfire* in 1990:

<iframe width="560" height="315" src="https://www.youtube.com/embed/quVZmJFoBaE?si=HG6dvWOI_O23PgRP" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Activism like this led him to collaborating with Dead Kennedy's Jello Biafra on an album in 1994 called *Prairie Home Invasion* which is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

*Prairie Home Invasion* is a pretty unique collaboration and doesn't sound like anything else in either artist's catalog. Much of the record is cover songs of old folk music performed with updated lyrics. Examples include "Convoy in the Sky" (originally by The Willis Brothers), "Love Me, I'm A Liberal" (originally by Phil Ochs) and a version of the traditional song "Old Joe Clark" retitled "Let's Go Burn Ole Nashville Down". 

Jello Biafra handles most of the vocals, but Mojo makes his presence known throughout with occasional singing and his guitar. The Toadlickers play like their hair is on fire on every tune. 

Perhaps the most bonkers song on the record is a cover (if you could call it that) of Bruce Springsteen's "Nebraska" called "Hamlet Chicken Plant Disaster" that recounts the [industrial fire at a chicken processing plant in North Carolina in 1990](https://en.wikipedia.org/wiki/Hamlet_chicken_processing_plant_fire).

Many of the lyrics are still topical today. The best example of this is their reimagining of "Will The Circle Be Unbroken" called "Will The Fetus Be Aborted". In fact, Jello Biafra created a new music video for it just last year:

<iframe width="560" height="315" src="https://www.youtube.com/embed/7T7EUSQGxXY?si=lxP4AXu2kDq-OUXX" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

*Prairie Home Invasion* is a fantastic protest record that I hope will get larger exposure. It is certainly a highlight in both Mojo Nixon's and Jello Biafra's lengthy discographies.

As a final tribute to Mojo, I'd like to highlight that he also DJed a radio show on SirisXM radio called *Loon In The Afternoon*. You can [listen to episodes of the show online](https://www.siriusxm.com/player/show/mojo-nixon-the-loon-in-the-afternoon/d6158c72-5cc4-4f3e-83c0-4286ac47320b?_branch_match_id=1284678817473574652), and I highly recommend it (along with listening to *Prairie Home Invasion*)!

{% iframe 'https://open.spotify.com/embed/album/2fdICuiz7D1akByF23WZxC?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B001ARRVKO/?id=xURv8BYXaQ&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/prairie-home-invasion/281666933' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/5569461' '100%' '96' %}
