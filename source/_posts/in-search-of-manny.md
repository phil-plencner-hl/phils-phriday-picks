---
title: Luscious Jackson - In Search Of Manny
author: Phil Plencner
date: 2020-04-14 14:51:07
tags:
- luscious jackson
- beastie boys
- grand royale records
- 90s rock
- 90s alternative rock
- hip-hop
---
Today's pick is the debut EP from Luscious Jackson. This was the first release on the Beastie Boy's Grand Royale label. 

LJ drummer Kate Schellenbach was an early member of the Beastie Boys during their punk-rock days (you can here her bash away on the *Pollywog Stew* EP!) 

Definitely a little more hip-hop influenced than some of their later albums. Still holds up pretty well today 28 years later!

<iframe src="https://open.spotify.com/embed/album/7HZK7QjHFQ5LXODDnrTYro" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>