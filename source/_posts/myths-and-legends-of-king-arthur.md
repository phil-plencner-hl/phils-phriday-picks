---
title: Rick Wakeman - Myths And legends Of King Arthur And The Knights Of The Round Table
author: Phil Plencner
date: 2020-06-29 21:57:15
tags:
- rick wakeman
- yes
- progressive rock
---
Today's pick is the completely over-the-top Rick Wakeman solo album *"The Myths and Legends of King Arthur and The Knights Of The Roundtable"*! 

Rick Wakeman had recently left the band Yes, because he thought they became too boring. So naturally he started creating completely bonkers thematic albums, of which this is his 3rd (following *The Six Wives of Henry VIII* and *Journey to the Centre of The Earth*). 

Obviously, the album recounts the tales of King Arthur in all it's excessive 70's progressive rock splendor! If that weren't enough, Rick wanted to do a multiple-night run with full orchestra of the album at Wembley Arena...however it was already booked with an Ice Skating show during the same time frame...so he decided to make it a show on ice as well!!!! 

Check out this [live footage](https://www.youtube.com/watch?v=AXlZ0x0Ghrk) of knights riding horses (on ice!!) along with mountains of synthesizers, triple-necked guitars and every other possible rock excess you can dream of!

<iframe src="https://open.spotify.com/embed/album/0ALQd0pjofVj8QQecUnP6l" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>