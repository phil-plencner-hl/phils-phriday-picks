---
title: John Coltrane and Eric Dolphy - Evenings At The Village Gate
date: 2023-07-21 12:21:36
tags:
- john coltrane
- eric dolphy
- elvin jones
- reggie workman
- mccoy tyner
- art davis
- chico hamilton
- charles mingus
- 60s jazz
- free jazz
- avant-garde jazz
---

![](EveningsAtTheVillageGate.jpg)

Earlier this summer, it was announced that a recording of John Coltrane and Eric Dolphy playing together was unearthed while a Bob Dylan archivist was digging through materials at the New York Public Library! What an incredible discovery...and one that highlights the need for institutions like public libraries to keep and archive everything for future enjoyment and research.

[The Guardian](https://www.theguardian.com/music/2023/jun/01/john-coltrane-recordings-lost-in-new-york-public-library-will-finally-be-heard) and [NPR](https://www.npr.org/2023/05/31/1179098682/john-coltrane-eric-dolphy-village-gate-1961-lost-album) recently wrote two excellent articles describing in detail the recordings that are worth diving into.

The recordings were released earlier this month as *Evenings At The Village Gate*, which is today's pick!

As someone who has been fanatically listening to both Coltrane and Dolphy recordings since high school, my excitement to hear this recording was off the charts, and it did not disappoint. Along with Coltrane and Dolphy, the band includes Art Davis (double bass), Elvin Jones (drums), McCoy Tyner (piano) and Reggie Workman (double bass).

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

I specifically remember hearing the *The Complete Africa / Brass Sessions* 2 CD set when it came out in 1995 and it was a game-changer for me. All of the players above played on the sessions...*Evening At The Village Gate* was recorded the same summer as *Africa / Brass* in 1961. Two of the songs, "Africa" and "Greensleeves" are played as well. The playing is wild and free, especially Elvin Jones' drumming. 

Eric Dolphy's mastery of several instruments is also showcased. He plays alto sax, bass clarinet and flute here. The flute solo that opens "My Favorite Things" is practically worth the price of admission alone. Eric Dolphy was only recorded in a very short period (1958 until his death in 1964) so any new recordings of him are obviously very welcome. Although he did pack a lot into those 6 short years (Multiple albums each with Chico Hamilton, Charles Mingus, John Coltrane and Ornette Coleman plus his untouchable solo albums like *Outward Bound*, *Out There* and especially *Out To Lunch*), it is still great to hear more.

There is also some incredible video from the same period floating around the internet with most of the same band. Art Davis isn't present, and the video description claims this is Jimmy Garrison on bass, but it looks and sounds like Reggie Workman to me:

<iframe width="560" height="315" src="https://www.youtube.com/embed/-jOH5UJUb8s" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

*Evenings At The Village Gate* is 5 songs, stretching into an almost 80 minute run-time. I will likely be spending a lot of time digging into and enjoying this incredible album for years to come! 

{% iframe 'https://open.spotify.com/embed/album/7AleWm3WG8Ko8AJjfaa3oW?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B0C54DW252/?id=TCdDSYcHtg&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/evenings-at-the-village-gate-john-coltrane-with/1687727133' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/303656220' '100%' '96' %}
