---
title: Iggy Pop - The Idiot
date: 2024-01-26 21:33:26
tags:
- iggy pop
- the stooges
- david bowie
- classic rock
- post-punk
- 70s rock
---

![](TheIdiot.jpg)

David Bowie and Iggy Pop first worked together professionally in 1972. David Bowie helped produce *Raw Power* the last studio album by The Stooges before they broke up. Bowie was not present for the recording sessions, but worked with Iggy Pop during post-production. Afterwards they went their separate ways, but stayed in touch.

In 1976, when David Bowie went on tour to promote *Station To Station* he invited Iggy Pop to join the entourage (Pop did not perform, presumably Bowie and Pop just did drugs together). The music performed likely had a profound influence on Iggy. Imagine watching something like this every night:

<iframe width="560" height="315" src="https://www.youtube.com/embed/XAj2iX9xqCo?si=7Vw-1bT-wvFhrlTL" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

How could you not take away something from that!

After the tour, both Iggy Pop and David Bowie were in bad shape. They collectively decided to move to France and detox. While there, they started working on Iggy Pop's first solo record, *The Idiot*, which is today's pick!

*The Idiot* is an anomaly in the Iggy Pop discography. Nothing else in his canon sounds quite like this. Gone are the punk and garage rock influences. Instead it is mostly subdued, electronic and avant-garde. 

The first song that was part of the sessions was "Sister Midnight" which David Bowie performed occasionally on the 1976 tour. Here's some rehearsal footage of the song on that tour:

<iframe width="560" height="315" src="https://www.youtube.com/embed/5cTEmaj1XVo?si=UOORXTG2wvWF4bLT" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Using this as a jumping off point they recorded similar material, with Iggy Pop writing lyrics in the studio inspired by the music being created. 

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

One of the most famous songs on the record is *China Girl* which was eventually re-recorded by Bowie in 1983 for his album *Let's Dance* which was a big hit. I prefer the Iggy Pop version though.

<iframe width="560" height="315" src="https://www.youtube.com/embed/_YC3sTbAPcU?si=NFBORwv0ilRci8BJ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Another song that is a highlight is "Nightclubbing". This song was later recorded by Grace Jones in 1981. She even named the album it appeared on after it. *Nightclubbing* was perhaps Grace Jones' most successful album. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/grJiFTEGWAg?si=rGB8Ppt-Y3hQ5buP" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

The original version on *The Idiot* was originally supposed to have live drums on it, but Iggy Pop loved the drum machine on it so much that they decided to keep it that way.

The entire album paved the way for David Bowie's next album, *Low*, which he recorded soon after the sessions for *The Idiot*. (Iggy Pop even contributed backing vocals on ""What In The World").

David Bowie never toured behind *Low*. Instead he joined Iggy Pop's band as a sideman playing keyboards! 

As part of that tour / promotion of *The Idiot* they played on the Dinah Shore TV show. Here they are in that incredible episode, playing "Funtime" and "Sister Midnight" along with an interview:

<iframe width="560" height="315" src="https://www.youtube.com/embed/i2eB8f020Pc?si=lqr02KsjYDE43EBR" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

David Bowie tried to keep a low profile on the tour and not take away the spotlight from Iggy Pop. However, that didn't really work. Critics and the general public basically focused on Bowie's involvement and influence more than Iggy Pop's performance. Because of this Iggy Pop dropped this musical direction and went back to a more hard rock sound for his next album, *Lust For Life*, and never looked back.

*The Idiot* remains a unique item in the Iggy Pop catalog and also remains one of my personal favorites.

{% iframe 'https://open.spotify.com/embed/album/78UazygH85UAB0qXqQpzg6?utm_source=generator' '100%' '352' %}

{% iframe 'https://music.amazon.com/embed/B071ZHZJLZ/?id=Dr57FW9Vys&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' '100%' '550' %}

{% iframe 'https://embed.music.apple.com/us/album/the-idiot/1440888621' '100%' '450' %}

{% iframe 'https://embed.tidal.com/albums/77642731' '100%' '96' %}
