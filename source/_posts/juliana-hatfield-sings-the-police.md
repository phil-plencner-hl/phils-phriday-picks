---
title: Juliana Hatfield - Sings The Police
author: Phil Plencner
date: 2019-11-15 14:01:08
tags:
- juliana hatfield
- the police
- 90s rock
- 80s rock
- alternative rock
- punk rock
---

Happy Friday! 

Juliana Hatfield's excellent new album of covers by The Police is out today!

<iframe src="https://open.spotify.com/embed/album/4n62euhfadC5Vycn1KumDk" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>