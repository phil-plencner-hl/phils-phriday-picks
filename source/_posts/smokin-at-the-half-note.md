---
title: Wes Montgomery - Smokin&#39; At The Half Note
date: 2021-03-30 08:03:49
tags:
- wes montgomery
- wynton kelly
- jimmy cobb
- paul chambers
- the half note
- jazz guitar
- 60s jazz
- miles davis
- rudy van gelder
---

*Smokin&#39; At The Half Note* is one of the greatest jazz guitar albums ever recorded. This is not up for debate in my mind.

Wes Montgomery in the mid 1960s was already gaining popularity for his unique and proficient guitar style. For example: He didn't play with a pick, instead playing with his thumb. 

He was basically sweeping the jazz guitar awards in *Downbeat* magazine for many years in a row. He was touring across the United States playing large jazz festivals. 

One of his secret weapons was he used the Wynton Kelly Trio as his backing band. The Wynton Kelly Trio consisted of the rhythm section from Miles Davis' *Kind Of Blue* album (Paul Chambers and Jimmy Cobb). The fact that *Smokin&#39; At The Half Note* doesn't get the same universal acclaim as *Kind Of Blue* has always been a mystery to me.

While it's presented as a live album, only a couple of the original 5 songs that appeared on the album were actually recorded at The Half Note Jazz Club in NYC. The rest were recorded by Rudy Van Gelder at his studio in NJ. It doesn't matter where these songs were recorded and how they were presented to the public...they rule anyways.

I originally heard this album when I was 13 years old. My high school band director made me a copy of his album on cassette and insisted I listen to it for inspiration in the jazz ensemble. 

It was probably my favorite homework assignment I ever received.

In 2005 the album was re-released with 6 additional songs from the sessions! This is the version that is chosen as my pick for today.

[Buy *Smokin&#39; At The Half Note* at Amazon](https://amzn.to/2PJLksd)

<iframe src="https://open.spotify.com/embed/album/2frSEIQat4tSS4nCcrW0Hy" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>


