---
title: Disco Tex And His Sex-O-Lettes - s/t
author: Phil Plencner
date: 2020-06-30 21:53:53
tags:
- disco tex
- ze records
- no wave
- disco music
---

Today's pick is one of the originators of Disco: Disco-Tex and his Sex-O-Lettes! 

Formed by Monte Rock III (who originally got famous doing slapstick on the Merv Griffen show and The Tonight Show). This was probably the first band to score a "hit" playing disco music (*"I Wanna Dance Wit'choo"* in 1974) but for some reason they are practically forgotten about today. 

Monte Rock's last gasp was probably his cameo in *Saturday Night Fever*. My personal favorite is the first song on the album "Jam Band" which sounds like some sort of demented variation on The Muppets Theme or something where all these casts of characters appear and sing a part. 

Hilarious and fun. Let's dance!

<iframe src="https://open.spotify.com/embed/album/1oQIFnmanMCJvKjfTZLa70" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>