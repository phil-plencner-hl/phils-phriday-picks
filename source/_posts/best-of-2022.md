---
title: Best Of 2022
date: 2023-01-12 14:12:05
tags:
- best of list
---

Welcome to 2023! Regular Phil's Phriday Picks posts of a single album a week will return next week after a long holiday hiatus. 

With 2022 finally coming to an end, it is inevitable that everyone has their opinions on what are the best albums of the year. Phil's Phriday Picks is no exception. 

As in years past, I don't rank the albums. Instead, I make a listenable playlist of my favorites. In 2022, it consists of 80 songs with a duration of over 6 hours from what I consider the best albums of the year. This is intended as a digestible snapshot of what new albums I enjoyed the most this year. Additionally, it provides listeners an opportunity to dive further into whatever albums catch their ear the most. 

Likely surprising nobody, it covers a huge swath of styles and genres. Everything from country to free jazz to progressive rock to death metal there is literally something for everybody.

The playlists are available in both Spotify and Amazon Music for your listening pleasure.

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/playlist/0v6Jz6GO7Iyy5MEeyPPBQY?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

<iframe id='AmazonMusicEmbed49cf92916fd349089a3fc6558ca9062bsune' src='https://music.amazon.com/embed/49cf92916fd349089a3fc6558ca9062bsune/?id=fsRfXHGMZ3&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

At the end of this post, I will write little blurbs about each album.

Aside from the songs in these playlists I want to highlight a few other albums that I listened to frequently this year that are not availble in either streaming service. They all feature drummer / composer Weasel Walter and are all worth checking out by people who are into heavy prog rock and underground metal.

**The Flying Luttenbachers - Terror Iridescence**

A fiendish twist on the Flying Luttenbachers usual style. 2 long tracks of slow-motion brutality.

<iframe style="border: 0; width: 100%; height: 42px;" src="https://bandcamp.com/EmbeddedPlayer/album=1182723290/size=small/bgcol=333333/linkcol=0f91ff/transparent=true/" seamless><a href="https://theflyingluttenbachers.bandcamp.com/album/terror-iridescence">Terror Iridescence by The Flying Luttenbachers</a></iframe>

**Encenathrakh - Ithate Thngth Oceate**

High speed / intensity exteme metal. Inhuman, alien sounds of overwhelming complexity.

<iframe style="border: 0; width: 100%; height: 42px;" src="https://bandcamp.com/EmbeddedPlayer/album=1331728500/size=small/bgcol=333333/linkcol=0f91ff/transparent=true/" seamless><a href="https://encenathrakh.bandcamp.com/album/ithate-thngth-oceate">Ithate Thngth Oceate by Encenathrakh</a></iframe>

**Vomitatrix - s/t**

Self-described "Acid Grind" band. A loud, twisted, and evil mind-bender of an album.

<iframe style="border: 0; width: 100%; height: 42px;" src="https://bandcamp.com/EmbeddedPlayer/album=3799876823/size=small/bgcol=333333/linkcol=0f91ff/transparent=true/" seamless><a href="https://vomitatrix.bandcamp.com/album/vomitatrix">Vomitatrix by Vomitatrix</a></iframe>

As promised, here is the summary of my top 80 albums....

**Cheer-Accident - Here Comes the Sunset**

Another solid entry into the large, unwieldy Cheer-Accident catalog. The centerpiece is their demented cover of Cheap Trick's "Dream Police" but the rest is more of their intriguing pop-prog madness.

**High Castle Teleorkestra - The Egg That Never Opened**

Featuring members of Estradasphere, Farmers Market, Secret Chiefs 3 and Umlaut. Inspired by the writings of Philip K. Dick (specifically *Radio Free Albemuth*). This is a wild ride of Eastern Eurpoean Folk / Death Metal / Beach Boys / Space Rock thrown into a blender at high speed.

**The Comet Is Coming - Hyper-Dimensional Expansion Beam**

The new release by this space-age jazz / funk / punk group is heavier and tighter than ever. Concise and catchy music from the outer reaches of the galaxy.

**Todd Rundgren - Space Force**

The new Todd Rundgren album follows the template of his last album *White Knight*. Solid, yet weird, pop songs with a wide variety of guests. This time around they include Sparks, Rivers Cuomo, Neil Finn, The Roots, Steve Vai, Adrian Belew and many more. 

**Trevor Dunn - Seances**

Trevor Dunn is most famous as the bass player of Mr. Bungle and Fantomas, but he also leads his own jazz outfit Trevor Dunn's Trio Convulsant. This is their latest effort and probably their best and most complex.

**Louis Cole - Quality Over Opinion**

An astonishing solo album from jack-of-all-trades Louis Cole who plays most of the instruments and sings here. Jazz fusion workouts meet new-millenium pop with a taste for a bizarre. 

**Makaya McCraven - In These Times**

Jazz drummer Makaya McCraven returns with another solid, burning record of original music. 

**Bjork - Fossora**

Bjork's new album keeps the weirdness quotient up at a high level. Originally it was supposed to be an all-bass-clarinet focused album it eventually expanded in scope but still predominantly features the bass clarienet sextet Murmuri. Lyrics inspired by the 2018 death of Bjork's mother.

**Domi & JD Beck - Not Tight**

Absolutely unreal jazz rock keyboard / drum duo's debut studio album. After years of being blown away by their videos on YouTube there is finally an album of their drum n' bass inspired jazz fusion.

**Imperial Triumphant - Spirit of Ecstasy**

The latest mind-bender from death metal crazies Imperial Triumphant exceeds all expectations. Completely suffocating complexity.

**Chat Pile - God's Country**

Midwest sludge-rock group Chat Pile delivers their intense, heavy intense take on 90s noise rock. Like Jesus Lizard and Godflesh smashed together with Helmet.

**Otoboke Beaver - Super Champion**

Otoboke Beaver is a unique all-female garage-punk band from Japan. Their album is a whirlwind of punk styles that induces whiplash. 

**L.S. Dunes - Past Lives**

Postpunk super-group L.S. Dunes (featuring members of Coheed and Cambria, My Chemical Romance, Circa Survie and Thursday) is a nice slice of aggressive rock anthems.

**Horse Lords - Comradely Objects**

Baltimore's experimental Horse Lords put out another stunner. A prog / krautrock / math / punk masterpiece that takes dozens of listens to wrap your brain around.

**Dead Cross - II**

Punk / Grind supergroup returns with another unrelenting masterclass in how to make a politically charged protest record.

**Delvon Lamarr Organ Trio - Cold as Weiss**

I can't say enough good things about this soul-jazz organ trio! They are bringing back mostly forgotten art form in the vein of Jimmy Smith.

**Cigar - The Visitor**

Cigar brings high-speed pop-punk into the 21st century. Forget about the Blink-182 reunion...the Cigar reunion is where it is at.

**Too Much Joy - All These Fucking Feelings**

The second new studio album from Too Much Joy since their original 1996 break-up. Even better than 2021's *Mistakes Were Made*.

**Brad Mehldau - Jacob's Ladder**

Piano virtuoso Brad Mehldau releases a love letter to all things prog rock, but specifically the band Rush. A beginning to end stunner of jazz that's not jazz.

**Fidlar - Taste the Money**

Heavy garage-rock trio Fidlar releases another heavy, vulgar splatter of music. 

**Bill Frisell - Four**

A solid release from the 70-year-old guitarist and elder statesman. An hour of new original compositions written during quarantine along with a couple old favorites. 

**King Garbage - Heavy Metal Greasy Love**

A duo who plays a warped version of soul music for the 21st century. Tight arrangements and great vocals.

**Exhumed - To the Dead**

Since the 90s Exhumed have been vomiting out brutal death metal of the highest order. *To The Dead* continues the trend of all-killer, no-filler from these scene elders.

**Envy Of None - s/t**

The new group founded by former Rush guitarist Alex Lifeson. Completely defying expections, it is a more subdued rock record featuring the sultry vocals of Maiah Wynne.

**Italian Surf Academy - Fake Worlds**

An amazing album of instrumental covers of Italian sountrack songs, classic noir jazz with a surf-rock bent.

**Skunk Baxter - Speed of Heat**

The first solo release ever by studio legend Skunk Baxter, who has played with everyone from Steely Dan and The Doobie Brothers to Joni Mitchell and Donna Summer. An amazing rock album that makes you wonder what took him so long to put something like this out.

**Bitchin Bajas - Bajascillators**

Interesting Chicago-based trio that keeps psychedelic rock and free jazz alive with every new release.

**Live Forever: A Tribute to Billy Joe Shaver**

Excellent tribute album for country legend Billy Joe Shaver. Includes Willie Nelson, Amanda Shires, Lucinda Williams, Edie Brickell, George Strait, Steve Earle, Margo Price and many more.

**Simple Minds - Direction of the Heart**

The new album from Scottish art-rock band Simple Minds lives up to the hype. Takes their classic sound and mixes it with a modern sheen.

**Leland Whitty - Anyhow**

Badbadnotgood sax player Leland Whitty goes out on his own and releases a great psychedelic jazz record.

**Taipai Houston - Once Bit Never Board**

Hear me out.... this duo featuring the sons of Metallica drummer Lars Ulrich is actually really cool!! 

**Archers Of Loaf - Reason In Decline**

First new album from indie rock band Archers Of Loaf in almost 25 years. Lived up to expectations. A fine release of jagged, shambling rock.  

**Skin Graft Presents...Sounds to Make You Shudder**

This is an excellent compilation of bands associated with the Skin Graft Records roster. Features The Flying Luttenbachers, Lovely Little Girls, Yowie, Jim O'Rourke, Tijuana Hercules etc. A who's who of underground avant-garde rock.

**Jack Irons - Dreamer's Ball**

Instrumental solo release from drummer Jack Irons who spent time in Eleven, Pearl Jam and the Red Hot Chili Peppers. However, this record sounds like none of those bands. Long, spacey instrumentals with a focus on percussion. 

**Weyes Blood - And In Darkness, Hearts Aglow**

Musician Natalie Mering releases another stunning chamber-pop record of immense depth and scope. An album that seems like a downer at first but contains a lot of optimism as well. 

**Microwaves - Discomfiture Atlas**

Pittsburgh's noise-rock heavyweights Microwaves delivers another mind-bending album that is like a roller coaster flying off the tracks and into a mountainside.

**Colour Flash - Zappa Speilt Fur Bach**

Really cool live album featuring blind piano player Mats Oberg and singer / saxophonist Napolean Murphy Brock. Playing mostly Zappa covers in a european cathedral. This is an epic tribute to one of my favorite guitarists and composers.

**Hellbound Glory - The Immortal Hellbound Glory: Nobody Knows You**

Leroy Virgil's honky-tonk, root-rock band Hellbound Glory releases another front-to-back banger. They are pushing outlaw country into the future.

**Autopsy - Morbidity Triumphant**

Another excellent release from Autopsy, and their first in 8 years. Doom / death metal never sounded better. The stuff nightmares are made of.

**The Bad Plus - s/t**

Drummer David King keeps The Bad Plus name alive after the two other original members jump ship. He recruits guitarist Ben Monder and saxophonist / clarinetist Chris Speed and puts out a surprisingly excellent album. Completely different in sound and feel from previous Bad Plus records but I think an interesting continuation of the brand.

**Clutch - Sunrise on Slaughter Beach**

Another solid release from crushing rock band Clutch. They've pretty much stayed in their comfort zone, but that's not entirely a bad thing.

**Joshua Hedley - Neon Blue**

Country traditionalist Joshua Hedley finally releases his debut. It's been a long time coming. If you are a fan of 50's and 60's classic country and/or it's 90's resurgence this will be right up your alley.

**Bennie Maupin - Symphonic Tone Poem for Brother Yusef**

A minimalist, ethereal tribute to Yusef Lateef who would have turned 100 years old this year. Requires full attention and maximum volume to feel its full effect.

**Danger Mouse / Black Thought - Cheat Codes**

Danger Mouse's first hip-hop album in over 15 years is a collaboration with The Root's Black Thought. A trippy rap album with lots of proggy organs and strings and great vocals.

**Wormrot - Hiss**

Wormrot are an absolutely unstoppable grindcore band from Singapore! *Hiss* is their first album in over 5 years and it is well worth the wait.

**Adrian Younge - Jazz Is Dead 13**

Adrian Younge continues his Jazz Is Dead series. He released five more volumes in 2022 alone, but I think Vol 13 is the best of the bunch. A collaboration with Tribe Called Quest's Ali Shaheed Muhammad and the band Katalyst is uniformly excellent. 

**Revocation - Netherheaven**

The new album by technical metal band Revocation ups the ante for all in the genre. 

**Jeff Cotton - The Fantasy of Reality**

Jeff Cotton is best known as Antennae Jimmy Semens in Captain Beefheart's Magic Band. He appeared on *Strictly Personal*, *Mirror Man* and *Trout Mask Replica*. To say his solo release is highly anticipated is an understatement. A whimsical, psychedelic blues rock album that I'm still just scratching the surface on after many repeated listenings. 

**Planet Drum - In the Groove**

Grateful Dead drummer Mickey Hart's pet project Planet Drum releases their first new album in 5 years. It's a surprisingly concise, danceable drum album. No wasted space or needless jamming on this rhythmic tour-de-force.

**Surfrajettes - Roller Fink**

Canadian surf-rock throwback band The Surfrajettes puts out a solid full-length album. It consists mostly of originals but has a fun cover of Blondie's "Heart of Glass" as well.

**Jeff Beck / Johnny Depp - 18**

Jeff Beck passed away while writing this post, so this is unfortunately likely his final studio album...nevertheless it is quite a cool record!  The timing of the release was not ideal as Johnny Depp was in the news over his courtroom drama. It includes a couple interesting Depp penned songs, but the real meat is the interesting and diverse covers (The Beach Boys, Smokey Robinson, Killing Joke, Marvin Gaye The Velvet Underground etc.). Between this and Jeff Beck's appearance on the latest Ozzy Osbourne album, Jeff Beck really went out with a loud bang. 

**Kelly Clarkson - Kellyoke**

Kelly Clarkson's TV show has a Kellyoke segment where she sings cover songs, and this record is a small, tight sample of some of the more interesting ones. A very cool interpretation of Radiohead's "Fake Plastic Trees" is a highlight along with Billie Elish's "Happier Than Ever".

**Origin - Chaosmos**

Origin is quite possibly my favorite technical death metal band of all time, so a new record from them is always welcome...even if they really do not escape the confines of their established sound. With drummer John Longstreth back in the fold they continue to be absolutely relentless.

**Caballero Reynaldo - In the Lounge Of The Naldo King**

Spanish artist Caballero Reynaldo has been on my radar for a while because he has released a series of interesting Frank Zappa covers albums. This time around he tackles King Crimson. These are very interesting takes on the prog rock classics.

**Artifical Brain - s/t**

Artificial Brain have been quietly releasing high quality brutal death metal over the past few years. The new self-titled release is quite possibly their masterwork.

**Bruce Hornsby - 'Flicted**

Bruce Hornsby releases perhaps his final album from the sessions that were originally part of 2019's *Absolute Zero*. An intriguing mix of 20th century classical with his pop sensibilities. While it doesn't reach the heights of *Absolute Zero* it does come pretty close.

**Ches Smith - Interpret It Well**

Drummer Ches Smith has been a frequent collaborator with many in the avant-rock underground including John Zorn, Secret Chiefs 3, David Torn and Tim Burne. He also releases albums under his own name and this one includes an amazing band that includes pianist Craig Taborn, guitarist Bill Frisell and Mat Maneri on viola. A wide-ranging jazz record that should become a classic.

**49 Winchester - Fortune Favors the Bold**

49 Winchester delivers high-energy alt-country on their long-awaited full-length record. Anchored by the single "Russel County Line", which makes me homesick for an area I did not grow up, the whole album is country music of the highest quality. 

**Kirk Hammett - Portals**

Metallica guitarist released a surprisingly great mini album of instrumental tunes that sound like they can be from soundtracks of old spaghetti westerns or horror flicks.

**Primus - Conspiranoid**

Alt-funk weirdos Primus continue their latter day resurgence. Ever since drummer Tim Alexander has been back in the group they have returned to form. A concept album focused on modern day misinformation campaigns, it includes the longest Primus song ever...the proggy 11 minute "Conspiranoia".

**Jack White - Fear of The Dawn**

Guitarist Jack White released two records in 2022, but I think *Fear of The Dawn* is the stronger one. Non-stop high energy songs that still leaves me grinning from ear to ear after dozens of listens.

**The Linda Lindas - Growing Up**

All female power pop group The Linda Lindas originally reached critical mass from their viral video of the song "Racist, Sexist Boy" that was recorded in their local public library. Since then, with the help of many celebrity endorsements they have signed with Epitaph records and put out this infectious album that is hopefully a sign of many years of excellent music to come. 

**Somali Yacht Club - The Space**

Somali Yacht Club are a Ukrainian doom / stoner rock band that blows away many of their American peers. If you like Kyuss or Pelican you will love this stuff. 

**Dream Widow - s/t**

Dave Grohl finally released his death metal solo record under the band name Dream Widow, along with the accompanying horror movie starring the rest of the Foo Fighters, in 2022. Unfortunately, around the same time long time Foo Fighters drummer Taylor Hawkins passed away which kind of soured the whole endeavor. It is a great album that proves that Grohl, along with his Probot album, has a great love for heavy metal music. 

**Black Midi - Cavalcovers**

British experimental rock band put out a very weird (and frankly not that enjoyable to me) new full-length record in 2022 called *Hellfire*. It was a little too cheesy for my liking, but they also put out this mini-EP of cover songs from the same session that I really enjoyed. It's only three songs, but when you cover King Crimson and Captain Beefheart you definitely have my attention.

**Orville Peck - Bronco**

Mysterious masked country singer Orville Peck continues his quality run of music. Bronco was originally released over the year as two separate EPs but eventually was combined into one brilliant album. 

**Tears For Fears - The Tipping Point**

First new Tears for Fears record in 18 years!! A brilliant art-pop record that stands well with the rest of their catalog. It almost didn't get completed due to internal struggles with the duo but I'm so glad they were able to deliver this excellent set of songs.

**Robert Glasper - Black Radio III**

Producer and keyboard player Robert Glasper releases the 3rd in his series of Black Radio albums. As usual it includes a large list of collaborators (Q Tip, Common, H.E.R., Me'Shell Ndegeocello, Esperanza Spalding, Jennifer Hudson, India.Arie and many more) and is a high-quality modern day take on jazz and R&B. 

**Los Bitchos - Let The Festivities Begin!**

Los Bitchos is a modern band that focus on instrumental music in the style of 1970's Columbian cumbia injected with a large dose of humor. If psychedelic surf rock was a thing, Los Bitchos would be the stars of that genre.

**Voivod - Synchro Anarchy**

Canadian thrash metal legends Voivod released another amazing album in 2022. Ever since 2013's *Target Earth* they have been consistently good. *Synchro Anarchy* continues the trend.

**Eddie Vedder - Earthling**

Eddie Vedder asks the question "Was the last Pearl Jam album not weird enough?" and answers that question with *Earthling*. With a core band of Chad Smith and Josh Klinghoffer and Andrew Watt (who also are the core band in the brand-new Iggy Pop record!) they deliver an almost proggy take on 70s rock and 90s alternative rock. 

**700 Bliss - Nothing To Declare**

When poet Moor Mother collaborates with DJ Haram they are known as 700 Bliss (Moor Mother is also a member of the excellent avant jazz outfit Irreversible Entanglements among other interesting projects). Here they make a twisted, noisy rap album that defies logic and expectations.

**Steve Vai - Inviolate**

Steve Vai continues to push guitar playing into uncharted territories on *Inviolate*. For this album he constructed a completely new instrument: a triple necked guitar synthesizer to help conceive these compositions. Breathtaking stuff.

**The Temptations - Tempations 60**

Early in the year The Temptations celebrated 60 years of existence by releasing this great new album. Mixing their old school sound with more modern R&B elements is a fine recipe for success and proves they are running stronger than many of their contemporaries who are more inclined to just tour behind their oldies.

**Stabbing - Extirpated Mortal Process**

Texas-based brutal death metal band Stabbing continue to pummel the listener into dust on their latest full-length album. Unstoppable.

**Konjur Collective - Blood In My Eye: A Soul Insurgent Guide**

Konjur Collective are a Baltimore free-jazz collective that harkens back to the genre's 60s heyday. This record is essential listening for people who are into that sort of thing.

**Blut Aus Nord - Disharmonium: Undreamable Abysses**

French avant-garde black metal band Blut Aus Nord continue to confuse. While more melodic than some of their past albums, this is still very bleak and full of odd rhythms and other surprises.

**Terri Lynn Carrington - New Standards Vol. 1**

Grammy award winning drummer Terri Lynn Carrington (who has played with high profile jazz legends Herbie Hancock and Wayne Shorter among many others) released an excellent record with her own group. The songs are all composed by women and the album is going to be part of a series showcasing 101 total songs. Since there are 11 excellent songs here, I presume there will be 10 more volumes in this series. I cannot wait to hear more.

**Oort Smorg - Every Motherfucker Is Your Brother**

Oort Smorg are an amazing saxophone / drums duo. This is their second record, which consists of an ultra-complex 28-minute song that mixes tightly arranged changes and free jazz improvisation.

**Willie Nelson - A Beautiful Time**

Willie Nelson continues to be a national treasure. This is his 72nd (!!!) album and it still contains the high quality playing and songwriting we all expect from this living legend.