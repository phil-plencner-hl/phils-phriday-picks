---
title: Charming Hostess - Eat
date: 2024-04-12 15:14:11
tags:
- charming hostess
- sleepytime gorilla museum
- idiot flesh
- jewlia eisenburg
- the residents
- progressive rock
- avant-garde rock
- bulgarian rock
- 90s rock
---

![](Eat.jpg)

Crazed avant-rock band Sleepytime Gorilla Museum recently reformed after a twelve-year hiatus. The put out an excellent new record, *Of the Last Human Being*, and went on a lengthy U.S. tour. If you're not familiar with their brand of theatrical, heavy art-rock here they are performing "Sleep Is Wrong" on their recent tour:

<iframe width="560" height="315" src="https://www.youtube.com/embed/7hJPrdwp0aU?si=-fGepgAc9s4--o9o" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Here is a music video they made for "Burn into Light" from their latest record:

<iframe width="560" height="315" src="https://www.youtube.com/embed/D-Un4vSMUfE?si=PT6RvhJ2tihQ_h2I" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Their triumphant return has me going back into their discography (sadly small with only four proper studio albums since 2001). This also includes some of their side-projects and groups the members performed in before forming Sleepytime Gorilla Museum. One of those bands I have a particular affinity for is Charming Hostess.

Charming Hostess was active mostly from 1999 - 2010. They were fronted by singer / musician Jewlia Eisenberg with a wide and ever-changing set of musicians. Their later material was more focused on a cappella material (such as the album *Trilectic* from 2001) or modern-day classical music (such as on *The Bowls Project* in 2010). However, their earlier songs were more rock based with what later became known as the "Charming Hostess Big Band". 

Members of the Charming Hostess Big Band included Carla Kihlstedt (voice, fiddle), Nils Frykdahl (guitar, flute, saxophone, percussion) and Dan Rathbun (bass) who all later became members of Sleepytime Gorilla Museum. The songs performed by this band were eclectic. Even though the instrumentation was mostly using traditional rock instruments, the style they played incorporated music from other cultures like Bulgaria, Turkey, and Hungary. There was also a chorus of singers (Carla Kihlstedt, Nina Rolle and Jewlia Eisenberg).

They put out three records in this format including *Punch* and *Sarajevo Blues* (both from 2004). Their first record, *Eat*, from 1999 is my personal favorite and is today's pick!

| Don't Miss A Pick! |
| :-------------:|
| Do you want to get Phil's Phriday Picks delivered to your inbox every Friday? <iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe> |
| Do you like being social? [Follow Phil's Phriday Picks on Facebook](https://www.facebook.com/Phils-Phriday-Picks-101983909166849/). 

I first heard *Eat* in 2001. It was soon after I saw Sleepytime Gorilla Museum perform at The Empty Bottle in Chicago (an excellent show by the way). I had heard about some earlier groups they were involved in such as Charming Hostess and Idiot Flesh, but their albums were hard to come by.

Surprisingly, one day while digging through the bargain bins at Hi-Fi Records on Lincoln Avenue (sadly, no longer open) I came across a mother lode of material!  I found a pile of CDs released by the Vaccination Records label for $1-$2 apiece. They included Charming Hostess' *Eat*, Idiot Flesh's *Fancy*, Eskimo's *Some Prefer Cake*, and a compilation album called *Rawk Party* (which included the bands Sleepytime Gorilla Museum, Idiot Flesh and Mumble & Peg). Unsurprisingly, I bought them all. Nowadays all those CDs fetch a pretty penny online. I sure do miss digging through record store bargain bins!

Anyways, *Eat* is an incredible album. Most of it is delivered with very high energy and is a mixture of progressive rock with those previously mentioned styles from other cultures. 

For example, the album starts with the Bulgarian song "Dali Tzerni" followed by an original tune in English called "Laws of Physics" then right into "Mi Nuera" which is a Moroccan wedding song. After that is a cover of "Won't You Keep Us Working" originally by The Residents on their *Mark of The Mole* album. Wild stuff!

Other highlights include the Bulgarian song "Elenke", the gospel song "When Jesus Christ Was Here on Earth" and an original song "Ferret Said".

Charming Hostess rarely played live shows, but speaking of "Ferret Said" here they are performing it (sadly not with the big band, but still great):

<iframe width="560" height="315" src="https://www.youtube.com/embed/V5JV3irTUcE?si=x-66Ig7X0dYNz7Oi" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Here is a local San Francisco TV station showcasing Charming Hostess in 1999 or so with a bunch of tantalizing footage of the Charming Hostess Big Band:

<iframe width="560" height="315" src="https://www.youtube.com/embed/Xql3fDruVkg?si=CTl7Aquw6sX9D0SY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

That is some incredible footage!!

Outside of Charming Hostess, Jewlia Eisenberg also had a band worth checking out called Red Pocket. The band comprised of bass, drums and cello and played a pretty heavy form of rock music...almost metal at times. 

Unfortunately, Jewlia Eisenberg passed away in 2021 due to complications from a rare immune disorder known as GATA2 deficiency. Her legacy will live on with the few recordings we have of Charming Hostess and Red Pocket. This includes a recent [re-release on vinyl of selected songs from both *Eat* and *Punch* which is worth acquiring](https://www.charminghostess.com/store/p/feast-an-eat-punch-fusion-on-vinyl).

You can [listen to Charming Hostess - *Eat* on your streaming platform of choice](https://songwhip.com/charminghostess/eat) (including Spotify, Amazon Music, Apple Music, Tidal, YouTube Music and more!).
