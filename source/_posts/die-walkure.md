---
title: Wagner - Die Walkure
author: Phil Plencner
date: 2020-08-03 15:20:20
tags:
- wagner
- ring cycle
- opera music
- metropolitan opera
---

Today's unusual Pick is today's free stream of The Metropolitan Opera's version of Wagner's "Die Walkure"...which is free until 6:30pm. 

The stage scenery is a huge machine that has all 24 sections on an axis that spins onto the stage at the appropriate scene. [Absolutely insane](https://metoperafree.brightcove.services/?videoId=6173674969001)

[Paybill Article about the stream](https://www.playbill.com/article/the-met-opera-revives-robert-lepages-hi-tech-staging-of-wagners-ring-cycle)