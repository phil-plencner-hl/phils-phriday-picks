---
title: David Torn / Mick Karn / Terry Bozzio - Polytown
date: 2021-01-08 09:21:00
tags: 
- frank zappa
- jazz fusion
- progressive rock
- 90s rock
- terry bozzio
- mick karn  
- japan
---

Terry Bozzio has always been one of my favorite drummers. He started his career with Frank Zappa in the 70s, eventually working his way to 80s one-hit-wonder success with The Missing Persons. In the mid-90s he was riding high as a member of Jeff Beck's group (He even played on the Arsenio Hall show with Jeff Beck at that time.)

Around that time he teamed up with Mick Karn (who used to play fretless bass in the new romantic / new wave group Japan) and David Torn (who rose to prominence putting out wild guitar albums on the ECM label). They formed a band called Polytown and put out an eponymous album, which is today's pick.

I came across this CD in the cut-out bin at a local CD store (plainly called The Compact Disc Shoppe) in the mid-90s. I bought it thinking it would be a bombastic progressive rock epic, but was pleasantly surprised by what I ultimately heard.

This is a highly creative album that is hard to classify. Bozzio was just starting to increase the size of his drumset and play a ton of melodic phrases instead of typical rock beats. Karn slinks around the drum parts with his smooth fretless (and plays some bass clarinet!). Torn layers his guitar in effects and solos over the top...sometimes ambient and sometimes forceful. 

This is best listened to with headphones for the full immersive experience.

<iframe src="https://open.spotify.com/embed/album/6iYmBvavYrqCD3Eu7hXAKg" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>