---
title: Ennio Morricone - Crime And Dissonance
author: Phil Plencner
date: 2020-07-06 21:43:37
tags:
- ennio morricone
- ipecac records
- crime and dissonance
- italian music
- soundtrack music
---
Early morning PPP today, as I just read on the internet that prolific Italian film composer Ennio Morricone has passed away. 

He's composed scores for over 300(!!!) films, most famously for Sergio Leone westerns in the 60s and 70s (*The Good, The Bad, and The Ugly*, *A Fistful of Dollars*, *Once Upon A Time In The West* etcetc). Ipecac Records put out an amazing 2CD set overview of a bunch of his "overlooked" compositions that is worth diving into anytime, but especially today. A personal favorite of mine.

<iframe src="https://open.spotify.com/embed/album/7EZoSIhrgcBJYanQWVnTFr" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>