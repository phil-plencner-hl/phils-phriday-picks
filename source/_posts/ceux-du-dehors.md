---
title: Univers Zero - Ceux Du Dehors
date: 2022-05-20 10:17:46
tags:
- univers zero
- ceux du dehors
- rock in opposition
- present
- henry cow
- progressive rock
- 70s rock
---

![](CeuxDuDehors.jpg)

Univers Zero is a long-running instrumental progressive rock band from Belgium. What sets them apart from typical bands in the genre is the influence of 20th century chamber music (Stravinsky, Bartok and Prokofiev all come to mind when listening to their songs).

They are part of a loose collective of like-minded bands called Rock In Opposition that is lead by the equally amazing group Henry Cow.

The band was formed in 1974 by drummer Daniel Denis, so a lot of their music is very rhythmically complex.

Apart from the usual rock instruments, they also typically include Oboe, Bassoon, Violin and Viola. The music is also typically very dark and depressive in nature.

Their first two albums (*1313* and *Heresie*) were very gloomy and dissonant much of it guitarist Roger Trigaux's compositions. 

He eventually left the group in 1980 to start a band called Present. They took the doom and gloom even further. Meanwhile, Univers Zero lightened up (just a little bit).

The first album after Trigaux left is *Ceux Du Dehors*, which is today's pick!

*Ceux Du Dehors* is french for "Outsiders". The album compositions are heavily inspired by HP Lovecraft.

One of the things that makes this perhaps my favorite Univers Zero album is the prominence of Mellotron throughout. 

Two standout tracks for me are the the lengthiest ones. Opener "Dense" which is full of quick tempo and time signature changes plus mournful violin and Mellotron. Later in the album the aptly named "Combat" is again full of mood and tempo changes and seems to accurately portray an actual battle or war. Intense stuff.

After *Ceux Du Dehors* Univers Zero started adding more electronic instruments to their sound and disbanded in 1987. 

In the early 2000s they reformed and still put out albums and occasionally play festivals. Here they are in 2005 playing "Dense":

<iframe width="560" height="315" src="https://www.youtube.com/embed/Vwjce5wcu4Y" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

If you dig that, then you'll likely find a lot to enjoy and uncover in *Ceux Du Dehors*.

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/album/1J0tUfb0y6YHoKF4uGCOzA?utm_source=generator" width="100%" height="380" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

<iframe id='AmazonMusicEmbedB00NO3038E' src='https://music.amazon.com/embed/B00NO3038E/?id=JBDfksTDm1&marketplaceId=ATVPDKIKX0DER&musicTerritory=US' width='100%' height='550px' style='border:1px solid rgba(0, 0, 0, 0.12);max-width:'></iframe>

<iframe allow="autoplay *; encrypted-media *; fullscreen *; clipboard-write" frameborder="0" height="450" style="width:100%;max-width:660px;overflow:hidden;background:transparent;" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation" src="https://embed.music.apple.com/us/album/ceux-du-dehors/919725527"></iframe>

<iframe src="https://embed.tidal.com/albums/34737222" allowfullscreen="allowfullscreen" frameborder="0" style="width:100%;height:96px"></iframe>



