---
title: Best Of 2019
author: Phil Plencner
date: 2019-12-14 13:46:05
tags:
- best of list
---
Happy Friday everyone! 

The end of 2019 is near, so it's time for "best of" lists aplenty. I'm no different in that regard. 

Here is a playlist of my favorite stuff from the year! If you want to distill my entire year down to a little shy of 5 hours, this would be a good way to do it...Running the gamut from pop-rock to jazz to country to hip-hop to death metal, it might be schizophrenic but it's how I roll! 

One of my favorite bands of all time, The Flying Luttenbachers, also reunited this year and put out two vastly different albums....but they chose not to put them on Spotify. 

So I'll include BandCamp links to those albums as well (as if the original playlist wasn't enough!!) .

[The Flying Luttenbachers - Shattered Dimension](https://theflyingluttenbachers.bandcamp.com/album/shattered-dimension)
[The Flying Luttenbachers - Imminent Death](https://theflyingluttenbachers.bandcamp.com/album/imminent-death)

<iframe src="https://open.spotify.com/embed/playlist/3y8jKsF7b8fposVLVltzS0" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>