---
title: The Flying Luttenbachers - Negative Infinity
date: 2021-07-09 17:14:30
tags:
- flying luttenbachers
- weasel walter
- brutal prog
- free jazz
- prog rock
- avant rock
---

One of my all-time favorite bands is The Flying Luttenbachers. Today, they dropped their new album *Negative Infinity*. It is not on Spotify, but you can listen to it over on Bandcamp.

The Flying Luttenbachers are one of the many bands associated with Weasel Walter. While he's a multi-instrumentalist, he has always played the drums in this particular band.

Today, as part of the announcement of the new album, he shocked longtime fans that he does not play drums on the new album. He has switched to guitar! Drumming duties are now performed by Sam Ospovat (from the band Unnatural Ways among others). He definitely rises to the occasion.

The rest of the band is similar to their last couple albums (Tim Dahl on bass, Matt Nelson on saxophones). However guitarist Brandon Seabrook has been replaced by Katie Battistoni.

The album description hypes this as a return to their "Brutal-Prog" sound of previous albums like *Infection and Decline* and *Systems Emerge From Complete Disorder*. This is definitely the case!!

Many long, thorough-composed compositions that have their trademark mix of progressive rock, death metal and free jazz. 

In fact the last song on the album ("On The Verge Of Destruction") is, according to the liner notes, "...a monolithic side-long extrapolation off of Albert Ayler's bizarre '66/'67 'marching band on acid' musical suites." 

YES, PLEASE!

I've only listened to it a couple of times so far, but I'm pretty confident this will be one of my favorite albums of 2021.

<iframe style="border: 0; width: 350px; height: 721px;" src="https://bandcamp.com/EmbeddedPlayer/album=3740669019/size=large/bgcol=ffffff/linkcol=0687f5/package=1628072627/transparent=true/" seamless><a href="https://theflyingluttenbachers.bandcamp.com/album/negative-infinity">Negative Infinity by The Flying Luttenbachers</a></iframe>


