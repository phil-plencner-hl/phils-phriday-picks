---
title: Cupcakes - s/t
author: Phil Plencner
date: 2020-05-07 15:11:36
tags:
- cupcakes
- matt walker
- smashing pumpkins
- filter
- 90s rock
- alternative rock
---
Cupcakes were an excellent alt-rock band from Chicago from the late 90s. 

Their drummer (Matt Walker) previously toured with Filter and then joined the Smashing Pumpkins after they kicked out Jimmy Chamberlin. 

Cupcakes got signed to a major label and he ended up quitting SP (which I wonder if he thinks is a mistake in retrospect as the band ultimately went nowhere). They released this one album in 2000 and its still something I listen to pretty regularly. 

I caught them a couple times in concert and they were even better live (One time they all dressed up in Teletubby costumes for their set! :D)

<iframe src="https://open.spotify.com/embed/album/1sNybNseWD1Xit5Jbj3aSm" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>