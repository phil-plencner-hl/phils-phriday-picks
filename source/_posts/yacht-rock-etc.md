---
title: Yacht Rock Etc
author: Phil Plencner
date: 2020-06-22 22:08:36
tags:
- yacht rock
- 70s rock
- jazz fusion
---
Over the weekend I came across a MentalFloss article about [Yacht Rock](https://www.mentalfloss.com/article/624635/yacht-rock-music-resurgence). 

This is always a fun topic for me, especially since we are now in the summer season. Yacht Rock is a pseudo-genre of music that's basically 70s/80s smooth rock. 

More about it [here](https://en.wikipedia.org/wiki/Yacht_rock), but the TL;DR about this is we're talking stuff like Hall & Oates, Steely Dan, Toto, Christopher Cross, Kenny Loggins...you get the gist. 

This is a total guilty pleasure of mine. Back in 2014 I created what I considered the definitive Yacht Rock playlist on Spotify. 

Rhino Records was running a contest at that time for "best playlist" and I [submitted mine and won!]((https://www.rhino.com/article/guest-rhino-musicologist-phil-p) They sent me a bunch of CDs, records and Rhino Records swag. Way cool! 

So here is the "award winning" playlist in all its glory!

<iframe src="https://open.spotify.com/embed/playlist/0085oinNJlIry4fT89DEvb" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>