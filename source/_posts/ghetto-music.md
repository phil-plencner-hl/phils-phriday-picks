---
title: Eddie Gale - Ghetto Music
author: Phil Plencner
date: 2020-07-14 16:16:09
tags:
- eddie gale
- ghetto music
- free jazz
- cecil taylor
- sun ra
- blue note records
---

This morning I read about the passing of free-jazz trumpeter Eddie Gale, after a lengthy battle with cancer. 

I first heard his playing on the fantastic Cecil Taylor album "Unit Structures" back in high school. That got some heavy rotation from me during that time.  

Later, when my obsession with Sun Ra took flight I heard a lot more of him, as he was part of the Arkestra for decades. 

But today's pick is his relatively obscure first solo album (released on Blue Note!) called "Ghetto Music". This is an absolutely underrated classic, mixing elements of free jazz, blues, gospel and soul music that was way ahead of its time in 1968!

<iframe src="https://open.spotify.com/embed/album/2DMKQV21wMBgodiBh22JdN" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>