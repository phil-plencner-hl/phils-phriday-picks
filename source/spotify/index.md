---
title: Spotify Playlist
date: 2020-10-16 18:43:30
---
Want to quickly catch up on all of Phil's Phriday Picks? Maybe you want to listen to random songs that were picked? Check out the full playlist including one song from all the Picks!
  
<iframe src="https://open.spotify.com/embed/playlist/730J0XZOp2SUfmAktPCE6G" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>