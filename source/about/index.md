---
title: About Phil's Phriday Picks
date: 2020-10-16 18:27:28
---
[Phil Plencner](https://library.harvard.edu/staff/philip-plencner) is a web developer for the [Library Technology Services](https://staff.library.harvard.edu/lts) team at [Harvard Library](https://library.harvard.edu).

He is an avid music listener. For the LTS team, he has set up a private Slack channel where he recommends an album to listen to. Generally on Fridays.

This is the Slack channel in a blog format.

Want to get Phil's Phriday Picks in your inbox? [Subscribe to the newsletter](/philsphridaypicks/newsletter/)!

[Archive Playlist here](https://open.spotify.com/playlist/730J0XZOp2SUfmAktPCE6G?si=NTFYtqFyRwuYn2BQbu6UIw) 
