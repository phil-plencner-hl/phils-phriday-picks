---
title: Sign up for the Phil's Phriday Picks Newsletter
date: 2021-07-18 19:21:20
---

Are you someone who enjoys convenience? Get the Picks sent straight to your inbox! Sign up for the email newsletter and get Phil's Phriday Picks delivered to you via electronic mail as soon as new recommendations are posted.

<iframe src="https://philsphridaypicks.substack.com/embed" width="480" height="320" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe>