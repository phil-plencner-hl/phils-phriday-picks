---
title: Contact Phil!
date: 2020-10-20 08:50:41
---
Have a suggestion or just want to talk music? Fill out the form below.

<form action="https://formspree.io/f/xvovazln" method="POST">
    <div class="form-group">
        <label for="name">Your name</label><br>
        <input type="text" class="form-control" name="name" placeholder="Name" required>
    </div>
    <div class="form-group">
        <label for="_replyto">Your email</label><br>
        <input type="email" class="form-control" name="_replyto" placeholder="example@domain.com" required>
    </div>
    <div class="form-group">
        <label for="message">Your message</label><br>
        <textarea name="message" class="form-control" rows="3" placeholder="Message" required></textarea>
    </div>
  <input class="hidden" type="text" name="_gotcha" style="display:none">
  <input class="hidden" type="hidden" name="_subject" value="Message from Phil's Phriday Picks!">
  <button class="btn btn-primary" type="submit">Send</button>
</form>