#!/bin/bash
# This script posts messages to Phil's Phriday Pick Slack Channels. 
# Make sure you have the Slack Bot User OAuth Token saved as the
# Environment Variable PPP_SLACK_TOKEN
# @See: https://phoenixnap.com/kb/set-environment-variable-mac
# @See: https://stackoverflow.com/a/63179601

echo "Sending Message to HL Discovery & Access #PhilsPhridayPicks Channel"
echo $PPP_SLACK_TOKEN

read -p "Artist: " ARTIST
if [ -z "$ARTIST" ]
    then
        echo "No artist supplied."
        exit
fi

read -p "Album Title: " TITLE
if [ -z "$TITLE" ]
    then
        echo "No album title supplied."
        exit
fi

read -p "Image URL: " IMAGE
if [ -z "$IMAGE" ]
    then
        echo "No Image URL supplied."
        exit
fi

read -p "Description: " DESCRIPTION
if [ -z "$DESCRIPTION" ]
    then
        echo "No Description supplied."
        exit
fi

read -p "Blog Link: " LINK
if [ -z "$LINK" ]
    then
        echo "No Blog Link supplied."
        exit
fi

PS3='Select Channel: '
options=("#philsphridaypicks" "#phil-test")
select opt in "${options[@]}"
do
    case $opt in
        "#philsphridaypicks")
            CHANNEL_ID=CHG1QGMST
            break
            ;;
        "#phil-test")
            CHANNEL_ID=C0520JCTLBC
            break
            ;;
        *) 
            echo "invalid option"
            break
            ;;
    esac
done

echo "Artist entered: ${ARTIST}"
echo "Album Title entered: ${TITLE}"
echo "Image URL entered: ${IMAGE}"
echo "Description entered: ${DESCRIPTION}"
echo "Blog Link entered: ${LINK}"
echo "Channel: ${opt}"

PAYLOAD="{\"channel\":\"$CHANNEL_ID\",\"blocks\":[{\"type\":\"section\",\"text\":{\"type\":\"mrkdwn\",\"text\":\"*Today's PPP: $ARTIST - _ $TITLE _*\"}},{\"type\":\"divider\"},{\"type\":\"section\",\"text\":{\"type\":\"mrkdwn\",\"text\":\"$DESCRIPTION\"},\"accessory\":{\"type\":\"image\",\"image_url\":\"$IMAGE\",\"alt_text\":\"$ARTIST - $TITLE \"}},{\"type\":\"section\",\"text\":{\"type\":\"mrkdwn\",\"text\":\"Read More at Phil's Phriday Picks...\"},\"accessory\":{\"type\":\"button\",\"text\":{\"type\":\"plain_text\",\"text\":\"Read The Pick!\",\"emoji\":true},\"value\":\"pick\",\"url\":\"$LINK\",\"action_id\":\"button-action\"}},{\"type\":\"divider\"},{\"type\":\"actions\",\"elements\":[{\"type\":\"button\",\"text\":{\"type\":\"plain_text\",\"text\":\"Sign Up For The Newsletter!\",\"emoji\":true},\"value\":\"newsletter\",\"url\":\"https://phil.share.library.harvard.edu/philsphridaypicks/newsletter/\"},{\"type\":\"button\",\"text\":{\"type\":\"plain_text\",\"text\":\"Browse The Archives!\",\"emoji\":true},\"value\":\"archives\",\"url\":\"https://phil.share.library.harvard.edu/philsphridaypicks/archives/\"},{\"type\":\"button\",\"text\":{\"type\":\"plain_text\",\"text\":\"Spotify Playlist!\",\"emoji\":true},\"value\":\"archives\",\"url\":\"https://open.spotify.com/playlist/730J0XZOp2SUfmAktPCE6G?si=61bb46c5e08e4b9c\"}]}]}"

echo "BEGIN PAYLOAD - Copy this into Block Kit Builder to distribute: https://app.slack.com/block-kit-builder/"
echo $PAYLOAD
echo "END PAYLOAD"

PS3='Send Payload?'
options=("Yes" "No")
select opt in "${options[@]}"
do
    case $opt in
        "Yes")
            echo "Sending Payload!"
            curl -H "Content-type: application/json" --data "${PAYLOAD}" -H "Authorization: Bearer $PPP_SLACK_TOKEN" -X POST https://slack.com/api/chat.postMessage
            break
            ;;
        *) 
            echo "Quitting!"
            break
            ;;
    esac
done

echo "Done!"