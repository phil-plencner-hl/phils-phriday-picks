# Phils Phriday Picks

## Overview 

Phil recommends an album to listen to.
* [Archive Playlist](https://open.spotify.com/playlist/730J0XZOp2SUfmAktPCE6G?si=NTFYtqFyRwuYn2BQbu6UIw)
* [Website](https://phil.share.library.harvard.edu/philsphridaypicks/index.html)
## Requirements

1. [NodeJS](https://nodejs.org/en/download/)
2. [Hexo](https://hexo.io/)

## Installation
To get started with this project, use the following commands:

```
# Clone the repo to your git
> git clone https://gitlab.com/phil-plencner-hl/phils-phriday-picks.git

# Enter the folder that was created by the clone
> cd phils-phriday-picks

# Install node dependencies
> npm install

# Install Hexo themes
> git submodule init
> git submodule update

# Build the public pages
>  npx hexo generate

# Serve the public pages
>  npx hexo serve

# Create a new post
>  npx hexo new [layout] <title>

# Send to Slack
> ./post-slack.sh

```
